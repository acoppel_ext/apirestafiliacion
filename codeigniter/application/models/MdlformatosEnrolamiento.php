<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlformatosEnrolamiento extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->bustramites = $this->load->database('bustramites', TRUE);
		$this->Informix = $this->load->database('Pgsafre', TRUE);
		$this->PgServiciosAfore = $this->load->database('PgServiciosAfore', TRUE);
	}

	
	function consultarTrabajadorCount($folioEnrolamiento)
	{
		$query = $this->Pgaforeglobal->query("SELECT COUNT(*) AS respuesta FROM fnconsultatrabajadorenrolamiento(".$folioEnrolamiento.");");
		return $query->result();
	}
	function consultarTrabajador($folioEnrolamiento)
	{
		$query = $this->Pgaforeglobal->query("SELECT folio, tiposolicitud, curp, nombre, apellidopa, apellidoma, texto FROM fnconsultatrabajadorenrolamiento(".$folioEnrolamiento.");");
		return $query->result();
	}

	function obtenerExcepcion($folioEnrolamiento)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenerexcepcionbitacoraenrol AS iexcepcion FROM fnobtenerexcepcionbitacoraenrol(". $folioEnrolamiento .", ". 1 .");");
		return $query->result();
	}

	function consultarEnrolamientoMovil($folioEnrolamiento)
	{
		$query = $this->Pgaforeglobal->query("SELECT folio, tiposolicitud, curp, nombre, apellidopa, apellidoma, texto FROM fnconsultatrabajadorenrolamientomovil(".$folioEnrolamiento.");");
		return $query->result();
	}

	function consultarEmpleadoEnroladoCount($enrolado)
	{
		$query = $this->Pgaforeglobal->query("SELECT COUNT(*) AS respuesta FROM fnconsultaempleadoenrolamiento(1,".$enrolado.");");
		return $query->result();
	}

	function consultarEmpleadoEnrolado($enrolado)
	{
		$query = $this->Pgaforeglobal->query("SELECT nombre,apellidopa,apellidoma,curp, dciudad, destado, nss, tipo, clave, texto FROM fnconsultaempleadoenrolamiento(1, ".$enrolado.");");
		return $query->result();
	}

	function fnconsultaempleadoenrolamientotesCount($itestigo)
	{
		$query = $this->Pgaforeglobal->query("SELECT COUNT(*) FROM fnconsultaempleadoenrolamientotes(".$itestigo.");");
		return $query->result();
	}

	function fnconsultaempleadoenrolamientotes($itestigo)
	{
		$query = $this->Pgaforeglobal->query("SELECT nombre,apellidopa,apellidoma,curp, nss FROM fnconsultaempleadoenrolamientotes(".$itestigo.");");
		return $query->result();
	}

	function actualizarImagenIndexada($data)
	{
		extract($data);
		$cFolio = $data['cFolio'];
		$folio = $data['folio'];
		$obtIpTienda = $data['obtIpTienda'];
		$query = $this->Pgaforeglobal->query("SELECT fnactualizarimagenindexada AS Dato FROM fnactualizarimagenindexada('".$cFolio."','".$folio.".tif','".$obtIpTienda."');");
		return $query->result();
	}

	function actualizarpublicarimagenservicios($iFolio,$iOpcion)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnpublicafirmaservicios from fnpublicafirmaservicios ($iFolio,$iOpcion);");
		return $query->result();
	}

	function actualizarpublicarimagenserviciosMovil($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnpublicafirmareenrolservicios ($iFolio);");
		return $query->result();
	}

	function actualizarfirmapublicacion($cFolio,$iOpcion,$iTes)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnpublicafirmaenrolamiento from fnpublicafirmaenrolamiento ('".$cFolio."',$iOpcion, $iTes);");
		return $query->result();
	}

	function actualizarpublicarimagen($iFolio, $iOpcion)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnpublicafirma from fnpublicafirma ($iFolio,$iOpcion);");
		return $query->result();
	}

	function actualizarpublicarimagenMovil($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnpublicafirmareenroltrabajador ($iFolio);");
		return $query->result();
	}

	function ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo)
	{
		$query = $this->Pgaforeglobal->query("SELECT tfolio FROM fnctrlimagenesportransmitir($iOpcion, $iFolio, '', 0, '$cNombreArchivo');");
		return $query->result();
	}
}