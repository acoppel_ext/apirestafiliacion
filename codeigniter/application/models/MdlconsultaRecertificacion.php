<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlconsultaRecertificacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->PgServiciosAfore = $this->load->database("PgServiciosAfore",true);
	}

	function obtnerinformacionpromotor($iEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT icodigo,cmensaje,cnombre,ctienda, espromotor,curppromotor,claveconsar FROM fnConsultarEmpleadoConstancia($iEmpleado);");
		return $query->result();
	}

	function obtenerSolicitudRecertificacion($iFolioservicio,$cCurp,$cNss){

		$query = $this->PgServiciosAfore->query("SELECT folioservicio,fechasolicitud,TRIM(cmensaje) AS mensaje,estatus,TRIM(estatusdesc) AS estatusdesc,TRIM(diagnostico) AS diagnostico FROM fnobtenerdiagnosticorecertificacion($iFolioservicio,'$cCurp','$cNss');");
		return $query->result();
	}

	function obtenerDatosTrabajadorAfop($iFolio){

		$query = $this->Pgaforeglobal->query("SELECT curp,nss,nombretrab,paternotrab,maternotrab,numempprom,nombreprom,paternoprom,maternoprom FROM fnconsultarecertificacion($iFolio);");
		return $query->result();
	}

	function fnobtenerdiagnosticoservicio($iFolio){

		$query = $this->PgServiciosAfore->query("SELECT estatus,diagnostico,estatuscod  FROM fnobtenerdiagnosticoservicio($iFolio);");
		return $query->result();
	}

}
?>