<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlcontratoSolicitud extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal", true);
	}

	function infoSolicitud($folio)
	{
		$query = $this->Pgaforeglobal->query("SELECT nombre, paterno, materno, curp, rfc, 											  ocupacion, actgironegocio, email, celular
											  FROM colgenerasolicitudregtrasp(".$folio.");");
		return $query->result();
	}

	function obtenerHuellas($data)
	{
		extract($data);
		$folio = $data['folio'];
		$curp = $data['curp'];
		$idServicio = $data['idServicio'];
		$tipoOperacion = $data['tipoOperacion'];

		if ($idServicio == 9909)
		{
			$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona 
												  FROM fnobtenerhuellastercerasfiguras('".$folio."','". $curp ."', '".$idServicio."'::SMALLINT, '".$tipoOperacion."')");
		}
		else
		{
			$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona 
												  FROM fnobtenerhuellasdoctosafiliacion('".$folio."','". $curp ."', '".$idServicio."'::SMALLINT, '".$tipoOperacion."')");
		}

		return $query->result();
	}

	function obtenerDatosComplementarios($folio)
	{
		$query = $this->Pgaforeglobal->query("SELECT iexcepcion, itiposolicitante, itipoconstancia 									  FROM fnobtenertiposolicitanteexcepcion($folio);");
		return $query->result();
	}
}