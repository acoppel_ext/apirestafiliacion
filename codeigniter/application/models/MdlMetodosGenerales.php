<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlMetodosGenerales extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        //load database library
        $this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
        $this->bustramites = $this->load->database('bustramites', true);
        $this->Informix = $this->load->database('Pgsafre', true);
    }

    function servicioEjecutarAplicacion($idServicio)
    {
        $query = $this->bustramites->query("select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio)");
        return $query->result();
    }

    function obtenerXML($array)
	{
		$xml = "";
		$xml = "<map>";
		foreach($array as $key => $value)
		{
			$mykey = $key;
			$myvalue= $value;
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";
		}
		$xml .= "</map>";
		return $xml;
    }

    function ejecutarWS($idServicio,$arrDatos){
        log_message("error", "WSID-> $idServicio");
        $Servicio = (object)array('idServicio' => 0 ,'parametros' => '' );
        //Creacion del XML en base al arreglo de datos a enviar
        $servicioXML = $this->obtenerXML($arrDatos);
        log_message("error", "WSXML-> $servicioXML");
        //Obtencion de URL del WS
        $servicioURL = $this->servicioEjecutarAplicacion($idServicio);
        //Id del WS y asignacion de xml a enviar
        $Servicio->idServicio = $idServicio;
        $Servicio->parametros = $servicioXML;
        //Extracion de datos de la liga
        $ipServidor = $servicioURL[0]->ipservidor;
        $puerto     = $servicioURL[0]->puerto;
        $url 		= $servicioURL[0]->urlservicio;
        $protocolo  = $servicioURL[0]->protocolo;
        //Armar liga del WS
        $urlServicio = $protocolo . $ipServidor . ":" . $puerto . $url ;
        log_message("error", "WSURL-> $urlServicio");
        //Cliente para la detonacion del SOAP
        $client = new SoapClient($urlServicio, array('trace' => true, 'exceptions' => true));
        try
        {
            $response   = $client->ejecutarAplicacion($Servicio);
            $datos      = $response->respuesta;
        }
        catch (SoapFault $fault)
        {
            $datos['descripcionRespuesta']='Se presento problemas al consultar servicio cliente';
        }

        return $datos;
    }
}
?>