<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdlexpedienteidentificacion extends CI_Model
{

	public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal", true);
		$this->Pgsafre = $this->load->database("Pgsafre", true);
		$this->bustramites = $this->load->database('bustramites', TRUE);

		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		
		$hostname = $this->Pgaforeglobal->hostname;
		$username = $this->Pgaforeglobal->username;
		$password = $this->Pgaforeglobal->password;
		$database = $this->Pgaforeglobal->database;

		$this->conexionDirecta =  new PDO( "pgsql:host=".$hostname.";port=5432;dbname=".$database.";options='-c client_encoding=SQL_ASCII'", $username, $password);
	}

	function verificarPromotor($iNumeroEmpleado)
	{

		$query = $this->Pgaforeglobal->query("SELECT fnespromotor(" . $iNumeroEmpleado . ") as promotorvalido;");

		return $query->result();
	}

	function obtenerInformacionTrabajadorAfiliacion($cFolio, $iOpcion)
	{
		if ($iOpcion == 0) {
			$query = $this->Pgaforeglobal->query("SELECT curp, appaterno, apmaterno, nombres, nss, tipotelefono, telefonocelular, telefonocontacto, comptelefonicacontacto, codigopostal, idestado, estado, idciudad, ciudad, iddelegmunic, delegmunic, idcolonia, colonia, calle, numexterior, numinterior, fechageneraconstancia, genero,fechanacimiento,entidadnacimiento, esextranjero FROM fnconsultarsolconstanciaei('$cFolio');");
		} else if ($iOpcion == 1) {
			$query = $this->Pgaforeglobal->query("SELECT nivelestudio, ocupacion, actividadgironegocio, tipotelefono02, telefono02, tipocorreoelectronico, correoelectronico, codigopostallaboral, idestadolaboral, estadolaboral, idciudadlaboral, ciudadlaboral, iddelegmuniclaboral, delegmuniclaboral, idcolonialaboral, colonialaboral, callelaboral, numexteriorlaboral, numinteriorlaboral FROM fnvalidarregistroprevioeiafiliacion('$cFolio');");
		} else if ($iOpcion == 2) {
			$query = $this->Pgaforeglobal->query("SELECT curp, nombre, appaterno, apmaterno, fecnacimiento, LPAD(entnacimiento::CHAR(2),2,'0') AS entnacimiento, genero, telefono, tipotelefono, parentescorelacion FROM fnvalidarregistroprevioeiafiliacionreferencias('$cFolio');");
		}

		return $query->result();
	}

	function ConsultacatalogoComprobante()
	{

		$query = $this->Pgaforeglobal->query("select numcomp,desccomp from fn_catcomprobantes();");
		return $query->result();
	}

	//Integracion Quetzaly
	function guardarExpedienteIdentificacion($arrDatos)
	{
		$cSql = "";
		$cFolio = "";
		$cSql = $arrDatos["cSql"];
		$cFolio = $arrDatos["cFolio"];

		$query = $this->conexionDirecta->query(" SELECT 1 AS guardo FROM fnguardarexpedienteidentificacion('$cSql','$cFolio'); ");
		return $query->result();
	}

	function validarExpedientePublicado($cfolio)
	{

		$query = $this->Pgaforeglobal->query("SELECT fnconsultaexpedientepublicado ('$cfolio') as estatus;");

		return $query->result();
	}

	function consultarEIDatosTrabajador($folio)
	{

		$query = $this->Pgaforeglobal->query("SELECT fechasolicitud, folio, nss, UPPER(curp) AS curp, UPPER(rfc) AS rfc, nombre, " .
			"apellidopaterno, apellidomaterno, promotor, (nombrepromotor || ' ' || apellidopaternopromotor || ' ' || apellidomaternopromotor) AS nombrefuncionario, " .
			"fechanacimiento, genero, cvenaturalgenero, descgenero, nacionalidad, cvenaturalnacionalidad, " .
			"descnacionalidad, entidaddenacimiento, cvenaturalentidaddenacimiento, " .
			"descentidaddenacimiento, ocupacion, cvenaturalocupacion, descocupacion, " .
			"actividadgironegocio, cvenaturalactividadgironegocio, descactividadgironegocio, " .
			"nivelestudio, cvenaturalnivelestudio, descnivelestudio, telefono01, tipotelefono01, " .
			"cvenaturaltipotelefono01, desctipotelefono01, telefono02, tipotelefono02, " .
			"cvenaturaltipotelefono02, desctipotelefono02, correoelectronico " .
			"FROM fnconsultareitrabajador('$folio');");

		return $query->result();
	}

	function consultarEIBeneficiarios($folio)
	{

		$query = $this->Pgaforeglobal->query("SELECT folio, UPPER(curp) AS curp, (nombre) AS nombre, " .
			"(apellidopaterno) AS apellidopaterno, " .
			"(apellidomaterno) AS apellidomaterno, parentesco, cvenatural, " .
			"(descparentesco) AS descparentesco, porcentaje " .
			"FROM fnconsultareibeneficiarios('$folio');");

		return $query->result();
	}

	function consultarEIDomicilios($folio)
	{

		$query = $this->Pgaforeglobal->query("SELECT folio, tipodomicilio, cvedomicilio, (desctipodomicilio) AS desctipodomicilio, " .
			"codigopostal, pais, cvenaturalpais, (descpais) AS descpais, entidadfederativa, " .
			"(descentidadfederativa) AS descentidadfederativa, delegacionmunicipio, " .
			"(descdelegacionmunicipio) AS descdelegacionmunicipio, ciudad, " .
			"(descciudad) AS descciudad, colonia, (desccolonia) AS desccolonia, " .
			"(calle) AS calle, numerointerior, numeroexterior FROM fnconsultareidomicilios('$folio');");

		return $query->result();
	}

	function obtenerExcepcionEnrolamiento($iFolio)
	{

		$query = $this->Pgaforeglobal->query("SELECT fnobtenerexcepcionbitacoraenrol AS resultado FROM fnobtenerexcepcionbitacoraenrol(" . $iFolio . ", 2);");

		return $query->result();
	}

	function actualizarEstatusExpediente($cFolio)
	{

		$query = $this->Pgaforeglobal->query("SELECT 1 FROM fnactualizarestatuseitrabajador('$cFolio',2::smallint);");

		return $query->result();
	}

	function obtenerDescripcionServicio($iTipoServicio)
	{

		$query = $this->Pgsafre->query("EXECUTE FUNCTION fnobtenerdescripcionservicioei('$iTipoServicio'); ");
		return $query->result();
	}

	function guadarLlamadaCat($arrDatos = array())
	{

		$cFolio = "";
		$cDescServicio = "";

		$cFolio = $arrDatos["cFolio"];
		$cDescServicio = $arrDatos["cDescServicio"];

		$query = $this->Pgaforeglobal->query("SELECT 1 as llamada FROM fnguardarsolllamadasconfirmacioncatei('$cFolio','$cDescServicio');");
		return $query->result();
	}

	function obtenerVigenciaFotografia($arrDatos = array())
	{

		$cCurp = "";
		$cNss = "";

		$cCurp = $arrDatos["cCurp"];
		$cNss = $arrDatos["cNss"];

		$query = $this->Pgaforeglobal->query(" SELECT fnpedirfotografiaei('$cCurp','$cNss') AS vigenciaexpirada ; ");
		return $query->result();
	}

	function obtenerFechaMaximaComprobanteDomicilio()
	{

		$query = $this->Pgaforeglobal->query(" SELECT TO_CHAR(now()::date - INTERVAL '3 months', 'DD-MM-YYYY') AS fechamaxima ; ");
		return $query->result();
	}
	function obtenerFechaMaximaMayoriaEdad()
	{

		$query = $this->Pgaforeglobal->query(" SELECT TO_CHAR(now()::date - INTERVAL '18 year', 'DD-MM-YYYY') AS fechamaxima ; ");
		return $query->result();
	}
	function obtenerVigenciaIdentificacionOficial($arrDatos = array())
	{

		$cNss = "";
		$cCurp = "";

		$cNss = $arrDatos["cNss"];
		$cCurp = $arrDatos["cCurp"];

		$query = $this->Pgaforeglobal->query(" SELECT fnpediridentificacionei('$cCurp','$cNss') AS vigenciaexpirada ; ");
		return $query->result();
	}
	function obtenerURLFormatoCurp($arrDatos = array())
	{

		$iOpcionFormatoRenapo = "";
		$iFolioAfore = "";
		$sIpRemoto = "";

		$iOpcionFormatoRenapo = $arrDatos["iOpcionFormatoRenapo"];
		$iFolioAfore = $arrDatos["iFolioAfore"];
		$sIpRemoto = $arrDatos["sIpRemoto"];

		$query = $this->Pgaforeglobal->query("SELECT fnobtenerligapaginacurprenapo as url
            	FROM fnobtenerligapaginacurprenapo( $iOpcionFormatoRenapo, $iFolioAfore, '$sIpRemoto');");
		return $query->result();
	}

	function obtenerExpedienteDeIdentificacion($sNss, $sCurp, $cFolio, $opcion)
	{

		if ($opcion == 1) {
			$query = $this->Pgaforeglobal->query("SELECT folio, fechasolicitud, nss, curp, rfc, " .
				" nombre, apellidopaterno, apellidomaterno, " .
				" fechanacimiento, genero, nacionalidad, " .
				" entidaddenacimiento, ocupacion, " .
				" actividadgironegocio, nivelestudio, " .
				" telefono01, tipotelefono01, telefono02 " .
				" tipotelefono02, correoelectronico " .
				" FROM fnconsultarexpedienteidentificaciontrabajador('$sNss','$sCurp')");
		}
		if ($opcion == 2) {
			$query = $this->Pgaforeglobal->query("SELECT tipodomicilio, codigopostal, pais, entidadfederativa, delegmunicipio, ciudad, " .
				" colonia, calle, numerointerior, numeroexterior, nombreciudad,nombreestado, " .
				" nombremunicipio, nombrecolonia" .
				" FROM fnconsultarexpedienteidentificaciondomicilio('$cFolio') ORDER BY tipodomicilio");
		}
		if ($opcion == 3) {
			$query = $this->Pgaforeglobal->query("SSELECT curp, nombre, apellidopaterno, apellidomaterno, fechanacimiento, entidaddenacimiento, " .
				" genero, telefono, tipotelefono, parentescorelacion" .
				" FROM fnconsultarexpedienteidentificacionreferencia('$cFolio')");
		}

		if ($opcion == 4) {
			$query = $this->Pgaforeglobal->query("SELECT curp, nombre, apellidopaterno, apellidomaterno, " . " parentescorelacion" . " FROM fnconsultarexpedienteidentificacionbeneficiario('$cFolio')");
		}

		return $query->result();
	}

	function extraerDatosBancoppel($cFolio)
	{
		$query = $this->Pgaforeglobal->query("select rfc,fechanacimiento,entidadnacimiento,genero,nacionalidad, escolaridad, profesion, actividad, telefonofijo,
		email, apellidopatref1, apellidomatref1, nombresref1 from fnobtenerdatosclientebancoppelexpident($cFolio);");

		return $query->result();
	}

	function extraerDatosRenapo($cFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fechanacimiento,entidadnacimiento,genero,nacionalidad FROM fnconsultadatoscurprenapoei($cFolio);");
		return $query->result();
	}

	function guardarSolicitudRenapo($iFolioAfore, $iFolioSolicitud, $iProceso, $iEmpleado, $idsubproceso, $cCurp, $cApellidopaterno, $cApellidomaterno, $cNombres, $cSexo, $cFechaNacimiento, $iSlcEntidadNacimiento,$cIpModulo)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnguardarrensolicitudescurp as valor FROM fnguardarrensolicitudescurp($iFolioAfore, $iFolioSolicitud, $iProceso, $idsubproceso, '$cCurp', $iEmpleado, '$cApellidopaterno', '$cApellidomaterno', '$cNombres', '$cSexo', '$cFechaNacimiento' , '$iSlcEntidadNacimiento', '$cIpModulo')");
		return $query->result();
	}

	function consultaRespuestaRenapo($iFolioAfore)
	{
		$query = $this->Pgaforeglobal->query("select idu_tipoerror, idu_codigoerrorrenapo, mensaje, ccurp, capellidopaterno, capellidomaterno, cnombre, isubproceso from fnconsultarespuestacurprenapo($iFolioAfore);");
		return $query->result();
	}

	function validarTelefonoListaNegra($cTelefono1, $cTelefono2, $cFolio, $iEmpleado)
	{
		$arrTelefonos[0] = $cTelefono1;
		$arrTelefonos[1] = $cTelefono2;

		for ($i = 0; $i < count($arrTelefonos); $i++) {
			$query = $this->Pgaforeglobal->query("SELECT  mensaje , identificador FROM fnvalidartelefonoscorrectos('$cFolio','$arrTelefonos[$i]' ,$iEmpleado , '' , 2 , $i  )");
			$queryResult = $query->result();
			$arrDatos[] = $queryResult;

		}
		return $arrDatos;
	}

	function consultarCatalogoTelefono()
	{
		$query = $this->Pgaforeglobal->query("SELECT idcatalogo,descripcion FROM fnconsultarcatalogotipotelefono();");
		return $query->result();
	}

	function consultarCatalogoCorreoElectronico()
	{
		$query = $this->Pgaforeglobal->query("SELECT idserviciocorreo,trim(descripcion) as descripcion FROM fnconsultarcatalogocorreoei();");
		return $query->result();
	}

	function consultarCatalogoOcupacion()
	{
		$query = $this->Pgaforeglobal->query("SELECT idocupacion, trim(descripcion) as descripcion FROM fnconsultarcatalogoocupacion();");
		return $query->result();
	}

	function consultarCatalogoActividadGiroNegocio()
	{
		$query = $this->Pgaforeglobal->query("SELECT idactividadgiro, trim(descripcion) as descripcion FROM fnconsultarcatalogoactividadgironegocio();");
		return $query->result();
	}

	function consultarCatalogoEntidadNacimiento()
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(identidadnacimiento) AS identidadnacimiento, trim(descripcion) AS descripcion , trim(nomeclatura) AS descorta FROM fnconsultarcatalogoentidadnacimiento();");
		return $query->result();
	}

	function consultarCatalogoIdentificacionesOficiales($sFecha)
	{
		$query = $this->Pgaforeglobal->query("SELECT idoficial as tpoIdentificador, descripcion FROM fnconsultarcatalogoidentificacionoficialei('$sFecha'::date);");
		return $query->result();
	}

	function consultarCatalogoGenero()
	{
		$query = $this->Pgaforeglobal->query("SELECT idgenero, trim(descripcion) as descripcion FROM fnconsultarcatalogogeneroei();");
		return $query->result();
	}

	function consultarCatalogoParentesco()
	{
		$query = $this->Pgaforeglobal->query("SELECT idparentesco, trim(descripcion) AS descripcion FROM fnconsultarcatalogoparentesco();");
		return $query->result();
	}

	function consultarCatalogoNivelEstudio()
	{
		$query = $this->Pgaforeglobal->query("SELECT idnivelestudio, descripcion FROM fnconsultarcatalogonivelestudio();");
		return $query->result();
	}

	function consultarCatalogoNacionalidad()
	{
		$query = $this->Pgaforeglobal->query("SELECT idnacionalidad, descripcion  FROM fnconsultarcatalogonacionalidadei();");
		return $query->result();
	}

	function consultarCatalogoComprobanteDomicilio()
	{
		$query = $this->Pgaforeglobal->query("SELECT idcomprobante, descripcion FROM fnconsultarcatalogocomprobantedomicilio();");
		return $query->result();
	}

	function obtenerExcepcionSolicitante($cFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT iexcepcion, itiposolicitante from fnobtenertiposolicitanteexcepcion($cFolio);");
		return $query->result();
	}

	function fnobtenerconstanciatrabajadorfallecido($cFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT * from fnobtenerconstanciatrabajadorfallecido($cFolio);");
		return $query->result();
	}
	function obtenerCatalogoParentesco($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT idcatalogo, descripcion from fnconsultarcatalogoparentescoei($iFolio);");
		return $query->result();
	}
	function obtenertiposolicitante($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT promotoractivo, claveconsar, tiposolicitante from fnobtenertiposolicitanteei($iFolio);");
		return $query->result();
	}
	function obtenerdatossolitante($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT appaternosol, apmaternosol, nombresol, curpsol, fechanacsol, generosol, entidadsol, rfc, esextranjero FROM fndatossolicitanteei($iFolio)");
		return $query->result();
	}
	function guardardatossolicitante(
		$iFolio,
		$sTipoSolicitante,
		$sApellidoPatSol,
		$sApellidoMatSol,
		$sNombresSol,
		$sCurpSol,
		$sFechaNacSol,
		$iEntidadSol,
		$iNacionalidadSol,
		$iGeneroSol,
		$sRFCSol,
		$iNumeroHoja,
		$iNumeroNotario,
		$sNombreNotario,
		$iNumeroTestimonio,
		$sFechaTestimonio,
		$sNombreJuzgado,
		$iNumeroJuicio,
		$iPromotorActivo,
		$iPromotorInterno,
		$iClaveConsar,
		$iParentesco
	) {

		$query = $this->Pgaforeglobal->query("SELECT 1 FROM fnguardaeitercerasfiguras($iFolio, '$sTipoSolicitante', '$sApellidoPatSol', '$sApellidoMatSol', '$sNombresSol','$sCurpSol', '$sFechaNacSol',$iParentesco::SMALLINT,
		$iEntidadSol, $iGeneroSol::SMALLINT, $iNacionalidadSol, '$sRFCSol', '$iNumeroHoja', '$iNumeroNotario', '$sNombreNotario', '$iNumeroTestimonio', '$sFechaTestimonio', '$sNombreJuzgado', '$iNumeroJuicio', $iPromotorActivo::SMALLINT, $iPromotorInterno::SMALLINT, $iClaveConsar)");
		return $query->result();
	}
	function permisosCurpsReferecias($iTipoServicio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenertiposervicioeiactivado ($iTipoServicio) AS serviciovalidado");
		return $query->result();
	}
	function curpsGenericasReferecias($iTipoReferencia)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenercurpgenericareferenciasei($iTipoReferencia) AS curpref ");
		return $query->result();
	}
	function vaidarMayorDeEdad()
	{
		$query = $this->Pgaforeglobal->query("SELECT current_date AS fechavalida;");
		return $query->result();
	}
	function actualizarpublicarimagenservicioei($iFolio, $iOpcion)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnpublicafirmaservicios FROM fnpublicafirmaservicios($iFolio, $iOpcion);");
		return $query->result();
	}
	
	function servicioEjecutarAplicacion($idservidor)
	{
		$query = $this->bustramites->query("SELECT ipservidor,puerto,urlservicio,protocolo FROM fnobtenerdatosservicio($idservidor);");
		return $query->result();
	}
	function obtenerCurpPromotor($iNumeroEmpleado)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnvalidapromotoractivo AS curp FROM fnvalidapromotoractivo('.$iNumeroEmpleado.')");
		return $query->result();
	}
	function servicioEjecutarAplicacionBus($idservidor)
	{
		$query = $this->bustramites->query("SELECT ipservidor,puerto,urlservicio,protocolo FROM fnobtenerinformacionurlservicio($idservidor,'0')");
		return $query->result();
	}
	function servicioObtenerRespuesta($idservidor)
	{
		$query = $this->bustramites->query("SELECT ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idservidor)");
		return $query->result();
	}

	function obtenerVerificacionei($sCurpTrab, $iNumeroEmpleado, $sTipoServicio)
	{
		$sql = "Select fnverificaobtencionservicio('$sCurpTrab', $iNumeroEmpleado, '$sTipoServicio') AS verificacionservicio;";

		log_message('error','Consulta-> '.$sql);
		$query = $this->Pgaforeglobal->query($sql);

		return $query->result();
	}
	
	function obtenerbanderabancoppel($Foliosolicitud)
	{
		$cSql = "select fnllenadobitacoraafiliacionbancoppel from fnllenadobitacoraafiliacionbancoppel".
			"('$cFolio','1900-01-01','',0,0,'0','0','','','','','','1900-01-01',0,'',0,'',0,'',0,0,0,'0','','',".
			"'','','','',0,0,0,0,'','','','','',0,0,0,0,'','','','','',0,0,0,0,'','','','','','','','','',3)";
			
		$query = $this->Pgaforeglobal->query($cSql);

		return $query->result();
	}



	function obtenerInformacionServicioEi($curpTrabajador)
	{
		$sql = "SELECT trim(estudios) as estudios, trim(ocupacion) as ocupacion, trim(actividadeconomica) as actividadeconomica, trim(correoelectronico) as correoelectronico, trim(codigopostal) as codigopostal, trim(delegacionmunicipio) as delegacionmunicipio, iddelegacionmunicipio, trim(calle) as calle, trim(pais) as pais, trim(ciudadpoblacion) as ciudadpoblacion, idciudadpoblacion, trim(numeroexterior) as numeroexterior, trim(entidadfederativa) as entidadfederativa, identidadfederativa, trim(colonia) as colonia, idcolonia, trim(numerointerior) as numerointerior,
		TRIM(codigopostallab) AS codigopostallab, TRIM(delegacionmunicipiolab) AS delegacionmunicipiolab, iddelegacionmunicipiolab, TRIM(callelab) AS callelab, TRIM(paislab) AS paislab, TRIM(ciudadpoblacionlab) AS ciudadpoblacionlab, idciudadpoblacionlab, TRIM(numeroexteriorlab) AS numeroexteriorlab, TRIM(entidadfederativalab) AS entidadfederativalab, identidadfederativalab, TRIM(colonialab) AS colonialab, idcolonialab, TRIM(numerointeriorlab) AS numerointeriorlab
		FROM fnobtencioninformacionservicioei('$curpTrabajador')";
		log_message('error','Consulta-> '.$sql);
		$query = $this->Pgaforeglobal->query($sql);

		return $query->result();
	}

	//Folio 1514
	function guardarDatosSwitch($cCurp, $iNss, $iTiposolicitud, $cCorreo, $iBandnotificaciones, $iBandedocta)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnagregarenviocorreo as valor FROM fnagregarenviocorreo('$cCurp', '$iNss', $iTiposolicitud, '$cCorreo', $iBandnotificaciones, $iBandedocta)");
		return $query->result();
	}
}
