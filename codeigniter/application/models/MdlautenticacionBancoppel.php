<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlautenticacionBancoppel extends CI_Model{

    public function __construct(){
        parent::__construct();
        //load database library
        $this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
        $this->Pgbustramites = $this->load->database("bustramites",true); 
        $this->PgAdmonAfore = $this->load->database("PgAdmonAfore",true);
        $this->Pgaforecoppel = $this->load->database("Pgaforecoppel",true);
        $this->PgVideos = $this->load->database("PgVideos",true);
        $this->PgServiciosAfore = $this->load->database("PgServiciosAfore",true);
        $this->PgCapacitacionAfore = $this->load->database("PgCapacitacionAfore",true); 
        $this->PgEdocta = $this->load->database("PgEdocta",true); 
        $this->PgExamenes = $this->load->database("PgExamenes",true);  
        $this->PgExamenes_Internos = $this->load->database("PgExamenes_Internos",true);  
        $this->PgHuellas = $this->load->database("PgHuellas",true);   
        $this->PgIMAGENES_HIST = $this->load->database("PgIMAGENES_HIST",true);   
        $this->PgImagenes = $this->load->database("PgImagenes",true);   
    }

    function obtenerHuellas($sNombreArchivo,$ruta){
		$sRutaArchivo   = $ruta.$sNombreArchivo;
		$sTexto         = "";
		if(file_exists($sRutaArchivo)){
            $sFile  = fopen($sRutaArchivo, "r");
            $sTexto = fgets($sFile);
            fclose($sFile);
            unlink($sRutaArchivo);
		}
        return $sTexto;
	}

    function agregaratostbautenticacionbancoppel($arrDatosBanco){
        $rutaHuella = "../../entrada/huellas/bancoppel/";
        $cCurp      = $arrDatosBanco["curp"];
        $cNomb      = $arrDatosBanco["nombre"];
        $cPaterno   = $arrDatosBanco["paterno"];
        $cMaterno   = $arrDatosBanco["materno"];
        $dFchNac    = $arrDatosBanco["fchnac"];
        $iGenero    = $arrDatosBanco["genero"];
        $tIzq       = $this->obtenerHuellas($arrDatosBanco["tizq"],$rutaHuella);
        $tDer       = $this->obtenerHuellas($arrDatosBanco["tder"],$rutaHuella);
        $wDer       = $this->obtenerHuellas($arrDatosBanco["wder"],$rutaHuella);
        $cModulo    = $arrDatosBanco["modulo"];
        $iPromotor  = $arrDatosBanco["promotor"];
        $query      = $this->Pgaforeglobal->query("SELECT fnagregardatostbautenticacionbancoppel AS respuesta FROM fnagregardatostbautenticacionbancoppel('$cCurp','$cNomb','$cPaterno','$cMaterno','$dFchNac',$iGenero,'$tIzq'::TEXT,'$tDer'::TEXT,'$wDer'::TEXT,'$cModulo',$iPromotor);");
        log_message("error", "FUNCION-> fnagregardatostbautenticacionbancoppel");
        return $query->result();
    }

    function creacionjsonbancoppel($foliobanco){
        log_message("error", "FOLIO BANCO-> $foliobanco");
        $jsonenvio      = $this->obtenercabeceraautenticacionbancoppel();
        $jsoncreacion   = $this->agregardatostbautenticacionbancoppelws($foliobanco,$jsonenvio[0]->jsonenvio);
        return $jsoncreacion;
    }

    function obtenercabeceraautenticacionbancoppel(){
        $cQuery     = "SELECT TRIM(fnobtenercabeceraautenticacionbancoppel) AS jsonenvio FROM fnobtenercabeceraautenticacionbancoppel(2::INTEGER);";
        $query      = $this->Pgbustramites->query($cQuery);
        log_message("error", "FUNCION-> $cQuery");
        return $query->result();
    }

    function agregardatostbautenticacionbancoppelws($foliobanco,$json){
        $cQuery     = "SELECT fnagregardatostbautenticacionbancoppelws AS respuesta FROM fnagregardatostbautenticacionbancoppelws($foliobanco,'$json'::TEXT);";
        $query      = $this->Pgaforeglobal->query($cQuery);
        log_message("error", "FUNCION-> $cQuery");
        return $query->result();
    }

    function validarautenticacionbancoppelws($foliobanco,$porcentaje){
        $cQuery     = "SELECT respuesta,valnombre,valpaterno,valmaterno,valfecnac,valgenero,TRIM(mensaje) AS mensaje FROM fnvalidarautenticacionbancoppelws($foliobanco::BIGINT,$porcentaje::DECIMAL(5,3));";
        $query      = $this->Pgaforeglobal->query($cQuery);
        log_message("error", "FUNCION-> $cQuery");
        return $query->result();
    }

    function ejecucionBD($Consulta, $BaseDatos)
	{		        
        $bd = $BaseDatos;
		$cSql = $Consulta;

        if($bd == 'ADMONAFORE') 
        {
			$query = $this->PgAdmonAfore->query($cSql);
        }
        else if($bd == 'AFORECOPPELCOM') 
        {
			$query = $this->Pgaforecoppel->query($cSql);
        }
        else if($bd == 'AFOREGLOBAL') 
        {
			$query = $this->Pgaforeglobal->query($cSql);
        }
        else if($bd == 'BUSTRAMITES') 
        {
			$query = $this->Pgbustramites->query($cSql);
        }
        else if($bd == 'CAPACITACIONAFORE')
        {
			$query = $this->PgCapacitacionAfore->query($cSql);
        }
        else if($bd == 'EDOCTA') 
        {
			$query = $this->PgEdocta->query($cSql);
        }
        else if($bd == 'EXAMENES') 
        {
			$query = $this->PgExamenes->query($cSql);
        }
        else if($bd == 'EXAMENES_INTERNOS') 
        {
			$query = $this->Pgbustramites->query($cSql);
        }
        else if($bd == 'HUELLASEMP') 
        {
			$query = $this->PgHuellas->query($cSql);
        }
        else if($bd == 'IMAGENES_HIST') 
        {
			$query = $this->PgIMAGENES_HIST->query($cSql);
        }
        else if($bd == 'IMAGENES_TEMPO') 
        {
			$query = $this->PgImagenes->query($cSql);
        }
        else if($bd == 'SERVICIOSAFORE') 
        {
			$query = $this->PgServiciosAfore->query($cSql);
        }
        else if($bd == 'VIDEOS') 
        {
			$query = $this->PgVideos->query($cSql);
        }   
        
		return $query->result();
    }
    
    function consultarColonia ($cCodPostal, $iEstado, $iCiudad, $iMunicipio, $cNomColonia){
        $cQuery     = "SELECT CodigoPostal,NombreColonia,NombreCiudad,NombreMunicipio,NombreEstado,idColonia,idCiudad,idMunicipio,idEstado from fnConsultarColonia('" . $cCodPostal . "', ".$iEstado .", ". $iMunicipio .", ". $iCiudad .", '" . $cNomColonia ."')";
        $query      = $this->Pgaforeglobal->query($cQuery);
        log_message("error", "FUNCION-> $cQuery");
        return $query->result();
    }

    function consultarParteDomicilio ($iIdParte, $iEstado){
        $cQuery     = "SELECT idCatalogo, Descripcion from fnConsultarCatEntidadDomicilio(" . $iIdParte . "::SMALLINT, ". $iEstado .");";
        $query      = $this->Pgaforeglobal->query($cQuery);
        log_message("error", "FUNCION-> $cQuery");
        return $query->result();
    }
}
?>