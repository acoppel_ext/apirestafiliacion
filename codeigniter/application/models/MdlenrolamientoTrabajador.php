<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlenrolamientoTrabajador extends CI_Model {

    public function __construct()
	{
		parent::__construct();

        //load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->Pgsafre = $this->load->database("Pgsafre",true);
    }

    function validarEstadoPromotor($iEmpleado)
	{
		$query = $this->Pgaforeglobal->query("select fnvalidarenroltrabajadores as valor from fnvalidarenroltrabajadores ($iEmpleado);");
		return $query->result();
	}

	function borrarExcepciones($iFolio)
	{
		$query = $this->Pgaforeglobal->query("select fnborrarexcepcionesenroltrab as borro from fnborrarexcepcionesenroltrab($iFolio);");
		return $query->result();
	}

	function actualizarEstatusEnrol($iFolio, $iEstatusCancelar)
	{
		$query = $this->Pgaforeglobal->query("select fnactestatusenrolatrabajador as actualizo from fnactestatusenrolatrabajador($iFolio,$iEstatusCancelar);");
		return $query->result();
	}

	function guardarDatos($iEmpleado, $solicitud, $curp, $iFolioSolicitud)
	{
		$query = $this->Pgaforeglobal->query("select fnguardarenrolamientotrabajador as ires from fnguardarenrolamientotrabajador($iEmpleado,$solicitud,'$curp',$iFolioSolicitud);");
		return $query->result();
	}

	function cancelarEnrolamiento($iFolio, $estatus)
	{
		$query = $this->Pgaforeglobal->query("select fnactestatusenrolatrabajador as val from fnactestatusenrolatrabajador($iFolio,$estatus);");
		return $query->result();
	}

	function validaNIST($iFolioEnrol)
	{
		$query = $this->Pgaforeglobal->query("select regresa as resulta from fnvalidarnisttrabajador($iFolioEnrol);");
		return $query->result();
	}

	function consultarDedosEnManos($iFolio, $iSelecciono)
	{
		$query = $this->Pgaforeglobal->query("select numdedo,trim(nombrededo) as nombrededo from fnvalidardedosenroltrabajador($iFolio,$iSelecciono);");
		return $query->result();
	}

	function guardaExcepciones($iFolio, $chExcepcion, $idedos)
	{
		$query = $this->Pgaforeglobal->query("select fngrabarenrolexcepcionestrabajador as ires from fngrabarenrolexcepcionestrabajador($iFolio,'$chExcepcion',$idedos);");
		return $query->result();
	}

	function obtenerLigaFormatoEnrolamiento($iFolio,$ipTienda,$iOpcion,$iTestigo,$iNumTestigo)
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(fnobtenerligapaginaenrolamiento) as dato FROM fnobtenerligapaginaenrolamiento( $iOpcion,$iFolio,$iTestigo,$iNumTestigo,'$ipTienda');");
		return $query->result();
	}

	function verificacionEnrolAutoriza($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnvalidarenrolautoriza AS retorno FROM fnvalidarenrolautoriza ($iFolio);");
		return $query->result();
	}

	function limpiarHuellasEnrol($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fneliminarhuellasenroltrabajador AS regreso FROM fneliminarhuellasenroltrabajador($iFolio);");
		return $query->result();
	}

	function actualizarEnrolamiento($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnactestatusenrolatrabajador AS regreso FROM fnactestatusenrolatrabajador($iFolio, 0);");
		return $query->result();
	}

	function excepcionarDedosEnRolTrabajador($iFolioEnrol)
	{
		$query = $this->Pgaforeglobal->query("select fnexcepcionardedosenroltrabajador as resulta from fnexcepcionardedosenroltrabajador($iFolioEnrol);");
		return $query->result();
	}

	function ObtenerDatosConfiguracion()
	{
		$query = $this->Pgaforeglobal->query("SELECT totalminutos, totaldias, totalintentos FROM fnobtenervaloresconfiguracion();");
		return $query->result();
	}

	function llamarMarcaEI($cFolioServicio)
	{
		$query = $this->Pgaforeglobal->query("select nss, folioenrolamiento, flujoservicio, codigomotivo from recserviciosafore Where folioservicio = $cFolioServicio;");
		return $query->result();
	}

	function llamarMarcaEIConsecutivo()
	{
		$query = $this->Pgsafre->query("EXECUTE FUNCTION fn_obten_ret_consecutivo();");
		return $query->result();
	}

	function llamarMarcaEIMarcarCuenta($iCodigoMotivo,$cNss,$iConsecutivo,$iEmpleado)
	{
		if ($iCodigoMotivo == 2022)
		{
			$query = $this->Pgsafre->query("EXECUTE FUNCTION fn_afop_marcarcuenta(1, '$cNss', $iConsecutivo, 'MD', '$iEmpleado')");
		}
		else
		{
			$query = $this->Pgsafre->query("EXECUTE FUNCTION fn_afop_marcarcuenta(1, '$cNss', $iConsecutivo, 'AE', '$iEmpleado')");
		}

		return $query->result();
	}

	function llamarMarcaEIActualizarServicioAfore($iConsecutivo,$cFolioServicio)
	{
		$query = $this->Pgaforeglobal->query("UPDATE recserviciosafore SET consecutivomarca = $iConsecutivo WHERE folioservicio = $cFolioServicio");
		return $query;
	}

	function validarLevantamientoEI($iFolio, $sCurpTrabajador)
	{
		$query = $this->Pgaforeglobal->query("SELECT tieneexpiden FROM fnconsultarestatusbiometricos($iFolio, '$sCurpTrabajador')");
		return $query->result();
	}

	function consultarnumempleado($iEmpleado)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnconsultarnumempleado AS resultado FROM fnconsultarnumempleado(".$iEmpleado.");");
		return $query->result();
	}

	function obtenermensajegerenteenrol($opcionmensaje)
	{
		$query = $this->Pgaforeglobal->query("SELECT idmensaje, mensaje FROM fnobtenermensajegerenteenrol(".$opcionmensaje.");");
		return $query->result();
	}

	function guardarbitacoraenrolamiento($iFolioSolicitud,$iFolioEnrol,$iTienda,$iEmpleado,$iNumEmpGerente,$curp,$iautorizaciongerencial,$itipoenrolamiento,$iExcepcionesHuellas)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnregistrarbitacoraautorizacionenrolamiento AS resultado FROM fnregistrarbitacoraautorizacionenrolamiento(".$iFolioSolicitud.", ".$iFolioEnrol.",".$iTienda.",".$iEmpleado.", ".$iNumEmpGerente.", '".$curp."', ".$iautorizaciongerencial.", ".$itipoenrolamiento.", ".$iExcepcionesHuellas.");");
		return $query->result();
	}

	function obtenerclaveoperacion($iFolioSolicitud)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenerdoctosdigitalizacion AS resultado FROM fnobtenerdoctosdigitalizacion(".$iFolioSolicitud.", 3);");
		return $query->result();
	}

	function obtenerclaveoperacionservicios($iFolioSolicitud)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenerdoctosdigitalizacionservicios AS claveoperacion FROM fnobtenerdoctosdigitalizacionservicios('".$iFolioSolicitud."', 50);");
		return $query->result();
	}

	function obtenertienda($ipModulo)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenertienda AS tienda FROM fnobtenertienda('".$ipModulo."');");
		return $query->result();
	}

}
?>