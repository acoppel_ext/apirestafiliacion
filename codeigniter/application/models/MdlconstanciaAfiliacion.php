<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlconstanciaAfiliacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->Pgsafre = $this->load->database('Pgsafre', TRUE);
		$this->PgVideos = $this->load->database("PgVideos",true);
		$this->bustramites = $this->load->database("bustramites",true);

		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		
		$hostname = $this->Pgaforeglobal->hostname;
		$username = $this->Pgaforeglobal->username;
		$password = $this->Pgaforeglobal->password;
		$database = $this->Pgaforeglobal->database;

		$this->conexionDirecta =  new PDO( "pgsql:host=".$hostname.";port=5432;dbname=".$database.";options='-c client_encoding=SQL_ASCII'", $username, $password);
	}

	function AlmacenaDatosConstacia($numerofolio,$iEmpleado,$iSiefore,$iTipoConstancia,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$sNss,$sNumeroCell,$iCompaniaCel,$sTipoComprobante,$sFechaComprobante,$sNumeroCliente,$iTipoTelefono,$sTelefonoContacto,$iCompTel,$sCalle,$sNumExt,$sNumInt,$sCodPost,$iColonia,$iDelegacion,$iEstado,$iPais,$sLugarSol,$iTienda,$iFirma,$iFolioBcpl,$sCurpSolicitante,$iTiposolicitante,$iFolioConstanciaImplicaciones,$aforeCedente,$tipoTraspaso,$iTipoExcepcion,$folioPrevalidador,$iFolioConocimientoTraspaso){

		$query = $this->conexionDirecta->query(utf8_decode("select fn_grabar_solconstancia03 from fn_grabar_solconstancia03($numerofolio,$iEmpleado,$iSiefore, $iTipoConstancia,'$sCurp','$sAppaterno','$sApmaterno','$sNombre','$sNss','$sNumeroCell',$iCompaniaCel,'$sTipoComprobante','$sFechaComprobante','$sNumeroCliente',$iTipoTelefono,'$sTelefonoContacto',$iCompTel,'$sCalle','$sNumExt','$sNumInt','$sCodPost',$iColonia,$iDelegacion,$iEstado,$iPais,$sLugarSol,$iTienda,$iFirma,$iFolioBcpl,'$sCurpSolicitante',$iTiposolicitante,'$iFolioConstanciaImplicaciones', '$aforeCedente', $tipoTraspaso, $iTipoExcepcion,$folioPrevalidador,'$iFolioConocimientoTraspaso');"));
		return $query;
	}
	
	function obtenerclaveoperacionconstancia($iFolio,$iTabla)
	{	
		$cadena = "select fnobtenerdoctosdigitalizacion from fnobtenerdoctosdigitalizacion ($iFolio, $iTabla);";
		$query = $this->Pgaforeglobal->query("select fnobtenerdoctosdigitalizacion from fnobtenerdoctosdigitalizacion ($iFolio, $iTabla);");
		return $query->result();
	}
	
	function actualizarestatusfirmas($iOpcion,$iFolio,$iEstatus)
	{	
		$query = $this->Pgaforeglobal->query("select fnactualizaestatusfirma from fnactualizaestatusfirma ($iOpcion,$iFolio,$iEstatus);");
		return $query->result();
	}
	
	function actualizarfirmapublicacion($iFolio,$iOpcion)
	{	
		
		$query = $this->Pgaforeglobal->query("select fnpublicafirma from fnpublicafirma ($iFolio,$iOpcion);");
		return $query->result();
	}
	
	function obtenerCurpPromotor($iNumeroEmpleado)
	{	
		$query = $this->Pgaforeglobal->query("select fnvalidapromotoractivo as curp from fnvalidapromotoractivo($iNumeroEmpleado)");
		return $query->result();
	}
	
	function validartiposolicitanteexcepcion($iFolioSol)
	{	
		$query = $this->Pgaforeglobal->query("SELECT iexcepcion, itiposolicitante, itipoconstancia from fnobtenertiposolicitanteexcepcion($iFolioSol)");
		return $query->result();
	}
	
	/*function consultarHuellas($iFolio,$sCurpTrab,$iIDServicio,$cTipoOperacion,$tiposolicitante)
	{
		if($tiposolicitante != 1)
		{
			$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellasdoctosafiliacion('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')");	
		}
		else
		{
			$cTipoOperacion = '0101';
			$iIDServicio = 9909;
			$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellastercerasfiguras('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')");	
		}
		
		return $query->result();
	}*/

	function ConsultacatalogoComprobante(){

		$query = $this->Pgaforeglobal->query("select numcomp,desccomp from fn_catcomprobantes();");
		return $query->result();
	}

	function ConsultacatalogoCompaCelulares(){

		$query = $this->Pgaforeglobal->query("select clavec,desccomp from fn_catcompacelulares();");
		return $query->result();
	}

	function Consultacurp($sCurp){

		$query = $this->Pgaforeglobal->query("select fecha,curp,estatus,diagnostico from fn_valida_curp03('$sCurp');");
		return $query->result();
	}

	function ConsultaRechazoCurp($sCurp){

		$query = $this->Pgaforeglobal->query("select estatus,curp,codigo,descripcion from fn_consulta_rechazo('$sCurp');");
		return $query->result();
	}

	function ConsultacatalogoLugarSolicitud(){

		$query = $this->Pgaforeglobal->query("select clave,descripcion as descripcioncat from fn_catlugarsolicitud();");
		return $query->result();
	}

	function validafechacomprobante($sFechaComprobante){

		$query = $this->Pgaforeglobal->query("select fn_valida_fechacomprobante as respuesta from fn_valida_fechacomprobante('$sFechaComprobante'::date);");
		return $query->result();
	}

	function validaNumeroTelefono($telefono){

		$query = $this->Pgaforeglobal->query("select fnvalidatelefono as respuesta from fnvalidatelefono('$telefono');");
		return $query->result();
	}

	function validadtelefonoSolConstancias($sTel, $sCurp, $op){

		$query = $this->Pgaforeglobal->query("select  fnvalidatelefono_solconstancia as respuesta from fnvalidatelefono_solconstancia('$sTel','$sCurp',$op);");
		return $query->result();
	}

	function validadtelefonoTelTiendas($sTel){

		$query = $this->Pgaforeglobal->query("select fn_val_telvstienda as respuesta from fn_val_telvstienda ('$sTel');");
		return $query->result();
	}

	function validadtelPromotores($sTel){

		$query = $this->Pgsafre->query("EXECUTE function fn_compara_promae('$sTel')");
		return $query->result();
	}

	function validarDomicilioTiendas($sCalle,$sNumExt,$sCodPost,$sDesCol,$sDesMun,$sDesEdo){

		$query = $this->Pgaforeglobal->query("select fnvalidaDomicilios as respuesta from fnvalidaDomicilios ( '$sCalle','$sNumExt','$sCodPost','$sDesCol','$sDesMun','$sDesEdo');");
		return $query->result();
	}

	function obtenervidencia($tiposolicitud)
	{

		if ($tiposolicitud ==  26)
		{
			$cSql = "select tipo4 from pafhardcode  where tipo1 = 176 and tipo2 = 3 and tipo3 = 1;"; //registro
		}
		else
		{
			$cSql = "select tipo4 from pafhardcode  where tipo1 = 176 and tipo2 = 3 and tipo3 = 2;"; //traspaso

		}

		$query = $this->Pgaforeglobal->query($cSql);
		return $query->result();
	}

	function obtenernumerofolio(){

		$query = $this->Pgaforeglobal->query("SELECT NEXTVAL('foliossolicitudes') as numerofolio;");
		return $query->result();
	}

	function obtenersiefore($sCurp){
		$query = $this->Pgaforeglobal->query("SELECT fnobtenersieforegeneracionalag AS siefore FROM fnobtenersieforegeneracionalag('$sCurp');");
		return $query->result();
	}

		function obtenerestatusempleado($iEmpleado){

		$query = $this->Pgaforeglobal->query("select fnvalidarenrolamiento as valor from fnvalidarenrolamiento($iEmpleado)");
		return $query->result();
	}

		function obtenermensajerespuestaAU($folioretorno)
		{
				$query = $this->Pgaforeglobal->query("select tipooperacion,codigo, mensajebancoppel as mensaje from fnmensajeretornobancoppel($folioretorno)");

				return $query->result();
		}

		function obteneriformacionclientebancoppel($keyxbancoppel)
		{
				$query = $this->Pgaforeglobal->query("select curp,nombre,app,apm,telefonocelular,numclientecoppel,telcontacto,calle,nexterior,ninterior,codpostal,colonia,delmun,estado,ciudad,calle1,nexterior1,ninterior1,codpostal1,colonia1,delmun1,estado1,ciudad1,idcolonia,iddelmun,idestado,idcolonia1,iddelmun1,idestado1,idanverso,idreverso from fnobtenerdatosclientebancoppel($keyxbancoppel)");

				return $query->result();
		}

		function actualizarflagclientebcpl($keyxbancoppel)
		{
				$query = $this->Pgaforeglobal->query("select fnactualizarflagauclientebcpl as iRetorono from fnactualizarflagauclientebcpl($keyxbancoppel)");

				return $query->result();
		}

		function validacion($keyxbancoppel)
		{
				$query = $this->Pgaforeglobal->query("select idanverso,idreverso from fnobtenerdatosclientebancoppel($keyxbancoppel)");

				return $query->result();
		}

		function consultaRespuestaRenapo($iFolioAfore)
		{
				$query = $this->Pgaforeglobal->query("select idu_tipoerror, idu_codigoerrorrenapo, mensaje from fnconsultarespuestacurprenapo($iFolioAfore);");

				return $query->result();
		}

		function consultarDatosRenapo($iFolioAfore)
		{
				$query = $this->Pgaforeglobal->query("select icoderror, trim(curp) as curp, trim(apellidopaterno) as apellidopaterno, trim(apellidomaterno) as apellidomaterno, trim(nombres) as nombres from fnconsultadatoscurprenapo($iFolioAfore);");

				return $query->result();
		}

	function removerCaracteresEspeciales($arrDatos = array(),$iTipo = 1)
	{
				/*iTipo = 1 es para eliminar los caracteres especiales a los Mensajes que arroja
				*/

				/**
				 * iTipo = 1 es para eliminar los caracteres especiales a los Mensajes que arroja
				 * iTipo = 2 es para elminar los caracteres especiales de los nombres de las personas
				 */

				$search = array();
				$replace = array();

				$search = array('&', '<', '>', '€', '‘', '’', '“', '”', '–', '—', '¡', '¢','£', '¤', '¥', '¦', '§', '¨', '©', 'ª', '«', '¬', '®', '¯', '°', '±', '²', '³', '´', 'µ', '¶', '·', '¸', '¹', 'º', '»', '¼', '½', '¾', '¿', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã','ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ','Œ', 'œ', '‚', '„', '…', '™', '•', '˜');

				$replace  = array('&amp;', '&lt;', '&gt;', '&euro;', '&lsquo;', '&rsquo;', '&ldquo;','&rdquo;', '&ndash;', '&mdash;', '&iexcl;','&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;','&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;','&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&OElig;', '&oelig;', '&sbquo;', '&bdquo;', '&hellip;', '&trade;', '&bull;', '&asymp;');

				switch ($iTipo) {
						case 1:
										foreach ($arrDatos['registros'] as $key => $object)
										{
												$sMensaje = $object->mensaje;

												$sMensaje = str_replace($search, $replace, utf8_encode($sMensaje));

												$object->mensaje = $sMensaje;
										}
								break;
						case 2:
										foreach ($arrDatos['registros'] as $key => $object)
										{

												$sApellidopaterno = $object->apellidopaterno;
												$sApellidomaterno = $object->apellidomaterno;
												$sNombres = $object->nombres;

												$sApellidopaterno = str_replace($search, $replace, utf8_encode($sApellidopaterno));
												$sApellidomaterno = str_replace($search, $replace, utf8_encode($sApellidomaterno));
												$sNombres = str_replace($search, $replace, utf8_encode($sNombres));

												$object->apellidopaterno = $sApellidopaterno;
												$object->apellidomaterno = $sApellidomaterno;
												$object->nombres = $sNombres;
										}
								break;
						default:
								$arrDatos['descripcion'] = "Error al consultar el metodo removerCaracteresEspeciales";
								$arrDatos['estatus'] = -1;
								break;
				}
				return $arrDatos;
		}

	function consultacatalogoestadocivil()
	{

		$query = $this->Pgaforeglobal->query("select clavec, trim(desccomp) as desccomp from fn_catestadocivil()");
		return $query->result();
	}

	function obtenercurpempleado($iEmpleado)
	{

		$query = $this->Pgaforeglobal->query("select keyx, curp from colinfxpromotores where empleado = $iEmpleado");

		return $query->result();

	}

	function obtenerdatospromotor($iEmpleado)
	{

		$query = $this->Pgaforeglobal->query("select trim(nombre) as nombre,claveconsar from fn_consultapromotor($iEmpleado)");

		return $query->result();

	}

	function consultacolsolicitudes($sCurp)
	{

		$query = $this->Pgaforeglobal->query("select trim(curp) as curp from fn_consultacolsolicitudes('$sCurp')");

		return $query->result();

	}

	function consultatipoconstancia($sCurp)
	{

		$query = $this->Pgaforeglobal->query("select tiposolicitud from fn_consultacolsolicitudes('$sCurp')");

		return $query->result();

	}

	function obtenertienda($ipTienda)
	{

		$query = $this->Pgaforeglobal->query("SELECT tipo3 FROM pafhardcode WHERE tipo1 = 12 AND concepto2 = '" . $ipTienda . "'");

					return $query->result();

	}

	function consultainformacionsolconstancia($sCurp, $iEstatusProceso)
	{

		$query = $this->Pgaforeglobal->query("select folio,numempleado,tipoconstancia,curp,appaterno,apmaterno,nombres,nss,
					telefonocelular,compatelefonica,tipocomprobante,fechacomprobante,noclientecoppel,
					tipotelefono,telefonocontacto,comptelefonicacontacto,calle, numexterior,numinterior,codigopostal,
					colonia,delegmunic,estado,pais,lugarsolicitud, idtipotraspaso, estadocivil
					from fnconsultainformacionsolconstancia02('$sCurp',$iEstatusProceso)");

					return $query->result();

	}

	function consultainformaciondireccionsolconstancia($sCodigopostal,$idColonia,$idDelegacion,$idEstado)
	{

		$query = $this->Pgaforeglobal->query("SELECT descolonia,desmunicipio,desestado
					FROM fnconsultainformaciondireccionsolconstancia ('$sCodigopostal',$idColonia,$idDelegacion,$idEstado)");

		return $query->result();

	}

	function consultaInvocacionServicio($sCurp)
	{

		$query = $this->Pgaforeglobal->query("SELECT fnobtenerestatusinvocacionserviciocontrasena as estatus FROM  fnobtenerestatusinvocacionserviciocontrasena('$sCurp')");

		return $query->result();

	}

	function servicioEjecutarAplicacion($idServicio)
	{

		$query = $this->bustramites->query("select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio)");

		return $query->result();

	}

	function consultadatosbiometricos($consultafolio,$verificacionFolio)
	{

            $query = $this->Pgaforeglobal->query("select cestatus,cdiagnosticos,vestatus,vdiagnosticos,cestatusexpediente, cestatusenrolamiento,
						vselloverificacion from fnobtenerrespconsultaverificacion($consultafolio,$verificacionFolio)");

			return $query->result();

    }
	function actualizarSolConstancia($folioactualizar,$estatusconsulta, $estatusexpedienteconsulta, $estatusenrolamientoconsulta, $diagnosticoconsulta, $estatusverificacion, $diagnosticoverificacion, $selloverificacion)
	{

            $query = $this->Pgaforeglobal->query("select fnactualizainformacionservicios as valor
						from fnactualizainformacionservicios($folioactualizar,'$estatusconsulta', '$estatusexpedienteconsulta', '$estatusenrolamientoconsulta', '$diagnosticoconsulta', '$estatusverificacion', '$diagnosticoverificacion', $selloverificacion)");

			return $query->result();

    }
	function obtenerenlace($iOpcion,$iFolio,$iTestigo,$iNumTestigo,$ipTienda)
	{

            $query = $this->Pgaforeglobal->query("SELECT trim(fnobtenerligapaginaenrolamiento) as dato FROM fnobtenerligapaginaenrolamiento($iOpcion,$iFolio,$iTestigo,$iNumTestigo,'$ipTienda')");

			return $query->result();

    }
	function obtenerestatusEnrolamiento($sCurp, $iFolioAfore)
	{
			$query = $this->Pgaforeglobal->query("SELECT fnconsultaestatusenrolamientoafiliacion AS valor FROM fnconsultaestatusenrolamientoafiliacion('$sCurp', $iFolioAfore)");

			return $query->result();

	}
	function dianosticoErrores($diagnostico)
	{

            $query = $this->Pgaforeglobal->query("select mensaje as valor from catdiagprocesarbiometricos where codigo = '$diagnostico'");

			return $query->result();

    }

	function ActualizarEstadoCivil($ifolioactualizaestadocivil, $iestadocivil)
	{
		$query = $this->Pgaforeglobal->query("select fnactualizarestadocivil from fnactualizarestadocivil('$ifolioactualizaestadocivil', '$iestadocivil')");

		return $query->result();
	}

	function validaTelefonoCelular($sNumeroCell)
	{

		$query = $this->Pgaforeglobal->query("select fnvalidatelefonocelular as iestatuscelular from fnvalidatelefonocelular('".$sNumeroCell."');");

		return $query->result();

	}

	function ValidarRegistroPrevio($cCurp)
	{

		$query = $this->Pgaforeglobal->query("SELECT  apellidopaterno, apellidomaterno, nombre, nss, celular, companiacel, comprobante, fechacomprobante, numclientecop, tipotelefono, telofonodecontacto, companiatelefono, calle, numeroexterior, numerointerior, codpostal, colonia, municipio, estado, pais,lugarsolicitud,estadocivil,icolonia,imunicipio,iestado,ipais FROM fnvalidarregistropreviosolconstancia('$cCurp');");

		return $query->result();

	}

	function validarCurpenReanpo($sCurp)
	{
		$query = $this ->Pgaforeglobal->query("SELECT fnValidarCurpenRenapo AS respuesta FROM fnValidarCurpenRenapo('$sCurp')");
		return $query ->result();
	}

	function actualizarFolRenapo($iFolioCons, $iFolioAfore)
	{
		$query = $this ->Pgaforeglobal->query(" SELECT fnactualizafoliorensolicitudescurp AS respuesta FROM fnactualizafoliorensolicitudescurp( $iFolioCons,$iFolioAfore)");
		return $query ->result();
	}

	function obtenerLigaRenapo($iOpcionRenapo,$iFolioAfore, $iPmodulo)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenerligapaginacurprenapo AS liga FROM fnobtenerligapaginacurprenapo($iOpcionRenapo,$iFolioAfore,'$iPmodulo')");
		return $query ->result();
	}

	function validarListaNegraTelefonoCel($sNumeroCell,$iEmpleado,$sCurp)
	{

		$query = $this->Pgaforeglobal->query("SELECT  mensaje , identificador FROM fnvalidartelefonoscorrectos('0','$sNumeroCell' ,$iEmpleado , '$sCurp' , 1 , 0)");
		return $query->result();

	}
	function validarListaNegraTelefonofijo($sTelefonoContacto,$iEmpleado,$sCurp)
	{

		$query = $this->Pgaforeglobal->query("SELECT  mensaje , identificador FROM fnvalidartelefonoscorrectos('0','$sTelefonoContacto' ,$iEmpleado , '$sCurp' , 1 , 1 )");
		return $query->result();

  	}

	function obtenerRespuestaEnrolHuellaServicio($folioOperacionC,$folioOperacionV,$sCurp,$idServicioC,$idServicioV,$idOperacionC,$idOperacionV)
	{
		$query = $this->Pgaforeglobal->query("select fngrabarenrolfolioservicio as dato from fngrabarenrolfolioservicio($folioOperacionC, $folioOperacionV, '$sCurp', $idServicioC::smallint, $idServicioV::smallint, '$idOperacionC', '$idOperacionV')");
		return $query->result();
	}

	function ejecutaPrevalidacion()
	{
		$query = $this->Pgaforeglobal->query("Select tipo3 as Estatus,trim(concepto) as Concepto From pafhardcode Where tipo1 = 99110501");
		return $query->result();
	}

	function obtenerDocRequeridosAfiliacion()
	{
			$query = $this->Pgaforeglobal->query("SELECT tpoidentificador as identificador, trim(tpodescripcion) as descripcion, trim(tpoiddocumento) as iddocumento FROM fnpafindexgetdoctosafiliaciontrabajador()");
			return $query->result();
	}

	function obtenerDocRequeridosReenrolamiento()
	{

			$query = $this->Pgaforeglobal->query("SELECT tpoidentificador as identificador, trim(tpodescripcion) as descripcion, trim(tpoiddocumento) as iddocumento FROM fnpafindexgetdoctosreenroltrabajador()");
			return $query->result();

	}

	function guardarFirmaTrabajador($iFolio,$iddocumento,$sUrlFirma)
	{

			$query = $this->Pgaforeglobal->query("SELECT fn_grabar_firma_trabajador as valor FROM fn_grabar_firma_trabajador($iFolio,'$iddocumento','$sUrlFirma')");
			return $query->result();

	}

	function obtenerDocFotoClienteAfiliacion()
	{

			$query = $this->Pgaforeglobal->query("SELECT tpoidentificador as identificador, trim(tpodescripcion) as descripcion, trim(tpoiddocumento) as iddocumento FROM fnpafindexgetdoctosfotoafiliaciontrabajador()");
			return $query->result();

	}

	function obtenerTemplatePromotor($iEmpleado)
	{
			$query = $this->Pgaforeglobal->query("SELECT stemplate AS template FROM fnobtenertemplatespromotor($iEmpleado)");
			return $query->result();
	}

	function obtenerTemplateTrabajador($sCurp)
	{
			$query = $this->Pgaforeglobal->query("SELECT stemplate AS template FROM fnobtenertemplatestrabajador('$sCurp')");
			return $query->result();
	}

	function almacenaDatosHuellasServicio($ccurptrab,$iempleado,$ifoliosolicitud,$itipopersona, $iidservicio, $ctipooperacion,$inumdedo,$inist,$cdispositivo,$ifap,$cimagen,$ctiposervicio)
	{
			$query = $this->Pgaforeglobal->query("SELECT fngrabarhuellaservicios as respuesta FROM fngrabarhuellaservicios('$ccurptrab','$iempleado','$ifoliosolicitud','$itipopersona','$iidservicio','$ctipooperacion','$inumdedo','$inist','$cdispositivo','$ifap','$cimagen','$ctiposervicio')");
			return $query->row();
	}

	function guardarDatosEnrolamiento($iFolioEnrolamiento,$sDispositivo,$ifap,$sArrayDedos)
	{
			$query = $this->Pgaforeglobal->query("SELECT fnguardarenrolcompleto FROM fnguardarenrolcompleto($iFolioEnrolamiento,'$sDispositivo',$ifap,'$sArrayDedos')");
			return $query->result();
	}

	function guardarUbicacionDocAfiliacion($folio, $documento, $ubicacion, $iporigen, $tipodocumento)
	{
			$query = $this->Pgaforeglobal->query("SELECT fnguardarubicaciondocafiliacion AS respuesta FROM fnguardarubicaciondocafiliacion($folio,'$documento', '$ubicacion', '$iporigen', '$tipodocumento',1)");
			return $query->row();
	}

	function obtenerUbicacionDocAfiliacion($folio)
	{
			$query = $this->Pgaforeglobal->query("SELECT folioconstancia, trim(documento) AS sdocumento, trim(ubicacion) AS subicacion, trim(iporigen) AS siporigen, trim(tipodocumento) AS stipodocumento FROM fnobtenerubicaciondocafiliacion(ARRAY[$folio])");
			return $query->result();
	}

	function obtenergerentebytemplate($template)
	{
			$query = $this->Pgaforeglobal->query("SELECT fnobtenergerentebytemplate AS iempleado FROM fnobtenergerentebytemplate('$template');");
			return $query->result();
	}

	function obtenerArchivosWsdl($lNumero)
	{
		$dirPadre = dirname(__FILE__);
		define('path_wsdl', 				'/servicios/wsHuellasHC.wsdl');

		$wsHuellasHC = $dirPadre.path_wsdl;

		$urlws = $this->Pgaforeglobal->query("SELECT trim(fnobtenerofflineempleado) AS urlws FROM fnobtenerofflineempleado($lNumero);");
		$urlws = $urlws->row();
		$urlws = $urlws->urlws;

		$cIpAfore = $this->Pgaforeglobal->query("SELECT trim(ipparametro) AS cipafore FROM catwebserviceremoto WHERE idservicio=102::smallint;");
		$cIpAfore = $cIpAfore->row();
		$cIpAfore = $cIpAfore->cipafore;

		$arrpath = array(	'wsHuellasHC' => $wsHuellasHC,
											'urlws' => 'http://'.$urlws.':20616',
											'cIpAfore' => $cIpAfore
											);
		return $arrpath;
	}

	function ejecutaServicioHuellas($lNumero,$cTemplate,$iTipoPersona)
	{
		$respuesta[] = $this->obtenerArchivosWsdl($lNumero);

		foreach($respuesta as $key => $value)
		{
			$wsHuellasHC = $value['wsHuellasHC'];
			$urlws = $value['urlws'];
			$cIpAfore = $value['cIpAfore'];
		}

		$opcionWs['location'] = $urlws;
		$opcionWs['cache_wsdl'] = WSDL_CACHE_NONE;
		$opcionWs['trace'] = true;
		$opcionWs['exceptions'] = true;

		$iSizeTemplate = base64_decode($cTemplate);

		$a = base64_decode($cTemplate);
		$b = array();
		foreach(str_split($a) as $c){
			$b[] = sprintf("%08b", ord($c));
		}

		$iSizeTemplate = count($b);

		$inParam = array(
			'lNumero' => $lNumero,
			'cTemplate' => $cTemplate,
			'iSizeTemplate' => $iSizeTemplate,
			'iTipoPersona' => $iTipoPersona == 2 ? 1 : 2,//Se hace esto para homologar la aplicacion con el servicio web
			'cIpAfore' => ''.$cIpAfore.' (http://'.$cIpAfore.'/)'
		);

		$clienteWs = new SoapClient($wsHuellasHC,$opcionWs);
		$resWs = $clienteWs->__call("AforeComparacionTemplate",array( $inParam ) );
		return $resWs;
	}

	function guardarBitacoraBioRechazos($iIdServicio,$iFolioSolicitud,$cTipoOperacion,$iNumEmpleado,$cCurpTrabajador,$iDedo,$iRetorno)
	{
			$query = $this->Pgaforeglobal->query("SELECT fnguardarbitacorabiometricosrechazos AS respuesta from fnguardarbitacorabiometricosrechazos($iIdServicio,$iFolioSolicitud,'$cTipoOperacion',$iNumEmpleado,'$cCurpTrabajador',$iDedo,$iRetorno)");
			return $query->row();
	}

	function obtenerTiendaEmpleado($iEmpleado)
	{
			$query = $this->Pgaforeglobal->query("SELECT tienda FROM colinfxpromotores WHERE empleado=$iEmpleado;");
			return $query->row();
	}
	function registrarDatosPrevalidador($data)
	{
			$apellidoPaterno = $data['apellidoPaterno'];
			$apellidoMaterno = $data['apellidoMaterno'];
			$nombres = $data['nombres'];
			$curpTrabajador = $data['curpTrabajador'];
			$curpPromotor = $data['curpPromotor'];
			$nss = $data['nss'];
			$selloBiometrico = $data['selloBiometrico'];
			$numeroEmpleado = $data['numeroEmpleado'];

			$query = $this->Pgaforeglobal->query("SELECT fnregistrardatosenviarprevalidador AS foliosolicitud FROM fnregistrardatosenviarprevalidador('$apellidoPaterno', '$apellidoMaterno', '$nombres', '$curpTrabajador', '$curpPromotor', '$nss', $selloBiometrico, $numeroEmpleado)");

			return $query->result();
	}

	function consultarFolioEnrolamiento($iFolioSolicitud)
	{
			$query = $this->Pgaforeglobal->query("SELECT FolioEnrolamiento FROM solconstancia where folio=$iFolioSolicitud");
			return $query->row();
	}

	function obtenerentolcapturadovozmovil($ID,$IdentIFicador,$Tienda,$Imposibilidad)
	{
			$query = $this->Pgaforeglobal->query("SELECT mensaje FROM fnobtenerentolcapturadovozmovil($ID::smallint,$IdentIFicador,$Tienda,$Imposibilidad);");
			return $query->row();
	}

	function obtenerRespuestaPrevalidador($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT valmarcas,
		trim(valfoliosconstanciasimplicaciones) AS valfoliosconstanciasimplicaciones,
		trim(valregimenpensionario) AS valregimenpensionario,
		trim(valnumtrasptreintaseismeses) AS valnumtrasptreintaseismeses,
		trim(valcitactiva) AS valcitactiva,
		trim(valrendimmenor) AS valrendimmenor,
		trim(traspasopreviorendimientos) AS traspasopreviorendimientos,
		trim(valcurpfuncionarioadministradora) AS valcurpfuncionarioadministradora,
		foliosolicitud,
		trim(valcurptrabajador) AS valcurptrabajador,
		trim(valnsstrabajador) AS valnsstrabajador,
		trim(valapellidopaterno) AS valapellidopaterno,
		trim(valapellidomaterno) AS valapellidomaterno,
		trim(valnombretrab) AS valnombretrab,
		cveenttransbd,
		motrec,
		folioservicioafore,
		clavepeticionservicio,
		codigo_error,
		curp_promotor,
		fecha1traspasoprevio AS fecha1traspasoprevio,
		fecha2traspasoprevio AS fecha2traspasoprevio,
		fecha3traspasoprevio AS fecha3traspasoprevio,
		fecha4traspasoprevio AS fecha4traspasoprevio,
		fecha5traspasoprevio AS fecha5traspasoprevio
		FROM fnobtenerrespuestaprevalidador($iFolio)");
		return $query->result();
	}

	function obtenerdialogograbadormovil($iTipodialogo,$iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT dialogo FROM fnobtenerdialogosvideomovil($iTipodialogo,$iFolio);");
		return $query->row();
	}

	function almacenanumerotarjetamovil($cNumeroTarjeta,$ipCliente)
	{
		$query = $this->Pgaforeglobal->query("INSERT INTO celafiliacionprosa (ipmodulo,tnumero) VALUES ('$ipCliente','$cNumeroTarjeta');");
		return $query;
	}

	function obtenerMensajeErrorPrevalidador($cCodigo)
	{
		$query = $this->Pgaforeglobal->query("SELECT mensaje FROM caterroresprevalidador WHERE codigo='$cCodigo';");
		return $query->result();
	}

	function ejecutaServicioBCpl($cNumTarjetaBCpl,$cTemplate)
	{
		$dirPadre = dirname(__FILE__);
		define('path_wsdl','/servicios/wsAforeAltaUnica.wsdl');
		$wsAforeAltaUnica = $dirPadre.path_wsdl;

		$query = $this->Pgaforeglobal->query("SELECT trim(urlws) AS urlws FROM fnobtener_url_webservice(101::smallint);");
		$urlws = $query->row();
		$urlws = $urlws->urlws;

		$opcionWs['location'] = $urlws;
		$opcionWs['cache_wsdl'] = WSDL_CACHE_NONE;
		$opcionWs['trace'] = true;
		$opcionWs['exceptions'] = true;

		$inParam = array(
			'NumeroTarjeta' => $cNumTarjetaBCpl,
			'Template' => $cTemplate
		);

		$clienteWs = new SoapClient($wsAforeAltaUnica,$opcionWs);
		$resWs = $clienteWs->__call("ConsultarCliente",array( $inParam ) );

		return $resWs;
	}

	function obtenerBandMensajePromotor()
	{
		$query = $this->Pgaforeglobal->query("SELECT fnconsultarpafhardcode AS valor FROM fnconsultarpafhardcode(991105010);");
		return $query->row();
	}

	function actualizarBandMensajePromotor($foliosolicitud,$valor)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnactualizardecisiontrabajadorporantiguedad AS respuesta FROM fnactualizardecisiontrabajadorporantiguedad($foliosolicitud,$valor);");
		return $query->row();
	}

	function ConsultacatalogoSolicitante(){

		$query = $this->Pgaforeglobal->query("SELECT iidtipo, cdescripcion FROM fnobtenercattiposolicitante() WHERE iidtipo <> 0 ORDER BY iidtipo ASC ;");
		return $query->result();
	}

	function obtenerIpCluster($iPmodulo){

		$query = $this->Pgaforeglobal->query("SELECT trim(ipofflinelnx) as ipoffline FROM fnobteneripcluster('$iPmodulo');");
		return $query->result();
	}

	function obtenerClaveOperacion($iPmodulo){

		$query = $this->Pgaforeglobal->query("SELECT obtenerclaveoperacionafiliacion as claveop FROM obtenerclaveoperacionafiliacion();");
		return $query->result();
	}

	function ConsultarDialogoVideo($TipoDialogo){

		$query = $this->Pgaforeglobal->query("SELECT tiempominimo, tiempomaximo, formato, calidad, dialogo FROM fnobtenerdialogosvideo($TipoDialogo);");
		return $query->result();
	}

	function ObtenerUrlsVideo($iPmodulo){

		$query = $this->Pgaforeglobal->query("SELECT trim(urlguardado) AS urlguardado, trim(urlgrabado) AS urlgrabado FROM fnobtenerurlsgrabadovideo('$iPmodulo');");
		return $query->result();
	}

	function fnobtenerDatosCapturaVideo($iFolioCons,$iEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT tipoconstancia, ciudad, nombrepromotor, apellidopaternoprom, apellidomaternoprom, claveconsar, nombretrabajador, apellidopaternotrab, apellidomaternotrab, curptrabajador, telefono, aforeactual, nombresolicitante, apellidopaternosol, apellidomaternosol,curpsolicitante, dia, mes, anio FROM fndatosgrabacionvideo($iFolioCons, $iEmpleado);");
		return $query->result();
	}

	function obtenerEstatusVideo($iFolioCons){

		$query = $this->Pgaforeglobal->query("SELECT fnobtenerestatusvideo AS estatus from fnobtenerestatusvideo($iFolioCons);");
		return $query->result();
	}

	function validarenrolamientoConstancia($sCurp){

		$query = $this->Pgaforeglobal->query("SELECT fnconsultarenrolconstancia AS estado from fnconsultarenrolconstancia('$sCurp');");
		return $query->result();
	}

	function guardarVideoAfiliacion($iFolioCons,$nombreVideo,$iPmodulo){

		$query = $this->Pgaforeglobal->query("SELECT fnguardarvideoafiliacion as respuesta FROM fnguardarvideoafiliacion($iFolioCons,'$nombreVideo','$iPmodulo');");
		return $query->result();
	}

	function obtenerestatusfinado($iFolioCons){

		$query = $this->Pgaforeglobal->query("SELECT fnobtenerestatustrabajadorfinado AS estatus FROM fnobtenerestatustrabajadorfinado('$iFolioCons');");
		return $query->result();
	}

	function actualizarestatusvideo($iFolioCons,$estatusvideo){

		$query = $this->Pgaforeglobal->query("SELECT fnactualizarestatusvideoafiliacion01 as respuesta FROM fnactualizarestatusvideoafiliacion01($iFolioCons, $estatusvideo);");
		return $query->result();
	}

	function verificarEnrolamiento($iFolioSolicitud){

		$query = $this->Pgaforeglobal->query("select fnverificarenrolamientotrabajador as respuesta from fnverificarenrolamientotrabajador($iFolioSolicitud);");
		return $query->result();
	}

	function actualizarEstatusEnrolamiento($iFolioEnrolamiento,$iEstatusEnrolamiento,$sCurp){

		$query = $this->Pgaforeglobal->query("select fnactestatusenrolatrabajador as respuesta from fnactestatusenrolatrabajador($iFolioEnrolamiento, '$iEstatusEnrolamiento','$sCurp');");
		return $query->result();
	}

	function guardarcontrolenviofolio($iFolioSolicitud,$IdentificadorEnvio){

		$query = $this->Pgaforeglobal->query("select fnguardarcontrolenviofolio as respuesta from fnguardarcontrolenviofolio($iFolioSolicitud, $IdentificadorEnvio);");
		return $query->result();
	}

	function fnactualizadatossolicitanterenapo($nombreSolicitante,$apellidoPatSolicitante,$apellidoMatSolicitante,$iFolioCons){

		$query = $this->Pgaforeglobal->query("select fnactualizadatossolicitanterenapo as respuesta from fnactualizadatossolicitanterenapo('$nombreSolicitante','$apellidoPatSolicitante','$apellidoMatSolicitante', $iFolioCons);");
		return $query->result();
	}

	function ActualizaestatusSolconstancia($iFolioCons,$status){

		$query = $this->Pgaforeglobal->query("select actualizarestatusprocesosolconstancia as respuesta from actualizarestatusprocesosolconstancia($iFolioCons,$status);");
		return $query->result();
	}

	function validarRespuestaFolio($iFolioCons){

		$query = $this->Pgaforeglobal->query("select respuesta, mensaje from validarRespuestaFolio($iFolioCons);");
		return $query->result();
	}

	function generarFolioEnrolamiento($iFolioCons,$iEmpleado,$sCurp,$iTipoConstancia){

		$query = $this->Pgaforeglobal->query("select fnguardarenrolamientotrabajador as ires from fnguardarenrolamientotrabajador($iEmpleado,$iTipoConstancia,'$sCurp',$iFolioCons);");
		return $query->result();
	}

	function validarExpediente($sCurp,$sIpRemoto){

		$query = $this->Pgaforeglobal->query("SELECT error, mensaje, redireccionar FROM fnValidarExpedienteIdentificacionIntegrado('$sCurp','$sIpRemoto');");
		return $query->result();
	}

	function grabarExcepcion($excepcion,$iFolioCons,$iEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT fn_grabar_excepcionVideo02 AS irespuesta FROM fn_grabar_excepcionVideo02($excepcion,$iFolioCons,$iEmpleado);");
		return $query->result();
	}

	function indexarFormatoEnrolamiento($iFolio,$documento,$ipOffline)
	{
		$query0 = $this->Pgaforeglobal->query("UPDATE stmppublicarimagen SET idimagen='$documento', ipofflinelnx='$ipOffline' WHERE folio=$iFolio AND (idimagen='FARE' OR idimagen='FAHD')");

		$query00 = $this->Pgaforeglobal->query("UPDATE stmppublicarimagen SET ipofflinelnx='$ipOffline' WHERE folio=$iFolio AND idimagen='$documento'");

		$query1 = $this->Pgaforeglobal->query("SELECT archivo FROM colimagenesindexadasdetalle where folio='$iFolio' AND archivo LIKE '%FARE%'");
		$result = $query1->row();
		$result = $result->archivo;

		$archivo = substr(trim($result), 0, -8);
		$archivo = $archivo.'FAHD.tif';

		$query = $this->Pgaforeglobal->query("UPDATE colimagenesindexadasdetalle SET archivo = '$archivo' WHERE folio='$iFolio' AND archivo LIKE '%FARE%'");

		return $query;
	}

	function fnExisteCurpRenapo($iFolioAfiSer,$var){
		$query = $this->Pgaforeglobal->query("SELECT count(*) FROM fnconsultardocumentoid($iFolioAfiSer,'$var');");
		return $query->result();
	}

	function fnconsultardocumento($iFolioAfiSer,$cNomenclatura,$iOpcion){

		$query = $this->Pgaforeglobal->query("SELECT * FROM fnconsultardocumento($iFolioAfiSer,'$cNomenclatura',$iOpcion);");
		return $query->result();
	}

	function CargarCatalogoDoctosFirmaDigital(){

		$query = $this->Pgaforeglobal->query("SELECT nomeclaturaDocto FROM catDoctosFirmaDigital;");
		return $query->result();
	}

	function ConsultarFirmaElectronica($iTabla,$iFolioAfiSer){

		if($iTabla==1 || $iTabla==2){
			$query = $this->Pgaforeglobal->query("SELECT firmarendimientoneto AS firma, publicorendimientoneto AS publico FROM firmadigitalafiliacion where folio=$iFolioAfiSer;");
		}
		if($iTabla==3){
			$query = $this->Pgaforeglobal->query("SELECT firmasolconstancia AS firma, publicosolconstancia AS publico FROM firmadigitalafiliacion where folio=$iFolioAfiSer;");
		}
		return $query->result();
	}

	function obtenertipocomprobante($iFolio){

		$query = $this->Pgaforeglobal->query("SELECT tipocomprobante FROM solconstancia WHERE folio=$iFolio;");
		return $query->row();
	}

	function obtenerLigaIne($empleado, $curp, $pasoRenapo, $ipModulo)
	{
		$query = $this->Pgaforeglobal->query("select obtenerLigaIne as liga from obtenerLigaIne($empleado,'$curp',$pasoRenapo,'$ipModulo');");
		return $query->result();
	}

	function obtenerLigaIne01($empleado, $curp, $pasoRenapo, $ipModulo, $iAuten)
	{
		$query = $this->Pgaforeglobal->query("select obtenerLigaIne01 as liga from obtenerLigaIne01($empleado,'$curp',$pasoRenapo,'$ipModulo', $iAuten);");
		return $query->result();
	}

	function actualizarFolioIne($folioCons, $curp, $empleado, $foliosoline)
	{
		$query = $this->Pgaforeglobal->query("select actualizarFolioIne as actualizo from actualizarFolioIne($folioCons, '$curp', $empleado, $foliosoline);");
		return $query->result();
	}

	function obtenerEstatusIne($curp)
	{
		$query = $this->Pgaforeglobal->query("select status, telefonoine, companiatel, folioine from obtenerEstatusIne('$curp');");
		return $query->result();
	}

	function indexarcodigopromotormovil($folio,$sTipo)
	{
		$query1 = $this->Pgaforeglobal->query("SELECT empcapturo FROM ColSolicitudes WHERE folio=$folio");
		$result = $query1->row();
		$codProm = $result->empcapturo;

		$query = $this->Pgaforeglobal->query("SELECT * FROM fnIndexarNombreDigMovil(1, '$folio', '$sTipo', '$codProm', 0)");

		return $query;
	}

	function subirvideo($folio, $nombreimagen, $base64, $fraccionado)
	{
		$query = $this->PgVideos->query("SELECT fnsubirvideosafiliacion FROM fnsubirvideosafiliacion('$folio','$nombreimagen','$base64',$fraccionado);");
		return $query->row();
	}

	function actualizaIDRE($iFolio)
	{
		$query1 = $this->Pgaforeglobal->query("SELECT archivo FROM colimagenesindexadasdetalle where folio='$iFolio' AND archivo LIKE '%IDRE%'");
		$result = $query1->row();
		$result = $result->archivo;

		$archivo = substr(trim($result), 0, -8);
		$archivo = $archivo.'ID00.tif';

		$query = $this->Pgaforeglobal->query("UPDATE colimagenesindexadasdetalle SET archivo = '$archivo' WHERE folio='$iFolio' AND archivo LIKE '%IDRE%'");

		return $query;
	}

	function almacenaDispositivoAfiliacion($folio,$sistemaOperativo,$tipoConexion)
	{
		$query = $this->Pgaforeglobal->query("INSERT INTO dispositivoAfiliacion (folio,sistemaOperativo,tipoConexion) VALUES ($folio,'$sistemaOperativo',$tipoConexion);");
		return $query;
	}

	function obtenerServidorIntermedio()
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(fnRegresaRegion(1)) AS ipintermedio;");
		return $query->row();
	}

	function obtenerImagenes($folio)
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(ubicacion) AS ubicacion, trim(documento) AS documento, trim(tipodocumento) AS  tipodocumento FROM tbdocumentosafiliacion WHERE folioconstancia=$folio;");
		return $query->result();
	}

	function obtenerDatosFolio($folio)
	{
		$query = $this->Pgaforeglobal->query("SELECT tipoconstancia, curp, folioenrolamiento FROM solconstancia WHERE folio=$folio;");
		return $query->row();
	}

	function actualizaImagenesEnBD($folio,$sNome,$newname,$folioenrolamiento)
	{
		$update1 = $this->Pgaforeglobal->query("UPDATE stmppublicarimagen SET estatuspublica=0 WHERE folio=$folio AND idimagen='$sNome';");

		$query1 = $this->Pgaforeglobal->query("SELECT trim(documentos) AS nomenclatura FROM colimagenesindexadas WHERE folio='$folio';");
		$nomenclatura = $query1->row();
		$nomenclatura = $nomenclatura->nomenclatura;

		if($nomenclatura){

			$documentos = $nomenclatura."|".$sNome;

			$update2 = $this->Pgaforeglobal->query("UPDATE colimagenesindexadas SET documentos='$documentos' WHERE folio='$folio';");

		}else{
			$query2 = $this->Pgaforeglobal->query("INSERT INTO colimagenesindexadas (folio,documentos) VALUES ('$folio','$sNome');");
		}

		$ip = $this->Pgaforeglobal->query("SELECT trim(fnobtenerofflineempleado) AS urlws FROM fnobtenerofflineempleado($folioenrolamiento);");
        $ip = $ip->row();
		$ip = $ip->urlws;

		$query3 = $this->Pgaforeglobal->query("INSERT INTO colimagenesindexadasdetalle (ip,folio,archivo,estatus) VALUES ('$ip','$folio','$newname',1);");

		//$update3 = $this->Pgaforeglobal->query("UPDATE stmppublicarimagen SET ipofflinelnx='$ip', estatuspublica=0 WHERE folio=$folio AND idimagen='FAHD';");

		$query4 = $this->Pgaforeglobal->query("SELECT fncambiarstatusdigitalizacionok($folio,1);");

	    return $ip;
	}

	function ligaMenuAfore()
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(concepto) AS respuesta FROM pafhardcode WHERE tipo1=171 AND tipo2=24;");
		return $query->row();
	}

	function obtenerEstatusExpedienteEnrolamiento($iFolioServicio)
	{
		$sql = "Select trim(estatusexpiden) As estatusexpiden,trim(estatusenrol) As estatusenrol,trim(indicadordecimo) As indicadordecimo,trim(curpduplicado) As curpduplicado From fnobtenerrespuestaconsultaexpediente($iFolioServicio)";
		log_message('error','Consulta-> '.$sql);
		$query = $this->Pgaforeglobal->query($sql);
		return $query->result();
	}

	function guardarHuellasIndices($sCurpTrabajador,$iEmpleado,$ipModulo,$arrIndiceIzquierdo,$arrIndiceDerecho){

		$arrIndiceIzquierdo = array_values($arrIndiceIzquierdo);
		$arrIndiceDerecho = array_values($arrIndiceDerecho);

		for ($i=0; $i < count($arrIndiceIzquierdo); $i++) {
			$arrIndiceIzquierdo[$i] = (int)$arrIndiceIzquierdo[$i];
		}

		for ($i=0; $i < count($arrIndiceIzquierdo); $i++) {
			$arrIndiceDerecho[$i] = (int)$arrIndiceDerecho[$i];
		}

		$arrIndiceIzquierdo = json_encode($arrIndiceIzquierdo);
		$arrIndiceDerecho = json_encode($arrIndiceDerecho);

		$sql = "Select fngrabarindicesafotemplatecliente($iEmpleado,'$sCurpTrabajador','$ipModulo',ARRAY".$arrIndiceIzquierdo.",ARRAY".$arrIndiceDerecho.") as iRespuesta";
		log_message('error','Consulta-> '.$sql);
		$query = $this->Pgaforeglobal->query($sql);
		return $query->result();
	}

	function limpiarHuellasServicio($arrDatosServicio)
	{
		$sNombreArchivoIzq = $arrDatosServicio['sNombreArchivoIzq'];
		$sNombreArchivoDer = $arrDatosServicio['sNombreArchivoDer'];

		$sRutaArchivoIzquierdo= "../../entrada/huellas/enrolamiento/".$sNombreArchivoIzq;
		$sRutaArchivoDerecho= "../../entrada/huellas/enrolamiento/".$sNombreArchivoDer;
		$sTexto= "";
		$borrado = 0;

		if(file_exists($sRutaArchivoIzquierdo))
		{
				$sFile = fopen($sRutaArchivoIzquierdo, "r");
				fclose($sFile);

				unlink($sRutaArchivoIzquierdo);
				$borrado++;

		}
		if(file_exists($sRutaArchivoDerecho))
		{
				$sFile = fopen($sRutaArchivoDerecho, "r");
				fclose($sFile);

				unlink($sRutaArchivoDerecho);
				$borrado++;
		}
		return $borrado;
	}

	function verificaEjecucionServicio()
	{
		$ipServer = $this->getRealIP();
		//$query = $this->Pgaforeglobal->query("Select tipo6 as Estatus , tipo5 as enrolamientopermanente From pafhardcode where tipo1= 238 And concepto = '99100407'");
		$query = $this->Pgaforeglobal->query("SELECT estatus, enrolamientopermanente FROM fnobtencionbanderasei('$ipServer');");
		return $query->result();
	}

	function elminarinformacionServicio()
	{
		$query = $this->Pgaforeglobal->query("select fneliminaextraccionei as respuesta from fneliminaextraccionei()");
		return $query->result();
	}

	function actualizarfoliobancoppel($folio,$curp,$empleado,$folioBcpl){
		$cQuery     = "SELECT fnactualizarfoliobancoppel AS respuesta FROM fnactualizarfoliobancoppel($folio,'$curp',$empleado,$folioBcpl);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function tieneautenticacionbancoppel($curp,$empleado,$ipmodulo){
		$cQuery     = "SELECT fntieneautenticacionbancoppel AS respuesta FROM fntieneautenticacionbancoppel('$curp',$empleado,'$ipmodulo');";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function ConsultaSolconstancia($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT estatus,diagnostico,curp FROM fnconsultasolconstancia($iFolio);");
		return $query->result();
	}

	function ConsultaRechazo($sCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT descripcion FROM fn_consulta_rechazo('$sCurp');");
		return $query->result();
	}

	function ActualizaestatusSolconstanciaSer($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnactualizaestatussolconstancia AS respuesta FROM fnactualizaestatussolconstancia($iFolio,'');");
		return $query->result();
	}

	function obtenerClaveOperacionImp($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenerdoctosdigitalizacion FROM fnobtenerdoctosdigitalizacion ($iFolio,3);");
		return $query->result();
	}

	function obtenerRespuestaRechazo($iFolioSerAfore)
	{
		$query = $this->bustramites->query("SELECT fnobtenervalorxml::character(3) AS rechazo FROM fnobtenervalorxml('diagnosticoProcesar', 'respuestaxml', 'bitacoracliente', 'folioservicioafore=$iFolioSerAfore');");
		return $query->result();
	}

	function consultarHuellas($sCurpTrab,$iFolio,$iIDServicio,$cTipoOperacion)
	{
		$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellasdoctosafiliacion('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."');");
		return $query->result();
	}

	function consultaCatalogoAfore()
	{
		$query = $this->Pgaforeglobal->query("SELECT claveafore,nombreafore FROM fnobtenerafores();");
		return $query->result();
	}

	function validaSolicitud($sCurp, $sContrasenia)
	{
		$query = $this->Pgaforeglobal->query("select respuesta,descripcion,folio,tipoconstancia,publicoimpconstancia,confirma from fnvalidaexistenciasolicitudconstancia('".$sCurp."','".$sContrasenia."') as respuesta;");
		return $query->result();
	}

	function obtenerDatosConstancia($sCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT fechavigenciacontrasenia, fechageneraconstancia, fechavigenciaconstancia,folioconstancia, nombreaforeactual, claveafore, nss, curp, TRIM(nombres) AS nombres,TRIM(appaterno) AS appaterno,TRIM(apmaterno) AS apmaterno,tipoconstancia, lugarsolicitud,fechaemision,rendimiento,folio FROM fnobtenerdatosconstancia_manual('".$sCurp."');");
		return $query->result();
	}

	function validarDiagnosticoConstancia($sCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT respuesta,diagnostico, descripciondiagnostico FROM fnvalidadiagnosticoconstancia('".$sCurp."');");
		return $query->result();
	}

	function grabarInformacionConstanciaImpresaESar($sCurp,$sContrasenia,$fechaEmision,$aforeActual,$fechaVigenciaConstancia,$folioConstancia)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnactualizardatosconstanciaesar('$sContrasenia','$fechaEmision','$sCurp','$aforeActual','$folioConstancia','$fechaVigenciaConstancia');");
		return $query->result();	
	}

	function validarDigitalizacion($folioSolicitud)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnexistedocumentopublicado('CTR0','$folioSolicitud');");
		return $query->result();	
	}

	function obternerDiasHbiles($fechaEmision)
	{
		$query = $this->Pgaforeglobal->query("SELECT fn_habil_siguiente('$fechaEmision'::date, 9) as fechamaxima;");
		return $query->result();	
	}

	function obternerFechaActual()
	{
		$query = $this->Pgaforeglobal->query("SELECT current_date AS fechaActual;");
		return $query->result();	
	}

	function revisarAfore($aforeRevisa)
	{
		$query = $this->Pgaforeglobal->query("SELECT afore FROM pafAfores WHERE CASE WHEN idfusionafore <> '' THEN idfusionafore ELSE afore END = '$aforeRevisa' LIMIT 1;");
		return $query->result();	
	}

	function obtenerFlagAudio($iFolioSolicitud, $iOpcion)
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(fnobtenernombrecapturavoz) AS respuesta FROM fnobtenernombrecapturavoz($iOpcion, $iFolioSolicitud);");
		return $query->result();	
	}

	function guardarExcepcionAudio($iFolioEnrolamiento, $sCodigo, $iNumDedo)
	{
		$query = $this->Pgaforeglobal->query("SELECT fngrabarenrolexcepcionestrabajador AS respuesta FROM fngrabarenrolexcepcionestrabajador($iFolioEnrolamiento, '$sCodigo', $iNumDedo);");
		return $query->result();	
	}

	function ejecutaSentencia($iOpcionFuncion,$iParametro1,$iParametro2,$iParametro3)
	{
		if($iOpcionFuncion == 1)
		{
		    $query = $this->Pgaforeglobal->query("SELECT fnImpresionConstanciasEjecutaSentencia(".$iParametro1.",".$iParametro2.",".$iParametro3.");");
			return $query->result();	
			
		}else if($iOpcionFuncion == 2)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnobtenerdoctosdigitalizacion (".$iParametro1.",".$iParametro2.");");
			return $query->result();
			
		}else if($iOpcionFuncion == 3)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnactualizarimagenindexada ('".$iParametro1."','".$iParametro2."','".$iParametro3."');");
		    return $query->result();
		}
	}

	function obtenerEstatusAfiliacion($curp)
	{
		$cQuery     = "SELECT fnobtenerestatusafiliacion as iestatusproceso FROM fnobtenerestatusafiliacion('$curp')";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function verificaExpedientePermanente($iFolioSerAfore)
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(estatusexpiden) AS estatusexpiden, trim(estatusenrol) AS estatusenrol, trim(indicadordecimo) AS indicadordecimo, trim(curpduplicado) AS curpduplicado FROM fnobtenerrespuestaconsultaexpediente($iFolioSerAfore);");
		return $query->result();	
	}
	function guardarNumPagDigitalizador($paginas,$ifolio)
	{
		$query = $this->Pgaforeglobal->query("UPDATE solconstancia set paginasverificaprobatorio = '$paginas' where folio = '$ifolio'");
		return $query;	
	}

	function verificarTelefono($numero,$numeroLada)
	{
		$cQuery     = "SELECT * From fntelefonovalido('$numero','$numeroLada') AS respuesta;";
		$query      = $this->Pgaforeglobal->query($cQuery);
		return $query->result();
	}
	function obtenerErrorMensajeFct($cFolioConoTrasp)
	{
		$cQuery     = "SELECT mensaje, codigo FROM fnobtenerMensajeError('$cFolioConoTrasp');";
		$query      = $this->Pgaforeglobal->query($cQuery);
		return $query->result();
	}
	function fnGuardarFolioColsolicitud($cfolioTrasCono,$cCurpBucar)
	{
		$cQuery     = "SELECT * FROM	fnGuardarFolioColsolicitudes('$cfolioTrasCono','$cCurpBucar') as respuesta;";
		$query      = $this->Pgaforeglobal->query($cQuery);
		return $query->result();
	}
	function insertarTipoEntregaCorreoTelefono($tipoEntrega,$cCurptrabajado,$tipoCorreoTelefono,$ipservidor,$numPromotor)
	{
		$cQuery     = "SELECT * FROM fnguardartipoentregacolmedioentregatraspasonuevo($tipoEntrega,'$cCurptrabajado','$tipoCorreoTelefono',$numPromotor,'$ipservidor') as respuesta;";
		$query      = $this->Pgaforeglobal->query($cQuery);
		return $query->result();
	}
	function obtencionFolioSulicitud($sCurp)
	{
		$cQuery     = "SELECT * FROM	fnObtenerFolioSolicitud('$sCurp') as respuesta;";
		$query      = $this->Pgaforeglobal->query($cQuery);
		return $query->result();
	}

	function obtenerRecuperacionFolioConstancia($sCurp)
	{
		$cQuery     = "SELECT fnobtenerRecuperacionFolioConstancia AS folio FROM fnobtenerRecuperacionFolioConstancia('$sCurp')";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function validarcodigoautenticacion($iFolioCons,$codigoIne)
	{
		$cQuery     = "select fnvalidarcodigoautenticacion as irespuesta from fnvalidarcodigoautenticacion($iFolioCons,'$codigoIne')";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function actualizarSolEstatusExpiden($folioCons,$curp)
	{
		$Sql = "SELECT fnactualizaexpedienteenrolpermanente AS actualizaestatus FROM fnactualizaexpedienteenrolpermanente($folioCons,'$curp');";
		log_message('error','Consulta actualizarSolEstatusExpiden: -> '.$Sql);
		$query = $this->Pgaforeglobal->query($Sql);
		return $query->result();

	}

	function obtenerIPServidor($sIpRemoto){

		$query = $this->Pgaforeglobal->query("SELECT trim(ipofflinelnx) AS ipofflinelnx FROM fnObtenerIpCluster('$sIpRemoto');");
		return $query->result();

	}
	function getRealIP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
			return $_SERVER['HTTP_CLIENT_IP'];
		   
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
	   
		return $_SERVER['REMOTE_ADDR'];
	}

	function insertaDatosAcuse($nombre,$curp,$folio, $opcionacuse)
	{
		$query = $this->Pgaforeglobal->query("SELECT fninsertdatosacuseafitrasrecer AS retorno FROM fninsertdatosacuseafitrasrecer('$nombre','$curp','$folio','$opcionacuse');");
		return $query->result();	
	}

	function envioexp($folioafiliacion,$archivo,$imdata)
	{
		$query = $this->Pgaforeglobal->query("SELECT * FROM fninsertadatosimgcartaderechos('$folioafiliacion','$archivo','$imdata',1,1,0,0);");
		return $query->result();
	}

	function ActualizarExpedientePermante($folioCons,$curp)
	{
		$cQuery     = "SELECT fnactualizaexpedienteenrolpermanente AS actualizaestatus FROM fnactualizaexpedienteenrolpermanente($folioCons,'$curp');";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION ActualizarExpedientePermante-> $cQuery");
		return $query->result();
	}

	function fntieneautenticacionine($sCurp,$iEmpleado)
	{
		$query = $this->Pgaforeglobal->query("SELECT fntieneautenticacionine as respuesta FROM fntieneautenticacionine('$sCurp',$iEmpleado);");
		return $query->result();
	}

	/*************************Folio 539**************************/
	function obtenerLlaveDatosBancoppel($curp){
		$query = $this->Pgaforeglobal->query("SELECT curp,nombres,apellidopaterno,apellidomaterno,fechanacimiento,genero,identidadfederativa,entidadfederativa,codigoerrorrenapo,codigoerror FROM fnobtenerllavedatosrenapobancoppel('$curp')");
		return $query->result();
	}

	function obtenerEntidadNacimiento(){
		$query = $this->Pgaforeglobal->query("SELECT trim(identidadnacimiento) AS identidadnacimiento, trim(descripcion) AS descripcion FROM fnconsultarcatalogoentidadnacimiento()");
		return $query->result();
	}

	function obtenerurlwsbancoppel(){
		$query = $this->Pgaforeglobal->query("SELECT url FROM urlserviciobancoppel WHERE keyx=1");
		return $query->result();
	}

	function guardarBitacoraRespuestaBancoppel($folioafiliacion,$fechaconsulta,$horaconsulta,$iTienda,$empleado,$idanverso,$idreverso,$curp,$appaterno,$apmaterno,$nombres,$rfc,$fechanac,$entidad,$genero,$nacionalidad,$edoCivil,$numClienteCoppel,$numClienteBancoppel,$nvlEstudio,$profesion,$giroNegocio,$telefono1,$telefono2,$correo,$calle,$numExterior,$numInterior,$codigoPostal,$colonia,$ciudad,$delegacion,$estado,$pais,$calleCobranza,$numExteriorCobranza,$numInteriorCobranza,$codigoPostalCobranza,$coloniaCobranza,$ciudadCobranza,$delegacionCobranza,$estadoCobranza,$paisCobranza,$calleLaboral,$numExteriorLaboral,$numInteriorLaboral,$codigoPostalLaboral,$coloniaLaboral,$ciudadLaboral,$delegacionLaboral,$estadoLaboral,$paisLaboral,$apPaternoRef1,$apMaternoRef1,$nombresRef1,$apPaternoRef2,$apMaternoRef2,$nombresRef2,$ctaTradicionalVigente,$ctaNomina,$opcionbcpl){
		$query = $this->Pgaforeglobal->query("SELECT * FROM fnllenadobitacoraafiliacionbancoppel($folioafiliacion,'$fechaconsulta','$horaconsulta',$iTienda,$empleado,'$idanverso','$idreverso','$curp','$appaterno','$apmaterno','$nombres','$rfc','$fechanac',$entidad,'$genero',$nacionalidad,'$edoCivil',$numClienteCoppel,'$numClienteBancoppel',$nvlEstudio,$profesion,$giroNegocio,'$telefono1','$telefono2','$correo','$calle','$numExterior','$numInterior','$codigoPostal',$colonia,$ciudad,$delegacion,$estado,'$pais','$calleCobranza','$numExteriorCobranza','$numInteriorCobranza','$codigoPostalCobranza',$coloniaCobranza,$ciudadCobranza,$delegacionCobranza,$estadoCobranza,'$paisCobranza','$calleLaboral','$numExteriorLaboral','$numInteriorLaboral','$codigoPostalLaboral',$coloniaLaboral,$ciudadLaboral,$delegacionLaboral,$estadoLaboral,'$paisLaboral','$apPaternoRef1','$apMaternoRef1','$nombresRef1','$apPaternoRef2','$apMaternoRef2','$nombresRef2','$ctaTradicionalVigente','$ctaNomina',$opcionbcpl)");
		return $query->result();
	}

	function tieneDatosBancoppel($folioafiliacion, $opcionbcpl){
		$query = $this->Pgaforeglobal->query("SELECT * FROM fnllenadobitacoraafiliacionbancoppel($folioafiliacion,'','',0,0,'','','','','','','','',0,'',0,'',0,'',0,0,0,'','','','','','','',0,0,0,0,'','','','','',0,0,0,0,'','','','','',0,0,0,0,'','','','','','','','','',$opcionbcpl)");
		return $query->result();
	}

	function obtenerParametrosWSBancoppel(){
		$query = $this->Pgaforeglobal->query("SELECT ip_origen,usuario,pass,codigotraspagente,agentecd FROM cat_parametrosbancoppel WHERE identificador = 1");
		return $query->result();
	}

	function validaEstatusVideo($folioafiliacion)
	{
		$cQuery     = "SELECT fnvalidaestatusvideo AS resultado FROM fnvalidaestatusvideo($folioafiliacion);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "validaEstatusVideo-> $cQuery");
		return $query->result();
	}
	function validarFoliosPendientesRenapo(){

		$query = $this->Pgaforeglobal->query("SELECT fechaalta, folio, horaconsulta, curp, nombre, apellidopat, apellidomat, nss, empleado FROM fun_validarfoliospendientesrenapo();");
		return $query->result();
	}
	function actualizaStatusProcesoSol($iOpcion, $iFolio, $sCurp){

		$query = $this->Pgaforeglobal->query("SELECT fun_actualizastatusprocesosol( $iOpcion, $iFolio, '$sCurp');");
		return $query->result();
	}
	function verificarBanderaRenapo(){

		$query = $this->Pgaforeglobal->query("SELECT fun_verificarbanderarenapo AS iBanderaRenapo FROM fun_verificarbanderarenapo();");
		return $query->result();
	}
	
	function consultarDiasSolicitudRenapo(){

		$query = $this->Pgaforeglobal->query("SELECT fun_consultardiassolicitudrenapo AS iDias FROM fun_consultardiassolicitudrenapo();");
		return $query->result();
	}
	
	function verificajfuse($iFolioAfore){
		$query = $this->Pgaforeglobal->query("select fnobtenerstatusjfuse from fnobtenerstatusjfuse($iFolioAfore)");
		return $query->result();
	}
	
	function consultarHorasSolicitudRenapo(){

		$query = $this->Pgaforeglobal->query("SELECT fun_consultarhorassolicitudrenapo AS iHoras FROM fun_consultarhorassolicitudrenapo();");
		return $query->result();
	}
	
		function consultartoken($token,$direccionip){

		$query = $this->Pgaforeglobal->query("SELECT stoken FROM fnobtenertokenbancoppel(1,'$token','$direccionip');");
		return $query->result();
	}
	
	function insertartoken($token,$direccionip){

		$query = $this->Pgaforeglobal->query("SELECT stoken FROM fnobtenertokenbancoppel(2,'$token','$direccionip');");
		return $query->result();
	}
	
	//FOLIO 1498
	function fnGestorBandera($iflag)
	{
			$query = $this->Pgaforeglobal->query("Select fnvalidarbanderagestor as respuesta from fnvalidarbanderagestor($iflag,2);");
			return $query->result();
	}
	
	function Autenticadactilar($iFolioGestor,$sCurp,$iopc){	
	
		$query = $this->Pgaforeglobal->query(" SELECT selloverificacionbiometricavoluntram, mensaje, resultadoverificacionvoluntram, banderadiag FROM fnobtenerrespuestagestordactilar($iFolioGestor,'$sCurp',$iopc)");
		return $query->result();
	}
	//FOLIO 1498
}
