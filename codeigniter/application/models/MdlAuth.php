<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Consulta para el inicio y cierre de sesion 
 */
class MdlAuth extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
	}
	/**
	 * Permite guardar el usuario que inicio sesion 
	 *
	 * @param [string] $codigoEmpleado
	 * @param [string] $token
	 * @return void
	 */
	public function guardarRegistro($codigoEmpleado,$token, $tienda)
	{
		$query = $this->Pgaforeglobal->query("INSERT INTO colsessiontoken (codigoempleado, token, tienda) VALUES ( '$codigoEmpleado', '$token', $tienda);");
		return $query;
    }
	/**
	 * verfica si el usuario esta logeado
	 * se busca por :
	 * @param [string] $codigoEmpleado
	 * @param [string] $token
	 * @return void
	 */
	public function verificarSesionCodigoyToken($codigoEmpleado,$token)
	{
		$query = $this->Pgaforeglobal->query("Select * from colsessiontoken where codigoEmpleado = '$codigoEmpleado' and token = '$token' ");
		return $query->result();
	}
	/**
	 * verfica si el usuario esta logeado
	 * se busca por :
	 * @param [string] $codigoEmpleado
	 * @return void
	 */
	public function verificarSesionCodigo($codigoEmpleado)
	{
		$query = $this->Pgaforeglobal->query("Select * from colsessiontoken where codigoEmpleado = '$codigoEmpleado' ");
		return $query->result();
	}
	/**
	* verfica si el usuario esta logeado
	* se busca por :
	* @param [string] $token
	* @return void
	*/
	public function verificarSesionToken($token)
	{
		$query = $this->Pgaforeglobal->query("Select * from colsessiontoken where token = '$token' ");
		return $query->result();
	}
	/**
	* elimina la sesion de usuario logueado
	* se busca por :
	* @param [string] $token
	* @return void
	*/
	public function eliminarToken($codigoEmpleado)
	{
		$query = $this->Pgaforeglobal->query("Delete from colsessiontoken where codigoEmpleado = '$codigoEmpleado' ");
		return $query;
	}

	public function eliminarTokenPorToken($token)
	{
		$query = $this->Pgaforeglobal->query("Delete from colsessiontoken where token = '$token' ");
		return $query;
	}

	public function obtenerIpLocal($token)
	{
		$query = $this->Pgaforeglobal->query("Select trim(iplocal) as iplocal, tienda from colsessiontoken where token = '$token' ");
		return $query->result();
	}

}
