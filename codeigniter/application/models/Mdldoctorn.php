<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdldoctorn extends CI_Model{

    public function __construct(){
        parent::__construct();
        //load database library
        $this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);          
    }

    function folioColSolicitudes ($iFolio){
        $cQuery     = "SELECT folio FROM colsolicitudes WHERE folio = $iFolio";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function empNominaColPromotorTitular ($iFolio){
        $cQuery     = "SELECT empnomina FROM ColPromotorTitular WHERE folio = $iFolio";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function empCapturoColSolicitudes ($iFolio){
        $cQuery     = "SELECT empcapturo FROM ColSolicitudes WHERE folio = $iFolio";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function claveConsarNombreEmpnomina ($iFolio){
        $cQuery     = "SELECT claveconsar, nombre, empnomina FROM colpromotortitular WHERE folio = $iFolio";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function claveConsarNombreEmpNominaColPromotor ($iFolio){
        $cQuery     = "SELECT claveconsar, nombre, empnomina FROM colpromotor WHERE folio = $iFolio";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function tipoSolicitudAforecedenteColsolicitudes ($iFolio){
        $cQuery     = "SELECT tiposolicitud, aforecedente, curp  FROM colsolicitudes  WHERE folio = $iFolio";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function nombreAforeCataforehrnIdCodigoAfore ($iAforeCedente){
        $cQuery     = "SELECT TRIM(nombreafore) FROM cataforehrn WHERE idcodigoafore = TRIM('".$iAforeCedente."')";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function coldoctosrendimientosneto ($iFolio){
        $cQuery     = "SELECT coldoctosrendimientosneto(".$iFolio.")";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function colDoctoComisiones ($iFolio){
        $cQuery     = "SELECT  TRIM(ctypecurp)::char(18) as curp, TRIM(ctypeclave)::char(16) as claveconsar, TO_CHAR(ctypefechafirma,'dd          mm        YYYY')::char(26) as fechafirma,TO_CHAR(NOW()::DATE,'YYYYmmdd')::CHAR(8) as fechaactual FROM colDoctoComisiones(".$iFolio." );";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function colinfxPromotoresClaveConsar($cDatPromotorCveConsar){
        $cQuery     = " SELECT TRIM(curp) AS curp FROM colinfxpromotores WHERE claveconsar =  ".(int)$cDatPromotorCveConsar.";";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function fnRespuestaDoctoRendimientoNetoProcesar ($iSiefore){
        $cQuery     = "SELECT posicionrendimientos::integer, nombreafore, rendimientoneto, promediosimple, fechainiciovigor, fechafinalvigor, fechaemision, fechacifrascierre FROM fnRespuestaDoctoRendimientoNetoProcesar(".$iSiefore.")";
        $query      = $this->Pgaforeglobal->query($cQuery);
        return $query->result();
    }

    function datosDoctorn($iFolio){
        $query = $this->Pgaforeglobal->query("SELECT ax.curp, TO_CHAR(ax.fechafirma,'ddmmYYYY')::char(26) as fechafirma, ax.tiposolicitud, ax.aforecedente, ax.empcapturo, ax.promotortitular, coalesce(bx.claveconsar, 0) claveconsar, coalesce(cx.claveconsar,0) cveconsar_titular FROM colsolicitudes as ax join colpromotor as bx on(ax.folio=bx.folio) left join colpromotortitular as cx on(ax.folio=cx.folio) WHERE ax.folio = $iFolio;");
        return $query->result();
    }

    function fnobtenersieforegeneracionalag($cCurpTrab){
        $query = $this->Pgaforeglobal->query("SELECT fnobtenersieforegeneracionalag AS siefore FROM fnobtenersieforegeneracionalag('$cCurpTrab');");
        return $query->result();
    }

    function fnobtenertiposolicitanteexcepcion($iFolio){
        $query = $this->Pgaforeglobal->query("SELECT iexcepcion, itiposolicitante from fnobtenertiposolicitanteexcepcion({$iFolio});");
        return $query->result();
    }
 
    function fnobtenerhuellasdoctosafiliacion($iFolio,$sCurpTrab,$iIDServicio,$cTipoOperacion,$tiposolicitante){
 
        if($tiposolicitante != 1)
        {
            $sSql="SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellasdoctosafiliacion('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')";
        }else{
            $sSql="SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellastercerasfiguras('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')";
        }
        $query = $this->Pgaforeglobal->query($sSql);
        return $query->result();
    }

    function fnobtenerfondosgeneracionalesag(){
        $query = $this->Pgaforeglobal->query("SELECT fondogeneracional,rangofechanacimiento FROM fnobtenerfondosgeneracionalesag(1);");
        return $query->result();
    }

    function fnobtenernombrefondosgeneracionalesag($folio){
        $query = $this->Pgaforeglobal->query("SELECT nombres, apellidos FROM fnobtenernombrefondosgeneracionalesag($folio);");
        return $query->result();
    }

    function actualizarpublicarimagenservicios($iFolio,$iOpcionFunc){
        $query = $this->Pgaforeglobal->query("SELECT fnpublicafirmaservicios01 FROM fnpublicafirmaservicios01($iFolio::BIGINT,$iOpcionFunc);");
        return $query->result();
    }

    function guardarpdf($iFolioSol,$rutaPdf){
        $query = $this->PgServiciosAfore->query("SELECT fnguardarpdfsrecertificacion AS respuesta ('$iFolioSol','$rutaPdf');");
        return $query->result();
    }

    function fnConsultarEmpleadoConstancia($iEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT icodigo, cmensaje, cnombre, ctienda, espromotor,  curppromotor, claveconsar FROM fnConsultarEmpleadoConstancia('.$iEmpleado.');");
		return $query->result();
    }

    function tbfondosgeneracionalesag($iSiefore){

		$query = $this->Pgaforeglobal->query("SELECT rangofechanacimiento FROM tbfondosgeneracionalesag WHERE tipo = 1 AND fondogeneracional = '.$iSiefore.';");
		return $query->result();
	}
}
?>