<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdledoctarecertificacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->PgAdmonAfore = $this->load->database("PgAdmonAfore",true);
	}

	function obtenAnioCuatrimestres(){

		$query = $this->Pgaforeglobal->query("SELECT FN_OBTENERCUATRIMESTRE() as fecha;");
		return $query->result();
	}

	function MostrarDatosCliente($curp,$fechaini,$fechafin,$iPeriodo){

		$query = $this->PgAdmonAfore->query("SELECT fechaini, fechafin, nombre, callenumero, colonia, codpostal, centroreparto, entidadfederativa, delegacionmunicipio, nss, rfc, curp,
		siefore, indicadorafiliacion, saldoahorroretiroini, saldoahorroretiroaporte, saldoahorroretiroretiros, saldoahorroretirorendimiento, saldoahorroretirocomision,
		saldoahorroretirofin, saldoahorrovoluntarioini, saldoahorrovoluntarioaporte, saldoahorrovoluntarioretiro, saldoahorrovoluntariorend, saldoahorrovoluntariocomision,
		saldoahorrovoluntariofin, saldoahorroviviendaini, saldoahorroviviendamovto, saldoahorroviviendafin, saldoahorrofin, imsssaldoinicialcvcs, imsssaldoaportecvcs,
		imsssaldoretirocvcs, imsssaldorendimientocvcs, imsssaldocomisioncvcs, imsssaldofinalcvcs, issstesaldoinicialrcv, issstesaldoaportercv, issstesaldoretirorcv,
		issstesaldorendimientorcv, issstesaldocomisionrcv, issstesaldofinalrcv, saldoahorrofovissste, saldoahorrofovissstemovto, saldoahorrofovissstefin,
		saldototalahorroissste, saldototal, saldoinicial, bonopensionactualudi, bonopensionnominal, bonopensionactpesos, bonopensionnompesos,
		consecutivotrabajador, estado FROM tbestadocuentaafore_gral  WHERE curp = '$curp' AND fechaini >= '$fechaini'::DATE AND  fechafin <= '$fechafin'::DATE ORDER BY nss DESC;");
		return $query->result();
	}

	function MostrarInformacionEstados(){

		$query = $this->Pgaforeglobal->query("SELECT estadoespecialuno, estadoespecialdos FROM tbinfoadminedocta;");
		return $query->result();
	}

	function UltimoCuatrimestre(){

		$query = $this->Pgaforeglobal->query("SELECT substring(obtenercuatrimestresedocta, 1,5) as ultimo FROM obtenercuatrimestresedocta();");
		return $query->result();
	}

	function ObtenerReverso($sCurp){

		$query = $this->PgAdmonAfore->query("SELECT fn_FormatoCuatrimestreEdocta AS respuesta FROM fn_FormatoCuatrimestreEdocta('$sCurp');");
		return $query->result();
	}

	function Reversocatorce($sCurp, $fechaini, $fechafin){

		$query = $this->PgAdmonAfore->query("SELECT b.id_rubro, b.fechamovimiento, b.concepto, b.referencia, b.diascotizados, b.salariobase, b.monto, b.tipo_movimiento 
		FROM tbestadocuentaafore_gral a JOIN tbestadocuentaafore_det b ON (a.nss = b.nss AND a.fechafin = b.fechafin) 
		WHERE a.curp = '$sCurp' AND a.fechafin = '$fechafin' AND b.id_rubro IN (2,3,4) ORDER BY b.keyx ASC;");
		return $query->result();
	}

}
?>