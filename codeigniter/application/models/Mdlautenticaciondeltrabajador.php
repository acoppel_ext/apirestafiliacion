<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdlautenticaciondeltrabajador extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->PgServiciosAfore = $this->load->database("PgServiciosAfore",true);
		$this->Informix = $this->load->database('Pgsafre', TRUE);
	}

	function obtnerinformacionpromotor($iEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT icodigo,cmensaje,cnombre,ctienda, espromotor,curppromotor,claveconsar FROM fnConsultarEmpleadoConstancia($iEmpleado);");
		return $query->result();
	}

	function obtenerfolioafilaicion($ibusqueda, $scurpnss){

		$query = $this->Pgaforeglobal->query("SELECT ierror,ifolio, cmensaje FROM fnconsultarfolioafiliacion($ibusqueda,'$scurpnss');");
		return $query->result();
	}
	
	function obtenerPaginaRedireccionar($opcionpag, $foliosol, $iEmpleado, $sIpRemoto){

		$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, descripcion, destino AS destino FROM fnobtenerligasolicitudrecertificacion($opcionpag,$foliosol,$iEmpleado,'$sIpRemoto');");
		return $query->result();
	}

	function validarPendiente($empleado){

		$query = $this->PgServiciosAfore->query("SELECT respuesta,folio,mensaje FROM fnvalidartramitependienterecertificacion($empleado);");
		return $query->result();
	}

	function obteneMotivosIncompleta(){

		$query = $this->PgServiciosAfore->query("SELECT respuesta,mensaje FROM fnobtenermotivosrecertificaiconincompleta();");
		return $query->result();
	}

	function actualizarPendiente($folio,$codMot){

		$query = $this->PgServiciosAfore->query("SELECT fnactualizarmotivosrecertificaiconincompleta FROM fnactualizarmotivosrecertificaiconincompleta($folio, $codMot);");
		return $query->result();
	}

	//Clase CAutenticarDomicilio

	function GrabarAutenticacion($metodoAutificacion,$nss,$curp,$calleRegistrada,$coloniaRegistrada,$ciudadRegistrada,$empleado,$origen){

		$query = $this->Pgaforeglobal->query("SELECT fn_guardarAutenticacionTrabajador AS respuesta from fn_guardarAutenticacionTrabajador($metodoAutificacion,'$nss','$curp','$calleRegistrada','$coloniaRegistrada','$ciudadRegistrada',$empleado,$origen);");
		return $query->result();
	}

	//Clase CGeneral

	function EntidadNacimiento($idcatalogo,$iEstado){

		$query = $this->Pgaforeglobal->query("SELECT idcatalogo, descripcion from fnconsultarcatentidaddomicilio(".$idcatalogo."::smallint,".$iEstado.") order by idcatalogo;");
		return $query->result();
	}

	function ejecutivoUeap($nEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT * from fnconsultaempleadoueap(".$nEmpleado.");");
		return $query->result();
	}

	function buscarFolioAfiliacion($iFolio, $sNss, $sCurp){

		$query = $this->Pgaforeglobal->query("SELECT folio FROM colsolicitudes WHERE folio = $iFolio AND ( nss = '$sNss' OR curp = '$sCurp' );");
		return $query->result();
	}

	function consultaClienteAutenticacion($iOrigen, $sNombre, $sApPaterno, $sApMaterno, $iEntidadNac, $sFechaNac){

		if($iOrigen == 1)//aforeglobal
		{
			$query = $this->Pgaforeglobal->query("SELECT nss, curp, folio, tiposolicitud, calle, colonia, ciudadpob from fnconsultaclienteautenticacion('".$sNombre."', '".$sApPaterno."', '".$sApMaterno."',".$iEntidadNac.",'".$sFechaNac."'::date);");
		}
		else//safre
		{
			$query = $this->Informix->query("execute function fnconsultaclienteautenticacion('".$sNombre."', '".$sApPaterno."', '".$sApMaterno."',".$iEntidadNac.", MDY(".$sFechaNac."));");
		}
		
		return $query->result();
	}

	function obtenerpuestoempleado($iEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT fnconsultarpuestoempleado as resp from fnconsultarpuestoempleado($iEmpleado);");
		return $query->result();
	}

	function ConsultarRequiereReEnrolamiento($sNombre, $sApPaterno, $sApMaterno, $iEntidadNac, $sFechaNac){

		$query = $this->Pgaforeglobal->query("SELECT fnvalidarrequirereenrol AS requierereenrol FROM fnvalidarrequirereenrol('".$sNombre."', '".$sApPaterno."', '".$sApMaterno."',".$iEntidadNac.",'".$sFechaNac."'::date);");
		return $query->result();
	}

	function validarAsignado($sNombre, $sApPaterno, $sApMaterno, $sFechaNac){

		$query = $this->Pgaforeglobal->query("SELECT cmensaje,irespuesta  FROM fnvalidarasignado('".$sApPaterno."', '".$sApMaterno."', '".$sNombre."','".$sFechaNac."'::date);");
		return $query->result();

	}

	function autenticarcita($iOpcion, $cCurp, $cNss, $iNumEmpleado){

		$query = $this->Pgaforeglobal->query("SELECT estatus, tipoaplicativo, trim(destino) AS destino FROM fnautenticaexistenciacitasesar($iOpcion, '$cCurp', '$cNss', $iNumEmpleado, '$sIpModulo');");
		return $query->result();
	}
}
?>