<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlimpresionDocumentosafiliacion extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal", true);
	}

	function obtnerinformacionpromotor($emplado)
	{
		$cQuery     = "SELECT icodigo,cmensaje,cnombre,ctienda,espromotor,curppromotor,claveconsar FROM fnconsultarempleadoconstancia($emplado);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function consultarcatalogocorreoei()
	{
		$cQuery     = "SELECT idserviciocorreo AS idcorreo,TRIM(descripcion) AS dscorreo FROM fnconsultarcatalogocorreoei();";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function obtenerdatossolicitudafi($folio)
	{
		$cQuery     = "SELECT respuesta,tiposolicitante,autenticacion,tiposolicitud,tipotraspaso,curp,nss,nombre,paterno,materno,folioenrol,manosenrol,email,enrolpermanente FROM fnobtenerdatossolicitudafi($folio);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function validarregistroenrolhuellaservicio($folio, $tiposol)
	{
		$cQuery     = "SELECT fnvalidarregistroenrolhuellaservicio AS respuesta FROM fnvalidarregistroenrolhuellaservicio($folio,$tiposol);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function obtenerligasimpafi($foliosol, $folioenrol, $promotor, $tipoafi, $enrol, $ipmodulo)
	{
		$cQuery     = "SELECT idliga,destino, descripcion FROM fnobtenerligasimpafi($foliosol,$folioenrol,$promotor,$tipoafi,$enrol,'$ipmodulo');";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function entregaexpedienteafiliacion($opcion, $foliosol, $correo, $entrega, $os, $estado, $ruta, $nombre)
	{
		$cQuery     = "SELECT fnentregaexpedienteafiliacion AS respuesta FROM fnentregaexpedienteafiliacion($opcion,$foliosol,'$correo',$entrega,$os,$estado,'$ruta','$nombre');";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function guardarllamadacatafiliacion($folio, $tiposol)
	{
		$cQuery     = "SELECT fnguardarllamadacatafiliacion AS respuesta FROM fnguardarllamadacatafiliacion('$folio',$tiposol);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}

	function obtenerCurp_TipoSolicitud($iFolioSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT tipoconstancia, TRIM(curp) AS curp, TRIM(curpsolicitante) AS curpsolicitante ,tiposolicitante , TRIM(codigoautenticacion) as codigoautenticacion , validacodautentica  FROM solconstancia WHERE folio = $iFolioSol;");
		return $query->result();
	}

	function validartiposolicitante($iFolioSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnvalidartiposolicitante02 AS respuesta FROM fnvalidartiposolicitante02($iFolioSol);");
		return $query->result();
	}

	function tieneexcepcionimpresion($iFolioSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT itiposolicitante AS respuesta from fnobtenertiposolicitanteexcepcion($iFolioSol);");
		return $query->result();
	}

	function validarCodigoAutenticacion($cCurp, $cCodigo)
	{
		$query = $this->Pgaforeglobal->query("SELECT CASE WHEN fechavigencia < CURRENT_DATE THEN 'NO_VIGENTE' ELSE CASE WHEN fechavigencia >= CURRENT_DATE THEN 'VIGENTE' ELSE '' END END AS valido FROM inecontrolcodigoautenticacion Where curp = '$cCurp' AND codigoautenticacion = '$cCodigo' Order by fechavigencia desc Limit 1;");
		return $query->result();
	}

	function GenerafolioAutenticacion($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fngenerafolioautenticaciontercerafigura AS res FROM fngenerafolioautenticaciontercerafigura($iFolio);");
		return $query->result();
	}
	function verificaEnrolamientoCompleto($iFolioSol, $sCurp)
	{
		$sQuery = "select fnverificaenrolamientocompleto($iFolioSol, '$sCurp') as enrolcompleto;";
		$query = $this->Pgaforeglobal->query($sQuery);
		$mensaje = 'consulta-> ' . $sQuery;
		log_message('error', $mensaje);
		return $query->result();
	}

	function validarEnrolhuellasservicio($iFolioSol, $iTipoSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnvalidarregistroenrolhuellaservicio AS respuesta FROM fnvalidarregistroenrolhuellaservicio(" . $iFolioSol . "," . $iTipoSol . ");");
		return $query->result();
	}

	function obtenerrespuesta($sIpLocal)
	{
		$query = $this->Pgaforeglobal->query("SELECT trim(ipofflinelnx) as valor FROM fnobteneripcluster('" . $sIpLocal . "');");
		return $query->result();
	}

	function obtnenersiefore($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT coldoctosrendimientosneto(" . $iFolio . ");");
		return $query->result();
	}

	function ExtraerTabla()
	{
		$query = $this->Pgaforeglobal->query("SELECT id, trim(descripcion) as descripcion, trim(liga) as liga FROM tbdoctosafore ORDER BY id;");
		return $query->result();
	}

	function obtenerInformacionAfiliacion($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT empcapturo as empleado FROM colsolicitudes WHERE folio = $iFolio");
		return $query->result();
	}

	function obtenerLigaImpresion($iTipoImpresion, $iFolioSol, $iNumeroEmpl, $sIpRemoto, $iReeimpresion)
	{
		$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, TRIM(descripcion) AS descripcionboton, TRIM(destino) AS destino FROM fnobtenerligasolicitudafiliacion($iTipoImpresion, $iFolioSol, $iNumeroEmpl, '$sIpRemoto', $iReeimpresion)");
		return $query->result();
	}

	function obtenerCurpTipoSolicitud($iFolioSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT tipoconstancia, TRIM(curp) AS curp FROM solconstancia WHERE folio = $iFolioSol;");
		return $query->result();
	}

	function cuentaConManos($iFolioSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnconsultarsinmanos2dosello AS respuesta FROM fnconsultarsinmanos2dosello($iFolioSol);");
		return $query->result();
	}
	
	// todas las consultas 1498
	
	function ObtenerRspuestaGestor($iFolio,$iOpc)
	{
	
		$cQuery = "select fnvalidarbanderagestor AS Bandera from fnvalidarbanderagestor($iFolio,$iOpc)";
		
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}
	
	
	function Insertarsegundosellodevoluntaddactilar($iFolioSol,$iEmpleado)
	{
		
		$query = $this->Pgaforeglobal->query("SELECT fnregistrarsellodevoluntaddactilar AS estatus FROM fnregistrarsellodevoluntaddactilar($iFolioSol,$iEmpleado)");
		return $query->result();
	}
	
	// aqui termina 1498 

	function consultarrespuestawebservices($foliosol,$emplado){
		$cQuery     = "SELECT selloverificacionbiometricavoluntram AS selloverificacion, TRIM(diagnosticovoluntram) AS diagnostico, TRIM(mensaje) AS mensaje, TRIM(resultadoverificacionvoluntram) AS resultadoverificacion
					   FROM fnconsultarespuestaprocesarsellos($foliosol,$emplado);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}
	
	function enviarcorreo($foliosol)
	{
		$cQuery     = "SELECT fnguardarfolioimpresionafiliacion AS respuesta FROM fnguardarfolioimpresionafiliacion($foliosol);";
		$query      = $this->Pgaforeglobal->query($cQuery);
		log_message("error", "FUNCION-> $cQuery");
		return $query->result();
	}
}
