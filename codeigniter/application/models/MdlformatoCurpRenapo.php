<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlformatoCurpRenapo extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->bustramites = $this->load->database('bustramites', TRUE);
		$this->Informix = $this->load->database('Pgsafre', TRUE);
		$this->PgServiciosAfore = $this->load->database('PgServiciosAfore', TRUE);
	}

	function consultarBotones($folio)
	{
		$query = $this->Pgaforeglobal->query("SELECT curp, appaterno, appmaterno, nombres, folioafore, codigorenapo,foliosolicitud, idsubproceso, nombpdf FROM fnconsultardatosformatocurprenapo(".$folio.");");
		return $query->result();
	}

	function estatusPDF($folioAfore, $folioSolicitud)
	{
		$query = $this->Pgaforeglobal->query("SELECT * FROM fnactualizarestatuscurprenapo(".$folioAfore.", ".$folioSolicitud.");");
		return $query->result();
	}

	function publicarImg($folio, $idOpcion)
	{
		if ($idOpcion == 14)
		{
			$query = $this->Pgaforeglobal->query("SELECT * FROM fnpublicafirmaservicios01($folio, 14);");
		}
		else
		{
			$query = $this->Pgaforeglobal->query("SELECT * FROM fnpublicafirma($folio,$idOpcion);");
		}

		return $query->result();
	}

	function afiliacionServicio($folio)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnesafiliacionservicio AS respuesta FROM fnesafiliacionservicio(".$folio.");");
		return $query->result();
	}

	function imagenesTransmitir($data)
	{
		extract($data);
		$iOpcion = $data['iOpcion'];
		$iFolio = $data['folio'];
		$cIdImagen = $data['cIdImagen'];
		$iEstatusPublica = $data['estatusPublica'];
		$cNombreArchivo = $data['nomDocumento'];
		$query = $this->Pgaforeglobal->query("SELECT tfolio FROM fnctrlimagenesportransmitir($iOpcion, $iFolio, '$cIdImagen', $iEstatusPublica, '$cNombreArchivo');");
		return $query->result();
	}

}
?>