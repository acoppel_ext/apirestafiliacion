<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlevaluacionRecertificacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->bustramites = $this->load->database('bustramites', TRUE);
		$this->Informix = $this->load->database('Pgsafre', TRUE);
		$this->PgServiciosAfore = $this->load->database('PgServiciosAfore', TRUE);
	}

	function eliminarCuestionarioServicio($folioServicio)
	{
		$query = $this->PgServiciosAfore->query("SELECT fneliminarcuestionarioservicio AS irespuesta FROM fneliminarcuestionarioservicio($folioServicio);");
		return $query->result();
	}

	function obtenerPreguntasCuestionario($number)
	{
		$query = $this->PgServiciosAfore->query("SELECT idpregunta, descripcionpreguntas FROM fnobtenerpreguntascuestionarioservicio($number);");
		return $query->result();
	}

	function obtenerRespuesCuestionarioServicio($number)
	{
		$query = $this->PgServiciosAfore->query("SELECT idrespuesta, descripcionrespuestas FROM fnobtenerrespuescuestionarioservicio($number);");
		return $query->result();
	}

	function consultarEmpleadoConstancia($numeroEmpleado)
	{
		$query = $this->Pgaforeglobal->query("SELECT icodigo,cmensaje,cnombre,ctienda, espromotor,curppromotor,claveconsar FROM fnConsultarEmpleadoConstancia($numeroEmpleado);");
		return $query->result();
	}

	function guardarInformacionEvaluacionRecertificacion($folioServicio, $idPregunta, $idRespuesta, $idCajaTexto)
	{
		$query = $this->PgServiciosAfore->query("SELECT fnguardarcuestionarioservicio AS irespuesta FROM fnguardarcuestionarioservicio($folioServicio, $idPregunta, $idRespuesta, TRIM('$idCajaTexto
			'));");
		return $query->result();
	}

	function obtenerLigaRecert($origen, $origenPagina, $ipRemoto, $imprimir, $rutaPdf, $folioSol, $empleado, $folioServicio, $reguieReeb, $servicio, $curp, $consultaEdoc, $tipoImpresion, $tipoAcuse, $siAfore)
	{
		$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, destino FROM " . 
				"fnobtenerligasrecertificacion($origen,$origenPagina,'$ipRemoto',$imprimir,'$rutaPdf',$folioSol,$empleado,$folioServicio,$reguieReeb,$servicio,'$curp',$consultaEdoc,$tipoImpresion,$tipoAcuse, $siAfore);");
		return $query->result();
	}

	function datosRecertificacion($origen, $folioServicio, $estatusRecer, $correoImpresion, $correo)
	{
		$query = $this->PgServiciosAfore->query("SELECT iretorno, ccorreo FROM fndatosrecertificacion($origen, $folioServicio, $estatusRecer, $correoImpresion, '$correo');");
		return $query->result();
	}

}
?>