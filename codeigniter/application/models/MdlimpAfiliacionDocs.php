<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlimpAfiliacionDocs extends CI_Model{

    public function __construct(){
        parent::__construct();
        //load database library
        $this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
        $this->PgIMAGENES_HIST = $this->load->database("PgIMAGENES_HIST",true);   
        $this->PgImagenes = $this->load->database("PgImagenes",true);
    }

    function consultarfoliocredencial($empleado){

        $query  = $this->Pgaforeglobal->query("SELECT TRIM(fnconsultarfoliocredencial) AS respuesta FROM fnconsultarfoliocredencial($empleado);");
        return $query->result();
    }

    function descargarimagen($opcion, $folioImagen){

        $cQuery = "SELECT irespuesta,TRIM(rimagen) AS rimagen,cdatos FROM fndescargarimagen('$folioImagen','CPRO');";

        if($opcion == 1)
        {
            $query = $this->PgImagenes->query($cQuery);
        }
        else 
        {
            $query = $this->PgIMAGENES_HIST->query($cQuery);
        }
    
        return $query->result();
    }

    function obtenerBase64($id){

        $query  = $this->Pgaforeglobal->query("SELECT fnobtenerbase64id FROM fnObtenerBase64Id($id);");
        return $query->result();
    }

    function obtenerCurp($folioCurp){

        $query  = $this->Pgaforeglobal->query("SELECT curp FROM fnobtenerdatossolicitudafi('$folioCurp');");
        return $query->result();
    }

    function obtenerSiefore($sCurp){

        $query  = $this->Pgaforeglobal->query("SELECT fnobtenersieforegeneracionalag AS siefore FROM fnobtenersieforegeneracionalag('$sCurp');");
        return $query->result();
    }

    function envioexp($folioafiliacion,$archivo,$imdata){

        $query  = $this->PgImagenes->query("select * from fninsertadatosimgcartaderechos('$folioafiliacion','$archivo','$imdata',1,1,0,0);");
        return $query->result();
    }
}
?>