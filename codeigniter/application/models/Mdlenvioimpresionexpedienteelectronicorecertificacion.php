<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdlenvioimpresionexpedienteelectronicorecertificacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->PgServiciosAfore = $this->load->database("PgServiciosAfore",true);
	}

	function fnconsultarempleadoconstancia($iempleado){

		$query = $this->Pgaforeglobal->query("SELECT icodigo,cmensaje,cnombre,ctienda, espromotor,curppromotor,claveconsar FROM fnconsultarempleadoconstancia($iempleado);");
		return $query->result();
	}

	function fnobtenerlistadodominioscorreos($iopciondominio,$iidcorreo){

		$query = $this->PgServiciosAfore->query("SELECT iidcorreo, TRIM(cdescripcion) AS cdescripcion FROM fnobtenerlistadodominioscorreos($iopciondominio,$iidcorreo);");
		return $query->result();
	}

	function fndatosrecertificacion($iorigen, $ifolioservicio, $iestatusrecer, $icorreoimpresion, $scorreo){

		$query = $this->PgServiciosAfore->query("SELECT iretorno, ccorreo FROM fndatosrecertificacion($iorigen, $ifolioservicio, $iestatusrecer, $icorreoimpresion, '$scorreo');");
		return $query->result();
	}

	function fnvalidarnumeromaximocorreocapturado($ccorreoelectronico){

		$query = $this->PgServiciosAfore->query("SELECT fnvalidarnumeromaximocorreocapturado AS contador FROM fnvalidarnumeromaximocorreocapturado('$ccorreoelectronico');");
		return $query->result();
	}

	function fnobtenerligasrecertificacion($iorigen,$iorigenpagina,$sipremoto,$iimprimir,$crutapdf,$ifolioafiliacion,$iempleado,$ifolioservicio,$ireguiereeb,$iservicio,$ccurp,$iconsultaedoc,$itipoimpresion,$itipoacuse, $isiefore){

		$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, destino FROM fnobtenerligasrecertificacion($iorigen,$iorigenpagina,'$sipremoto',$iimprimir,'$crutapdf',$ifolioafiliacion,$iempleado,$ifolioservicio,$ireguiereeb,$iservicio,'$ccurp',$iconsultaedoc,$itipoimpresion,$itipoacuse,$isiefore);");
		return $query->result();
	}

	function fnobtenerpdfsrecertificacion($ifolioservicio){

		$query = $this->PgServiciosAfore->query("SELECT TRIM(fnobtenerpdfsrecertificacion) AS cdestino FROM fnobtenerpdfsrecertificacion('$ifolioservicio');");
		return $query->result();
	}

}
?>