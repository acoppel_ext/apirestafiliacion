<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdlrecertificacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->PgImagenes = $this->load->database('PgImagenes', TRUE);
		$this->PgServiciosAfore = $this->load->database("PgServiciosAfore",true);
		$this->PgVideos = $this->load->database("PgVideos",true);
		$this->Informix = $this->load->database("Pgsafre",true);
	}

	function obtnerinformacionpromotor($iEmpleado)
	{
		$query = $this->Pgaforeglobal->query("SELECT icodigo,cmensaje,cnombre,ctienda,espromotor,curppromotor,claveconsar FROM fnConsultarEmpleadoConstancia($iEmpleado);");
		return $query->result();
	}

	function obtnerinformacionpromotor_ipRemoto($sIpRemoto)
	{
		$query = $this->Pgaforeglobal->query("SELECT ipoffline,itienda,ctienda,iregion,cregion,icoordinacion FROM fnobtenerdatostiendarec('" . $sIpRemoto . "');");
		return $query->result();
	}

	function tienerecertificacionpendiente($cCurpNss)
	{
		$query = $this->Pgaforeglobal->query("select resp, fechafinrecertificacion, folioservicio, curptrabajador, nss, fechaalta from fnrecertificacionlinea('$cCurpNss');");
		return $query->result();
	}

	function tienerecertificacionpendienteFolio($iFoliosol,$cCurpNss,$iOpcion,$cfechaFin,$cFolioSer,$cCurp1,$cNss1,$cFechaAlta)
	{
		$query = $this->PgServiciosAfore->query("SELECT iretorno, ifolioser, scurp, snss, TRIM(smensaje) AS smensaje FROM fnvalidarvigenciaSolRecertificacion($iFoliosol,'$cCurpNss',$iOpcion,'$cfechaFin','$cFolioSer','$cCurp1','$cNss1','$cFechaAlta');");
		return $query->result();
	}

	function obtenerTipoTrabajador($cNss)
	{
		$query = $this->Informix->query("EXECUTE FUNCTION fn_obtener_tipo_trabajador('$cNss');");
		return $query->result_array();
	}

	function validarsolicitudrecertificacion($iFoliosol, $cCurpNss)
	{
		$query = $this->Pgaforeglobal->query("SELECT iretorno, scurp, snss, TRIM(smensaje) AS smensaje FROM fnvalidarsolicitudrecertificacion($iFoliosol,'$cCurpNss');");
		return $query->result();
	}

	function obtenerLigaRecert($iOrigen, $iOrigenPagina, $sIpRemoto, $iImprimir, $cRutaPDF, $iFoliosol, $iEmpleado, $iFolioSer, $iReguiereEB, $iServicio, $cCurp, $iConsultaEdoc, $iTipoImpresion, $iTipoAcuse, $iSiefore)
	{
		$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, destino FROM fnobtenerligasrecertificacion($iOrigen,$iOrigenPagina,'$sIpRemoto',$iImprimir,'$cRutaPDF',$iFoliosol,$iEmpleado,$iFolioSer,$iReguiereEB,$iServicio,'$cCurp',$iConsultaEdoc,$iTipoImpresion,$iTipoAcuse,$iSiefore);");
		return $query->result();
	}

	function insertarDatosRecservicios($arrDatosRec, $sIpRemoto)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnguardarecserviciosafore AS respuesta FROM fnguardarecserviciosafore($arrDatosRec'$sIpRemoto');");
		return $query->result();
	}

	function insertarDatosSolrecertificacion($arrDatosSol)
	{
		$query = $this->PgServiciosAfore->query("SELECT fnguardasolrecertificacion AS respuesta FROM fnguardasolrecertificacion($arrDatosSol);");
		return $query->result();
	}

	function datosrecertificacion($iOrigen, $iFolioSer, $iEstatusRecer, $iCorreoImpresion, $sCorreo)
	{
		$query = $this->PgServiciosAfore->query("SELECT iretorno AS respuesta FROM fndatosrecertificacion($iOrigen, $iFolioSer, $iEstatusRecer, $iCorreoImpresion, '$sCorreo');");
		return $query->result();
	}

	function obtenerSiefore()
	{
		$query = $this->Pgaforeglobal->query("SELECT tipo2 AS fecha FROM pafhardcode WHERE tipo1 = 138;");
		return $query->result();
	}

	function obtenerSieforeCurp($cCurp, $fechaini, $fechafin)
	{
		$query = $this->PgAdmonafore->query("SELECT respuesta, TRIM(mensaje) AS mensaje, siefore FROM fnvalidaredoctarecertificacion('$cCurp','$fechaini','$fechafin');");
		return $query->result();
	}

	function marcarCuenta($iTipoTrabajador, $cNss)
	{
		$query = $this->PgServiciosAfore->query("SELECT fnmarcarcuenta AS respuesta FROM fnmarcarcuenta($iTipoTrabajador,'$cNss','RP');");
		return $query->result();
	}

	function obtenerConfiguracionSUV()
	{
		$query = $this->Pgaforeglobal->query("SELECT totalminutos, totaldias, totalintentos FROM fnobtenervaloresconfiguracion();");
		return $query->result();
	}

	function validarrequieresellosuv($iFolioServicio)
	{
		$query = $this->Pgaforeglobal->query("SELECT estatus,sello FROM fnvalidarrequieresellosuv($iFolioServicio,'2027');");
		return $query->result();
	}

	function verificaFechaFirma($cNss, $cCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnconsultarfechaaficontrol as respuesta from fnconsultarfechaaficontrol('$cNss','$cCurp');");
		return $query->result();
	}

	function verificarRequiereExpediente($iServicio)
	{
		$query = $this->Pgaforeglobal->query("SELECT irequiereei,irequiereeb from fnobtenercatalogoservicio01(2,$iServicio);");
		return $query->result();
	}

	function validarExpedientes($iFolioSer, $cCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT retorno, solexpiden, solexpbio, mensaje FROM fnestatusprocesarrecertificacion($iFolioSer,'$cCurp');");
		return $query->result();
	}
	function actualizarflujoservicio($iFolioSer)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnguardarflujoserviciorecertificacion AS respuesta FROM fnguardarflujoserviciorecertificacion($iFolioSer);");
		return $query->result();
	}

	function datosCurpEI($cNss, $cCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT iretorno,ifolio,itiposervicio FROM fnobtenerfolioeimaestro('$cCurp','$cNss');");
		return $query->result();
	}

	function buscarImganeCurp($iFolioSer, $itiposervicio, $cCurp)
	{
		$query = $this->PgImagenes->query("SELECT fnvalidarimagencurpexpedienteidentificacion AS respuesta FROM fnvalidarimagencurpexpedienteidentificacion($iFolioSer,$itiposervicio,'$cCurp');");
		return $query->result();
	}

	function generoFormatoRenapo($iFolioSer)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnexistecurprenapo AS respuesta FROM fnexistecurprenapo(1, $iFolioSer, 8);");
		return $query->result();
	}
	
	function consultarNss($cCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnobtenernss as nss FROM fnObtenerNss('" . $cCurp . "');");
		return $query->result();
	}

	function huellastrabajador($cCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT dedo FROM fnobtenernisthuellastrabajador('$cCurp') ORDER BY calidad ASC;");
		return $query->result();
	}

	function huellaspromotor($iEmpleado)
	{
		$query = $this->Pgaforeglobal->query("SELECT dedo FROM fnobtenernisthuellaspromotor($iEmpleado) ORDER BY calidad ASC;");
		return $query->result();
	}

	function excepciones()
	{
		$query = $this->PgServiciosAfore->query("SELECT clave,descripcion FROM fnobtenerexcepcionesvideorecer();");
		return $query->result();
	}

	function ObtenerUrlsVideo($iPmodulo)
	{
		$query = $this->Pgaforeglobal->query("SELECT urlguardado, urlgrabado FROM fnobtenerurlsgrabadovideo('$iPmodulo');");
		return $query->result();
	}

	function ConsultarDialogoVideo($TipoDialogo,$iFolioSer)
	{
		$query = $this->Pgaforeglobal->query("SELECT tiempominimo, tiempomaximo, formato, calidad, dialogo,nombrevideo FROM fnobtenerdialogosvideorecertificacion($TipoDialogo,$iFolioSer);");
		return $query->result();
	}

	function subirvideo($folio, $nombreimagen, $base64, $fraccionado)
	{
		$query = $this->PgVideos->query("SELECT fnsubirvideos FROM fnsubirvideos('$folio','$nombreimagen','$base64',$fraccionado);");
		return $query->result();
	}

	function validarMovilExpIE($iFolioSer)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnvalidarmovilexprecertificacion AS respuesta FROM fnvalidarmovilexprecertificacion($iFolioSer);");
		return $query->result();
	}

	function catalogoCompaniaMovil()
	{
		$query = $this->Pgaforeglobal->query("SELECT clavec, TRIM(desccomp) AS nombre FROM fn_catcompacelulares();");
		return $query->result();
	}

	function verificarTelefono($iFolioSer,$movil)
	{
		$query = $this->Pgaforeglobal->query("SELECT respuesta, TRIM(mensaje) AS mensaje FROM fnvalidarmovilrecertificacion($iFolioSer,'$movil');");
		return $query->result();
	}

	function guardarMovilCAT($iFolioSer, $movil)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnguadarregcatrecertificacion AS respuesta FROM fnguadarregcatrecertificacion($iFolioSer,'$movil');");
		return $query->result();
	}
}
?>