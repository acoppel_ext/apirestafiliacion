<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlconsultaCurprenapo extends CI_Model {

    public function __construct()
	{
			parent::__construct();

			//load database library
			$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
			$this->bustramites = $this->load->database("bustramites",true);
	}

	function obtenerCurpPromotor($iNumeroEmpleado)
	{
		$query = $this->Pgaforeglobal->query("select fnvalidapromotoractivo as curp from fnvalidapromotoractivo($iNumeroEmpleado)");
		return $query->result();
    }

	function servicioEjecutarAplicacion($idServicio)
	{
		$query = $this->bustramites->query("select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio,'0')");
		return $query->result();
	}

	function servicioObtenerRespuesta($idServicio)
	{
		$query = $this->bustramites->query("select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio)");
		return $query->result();
	}

	function consultaRespuestaRenapo($iFolioAfore)
	{
		$query = $this->Pgaforeglobal->query("select idu_tipoerror, idu_codigoerrorrenapo, mensaje from fnconsultarespuestacurprenapo($iFolioAfore);");
		return $query->result();
	}

	function guardarSolicitudRenapo($iFolioAfore,$iFolioSolicitud,$iProceso,$iEmpleado,$idsubproceso,$cCurp,$cApellidopaterno,$cApellidomaterno,$cNombres,$cSexo,$cFechaNacimiento,$iSlcEntidadNacimiento,$iPmodulo)
	{
		$query = $this->Pgaforeglobal->query("SELECT fnguardarrensolicitudescurp as valor FROM fnguardarrensolicitudescurp($iFolioAfore, $iFolioSolicitud, $iProceso, $idsubproceso, '$cCurp', $iEmpleado, '$cApellidopaterno', '$cApellidomaterno', '$cNombres', '$cSexo', '$cFechaNacimiento' , '$iSlcEntidadNacimiento', '$iPmodulo')");
		return $query->result();
	}

	function ValidarMayoriaEdad()
	{
		$query = $this->Pgaforeglobal->query("SELECT current_date as fechavalida;");
		return $query->result();
	}

	function FechaMaximaMayoriaEdad()
	{
		$query = $this->Pgaforeglobal->query("SELECT TO_CHAR(now()::date - INTERVAL '15 year', 'DD-MM-YYYY') AS fechamaxima;");
		return $query->result();
	}

	function consultarCatalogoEntidadNacimiento()
	{
		$query = $this->Pgaforeglobal->query("SELECT nomeclatura, descripcion FROM fnconsultarcatalogoentidadnacimiento();");
		return $query->result();
	}
}
?>