<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlsolicitudAfiliacion extends CI_Model {

    public function __construct()
	{
		parent::__construct();

        //load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
    }
	
	
	function consultarHuellas($iFolio,$sCurpTrab,$iIDServicio,$cTipoOperacion)
	{
		$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellasdoctosafiliacion('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')");
		
		
		return $query->result();
	}
	
	function consultarHuellasRegistro($iFolio,$sCurpTrab,$iIDServicio,$cTipoOperacion,$tiposolicitante)
	{
		if($tiposolicitante != 1)
		{
			$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellasdoctosafiliacion('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."');");
		}
		else
		{
			$query = $this->Pgaforeglobal->query("SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellastercerasfiguras('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."');");
		}		
		
		return $query->result();
	}
	
	function validartiposolicitanteexcepcion($iFolioSol)
	{
		$query = $this->Pgaforeglobal->query("SELECT iexcepcion, itiposolicitante from fnobtenertiposolicitanteexcepcion($iFolioSol)");
		return $query->result();
	}

    function obtenerCurpPromotor($iNumeroEmpleado)
	{
		$query = $this->Pgaforeglobal->query("select fnvalidapromotoractivo as curp from fnvalidapromotoractivo($iNumeroEmpleado)");
		return $query->result();
	}

	function actualizarFirmaPublicacion($iFolio,$iOpcion)
	{
		$query = $this->Pgaforeglobal->query("select fnpublicafirma from fnpublicafirma ($iFolio,$iOpcion);");
		return $query->result();
	}

	function obtenerclaveoperacion($iFolio,$iTabla)
	{
		$query = $this->Pgaforeglobal->query("select fnobtenerdoctosdigitalizacion from fnobtenerdoctosdigitalizacion ($iFolio,$iTabla)");
		return $query->result();
	}

	function consultaColSolicitudes($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT COUNT(*) AS respuesta FROM ColSolicitudes WHERE folio =$iFolio;");
		return $query->result();
	}

	function consultaTiposolicitud($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT tiposolicitud AS respuesta FROM ColSolicitudes WHERE folio=$iFolio;");
		return $query->result();
	}

	function consultaMotivotraspaso($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT 
												CASE substr(a.motivo,1,2)
													WHEN '00' THEN  substr(a.motivo,4,41)
													ELSE b.descripcion
												END AS motivo
											FROM colmotivotraspaso a
											INNER JOIN colcatmotivotraspaso b
											ON(b.id = substr(a.motivo,1,2)::INTEGER OR substr(a.motivo,1,2)::INTEGER = 0)
											AND a.folio = $iFolio limit 1;");
		return $query->result();
	}

	function consultaTelefonos($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT COALESCE('('||lada ||')'|| telefono,'') AS telefonotrab FROM coltelefonos WHERE tipocontacto = 159  AND folio =$iFolio;");
		return $query->result();
	}

	function consultaColgenerasolicitudregtrasp($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT folio,fechafirma,fechavigencia,fechaimpresion,nombre,paterno,materno,nss,curp,rfc,fechanacimiento,
		genero,ocupacion,actgironegocio,nacionalidad,nivelestudios,calle,colonia,ciudad,delegacionmunicipio,
		entidadfederativa,pais,email,numeroexterior,numerointerior,codigopostal,telefono1,tipotelefono1,
		telefono2,tipotelefono2,calletrab,coloniatrab,ciudadtrab,delegacionmunicipiotrab,entidadfederativatrab,
		paistrab,numeroexteriortrab,numerointeriortrab,codigopostaltrab,telefono1trab,tipotelefono1trab,
		TRIM(nombresref1)AS nombresref1,TRIM(paternoref1)AS paternoref1,TRIM(maternoref1)AS maternoref1,curpref1,telref1,parentescoref1,nombresref2,paternoref2,
		maternoref2,curpref2,telref2,parentescoref2,nombresprom,paternoprom,maternoprom,curpprom,
		folioconst,cons_implica,edo_cuenta,afore_cedente,numregistro,nombresbenf1,paternobenf1,
		maternobenf1,curpbenf1,porcentajebenef1,parentescobenef1,nombresbenf2,paternobenf2,maternobenf2,
		curpbenf2,porcentajebenef2,parentescobenef2,nombresbenf3,paternobenf3,maternobenf3,curpbenf3,
		porcentajebenef3,parentescobenef3,nombresbenf4,paternobenf4,maternobenf4,curpbenf4,porcentajebenef4,
		parentescobenef4,nombresbenf5,paternobenf5,maternobenf5,curpbenf5,porcentajebenef5,parentescobenef5,
		conexion FROM colgenerasolicitudregtrasp($iFolio);");
		
		return $query->result();
	}

	function curpColgenerasolicitudregtrasp($iFolio)
	{
		$query = $this->Pgaforeglobal->query("SELECT curp FROM colgenerasolicitudregtrasp($iFolio);");
		return $query->result();
	}

	function ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo)
	{
		$query = $this->Pgaforeglobal->query("SELECT tfolio FROM fnctrlimagenesportransmitir($iOpcion, $iFolio, '', 0, '$cNombreArchivo');");
		return $query->result();
	}
}
?>