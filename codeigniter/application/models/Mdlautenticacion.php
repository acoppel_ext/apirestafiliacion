<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdlautenticacion extends CI_Model {

    public function __construct()
		{
			parent::__construct();

			//load database library
			$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
			$this->Pgaforecoppel = $this->load->database("Pgaforecoppel",true);
			$this->Pgbustramites = $this->load->database("bustramites",true);
		}

	function Obtenertiposconstancia(){

		$query = $this->Pgaforeglobal->query("Select id, trim(descripcion) as descripcion from fnobtenercattipoconstancia()");
		return $query->result();
	}

	function guardarSolicitudine($datosValidar,$curp){

		$query = $this->Pgaforeglobal->query("Select fnregistrardatosenviarine as foliosolicitud From fnregistrardatosenviarine('$datosValidar','$curp')");
		return $query->result();
	}

	function guardarDatosIne($arrDatosIne)
	{
		$reenvio = $arrDatosIne["reenviointerno"];
		$ocr = $arrDatosIne["ocr"];
		$cic = $arrDatosIne["cic"];
		$apellidoPaterno = $arrDatosIne["apellidoPaterno"];
		$apellidoMaterno = $arrDatosIne["apellidoMaterno"];
		$nombre = $arrDatosIne["nombre"];
		$anioRegistro = $arrDatosIne["anioRegistro"];
		$numeroEmisionCredencial = $arrDatosIne["numeroEmisionCredencial"];
		$claveElector = $arrDatosIne["claveElector"];
		$curp = $arrDatosIne["curp"];
		$osName = $arrDatosIne['osName'] == "" || isset($arrDatosIne['osName']) == false ? 'Windows' : $arrDatosIne['osName'];

		if ($reenvio == 1)
		{
			$minucia2 = $arrDatosIne['esUtileria'] == 1 || $osName == 'Android' ? $arrDatosIne['minucia2'] : $this->obtenerMinucia($arrDatosIne['minucia7'],$reenvio);
			$minucia7 = $arrDatosIne['esUtileria'] == 1 || $osName == 'Android' ? $arrDatosIne['minucia7'] : $this->obtenerMinucia($arrDatosIne['minucia2'],$reenvio);
		}
		else
		{

			$minucia2 = $arrDatosIne['esUtileria'] == 1 || $osName == 'Android' ? $arrDatosIne['minucia2'] : $this->obtenerMinucia($arrDatosIne['minucia2'],$reenvio);
			$minucia7 = $arrDatosIne['esUtileria'] == 1 || $osName == 'Android' ? $arrDatosIne['minucia7'] : $this->obtenerMinucia($arrDatosIne['minucia7'],$reenvio);
		}
		$iEmpleado = $arrDatosIne["iEmpleado"];
		$ipModulo = $arrDatosIne["ipModulo"];
		$esUtileria = $arrDatosIne['esUtileria'];

		$query = $this->Pgaforeglobal->query("Select fnagregardatosapiine as iRespuesta From fnagregardatosapiine ('$curp','$ocr','$cic',
		'$nombre','$apellidoPaterno','$apellidoMaterno','$anioRegistro','$numeroEmisionCredencial',$iEmpleado,
		'$claveElector','$minucia7','$minucia2','$ipModulo',$esUtileria);");

		return $query->result();
	}

	function obtenerUbicacion($idTienda)
	{
		$query = $this->Pgaforecoppel->query("SELECT respuesta,latitud,longitud FROM fnobtenerubicaciontiendaine($idTienda);");
		return $query->result();
	}

	function auntenticacionIne($arrAutenticacion,$curp,$latitud,$longitud)
	{
		$arrDatos['estatus'] = 0;
		$arrDatos['descripcion'] = "";
		$sJson = "";

		$arrDatosEncriptados = $this->obtenerEncriptacion($arrAutenticacion,$latitud,$longitud);

		if($arrDatosEncriptados['verificacion'])
		{
			$minucia2 = $arrAutenticacion[0]->minucia2;
			$minucia7 = $arrAutenticacion[0]->minucia7;

			$sJson = $this->obtenerJson($arrDatosEncriptados,$minucia2,$minucia7,$latitud,$longitud);
			$this->obtenerJsonSinFirma($arrDatosEncriptados,$minucia2,$minucia7,$latitud,$longitud,$curp);

			$respuestaSolicitud = $this->guardarSolicitudine($sJson,$curp);

			foreach ($respuestaSolicitud as $key => $value) {
				$foliosolicitud = $value->foliosolicitud;
			}

			$arrDatos['foliosolicitud'] = (int)$foliosolicitud > 0 ? $foliosolicitud : "";
			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";
		}
		else
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $arrDatosEncriptados['descripcionVerificacion'];
		}

		return $arrDatos;
	}

	function obtenerJson($arrDatosEncriptados,$minucia2,$minucia7,$latitud,$longitud)
	{
		$certificado = $this->obtenerPropiedadesCertificado();
		$datosJSON['data'] = "";

		//Se crea la estructura que tendra el JSON.
		$datosJSON['data']['datosCifrados'] = $arrDatosEncriptados['datosCifrados'];
		$datosJSON['data']['minucias']['tipo'] = 2;
		$datosJSON['data']['minucias']['ancho'] = null;
		$datosJSON['data']['minucias']['alto'] = null;
		$datosJSON['data']['minucias']['minucia2'] = $minucia2;
		$datosJSON['data']['minucias']['minucia7'] = $minucia7;
		$datosJSON['data']['ubicacion']['posicionSatelital']['latitud'] = (float)$latitud;
		$datosJSON['data']['ubicacion']['posicionSatelital']['longitud'] = (float)$longitud;
		$datosJSON['data']['ubicacion']['localidad'] = null;
		/*$datosJSON['data']['ubicacion']['localidad']['codigoPostal'] = "80189";
		$datosJSON['data']['ubicacion']['localidad']['ciudad'] = "SINALOA";
		$datosJSON['data']['ubicacion']['localidad']['estado'] = 25;*/
		$datosJSON['data']['consentimiento'] = true;
		$datosJSON['signature']['signedInfo']['canonicalizationMethod']['algorithm'] = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
		$datosJSON['signature']['signedInfo']['signatureMethod']['algorithm'] = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
		$datosJSON['signature']['signedInfo']['reference']['digestMethod']['algorithm'] = "http://www.w3.org/2001/04/xmlenc#sha256";
		$datosJSON['signature']['signedInfo']['reference']['digestValue'] = $arrDatosEncriptados['hashFirma'];
		$datosJSON['signature']['signedInfo']['reference']['uri'] = "#DATA";
		$datosJSON['signature']['signatureValue'] = $arrDatosEncriptados['firmaDigital'];
		$datosJSON['signature']['keyInfo']['x509Data']['x509SerialNumber'] = dechex((int)$certificado["serialNumber"]);
		$datosJSON['timeStamp']['momento'] = $arrDatosEncriptados['momento'];
		$datosJSON['timeStamp']['indice'] = $arrDatosEncriptados['indice'];
		$datosJSON['timeStamp']['numeroSerie'] = "";

		$datosJSON = json_encode($datosJSON,JSON_UNESCAPED_SLASHES);
		$datosJSON = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $datosJSON);

		return $datosJSON;
	}
	
	function obtenerJsonSinFirma($arrDatosEncriptados,$minucia2,$minucia7,$latitud,$longitud,$curp)
	{
		$certificado = $this->obtenerPropiedadesCertificado();
		$datosJSON['data'] = "";

		//Se crea la estructura que tendra el JSON.
		$datosJSON['data']['datosCifrados'] = $arrDatosEncriptados['datosCifrados'];
		$datosJSON['data']['minucias']['tipo'] = 2;
		$datosJSON['data']['minucias']['ancho'] = null;
		$datosJSON['data']['minucias']['alto'] = null;
		$datosJSON['data']['minucias']['minucia2'] = $minucia2;
		$datosJSON['data']['minucias']['minucia7'] = $minucia7;
		$datosJSON['data']['ubicacion']['posicionSatelital']['latitud'] = (float)$latitud;
		$datosJSON['data']['ubicacion']['posicionSatelital']['longitud'] = (float)$longitud;
		$datosJSON['data']['ubicacion']['localidad'] = null;
		/*$datosJSON['data']['ubicacion']['localidad']['codigoPostal'] = "80189";
		$datosJSON['data']['ubicacion']['localidad']['ciudad'] = "SINALOA";
		$datosJSON['data']['ubicacion']['localidad']['estado'] = 25;*/
		$datosJSON['data']['consentimiento'] = true;
		$datosJSON['signature']['signedInfo']['canonicalizationMethod']['algorithm'] = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
		$datosJSON['signature']['signedInfo']['signatureMethod']['algorithm'] = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
		$datosJSON['signature']['signedInfo']['reference']['digestMethod']['algorithm'] = "http://www.w3.org/2001/04/xmlenc#sha256";
		$datosJSON['signature']['signedInfo']['reference']['digestValue'] = '';
		$datosJSON['signature']['signedInfo']['reference']['uri'] = "#DATA";
		$datosJSON['signature']['signatureValue'] = '';
		$datosJSON['signature']['keyInfo']['x509Data']['x509SerialNumber'] = dechex((int)$certificado["serialNumber"]);
		$datosJSON['timeStamp']['momento'] = $arrDatosEncriptados['momento'];
		$datosJSON['timeStamp']['indice'] = $arrDatosEncriptados['indice'];
		$datosJSON['timeStamp']['numeroSerie'] = "";

		$datosJSON = json_encode($datosJSON,JSON_UNESCAPED_SLASHES);
		$datosJSON = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $datosJSON);
		
		
		
		
	}

	function obtenerEncriptacion($arrAutenticacion,$latitud,$longitud)
	{
		$datosIne = "";
		$arrReturn = "";
		$datosTimeStamp = "";

		$private_key = PRIVATE_KEY;
		$public_key = PUBLIC_KEY;

		$datosIne['datosCifrados'] = "";
		$datosIne['minucias']['tipo'] = 2;
		$datosIne['minucias']['ancho'] = null;
		$datosIne['minucias']['alto'] = null;
		$datosIne['minucias']['minucia2'] = $arrAutenticacion[0]->minucia2;
		$datosIne['minucias']['minucia7'] = $arrAutenticacion[0]->minucia7;
		$datosIne['ubicacion']['posicionSatelital']['latitud'] = (float)$latitud;
		$datosIne['ubicacion']['posicionSatelital']['longitud'] = (float)$longitud;
		$datosIne['ubicacion']['localidad'] = null;
		/*$datosIne['ubicacion']['localidad']['codigoPostal'] = "80189";
		$datosIne['ubicacion']['localidad']['ciudad'] = "SINALOA";
		$datosIne['ubicacion']['localidad']['estado'] = 25;*/
		$datosIne['consentimiento'] = true;

		$datosTimeStamp['momento'] = "";
		$datosTimeStamp['indice'] = $arrAutenticacion[0]->curp.time();
		$datosTimeStamp['numeroSerie'] = "";

		//Se le pasa la llave publica como parametro. Y se obtienen los datos encriptados.
		$datosCifrados = $this->obtenerDatosEncriptados($public_key,$arrAutenticacion);
		$datosIne['datosCifrados'] = $datosCifrados;

		//Se hace la lectura del certificado para obtener sus propiedades.
		$certtificado = $this->obtenerPropiedadesCertificado();
		$datosTimeStamp['momento'] = $certtificado['validTo'];

		//Se obtiene el json de cada arreglo y se integran.
		$datosIne = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, json_encode($datosIne,JSON_UNESCAPED_SLASHES));

		$datosTimeStamp = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, json_encode($datosTimeStamp));
		$datosFirma = $datosIne.$datosTimeStamp;

		//Obtener la firma digital. Se pasa la llave privada y los datos que se van a firmar.
		$firmaDigital = $this->obtenerFirmaDigital($private_key,$datosFirma);

		//Obtener el hash de la firma digital.
		$hashFirma = hash('sha256', $firmaDigital, false);
		$firmaDigital = base64_encode($firmaDigital);

		//Verificamos si la firma es correcta.
		$arrVerificacion = $this->verificarFirma($datosFirma, $firmaDigital);

		//Armamos el arreglo que regresara los datos encriptados.
		$arrReturn['datosCifrados'] = $datosCifrados;
		$arrReturn['momento'] = $certtificado['validTo'];
		$arrReturn['indice'] = $arrAutenticacion[0]->curp.time();
		$arrReturn['numeroSerie'] ="";
		$arrReturn['firmaDigital'] = $firmaDigital;
		$arrReturn['hashFirma'] = $hashFirma;
		$arrReturn['verificacion'] = $arrVerificacion["verificacion"];
		$arrReturn['descripcionVerificacion'] = $arrVerificacion["descripcionVerificacion"];

		return $arrReturn;
	}

	function obtenerDatosEncriptados($public_key,$arrAutenticacion)
	{
		$sDatosCifrados = "";
		$objDatos=array(
			'ocr' => $arrAutenticacion[0]->ocr,
			'cic' => $arrAutenticacion[0]->cic,
			'apellidoPaterno' => $arrAutenticacion[0]->appaterno,
			'apellidoMaterno' => $arrAutenticacion[0]->apmaterno,
			'nombre' => $arrAutenticacion[0]->nombres,
			'anioRegistro' => $arrAutenticacion[0]->anioregistro,
			'anioEmision' => $arrAutenticacion[0]->anioemision,
			'numeroEmisionCredencial' => $arrAutenticacion[0]->numeroemisioncredencial,
			'claveElector' => $arrAutenticacion[0]->claveelector,
			'curp' => $arrAutenticacion[0]->curp
		);

		$objDatos = json_encode($objDatos);
		$objDatos = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $objDatos);

		openssl_public_encrypt($objDatos, $sDatosCifrados, $public_key,OPENSSL_PKCS1_OAEP_PADDING);

		$sDatosCifrados = base64_encode($sDatosCifrados);

		return $sDatosCifrados;
	}

	function obtenerFirmaDigital($private_key, $datosFirma)
	{
		$sFirma = "";

		openssl_sign($datosFirma, $sFirma, $private_key, "sha256");

		return $sFirma;
	}

	function obtenerPropiedadesCertificado()
	{
		$certificado = "";
		$certificate = CERTIFICATE;

		$certificado = openssl_x509_parse($certificate,false);

		return $certificado;
	}

	function verificarFirma($datosIne, $firmaDigital)
	{
		$verify = 0;
		$arrVerificacion["verificacion"] = "";
		$arrVerificacion["descripcionVerificacion"] = "";
		$public_key_verify = PUBLIC_KEY_VERIFY;

		$verify = openssl_verify($datosIne, base64_decode($firmaDigital), $public_key_verify,"sha256WithRSAEncryption");

		$verify = $verify == 1 ? true : false;
		$arrVerificacion["verificacion"] = $verify;

		if ($verify)
		{
			$arrVerificacion["descripcionVerificacion"] = "Se genero correctamente la firma";
		}
		else
		{
			$arrVerificacion["descripcionVerificacion"] = "Error al generar la firma";
		}

		return $arrVerificacion;
	}

	function obtenerRespuestaIne($folioSolicitud)
	{
		$query = $this->Pgaforeglobal->query("select respuesta,codigo,json From fnobtenerrespuestaine($folioSolicitud)");
		return $query->result();
	}

	function obtenerDatosIne($sCurp)
	{
		$query = $this->Pgaforeglobal->query("SELECT respuesta,
																					trim(ocr) as ocr,
																					trim(cic) as cic,
																					trim(nombres) as nombres,
																					trim(appaterno) as appaterno,
																					trim(apmaterno) as apmaterno,
																					trim(anioregistro) as anioregistro,
																					trim(anioemision) as anioemision,
																					trim(numeroemisioncredencial) as numeroemisioncredencial,
																					trim(claveelector) as claveelector,
																					trim(curp) as curp,
																					indiceizqwsq as minucia7,
																					indicederwsq as minucia2  FROM fntraerdatosine('$sCurp')");
		return $query->result();
	}

	function obtenerMinucia($sNombreArchivo,$reenvio)
	{
		$sRutaArchivo= "../../entrada/huellas/ine/".$sNombreArchivo;
		$sTexto= ""; 

		if(file_exists($sRutaArchivo))
		{
				$sFile = fopen($sRutaArchivo, "r");
				$sTexto = fgets($sFile);
				fclose($sFile);
				if($reenvio == 1)
				{
					unlink($sRutaArchivo);
				}
		}

    return $sTexto;
	}

	function limpiarhuellas($arrDatosIne)
	{

		$sNombreArchivo2 = $arrDatosIne['minucia2'];
		$sNombreArchivo7 = $arrDatosIne['minucia7'];

		$sRutaArchivo2= "../../entrada/huellas/ine/".$sNombreArchivo2;
		$sRutaArchivo7= "../../entrada/huellas/ine/".$sNombreArchivo7;
		$sTexto= "";
		$borrado = 0;

		if(file_exists($sRutaArchivo2))
		{
				$sFile = fopen($sRutaArchivo2, "r");
				fclose($sFile);

				unlink($sRutaArchivo2);
				$borrado++;

		}
		if(file_exists($sRutaArchivo7))
		{
				$sFile = fopen($sRutaArchivo7, "r");
				fclose($sFile);

				unlink($sRutaArchivo7);
				$borrado++;
		}

		return $borrado;
	}


	function obtenerTienda($ipModulo)
	{
		$query = $this->Pgaforeglobal->query("SELECT ipoffline,itienda,ctienda,iregion,cregion,icoordinacion, ccoordinacion FROM fnobtenerdatostiendarec('$ipModulo');");
		return $query->result();
	}

	function actualizaBitacoraIne($arrDatosIne)
	{
		$foliosolicitud = $arrDatosIne['foliosolicitud'];
		$curp = $arrDatosIne['curp'] == "" || $arrDatosIne['curp'] == 'false' ? 0 : 1;
		$ocr = $arrDatosIne['ocr'] == "" || $arrDatosIne['ocr'] == 'false' ? 0 : 1;
		$cic = $arrDatosIne['cic'] == "" || $arrDatosIne['cic'] == 'false' ? 0 : 1;
		$nombres = $arrDatosIne['nombres'] == "" || $arrDatosIne['nombres'] == 'false' ? 0 : 1;
		$apPaterno = $arrDatosIne['apPaterno'] == "" || $arrDatosIne['apPaterno'] == 'false' ? 0 : 1;
		$apMaterno = $arrDatosIne['apMaterno'] == "" || $arrDatosIne['apMaterno'] == 'false' ? 0 : 1;
		$anioRegistro = $arrDatosIne['anioRegistro'] == "" || $arrDatosIne['anioRegistro'] == 'false' ? 0 : 1;
		$anioEmision = $arrDatosIne['anioEmision'] == "" || $arrDatosIne['anioEmision'] == 'false' ? 0 : 1;
		$numeroEmisionCredencial = $arrDatosIne['numeroEmisionCredencial'] == "" || $arrDatosIne['numeroEmisionCredencial'] == 'false' ? 0 : 1;
		$claveElector = $arrDatosIne['claveElector'] == "" || $arrDatosIne['claveElector'] == 'false' ? 0 : 1;
		$consutawsIne = $arrDatosIne['consutawsIne'];
		$consultaServicioIne = $arrDatosIne['consultaServicioIne'];
		$similitudIndiceIzqwsq = $arrDatosIne['similitudIndiceIzqwsq'];
		$similitudIndiceDerwsq = $arrDatosIne['similitudIndiceDerwsq'];
		$fechaIneSalida = $arrDatosIne['fechaIneSalida'];
		$folioIne = $arrDatosIne['folioIne'];

		$query = $this->Pgaforeglobal->query("Select fnactualizadatosinews as respuesta	From fnactualizadatosinews($foliosolicitud,$curp::boolean,$ocr::boolean,$cic::boolean,$nombres::boolean,$apPaterno::boolean,$apMaterno::boolean,$anioRegistro::boolean,$anioEmision::boolean,$numeroEmisionCredencial::boolean,$claveElector::boolean,'$consutawsIne'::char(3),'$consultaServicioIne'::char(3),'$similitudIndiceIzqwsq'::char(6),'$similitudIndiceDerwsq'::char(6),'$fechaIneSalida'::timestamp,'$folioIne'::char(80))");
		return $query->result();
	}

	function obtenerHuellasUtileriaIne($curp)
	{
		$query = $this->Pgaforeglobal->query("Select keyx, huella From fnobtenerhuellasutileriaine('$curp')");
		return $query->result();
	}

	function ejecucionBD($Consulta, $BaseDatos)
	{
		$bd = $BaseDatos;
		$cSql = $Consulta;

		if($bd == 'AFOREGLOBAL'){
			$query = $this->Pgaforeglobal->query($cSql);
		}else if($bd == 'BUSTRAMITES'){
			$query = $this->Pgbustramites->query($cSql);
		}
		return $query->result();
	}
	
	function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  '/sysx/progs/afore/log/ApiAutenticacion-'.date("Y-m-d").".log";
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

    function getRealIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

		return $_SERVER['REMOTE_ADDR'];
    }
}
?>