<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdldocumentosmenuafore extends CI_Model {

    public function __construct()
	{
		parent::__construct();

		//load database library
		$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
		$this->PgAdmonAfore = $this->load->database("PgAdmonAfore",true);
	}

	function obtenerdocumentos($tipodoc,$ejecuta,$texto){

		$query = $this->PgAdmonAfore->query("select nombre, ruta, base from fnobtenerdocumentosmenuafore($tipodoc,$ejecuta,'$texto')");
		return $query->result();
	}

}
?>