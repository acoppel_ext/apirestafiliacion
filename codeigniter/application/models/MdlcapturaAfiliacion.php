<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlcapturaAfiliacion extends CI_Model {

		public function __construct()
		{
				parent::__construct();

				//load database library
				$this->Pgaforeglobal = $this->load->database("Pgaforeglobal",true);
				$this->bustramites = $this->load->database('bustramites', TRUE);
				$this->Informix = $this->load->database('Pgsafre', TRUE);
		}

		function obtnerinformacionpromotor($iEmpleado){

			$query = $this->Pgaforeglobal->query("SELECT icodigo,cmensaje,cnombre,ctienda,espromotor,curppromotor,claveconsar FROM fnConsultarEmpleadoConstancia($iEmpleado);");
			return $query->result();

		}

		function obtenertiposolicitud(){

			$query = $this->Pgaforeglobal->query("SELECT itiposolictud, itipotraspoaso, TRIM(cdescripcion) AS cdescripcion FROM fnobtenertiposolicitudafiliacion(2);");
			return $query->result();

		}
		function fnobtenerfolioine($foliosol){

			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnobtenerfolioine) AS ifolioine FROM fnobtenerfolioine($foliosol);");
			return $query->result();

		}
		function fnobtenerparaivr($sCurp, $folioine){

			$query = $this->Pgaforeglobal->query("SELECT telefono,codigoautenticacion FROM fnobneterdatosivr('$sCurp',$folioine);");
			return $query->result();

		}
		function fnobtenerLigaIVR($iopcionLigaine){

			$query = $this->Pgaforeglobal->query("SELECT TRIM(liga) AS liga FROM fnobtenerligasservicioine($iopcionLigaine);");
			return $query->result();

		}
		function obtenerDiagnosticoSolicitudSafre_af($foliosol){

			$query = $this->Informix->query("EXECUTE FUNCTION fn_Obtener_Diagnostico_Rechazo_Sol(".$foliosol.");");
			return $query->result();

		}
		function fnenvioIVR(){

			$query = $this->Pgaforeglobal->query("SELECT fnenviarporivr AS irespuesta FROM fnenviarporivr();");
			return $query->result();

		}
		function fnactualizaropcioncombo($sCurp, $opcioncombo){

			$query = $this->Pgaforeglobal->query("SELECT fnactualizaropcionenvio  AS irespuesta FROM fnactualizaropcionenvio('$sCurp',$opcioncombo);");
			return $query->result();

		}
		function validacionCurpAfiliacion($sCurp,$iTipoSol,$sIpRemoto){

			$query = $this->Pgaforeglobal->query("SELECT ierror, inss, cmensaje, credireccionar FROM fnvalidacionCurpAfiliacion20('$sCurp',$iTipoSol,'$sIpRemoto');");
			return $query->result();

		}

		function validarVideo($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnexistevideo FROM fnexistevideo($foliosol);");
			return $query->result();
		}

		function validarExpediente($sCurp,$sIpRemoto){

			$query = $this->Pgaforeglobal->query("SELECT error, mensaje, redireccionar FROM fnValidarExpedienteIdentificacionIntegrado('$sCurp','$sIpRemoto');");
			return $query->result();

		}

		function obtenerFolioEnrolamiento($foliosol){

			$query = $this->Pgaforeglobal->query("SELECT fnobtenerfolioenrolamientosolafiliacion AS folioenrolamiento FROM fnobtenerfolioenrolamientosolafiliacion($foliosol);");
			return $query->result();

		}

		function obtenerNombreAdudio($foliosol){

			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnobtenernombrecapturavoz) AS nombreaudio FROM fnobtenernombrecapturavoz(2, $foliosol);");
			return $query->result();

		}

		function actualizaAudioGrabado($folioenrol){

			$query = $this->Pgaforeglobal->query("SELECT fnactestatusenrolatrabajador FROM fnactestatusenrolatrabajador($folioenrol,0);");
			return $query->result();

		}

		function validarFolioSol($foliosol){

            $query = $this->Pgaforeglobal->query("SELECT COUNT(folio) AS valor FROM colsolicitudes WHERE folio = $foliosol;");
            return $query->result();

		}

		function cuentaConManos($foliosol){

			$query = $this->Pgaforeglobal->query("SELECT fnconsultarsinmanos2dosello AS respuesta FROM fnconsultarsinmanos2dosello($foliosol);");
			return $query->result();

		}

		function obtenerIPServidor($sIpRemoto){

			$query = $this->Pgaforeglobal->query("SELECT trim(ipofflinelnx) AS ipofflinelnx FROM fnObtenerIpCluster('$sIpRemoto');");
			return $query->result();

		}

		function guardarInfoAfiliacion($iOpcGuar,$foliosol,$iTipoSol,$esimss,$tipotrasp,$cFolioEdo,$cAforeEdo,$cCuatriEdo,$cMotivo,$cFolioImpli,$iTipoAdmon,$cDepPrev,$aforeced){

			$query = $this->Pgaforeglobal->query("SELECT fnguardardatosstmpcapturaafiliacion AS respuesta FROM fnguardardatosstmpcapturaafiliacion($iOpcGuar,$foliosol,$iTipoSol,$esimss,$tipotrasp,'$cFolioEdo','$cAforeEdo','$cCuatriEdo','$cMotivo','$cFolioImpli',$iTipoAdmon,'$cDepPrev',$aforeced);");
			return $query->result();

		}

		function obtenerPaginaRedireccionar($opcionpag,$foliosol,$iEmpleado,$sIpRemoto,$reimpre){

			$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, TRIM(descripcion) AS descripcionBoton, TRIM(destino) AS destino FROM fnobtenerligasolicitudafiliacion($opcionpag,$foliosol,$iEmpleado,'$sIpRemoto',$reimpre);");
			return $query->result();

		}

		function obtenerCurpSolcontancia($nss,$iTipoSol,$sIpRemoto){

			$query = $this->Pgaforeglobal->query("SELECT error, mensaje, redireccionar FROM fnvalidacionnssafiliacion('$nss',$iTipoSol,'$sIpRemoto');");
			return $query->result();

		}

		function consultarNSS($curp){

			$query = $this->Pgaforeglobal->query("SELECT fnobtenernssdesolconstanciaparaafiliacion AS nss from fnobtenernssdesolconstanciaparaafiliacion('$curp');");
			return $query->result();

		}

		function ConsultarSolicitudConstancia($sDato){

			$query = $this->Pgaforeglobal->query("SELECT tipoconstancia FROM ObtenerSolicitudSolconstancia('$sDato');");
			return $query->result();

		}
		//METODOS PARA LA CLASE CRetMotivoafiliacion.php
		function validarfolioSolicitud($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT folio, tiposolicitud, curp, esimss, tipotraspaso, folioedocta, aforeedocta, cuatrimestreedocta, motivo, folioimplicaciontraspaso, tipodeadmon, depositoprevio,folioprocesar FROM fnobtenerdatosstmpcapturaafiliacion03($foliosol);");
			return $query->result();
		}

		function llenarcbxMotivoRegistro()
		{
			$query = $this->Pgaforeglobal->query("SELECT id, TRIM(descripcion) AS descripcion FROM colcatmotivotraspaso;");
			return $query->result();
		}

		function llenarcbxTipoAdmin()
		{
			$query = $this->Pgaforeglobal->query("SELECT CAST( idcatalogo AS INT ) AS id, TRIM(deslarga) AS descripcion FROM ColInfxCatalogoGeneral WHERE idPadre::int = 284;");
			return $query->result();
		}

		function guardarDatosMotivoAfiliacion($opc,$foliosol,$tipoafi,$esimss,$tipotraspaso,$motregi,$folconstaimple,$iTipoAdmin,$depo)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnguardardatosstmpcapturaafiliacion AS respuesta FROM fnguardardatosstmpcapturaafiliacion($opc,$foliosol,$tipoafi,$esimss,$tipotraspaso,'', '', '','$motregi','$folconstaimple',$iTipoAdmin,'$depo',0);");
			return $query->result();
		}

		function guardarSolicitudBD($foliosol, $iEmpleado, $sIpRemoto, $fecha)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnguardarsolicitudafiliacion AS respuesta FROM fnguardarsolicitudafiliacion($foliosol,$iEmpleado,$iEmpleado,'$sIpRemoto','$fecha');");
			return $query->result();
		}

		//METODOS PARA LA CLASE CTipoTraspasoAfiliacion
		function obtenerCURP($foliosol){

			$query = $this->Pgaforeglobal->query("SELECT TRIM(curp) AS curp FROM solconstancia WHERE folio = $foliosol;");
			return $query->result();

		}

		function comboTipoTraspaso(){

			$query = $this->Pgaforeglobal->query("SELECT clavec AS clave, TRIM(desccomp) AS descripcion FROM fnobtenertipotraspasoafiliacion();");
			return $query->result();

		}

		function comboAforeCedente(){

			$query = $this->Pgaforeglobal->query("SELECT clavec AS clave, TRIM(desccomp) AS descripcion FROM fnobteneraforesafiliacion();");
			return $query->result();

		}

		function comboPeriodoEstado(){

			$query = $this->Pgaforeglobal->query("SELECT validar AS clave, cuatrimestre FROM fnobtenerCuatrimestresEdoCuenta();");
			return $query->result();

		}

		function ObtenerNumeroTarjeta($sIpRemoto){

			$query = $this->Pgaforeglobal->query("SELECT fnobtenerNumeroTarjetaAfiliacion AS respuesta FROM fnobtenerNumeroTarjetaAfiliacion('$sIpRemoto');");
			return $query->result();

		}

		function guardarNumeroTarjeta($foliosol,$numtarjeta){

			$query = $this->Pgaforeglobal->query("SELECT fnGuardarNumTarjetaTraspaso AS respuesta FROM fnGuardarNumTarjetaTraspaso($foliosol,'$numtarjeta');");
			return $query->result();

		}

		//METODOS PARA LA CLASE CRetConsultaSolicitudesAfiliacion
		function obtenerSolicitudAfiliacion($foliosol,$curp,$nss,$nocliente)
		{
			$cSql = "";
			$sAuxSql = "";

			if ($foliosol != 0)
			$cSql = $cSql." AND folio IN (".$foliosol.") ";

			if ($curp != "")
			$cSql = $cSql." AND curp = $$".$curp."$$ ";

			if ($nss != 0)
			$cSql = $cSql." AND nss = $$".$nss."$$ ";

			if ($nocliente != "")
			$cSql = $cSql." AND cliente IN ($$".$nocliente."$$) ";

			$sAuxSql    = substr($cSql,5,150);
			$cSql       = "SELECT DISTINCT(folio) FROM colsolicitudes WHERE " . $sAuxSql . " ORDER BY folio LIMIT 11 OFFSET 0;";
			$cSql       = "SELECT folio,fechacaptura,tipo,digitalizada,estado,nombrecliente,curp,nss,nocliente,promotor,tienda FROM fnconsultasolicitudesafiliacion('$cSql');";

			$query = $this->Pgaforeglobal->query($cSql);
			return $query->result();
		}

		function obtenerDatosSolicitudAfiliacionGeneral($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT error,finado,fechasolicitud,tiposolicitud,tiposolicituddesc,folio,foliodetraspaso,estatus,apaterno,amaterno,nombres,fechanac,sexo,curp,nss,rfc,nacionalidad,entidadnac,estadocivil,tipodomicilio,compdomicilio,prioridad,email,ocupacionprof,activinegocio,nivelestudio,aportaciones,comentarios,huelladigital  FROM fnobtenerdatosconsultasolicitudafiliaciongenerales('$foliosol');");
			return $query->result();
		}

		function obtenerDomicilio($foliosol,$tipodom)
		{
			$query = $this->Pgaforeglobal->query("SELECT tipodom,calle,numext,codpostal,estado,ciudad,numint,pais,delegnmun,colonia FROM fnobtenerdatosconsultasolicitudafiliaciondomicilio($foliosol,$tipodom);");
			return $query->result();
		}

		function obtenerComprobanteDomicilio($compdom)
		{
			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnobtenerdatosconsultasolicitudafiliacioncomprobantedom) AS comprobante FROM fnobtenerdatosconsultasolicitudafiliacioncomprobantedom($compdom);");
			return $query->result();
		}

		function obtenerDatosSolicitudAfiliacionTelefonos($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT tipocontacto,lada,telefono,extensiont FROM fnobtenerdatosconsultasolicitudafiliaciontelefono($foliosol);");
			return $query->result();
		}

		function obtenerOcupacionProf($ocupacionprof)
		{
			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnobtenerdatosconsultasolicitudafiliacionocupacionprof) AS ocupacion FROM fnobtenerdatosconsultasolicitudafiliacionocupacionprof($ocupacionprof);");
			return $query->result();
		}

		function obtenerActividadGiro($activinegocio)
		{
			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnobtenerdatosconsultasolicitudafiliacionactividadgiro) AS actividad FROM fnobtenerdatosconsultasolicitudafiliacionactividadgiro($activinegocio);");
			return $query->result();
		}

		function obtenerDatosSolicitudAfiliacionBeneficiarios($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT apaterno,amaterno,nombres,porcentaje,parentesco FROM fnobtenerdatosconsultasolicitudafiliacionbeneficiarios($foliosol);");
			return $query->result();
		}

		function obtenerDiagnosticoSolicitud($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnObtenerDiagnosticoRechazoSolicitud) AS diagnosticosolicitud FROM fnObtenerDiagnosticoRechazoSolicitud($foliosol);");
			return $query->result();
		}

		function usarConexionInformix()
		{
			$query = $this->Pgaforeglobal->query("SELECT Tipo2 AS respuesta FROM pafHardCode WHERE Tipo1 = 115;");
			return $query->result();
		}

		function obtenercodigoHTML()
		{
			$query = $this->Pgaforeglobal->query("SELECT trim(html) AS html, tiempo FROM fnconsultarhtmlmateriafinanciera();");
			return $query->result();
		}

		function obtenerligapagina($iOpcionPagina, $iFolio, $iEmpleado, $sIp)
		{
			$query = $this->Pgaforeglobal->query("SELECT tipoaplicativo, trim(descripcion) AS descripcionboton, TRIM(destino) AS destino FROM fnobtenerligasolicitudafiliacion($iOpcionPagina, $iFolio, $iEmpleado, '$sIp', 0 );");
			return $query->result();
		}

		function removerCaracteresEspeciales($arrDatos = array(),$iTipo = 1)
		{
					/*iTipo = 1 es para eliminar los caracteres especiales a los Mensajes que arroja
					*/

					/**
					 * iTipo = 1 es para eliminar los caracteres especiales a los Mensajes que arroja
					 * iTipo = 2 es para elminar los caracteres especiales de los nombres de las personas
					 */

					$search = array();
					$replace = array();

					$search = array('&', '<', '>', '€', '‘', '’', '“', '”', '–', '—', '¡', '¢','£', '¤', '¥', '¦', '§', '¨', '©', 'ª', '«', '¬', '®', '¯', '°', '±', '²', '³', '´', 'µ', '¶', '·', '¸', '¹', 'º', '»', '¼', '½', '¾', '¿', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã','ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ','Œ', 'œ', '‚', '„', '…', '™', '•', '˜');

					$replace  = array('&amp;', '&lt;', '&gt;', '&euro;', '&lsquo;', '&rsquo;', '&ldquo;','&rdquo;', '&ndash;', '&mdash;', '&iexcl;','&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;','&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;','&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&OElig;', '&oelig;', '&sbquo;', '&bdquo;', '&hellip;', '&trade;', '&bull;', '&asymp;');

					switch ($iTipo) {
							case 1:
											foreach ($arrDatos['registros'] as $key => $object)
											{
													$sMensaje = $object->mensaje;

													$sMensaje = str_replace($search, $replace, utf8_encode($sMensaje));

													$object->mensaje = $sMensaje;
											}
									break;
							case 2:
											foreach ($arrDatos['registros'] as $key => $object)
											{

													$sApellidopaterno = $object->apellidopaterno;
													$sApellidomaterno = $object->apellidomaterno;
													$sNombres = $object->nombres;

													$sApellidopaterno = str_replace($search, $replace, utf8_encode($sApellidopaterno));
													$sApellidomaterno = str_replace($search, $replace, utf8_encode($sApellidomaterno));
													$sNombres = str_replace($search, $replace, utf8_encode($sNombres));

													$object->apellidopaterno = $sApellidopaterno;
													$object->apellidomaterno = $sApellidomaterno;
													$object->nombres = $sNombres;
											}
									break;
							default:
									$arrDatos['descripcion'] = "Error al consultar el metodo removerCaracteresEspeciales";
									$arrDatos['estatus'] = -1;
									break;
					}
					return $arrDatos;
		}

		function fnexistenciafolioprocesar($foliosol,$foliosolpro)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnexistenciafolioprocesar01 AS irespuesta FROM fnexistenciafolioprocesar01(1,$foliosol,'$foliosolpro'::CHARACTER(6));");
			return $query->result();
		}

		function existenciafechaProcesar($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnvalidavigenciafolioregtrasp AS irespuesta FROM fnvalidavigenciafolioregtrasp($foliosol);");
			return $query->result();
		}

		function tieneexcepcion($iFolioSol)
		{
			$query = $this->Pgaforeglobal->query("SELECT iexcepcion AS respuesta from fnobtenertiposolicitanteexcepcion($iFolioSol);");
			return $query->result();
		}

		//METODOS DEL FOLIO 654

		function validacionine($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnvalidacionauine AS respuesta FROM fnvalidacionauine($foliosol);");
			return $query->result();
		}

		function fnvalidavigenciasemilla($foliosol,$foliosemilla)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnvalidarcodigoautenticacion AS irespuesta FROM fnvalidarcodigoautenticacion($foliosol,'$foliosemilla');");
			return $query->result();
		}

		function fnobtenercurpconstancia($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT TRIM(fnobtenercurpconstancia) AS curp FROM fnobtenercurpconstancia($foliosol);");
			return $query->result();
		}

		function generarFolioIne($sCurp,$foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT fngeneracioncodigoautenticacionxfolio as codigoautenticacion from fngeneracioncodigoautenticacionxfolio('$sCurp',$foliosol);");
			return $query->result();
		}

		function fnvalidastatusenvioine($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnvalidastatusenviosms AS irespuesta FROM fnvalidastatusenviosms($foliosol);");
			return $query->result();
		}

		function fnejecutapy()
		{
			$query = $this->Pgaforeglobal->query("SELECT fnejecutapy FROM fnejecutapy();");
			return $query->result();
		}

		function actualizarfolioenrolamiento($iFolioSolicitud, $iFolioEnrolamiento)
		{
			$query = $this->Pgaforeglobal->query("UPDATE solconstancia SET folioenrolamiento=$iFolioEnrolamiento WHERE folio=$iFolioSolicitud;");
			return $query;
		}

		function actualizarTipoConexion($folio,$tipoConexion)
		{
			$query = $this->Pgaforeglobal->query("UPDATE dispositivoAfiliacion SET tipoconexion = $tipoConexion WHERE folio=$folio;");
			return $query;
		}
		//folio 877

		function configuracionconsultafolioafi()
		{
			$sQuery = "SELECT iIntentos, iTiempo FROM fnconfiguracionconsultafolioafiliacion();";
			$query 	= $this->Pgaforeglobal->query($sQuery);
			log_message('error', "FUNCTION-> $sQuery");
			return $query->result();
		}

		function actualizarconsultaregistra($iFoliosol, $cCurp, $iAccion, $cFolioprocesar)
		{
			$sQuery = "SELECT fnactualizarconsultaregtra AS respuesta FROM fnactualizarconsultaregtra($iFoliosol, '$cCurp', $iAccion, '$cFolioprocesar');";
			$query 	= $this->Pgaforeglobal->query($sQuery);
			log_message('error', "FUNCTION-> $sQuery");
			return $query->result();
		}

		function validardatosconsultafolioafiliacion($iKeyx)
		{
			$sQuery = "SELECT respuesta, TRIM(diagnostico) AS diagnostico, TRIM(mensaje) AS mensaje, TRIM(cMensajeError) AS mensajeerror, TRIM(accion) AS accion FROM fnvalidarconsultafolioafiliacion($iKeyx);";
			$query 	= $this->Pgaforeglobal->query($sQuery);
			log_message('error', "FUNCTION-> $sQuery");
			return $query->result();
		}

		function agregardatosconsultafolioafiliacion($cCurp, $cClaveaforegenerofolio, $iTipofolio, $cFolioregtrasp, $iFoliosolicitud)
		{
			$sQuery = "SELECT fnagregardatosconsultafolioafiliacion AS respuesta FROM fnagregardatosconsultafolioafiliacion ('$cCurp', '$cClaveaforegenerofolio', $iTipofolio, '$cFolioregtrasp', $iFoliosolicitud);";
			$query 	= $this->Pgaforeglobal->query($sQuery);
			log_message('error', "FUNCTION-> $sQuery");
			return $query->result();
		}

		public function obtenerXML($array)   
		{        
			$xml = ""; 
			$xml = "<map>";  
			foreach($array as $key => $value)
			{ 
			$mykey = $key; 
			$myvalue= $value;  
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";   
			}
			$xml .= "</map>";        
			$mensaje = 'XML-> ' . $xml;
			log_message('error', $mensaje);
			return $xml;
		}

		function servicioEjecutarAplicacion($idServicio)
		{
			$query = $this->bustramites->query("select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio)");
			return $query->result();
			$mensaje = 'Consulta-> ' . $query;
			log_message('error', $mensaje);
		}
		function obTenercurpTitular($iFolio)
		{
			//$query = $this->Pgaforeglobal->query("UPDATE solconstancia SET folioenrolamiento=$iFolioEnrolamiento WHERE folio=$iFolioSolicitud;");
			$query = $this->Pgaforeglobal->query("SELECT curp as respuesta FROM solconstancia where folio = $iFolio limit 1;");
			return $query->result();
		}

		function fnobtenersemillaafiliacion($foliosol)
		{
			$query = $this->Pgaforeglobal->query("SELECT fnobtenersemillaafiliacion AS semilla FROM fnobtenersemillaafiliacion($foliosol);");
			return $query->result();
		}
		function ValidaConsultaRenapoPrevia($sCurp, $iEsTitular)
		{
			$query = $this->Pgaforeglobal->query("SELECT fun_verificarintentosrenapo AS renapo FROM fun_verificarintentosrenapo('$sCurp',$iEsTitular);");
			return $query->result();
		}
		function obtenerCurpBeneficiario($sCurp)
		{
			$query = $this->Pgaforeglobal->query("SELECT fun_obtenerfolioafiliacion AS curpsolicitante FROM fun_obtenerfolioafiliacion(2,'$sCurp');");
			return $query->result();
		}
}
?>