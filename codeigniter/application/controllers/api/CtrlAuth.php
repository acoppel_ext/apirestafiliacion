<?php

if (!defined("BASEPATH")) exit("No direct script access allowed");
//include Rest Controller library
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . '/controllers/util/Util.php');

/**
 * Registro y Eliminacion de token
 */
class CtrlAuth extends Util
{

    public function __construct()
    {  
        // API Configuration
        parent::__construct();
        $this->load->model("MdlAuth");
        
        $cadena = "\r\n-Controlador: " .  $this->uri->segment(2) .
        "\r\n-Metodo: " .  $this->uri->segment(3) . 
        "\r\n-Datos: " .json_encode($this->post(), JSON_UNESCAPED_SLASHES);

        $this->grabarLogx($cadena,"1");
       
    }

    /**
     * Registro de token (INICIO DE SESION)
     * @method POST
     * @return Response|void
     */
    public function RegistroToken_post()
    {

        // Cargar biblioteca de autorizaciones o cargar en el archivo 
        // de configuración de carga automática
        $this->load->library("Authorization_Token"); 
        $configaraciones = simplexml_load_file("../../conf/apiconfig.xml");
        $SesionesMultiples = $configaraciones->SesionesMultiples;
        $tienda = $this->obtenerTienda($this->post("codigoEmpleado"));

        $data = [
            "codigoEmpleado" => $this->post("codigoEmpleado"),
            "data" => date("Y-m-d h:i:s a"),
            "tienda" => $tienda
        ];
        
        if ($SesionesMultiples == 0 &&  $this->MdlAuth->verificarSesionCodigo($data["codigoEmpleado"])) {
            $this->response(
                [
                    "status" => false,
                    "result" => [
                        "codigoEmpleado" => $this->post("codigoEmpleado"),
                        "token" => "",
                        "session" => 1,
                    ],
                    "estatus" => 0,
                    "tipo" => "token",
                    "descripcion" => "El promotor ya ha iniciado sesión",
                ],
                200
            );
            
        } else {
           
            // generar  token
            $token = $this->authorization_token->generateToken($data);


            try {
                $this->load->library("Encriptar_Token"); 
                $respuesta = $this->MdlAuth->guardarRegistro($data["codigoEmpleado"], $token, $tienda);
                if ($respuesta == 1) {

                    if($this->post("modulo") == 1){
                        $token = $this->encriptar_token->encrypt($token, $this->config->item('encryption_key'));
                    }

                     $this->response(
                        [
                            "status" => true,
                            "result" => [
                                "codigoEmpleado" => $this->post("codigoEmpleado"),
                                "token" =>  $token,
                                "session" => 0
                            ],
                            "estatus" => 1,
                            "tipo" => "token",
                            "descripcion" => "El promotor ha iniciado sesión",
                        ],
                        200
                    );
                } else {
                    $this->response(
                        [
                            "status" => false,
                            "result" => [
                                "token" => "Intente de nuevo",
                                "session" => 0
                            ],
                            "estatus" => 0,
                            "tipo" => "token",
                            "descripcion" => "Intente de nuevo",
                        ],
                        400
                    );
                }
            } catch (Exception $mensaje) {
                $this->response(
                    [
                        "status" => false,
                        "result" => [
                            "token" => $mensaje,
                            "session" => 0
                        ],
                        "estatus" => 0,
                        "tipo" => "token",
                        "descripcion" => $mensaje,
                    ],
                    500
                );
            }
        }
    }

    /**
     * Cerrar sesion (CERRAR SESION)
     * @method POST
     * @return Response|void
     */
    public function eliminarToken_post()
    {
        $configaraciones = simplexml_load_file("../../conf/apiconfig.xml");
        $SesionesMultiples = $configaraciones->SesionesMultiples;

        try {
            $data = [

                "codigoEmpleado" => $this->post("codigoEmpleado")

            ];

            if ($SesionesMultiples == 0) {

                $respuesta =  $this->MdlAuth->eliminarToken($data["codigoEmpleado"]);
            } else {

                $respuesta =  $this->MdlAuth->eliminarTokenPorToken(getallheaders()["Authorization"]);
            }

            if ($respuesta) {

                $this->response(
                    [
                        "status" => 1,
                        "result" => $respuesta,
                        "estatus" => 1,
                        "tipo" => "token",
                        "descripcion" => "Token eliminado",
                    ],

                    200
                );
            } else {
                $this->response(
                    [
                        "status" => 0,
                        "result" =>  "Intente de nuevo",
                        "estatus" => 0,
                        "tipo" => "token",
                        "descripcion" => "Intente de nuevo",
                    ],
                    400
                );
            }
        } catch (Exception $mensaje) {
            $this->response(
                [
                    "status" => 0,
                    "result" => $mensaje,
                    "estatus" => 0,
                    "tipo" => "token",
                    "descripcion" => $mensaje,
                ],
                500
            );
        }
    }
}
