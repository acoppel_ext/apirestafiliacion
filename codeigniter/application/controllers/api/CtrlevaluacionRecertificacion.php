<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlEvaluacionRecertificacion extends Util
{

    // SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
    private $result = array();

    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }

        parent::__construct();

        $componente = (!$this->post('componente')) ? false : true;
        $this->result = $this->verificacionPermisos($componente, "");


        //load models
        $this->load->model('MdlevaluacionRecertificacion');
    }

    public function eliminarCuestionarioServicio_post()
    {
        $arrDatos = array();
        $folioServicio = $this->post('folioServicio');

        try {
            $response = $this->MdlevaluacionRecertificacion->eliminarCuestionarioServicio($folioServicio);
            $arrDatos = successforeachSinObject($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function obtenerPreguntasCuestionario_post()
    {
        $arrDatos = array();
        $number = $this->post('number');

        try {
            $response = $this->MdlevaluacionRecertificacion->obtenerPreguntasCuestionario($number);
            $arrDatos = successforeachSinObject($response);
            if ($arrDatos['estatus'] != 1) {
                $arrDatos = convertircaracteresespeciales($arrDatos, 'descripcionpreguntas');
            }
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function obtenerRespuesCuestionarioServicio_post()
    {
        $arrDatos = array();
        $number = $this->post('numeroPregunta');

        try {
            $response = $this->MdlevaluacionRecertificacion->obtenerRespuesCuestionarioServicio($number);
            $arrDatos = successforeachSinObject($response);
            if ($arrDatos['estatus'] != 1) {
                $arrDatos = convertircaracteresespeciales($arrDatos, 'descripcionrespuestas');
            }
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function consultarEmpleadoConstancia_post()
    {
        $arrDatos = array();
        $number = $this->post('numeroEmpleado');

        try {
            $response = $this->MdlevaluacionRecertificacion->consultarEmpleadoConstancia($number);
            $arrDatos = successforeach($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function guardarInformacionEvaluacionRecertificacion_post()
    {
        $arrDatos = array();
        $folioServicio = $this->post('folioServicio');
        $idPregunta  = $this->post('idPregunta');
        $idRespuesta = $this->post('idRespuesta');
        $idCajaTexto = $this->post('idCajaTexto');

        try {
            $response = $this->MdlevaluacionRecertificacion->guardarInformacionEvaluacionRecertificacion($folioServicio, $idPregunta, $idRespuesta, $idCajaTexto);
            $arrDatos = successforeachSinObject($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function obtenerLigaRecert_post()
    {
        $arrDatos = array();
        $origen = $this->post('origen');
        $origenPagina  = $this->post('origenPagina');
        $ipRemoto = $this->post('ipRemoto');
        $imprimir = $this->post('imprimir');
        $rutaPdf = $this->post('rutaPdf');
        $folioSol  = $this->post('folioSol');
        $empleado = $this->post('empleado');
        $folioServicio = $this->post('folioServicio');
        $reguieReeb = $this->post('reguieReeb');
        $servicio  = $this->post('servicio');
        $curp = $this->post('curp');
        $consultaEdoc = $this->post('consultaEdoc');
        $tipoImpresion = $this->post('tipoImpresion');
        $tipoAcuse  = $this->post('tipoAcuse');
        $siAfore = $this->post('siAfore');

        try {
            $response = $this->MdlevaluacionRecertificacion->obtenerLigaRecert($origen, $origenPagina, $ipRemoto, $imprimir, $rutaPdf, $folioSol, $empleado, $folioServicio, $reguieReeb, $servicio, $curp, $consultaEdoc, $tipoImpresion, $tipoAcuse, $siAfore);
            $arrDatos = successforeachSinObject($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function datosRecertificacion_post()
    {
        $arrDatos = array();
        $origen = $this->post('origen');
        $folioServicio = $this->post('folioServicio');
        $estatusRecer = $this->post('estatusRecer');
        $correoImpresion  = $this->post('correoImpresion');
        $correo = $this->post('correo');

        try {
            $response = $this->MdlevaluacionRecertificacion->datosRecertificacion($origen, $folioServicio, $estatusRecer, $correoImpresion, $correo);
            $arrDatos = successforeachSinObject($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

}
