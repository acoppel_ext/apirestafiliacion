<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');
class ctrlautenticacion extends Util
{
	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");

		$this->load->model('Mdlautenticacion');
		$this->load->model('MdlMetodosGenerales');
	}

	public function Obtenertiposconstancia_post()
	{
		$arrDatos = array();

		try {

			$response = $this->Mdlautenticacion->Obtenertiposconstancia();
			$arrDatos = successforeach($response);
			
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarSolicitudine_post()
	{
		$arrDatos = array();

		try {

			$datosValidar = $this->post('datosValidar');
			$response = $this->Mdlautenticacion->guardarSolicitudine($datosValidar);
			$arrDatos = successforeach($response);

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarDatosIne_post()
	{
		$arrDatos = array();
		$arrDatosIne = array(
			'reenviointerno' => $this->post('reenviointerno'),
			'ocr' => $this->post('ocr'),
			'cic' => $this->post('cic'),
			'apellidoPaterno' => $this->post('apellidoPaterno'),
			'apellidoMaterno' => $this->post('apellidoMaterno'),
			'nombre' => $this->post('nombre'),
			'anioRegistro' => $this->post('anioRegistro'),
			'numeroEmisionCredencial' => $this->post('numeroEmisionCredencial'),
			'claveElector' => $this->post('claveElector'),
			'curp' => $this->post('curp'),
			'minucia2' => $this->post('minucia2'),
			'minucia7' => $this->post('minucia7'),
			'iEmpleado' => $this->post('iEmpleado'),
			'ipModulo' => $this->post('ipModulo'),
			'esUtileria'  => $this->post('esUtileria') == "" || $this->post('esUtileria') == 0 ? 0 : $this->post('esUtileria'),
			'osName' => $this->post('osName')
		);

		try {
			$response = $this->Mdlautenticacion->guardarDatosIne($arrDatosIne);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['registros'][] = $response;
			} else {
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error al ejecutar la consulta";
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function auntenticacionIne_post()
	{
		$arrDatos = array();
		$curp = $this->post('curp');
		$numEmpleado = $this->post('numEmpleado');
		$latitud = $this->post('latitud');
		$longitud = $this->post('longitud');

		try {
			$arrAutenticacion = $this->Mdlautenticacion->obtenerDatosIne($curp);

			if ($arrAutenticacion[0]->respuesta == 1) {
				$response = $this->Mdlautenticacion->auntenticacionIne($arrAutenticacion, $curp, $latitud, $longitud);
				$foliosolicitud = (int) $response['foliosolicitud'];

				if ($foliosolicitud > 0) {
					/*********************************************************************************
					 * Se envia el ID del servicio para la detonacion del WS
					 * Se arma el XML con los datos pertinentes del WS
					 * Se obtiene la URL del WS
					 * Se detona el SOAP con el xml
					 * Retonar la respuesta del WS
					 *********************************************************************************/
					$idServicio 	= 9943; //ID WS Autenticacion INE
					$arrDatosXML 	= array(
						'folioServicio' 	=> $foliosolicitud,
						'curp' 				=> $curp,
						'numeroEmpleado' 	=> $numEmpleado
					);
					$responseWS = $this->MdlMetodosGenerales->ejecutarWS($idServicio, $arrDatosXML);
					$responseWS->foliosolicitud = $foliosolicitud;
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';
					$arrDatos['registros'] = $responseWS;
				} else {
					$arrDatos['estatus'] = -1;
					$arrDatos['descripcion'] = "Error al obtener el folio de solicitud";
				}
			} else {
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error al obtener el folio de solicitud";
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerRespuestaIne_post()
	{
		$arrDatos = array();
		$folioSolicitud = $this->post('foliosolicitud');

		try {

			$response = $this->Mdlautenticacion->obtenerRespuestaIne($folioSolicitud);
			$arrDatos = successSinforeachSinObject($response);

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function actualizaBitacoraIne_post()
	{
		$arrDatos = array();
		$arrDatosIne = array(
			'foliosolicitud' => $this->post('foliosolicitud'),
			'curp' => $this->post('curp'),
			'ocr' => $this->post('ocr'),
			'cic' => $this->post('cic'),
			'nombres' => $this->post('nombres'),
			'apPaterno' => $this->post('apPaterno'),
			'apMaterno' => $this->post('apMaterno'),
			'anioRegistro' => $this->post('anioRegistro'),
			'anioEmision' => $this->post('anioEmision'),
			'numeroEmisionCredencial' => $this->post('numeroEmisionCredencial'),
			'claveElector' => $this->post('claveElector'),
			'consutawsIne' => $this->post('consutawsIne'),
			'consultaServicioIne' => $this->post('consultaServicioIne'),
			'similitudIndiceIzqwsq' => $this->post('similitudIndiceIzqwsq'),
			'similitudIndiceDerwsq' => $this->post('similitudIndiceDerwsq'),
			'fechaIneSalida' => $this->post('fechaIneSalida'),
			'folioIne' => $this->post('folioIne')
		);

		try {

			$response = $this->Mdlautenticacion->actualizaBitacoraIne($arrDatosIne);
			$arrDatos = successSinforeachSinObject($response);

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtenerUbicacion_post()
	{
		$arrDatos = array();
		$idTienda = $this->post('idTienda');

		try {

			$response = $this->Mdlautenticacion->obtenerUbicacion($idTienda);
			$arrDatos = successSinforeachSinObject($response);

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtenerTienda_post()
	{
		$arrDatos = array();
		$ipModulo = $this->post('ipModulo');

		try {

			$response = $this->Mdlautenticacion->obtenerTienda($ipModulo);
			$arrDatos = successSinforeachSinObject($response);

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtenerHuellasUtileriaIne_post()
	{
		$arrDatos = array();
		$curp = $this->post('curp');

		try {

			$response = $this->Mdlautenticacion->obtenerHuellasUtileriaIne($curp);
			$arrDatos = successSinforeachSinObject($response);

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function limpiarhuellas_post()
	{
		$arrDatos = array();
		$arrDatosIne = array(
			'minucia2' => $this->post('minucia2'),
			'minucia7' => $this->post('minucia7')
		);
		try {
			$response = $this->Mdlautenticacion->limpiarhuellas($arrDatosIne);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['borrado'] = $response;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = "ERROR";
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function python_post()
	{
		
		try {
			$folioserv = $this->post('folioserv');
			$output = array();

			$resultado = exec("/bin/python /sysx/progs/afore/enviosmsine/enviosmsine.py " . $folioserv . " 2", $output);

			if ($resultado == 1) {
				$respuesta = 1;
			} else {
				$respuesta = 0;
			}
		} catch (Exception $mensaje) {
			$respuesta = -1;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function leerIP_post()
	{
		
		try {
			if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
				$resultado = $_SERVER["HTTP_CLIENT_IP"];
			} else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
				$resultado = $_SERVER["HTTP_X_FORWARDED_FOR"];
			} else {
				$resultado = $_SERVER["REMOTE_ADDR"];
			}
		} catch (Exception $mensaje) {
			$resultado = "";
		}

		$this->response($resultado, REST_Controller::HTTP_OK);
	}

	public function llamarivr_post()
	{
		$url = $this->post('url');

		try {
			$res = file_get_contents($url);

		} catch (Exception $mensaje) {
			$res = -1;
		}

		$this->response($res, REST_Controller::HTTP_OK);
	}

	public function ejecucionBD_post()
	{
		$arrDatos = array();
		define("DEFAULT__", 		0);
		define("CNXEXP__", 			-3);
		define("CONSOK__", 			4);
		define("CONSERR__", 		-5);

		$idCons = $this->post('idCons');
		$Consulta = $this->post('Consulta');
		$BaseDatos = $this->post('BaseDatos');

		$arrDatos = array("irespuesta" => DEFAULT__, $idCons => null);

		try {

			$response = $this->Mdlautenticacion->ejecucionBD($Consulta, $BaseDatos);

			if ($response) {

                $arrDatos['estatus']  = 1;
				$arrDatos['descripcion']   = "EXITO";
				$arrDatos["irespuesta"] = CONSOK__;
                
                foreach ($response as $reg) {
                    $arrDatos[$idCons][] = $reg;
                }
            } else {
                $arrDatos['estatus']  = -1;
                $arrDatos['descripcion'] = "Error al ejecutar la consulta";
                $arrDatos["irespuesta"] = CONSERR__;
			}

		} catch (Exception $mensaje) {
			$arrDatos["irespuesta"] = CNXEXP__;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

}
