<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlConstanciaAfiliacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];

		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$metodo = $this->post('metodo');
		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, $metodo);

	

		//load models
		$this->load->model('MdlconstanciaAfiliacion');
		$this->load->model('MdlMetodosGenerales');
	}

	public function Consultacatalogoestadocivil_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlconstanciaAfiliacion->consultacatalogoestadocivil();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Consultacolsolicitudes_post()
	{
		$arrDatos = array();

		try {
			$sCurp = $this->post('sCurp');
			$response = $this->MdlconstanciaAfiliacion->consultacolsolicitudes($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Obtenercurpempleado_post()
	{
		$arrDatos = array();

		try {
			$iEmpleado = $this->post('iEmpleado');
			$response = $this->MdlconstanciaAfiliacion->obtenercurpempleado($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Obtenerdatospromotor_post()
	{
		$arrDatos = array();

		try {

			$iEmpleado = $this->post('iEmpleado');
			$response = $this->MdlconstanciaAfiliacion->obtenerdatospromotor($iEmpleado);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'nombre');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Consultatipoconstancia_post()
	{
		$arrDatos = array();

		try {

			$sCurp = $this->post('scurp');
			$response = $this->MdlconstanciaAfiliacion->consultatipoconstancia($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($response, REST_Controller::HTTP_OK);
	}

	public function Obtenertienda_post()
	{
		$arrDatos = array();

		try {

			$ipTienda = $this->post('ipTienda');
			$response = $this->MdlconstanciaAfiliacion->obtenertienda($ipTienda);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenermensajerespuestaAU_post()
	{
		$arrDatos = array();
		$arrFinal = array();
		$folioretorno = (int) $this->post('folioretorno');

		try {

			$response = $this->MdlconstanciaAfiliacion->obtenermensajerespuestaAU($folioretorno);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg => $key) {
					$arrDatos['registros'][] = $key;
				}
				$arrFinal = $this->MdlconstanciaAfiliacion->removerCaracteresEspeciales($arrDatos);
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrFinal, REST_Controller::HTTP_OK);
	}

	public function obteneriformacionclientebancoppel_post()
	{
		$arrDatos = array();
		$keyxbancoppel = (int) $this->post('keyxbancoppel');

		try {
			$response = $this->MdlconstanciaAfiliacion->obteneriformacionclientebancoppel($keyxbancoppel);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarflagclientebcpl_post()
	{
		$arrDatos = array();

		$keyxbancoppel = (int) $this->post('keyxbancoppel');

		try {

			$response = $this->MdlconstanciaAfiliacion->actualizarflagclientebcpl($keyxbancoppel);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validacion_post()
	{
		$arrDatos = array();
		$keyxbancoppel = (int) $this->post('keyxbancoppel');

		try {

			$response = $this->MdlconstanciaAfiliacion->validacion($keyxbancoppel);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaRespuestaRenapo_post()
	{
		$arrDatos = array();
		$arrFinal = array();

		$iFolioAfore = (int) $this->post('iFolioAfore');

		try {

			$response = $this->MdlconstanciaAfiliacion->consultaRespuestaRenapo($iFolioAfore);

			if ($response != null) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}

				$arrFinal = $this->MdlconstanciaAfiliacion->removerCaracteresEspeciales($arrDatos);
			} else {
				$arrDatos['estatus'] = 0;
				$arrDatos['descripcion'] = "No se encontraron datos en la consulta";
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrFinal, REST_Controller::HTTP_OK);
	}

	public function consultarDatosRenapo_post()
	{
		$arrDatos = array();
		$iFolioAfore = 0;
		$iFolioAfore = (int) $this->post('iFolioAfore');

		try {

			$response = $this->MdlconstanciaAfiliacion->consultarDatosRenapo($iFolioAfore);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespecialenie($arrDatos, 'apellidopaterno');
				$arrDatos  = convertircaracteresespecialenie($arrDatos, 'apellidomaterno');
				$arrDatos  = convertircaracteresespecialenie($arrDatos, 'nombres');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Consultainformacionsolconstancia_post()
	{
		$arrDatos = array();

		try {

			$sCurp = $this->post('scurp');
			$iEstatusProceso = $this->post('iEstatusProceso');
			$response = $this->MdlconstanciaAfiliacion->consultainformacionsolconstancia($sCurp, $iEstatusProceso);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Consultainformaciondireccionsolconstancia_post()
	{
		$arrDatos = array();

		try {
			$sCodigopostal = $this->post('sCodigopostal');
			$idColonia = $this->post('idColonia');
			$idDelegacion = $this->post('idDelegacion');
			$idEstado = $this->post('idEstado');

			$response = $this->MdlconstanciaAfiliacion->Consultainformaciondireccionsolconstancia($sCodigopostal, $idColonia, $idDelegacion, $idEstado);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultaInvocacionServicio_post()
	{
		$arrDatos = array();

		try {
			$sCurp = $this->post('sCurp');

			$response = $this->MdlconstanciaAfiliacion->consultaInvocacionServicio($sCurp);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ServicioEjecutarAplicacion_post()
	{
		$arrDatos = array();

		try {

			$idServicio = $this->post('idServicio');

			$response = $this->MdlconstanciaAfiliacion->servicioEjecutarAplicacion($idServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Consultadatosbiometricos_post()
	{
		$arrDatos = array();

		try {
			$consultafolio = $this->post('consultafolio');
			$verificacionFolio = $this->post('verificacionFolio');

			$response = $this->MdlconstanciaAfiliacion->consultadatosbiometricos($consultafolio, $verificacionFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ActualizarSolConstancia_post()
	{
		$arrDatos = array();

		try {
			$folioactualizar = $this->post('folioactualizar');
			$estatusconsulta = $this->post('estatusconsulta');
			$estatusexpedienteconsulta = $this->post('estatusexpedienteconsulta');
			$estatusenrolamientoconsulta = $this->post('estatusenrolamientoconsulta');
			$diagnosticoconsulta = $this->post('diagnosticoconsulta');
			$estatusverificacion = $this->post('estatusverificacion');
			$diagnosticoverificacion = $this->post('diagnosticoverificacion');
			$selloverificacion = $this->post('selloverificacion');

			$response = $this->MdlconstanciaAfiliacion->actualizarSolConstancia($folioactualizar, $estatusconsulta, $estatusexpedienteconsulta, $estatusenrolamientoconsulta, $diagnosticoconsulta, $estatusverificacion, $diagnosticoverificacion, $selloverificacion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Obtenerenlace_post()
	{
		$arrDatos = array();

		try {
			$iOpcion = $this->post('iOpcion');
			$iFolio = $this->post('iFolio');
			$iTestigo = $this->post('iTestigo');
			$iNumTestigo = $this->post('iNumTestigo');
			$ipTienda = $this->post('ipTienda');

			$response = $this->MdlconstanciaAfiliacion->obtenerenlace($iOpcion, $iFolio, $iTestigo, $iNumTestigo, $ipTienda);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ObtenerestatusEnrolamiento_post()
	{

		$arrDatos = array();

		try {
			$sCurp = $this->post('sCurp');
			$iFolioAfore = $this->post('iFolioAfore');

			$response = $this->MdlconstanciaAfiliacion->obtenerestatusEnrolamiento($sCurp, $iFolioAfore);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function DianosticoErrores_post()
	{

		$arrDatos = array();

		try {

			$diagnostico = $this->post('diagnostico');
			$response = $this->MdlconstanciaAfiliacion->dianosticoErrores($diagnostico);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'valor');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function AlmacenaDatosConstacia_post()
	{

		$arrDatos = array();
		$numerofolio = $this->post('numerofolio');
		$iEmpleado = $this->post('iEmpleado');
		$iSiefore = $this->post('iSiefore');
		$iTipoConstancia = $this->post('iTipoConstancia');
		$sCurp = $this->post('sCurp');
		$sAppaterno = $this->post('sAppaterno');
		$sApmaterno = $this->post('sApmaterno');
		$sNombre = $this->post('sNombre');
		$sNss = $this->post('sNss');
		$sNumeroCell = $this->post('sNumeroCell');
		$iCompaniaCel = $this->post('iCompaniaCel');
		$sTipoComprobante = $this->post('sTipoComprobante');
		$sFechaComprobante = $this->post('sFechaComprobante');
		$sNumeroCliente = $this->post('sNumeroCliente');
		$iTipoTelefono = $this->post('iTipoTelefono');
		$sTelefonoContacto = $this->post('sTelefonoContacto');
		$iCompTel = $this->post('iCompTel');
		$sCalle = $this->post('sCalle');
		$sNumExt = $this->post('sNumExt');
		$sNumInt = $this->post('sNumInt');
		$sCodPost = $this->post('sCodPost');
		$iColonia = $this->post('iColonia');
		$iDelegacion = $this->post('iDelegacion');
		$iEstado = $this->post('iEstado');
		$iPais = $this->post('iPais');
		$sLugarSol = $this->post('sLugarSol');
		$iTienda = $this->post('iTienda');
		$iFirma = $this->post('iFirma');
		$iFolioBcpl = $this->post('iFolioBcpl');
		$sCurpSolicitante = $this->post('sCurpSolicitante');
		$iTiposolicitante = $this->post('iTiposolicitante');
		$iFolioConstanciaImplicaciones = $this->post('iFolioImplicacionTraspaso');
		$aforeCedente = $this->post('aforeCedente');
		$tipoTraspaso = $this->post('tipoTraspaso');
		$iTipoExcepcion = $this->post('iTipoExcepcion');
		$folioPrevalidador = $this->post('folioPrevalidador');
		$iFolioConocimientoTraspaso = $this->post('iFolioConocimientoTraspaso');

		try {

			$response = $this->MdlconstanciaAfiliacion->AlmacenaDatosConstacia($numerofolio, $iEmpleado, $iSiefore, $iTipoConstancia, $sCurp, $sAppaterno, $sApmaterno, $sNombre, $sNss, $sNumeroCell, $iCompaniaCel, $sTipoComprobante, $sFechaComprobante, $sNumeroCliente, $iTipoTelefono, $sTelefonoContacto, $iCompTel, $sCalle, $sNumExt, $sNumInt, $sCodPost, $iColonia, $iDelegacion, $iEstado, $iPais, $sLugarSol, $iTienda, $iFirma, $iFolioBcpl, $sCurpSolicitante, $iTiposolicitante, $iFolioConstanciaImplicaciones, $aforeCedente, $tipoTraspaso, $iTipoExcepcion, $folioPrevalidador, $iFolioConocimientoTraspaso);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultacatalogoComprobante_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlconstanciaAfiliacion->ConsultacatalogoComprobante();
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'desccomp');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultacatalogoCompaCelulares_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlconstanciaAfiliacion->ConsultacatalogoCompaCelulares();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Consultacurp_post()
	{
		$arrDatos = array();

		$sCurp = $this->post('sCurp');

		try {

			$response = $this->MdlconstanciaAfiliacion->Consultacurp($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultaRechazoCurp_post()
	{
		$arrDatos = array();

		$sCurp = $this->post('sCurp');

		try {

			$response = $this->MdlconstanciaAfiliacion->ConsultaRechazoCurp($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultacatalogoLugarSolicitud_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlconstanciaAfiliacion->ConsultacatalogoLugarSolicitud();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validafechacomprobante_post()
	{
		$arrDatos = array();

		$sFechaComprobante = $this->post('sFechaComprobante');

		try {

			$response = $this->MdlconstanciaAfiliacion->validafechacomprobante($sFechaComprobante);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validaNumeroTelefono_post()
	{
		$arrDatos = array();

		$sNumeroCell = $this->post('sNumeroCell');

		try {

			$response = $this->MdlconstanciaAfiliacion->validaNumeroTelefono($sNumeroCell);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validadtelefonoSolConstancias_post()
	{
		$arrDatos = array();

		$sTel = $this->post('sTel');
		$sCurp = $this->post('sCurp');
		$op = $this->post('op');

		try {

			$response = $this->MdlconstanciaAfiliacion->validadtelefonoSolConstancias($sTel, $sCurp, $op);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validadtelefonoTelTiendas_post()
	{
		$arrDatos = array();

		$sTel = $this->post('sTel');

		try {

			$response = $this->MdlconstanciaAfiliacion->validadtelefonoTelTiendas($sTel);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validadtelPromotores_post()
	{
		$arrDatos = array();

		$sTel = $this->post('sTel');
		try {

			$response = $this->MdlconstanciaAfiliacion->validadtelPromotores($sTel);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarDomicilioTiendas_post()
	{
		$arrDatos = array();

		$sCalle = $this->post('sCalle');
		$sNumExt = $this->post('sNumExt');
		$sCodPost = $this->post('sCodPost');
		$sDesCol = $this->post('sDesCol');
		$sDesMun = $this->post('sDesMun');
		$sDesEdo = $this->post('sDesEdo');

		try {

			$response = $this->MdlconstanciaAfiliacion->validarDomicilioTiendas($sCalle, $sNumExt, $sCodPost, $sDesCol, $sDesMun, $sDesEdo);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response[0] as $reg) {
					$arrDatos['respuesta'] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenervidencia_post()
	{
		$arrDatos = array();

		$tiposolicitud = $this->post('tiposolicitud');
		try {

			$response = $this->MdlconstanciaAfiliacion->obtenervidencia($tiposolicitud);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response[0] as $reg) {
					$arrDatos['tipo4'] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenernumerofolio_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->obtenernumerofolio();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenersiefore_post()
	{
		$arrDatos = array();

		$sCurp = $this->post('sCurp');
		try {

			$response = $this->MdlconstanciaAfiliacion->obtenersiefore($sCurp);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response[0] as $reg) {
					$arrDatos['siefore'] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerestatusempleado_post()
	{
		$arrDatos = array();
		$iEmpleado = $this->post('iEmpleado');

		try {

			$response = $this->MdlconstanciaAfiliacion->obtenerestatusempleado($iEmpleado);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response[0] as $reg) {
					$arrDatos['valor'] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ActualizarEstadoCivil_post()
	{
		$arrDatos = array();
		try {
			$arrDatos = array();
			$ifolioactualizaestadocivil = $this->post('ifolioactualizaestadocivil');
			$iestadocivil = $this->post('iestadocivil');

			$response = $this->MdlconstanciaAfiliacion->ActualizarEstadoCivil($ifolioactualizaestadocivil, $iestadocivil);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validaTelefonoCelular_post()
	{
		$arrDatos = array();

		try {
			$arrDatos = array();
			$sNumeroCell = $this->post('sNumeroCell');
			$response = $this->MdlconstanciaAfiliacion->validaTelefonoCelular($sNumeroCell);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}


	public function ValidarRegistroPrevio_post()
	{
		$arrDatos = array();

		try {
			$cCurp = $this->post('cCurp');
			$response = $this->MdlconstanciaAfiliacion->ValidarRegistroPrevio($cCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarCurpenReanpo_post()
	{
		$arrDatos = array();

		try {
			$sCurp = $this->post('scurp');
			$response = $this->MdlconstanciaAfiliacion->validarCurpenReanpo($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarFolRenapo_post()
	{
		$arrDatos = array();

		try {
			$iFolioCons = $this->post('iFolioCons');
			$iFolioAfore = $this->post('iFolioAfore');


			$response = $this->MdlconstanciaAfiliacion->actualizarFolRenapo($iFolioCons, $iFolioAfore);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerLigaRenapo_post()
	{
		$arrDatos = array();

		try {
			$iOpcionRenapo = $this->post('iOpcionRenapo');
			$iFolioAfore = $this->post('iFolioAfore');
			$iPmodulo = $this->post('iPmodulo');

			$response = $this->MdlconstanciaAfiliacion->obtenerLigaRenapo($iOpcionRenapo, $iFolioAfore, $iPmodulo);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}

				$arrDatos = $this->cambiarliga($arrDatos, 'liga');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarListaNegraTelefonoCel_post()
	{
		$arrDatos = array();

		try {
			$sNumeroCell = $this->post('sNumeroCell');
			$iEmpleado = $this->post('iEmpleado');
			$sCurp = $this->post('sCurp');

			$response = $this->MdlconstanciaAfiliacion->validarListaNegraTelefonoCel($sNumeroCell, $iEmpleado, $sCurp);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarListaNegraTelefonofijo_post()
	{
		$arrDatos = array();
		try {
			$sTelefonoContacto = $this->post('sTelefonoContacto');
			$iEmpleado = $this->post('iEmpleado');
			$sCurp = $this->post('sCurp');

			$response = $this->MdlconstanciaAfiliacion->validarListaNegraTelefonofijo($sTelefonoContacto, $iEmpleado, $sCurp);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerRespuestaEnrolHuellaServicio_post()
	{
		$arrDatos = array();
		$folioOperacionC = $this->post('folioOperacionC');
		$folioOperacionV = $this->post('folioOperacionV');
		$sCurp = $this->post('sCurp');
		$idServicioC = $this->post('idServicioC');
		$idServicioV = $this->post('idServicioV');
		$idOperacionC = $this->post('idOperacionC');
		$idOperacionV = $this->post('idOperacionV');

		try {
			$response = $this->MdlconstanciaAfiliacion->obtenerRespuestaEnrolHuellaServicio($folioOperacionC, $folioOperacionV, $sCurp, $idServicioC, $idServicioV, $idOperacionC, $idOperacionV);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}


		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ejecutaPrevalidacion_post()
	{
		$arrDatos = array();
		try {
			$response = $this->MdlconstanciaAfiliacion->ejecutaPrevalidacion();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDocRequeridosAfiliacion_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->obtenerDocRequeridosAfiliacion();


			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDocFotoClienteAfiliacion_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->obtenerDocFotoClienteAfiliacion();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDocRequeridosReenrolamiento_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->obtenerDocRequeridosReenrolamiento();


			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerTemplatePromotor_post()
	{

		try {

			$respuesta = 0;
			$iEmpleado = $this->post('iEmpleado');
			$sTemplate = $this->post('sTemplate');

			$response = $this->MdlconstanciaAfiliacion->obtenerTemplatePromotor($iEmpleado);

			if ($response) {
				foreach ($response as $reg) {
					$var = $reg->template;
					if (strcasecmp($var, $sTemplate) == 0) {
						$respuesta = 1;
					}
				}
			}
		} catch (Exception $mensaje) {
			$respuesta = 0;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function obtenerTemplateTrabajador_post()
	{
		try {

			$respuesta = 0;
			$cCurp = $this->post('cCurp');
			$sTemplate = $this->post('sTemplate');

			$response = $this->MdlconstanciaAfiliacion->obtenerTemplateTrabajador($cCurp);

			if ($response) {
				foreach ($response as $reg) {
					$var = $reg->template;

					if (strcasecmp(trim($var), trim($sTemplate)) === 0) {
						$respuesta = 1;
					}
				}
			}
		} catch (Exception $mensaje) {
			$respuesta = 0;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function guardarUbicacionDocAfiliacion_post()
	{
		$arrDatos = array();

		try {
			$folio = $this->post('folio');
			$documento = $this->post('documento');
			$ubicacion = $this->post('ubicacion');
			$iporigen = $this->post('iporigen');
			$tipodocumento = $this->post('tipodocumento');

			//Subir al servidor los archivos del trabajor que se generaron en el proceso constancia afiliación

			$response = $this->MdlconstanciaAfiliacion->guardarUbicacionDocAfiliacion($folio, $documento, $ubicacion, $iporigen, $tipodocumento);

			if ($response) {
				$arrDatos = $response->respuesta;
			}
		} catch (Exception $mensaje) {
			$arrDatos = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerUbicacionDocAfiliacion_post()
	{
		$arrDatos = array();

		try {
			$arrDatos = array();
			$folio = $this->post('folio');

			$response = $this->MdlconstanciaAfiliacion->obtenerUbicacionDocAfiliacion($folio);

			if ($response) {
				$arrDatos = $response;
			}
		} catch (Exception $mensaje) {
			$arrDatos = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerUbicacionDocAfiliacion2_post()
	{
		$arrDatos = array();
		try {

			$folio = $this->post('folio');

			$response = $this->MdlconstanciaAfiliacion->obtenerUbicacionDocAfiliacion2($folio);

			if ($response) {
				foreach ($response as $reg) {
					$arrDatos[] = "http://" . $reg->siporigen . $reg->subicacion . $reg->sdocumento;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenergerentebytemplate_post()
	{
		$arrDatos = array();

		try {
			$template = $this->post('template');

			$response = $this->MdlconstanciaAfiliacion->obtenergerentebytemplate($template);

			if ($response) {
				foreach ($response as $reg) {
					$arrDatos = $reg->iempleado;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function enviarCorreoDocAfiliacion_post()
	{

		try {
			$folio = $this->post('folio');
			$configaraciones = simplexml_load_file("../../conf/webconfig.xml");
			$ipBD = $configaraciones->Spa;
			$ipBD = (array) $ipBD;
			$ipBD = $ipBD[0];

			$instruccion = ("/bin/python /sysx/progs/web/enviarcorreosexpedientetrabajador/enviacorreodatosfunnelclientedigitalafore.py " . $folio . " " . $ipBD);

			$resultado = shell_exec($instruccion);

			if ($resultado == 1 || $resultado == '1\n') {
				$respuesta = 1;
			} else {
				$respuesta = 0;
			}
		} catch (Exception $mensaje) {
			$respuesta = 0;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function renombrarImgFirmas_post()
	{
		try {
			$arrDatos = 0;

			$liga = "/sysx/progs/web/entrada/firmas/";

			$oldName = $this->post('oldName');
			$oldName = $liga . $oldName;
			$newName = $this->post('newName');
			$newName = $liga . $newName;

			if (file_exists($oldName)) {
				if (rename($oldName, $newName)) {
					$arrDatos = 1;
				} else {
					$arrDatos = 0;
				}
			} else {
				$arrDatos = "Imagen no exixte en el directorio";
			}
		} catch (Exception $mensaje) {
			$arrDatos = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarHuella_post()
	{
		try {
			$ccurptrab = strval($this->post('sCURPTrab'));
			$iempleado = strval($this->post('iNumFuncionario'));
			$ifoliosolicitud = strval($this->post('iFolioSolicitud'));
			$itipopersona = strval($this->post('iTipoPerson'));
			$iidservicio = strval($this->post('iTipoServ'));
			$ctipooperacion = strval($this->post('sTipoOperac'));
			$inumdedo = strval($this->post('iNumDedo'));
			$inist = strval($this->post('iNist'));
			$cdispositivo = strval($this->post('sDispositivo'));
			$ifap = strval($this->post('iFap'));
			$cimagen = strval($this->post('sImagen'));
			$ctiposervicio = strval($this->post('sTipoServicio'));

			$cTemplate = $this->post('cTemplate');

			$cTemplate = str_replace('\/', '/', $cTemplate);

			$cimagen = str_replace('_', '/', $cimagen);
			$cimagen = str_replace('-', '+', $cimagen);

			$respuesta = 0;

			//Valida la Huella del Empleado/Trabajador
			$valida = $this->MdlconstanciaAfiliacion->ejecutaServicioHuellas($iempleado, $cTemplate, $itipopersona);

			if ($valida) {

				if ($valida->lEstado >= 1 && $valida->lEstado <= 10) { //LA HUELLA HACE MATCH CON ALGUNO DE LOS DEDOS

					$inumdedo = $valida->lEstado;
					//Almacena los Datos de la Huella
					$almacena = $this->MdlconstanciaAfiliacion->almacenaDatosHuellasServicio($ccurptrab, $iempleado, $ifoliosolicitud, $itipopersona, $iidservicio, $ctipooperacion, $inumdedo, $inist, $cdispositivo, $ifap, $cimagen, $ctiposervicio);
					$respuesta = 1;
				} else //LA HUELLA NO HACE MATCH
				{
					if ($itipopersona == 3) { //TRABAJADOR

						if ($valida->lEstado == -17) { //NO SE ENCONTRARON REGISTROS PARA LA CONSULTA
							$iRetorno = 105; //SIN ENROLAMIENTO
							$respuesta = 1;
						} else {
							$iRetorno = 106; //HUELLA NO COINCIDE CON LA DE SU ENROLAMIENTO
							$respuesta = 106;
						}

						$almacena = $this->MdlconstanciaAfiliacion->almacenaDatosHuellasServicio($ccurptrab, $iempleado, $ifoliosolicitud, $itipopersona, $iidservicio, $ctipooperacion, $inumdedo, $inist, $cdispositivo, $ifap, $cimagen, $ctiposervicio);

						//ALMACENA EN BITACORA LOS BIOMETRICOS RECHAZADOS
						$ejecutaBitacora = $this->MdlconstanciaAfiliacion->guardarBitacoraBioRechazos($iidservicio, $ifoliosolicitud, $ctipooperacion, $iempleado, $ccurptrab, $inumdedo, $iRetorno);
						//$respuesta = $valida->lEstado;
					}
				}
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function guardarBitacoraBioRechazos_post()
	{
		try {

			$iIdServicio = $this->post('iIdServicio');
			$iFolioSolicitud = $this->post('iFolioSolicitud');
			$cTipoOperacion = $this->post('cTipoOperacion');
			$iNumEmpleado = $this->post('iNumEmpleado');
			$cCurpTrabajador = $this->post('cCurpTrabajador');
			$iDedo = $this->post('iDedo');
			$iRetorno = $this->post('iRetorno');

			$response = $this->MdlconstanciaAfiliacion->guardarBitacoraBioRechazos($iIdServicio, $iFolioSolicitud, $cTipoOperacion, $iNumEmpleado, $cCurpTrabajador, $iDedo, $iRetorno);

			$respuesta = $response->respuesta;
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function obtenerTiendaEmpleado_post()
	{
		$arrDatos = array();
		try {
			$arrDatos = array();
			$iEmpleado = $this->post('iEmpleado');

			$response = $this->MdlconstanciaAfiliacion->obtenerTiendaEmpleado($iEmpleado);

			$arrDatos = (int) $response->tienda;
		} catch (Exception $mensaje) {
			$arrDatos = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function registrarDatosPrevalidador_post()
	{
		$arrDatos = array();
		try {

			$data = array(
				'apellidoPaterno' => $this->post("apellidoPaterno"),
				'apellidoMaterno' => $this->post("apellidoMaterno"),
				'nombres' => $this->post("nombres"),
				'curpTrabajador' => $this->post("curpTrabajador"),
				'curpPromotor' => $this->post("curpPromotor"),
				'nss' => $this->post("nss"),
				'selloBiometrico' => $this->post("selloVerificacion"),
				'numeroEmpleado' => $this->post("numeroEmpleado")
			);
			$response = $this->MdlconstanciaAfiliacion->registrarDatosPrevalidador($data);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarHuellaCertificado_post()
	{
		try {
			$iempleado = strval($this->post('iNumFuncionario'));
			$ifoliosolicitud = strval($this->post('iFolioSolicitud'));
			$itipopersona = strval($this->post('iTipoPerson'));

			$cTemplate = $this->post('cTemplate');

			$cTemplate = str_replace('\/', '/', $cTemplate);

			$folioEnrol = $this->MdlconstanciaAfiliacion->consultarFolioEnrolamiento($ifoliosolicitud);

			$folioEnrol = $folioEnrol->folioenrolamiento;

			$respuesta = 0;

			if ($itipopersona == 2) { //EMPLEADO

				//Valida la Huella del Empleado
				$valida = $this->MdlconstanciaAfiliacion->ejecutaServicioHuellas($iempleado, $cTemplate, $itipopersona);
			} else if ($itipopersona == 3) { //TRABAJADOR

				//Valida la Huella del Trabajador
				$valida = $this->MdlconstanciaAfiliacion->ejecutaServicioHuellas($folioEnrol, $cTemplate, $itipopersona);
			}

			if ($valida) {

				if ($valida->lEstado >= 1 && $valida->lEstado <= 10) {

					if ($valida->cDescripcion == "") {

						$respuesta = 106; //HUELLA NO COINCIDE CON LA DE SU ENROLAMIENTO

					} else { //LA HUELLA HACE MATCH

						$respuesta = 1;
					}
				} else //LA HUELLA NO HACE MATCH
				{
					if ($valida->lEstado == -17) { //NO SE ENCONTRARON REGISTROS PARA LA CONSULTA
						$respuesta = 105; //SIN ENROLAMIENTO

					} else {
						$respuesta = 106; //HUELLA NO COINCIDE CON LA DE SU ENROLAMIENTO
					}
				}
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function obtenerentolcapturadovozmovil_post()
	{
		try {

			$ID = $this->post("ID");
			$IdentIFicador = (int) $this->post("IdentIFicador");
			$Imposibilidad = (int) $this->post("Imposibilidad");
			$iEmpleado = (int) $this->post('iEmpleado');

			$Tienda = $this->MdlconstanciaAfiliacion->obtenerTiendaEmpleado($iEmpleado);

			$Tienda = (int) $Tienda->tienda;

			$response = $this->MdlconstanciaAfiliacion->obtenerentolcapturadovozmovil($ID, $IdentIFicador, $Tienda, $Imposibilidad);

			if ($response) {
				$repuesta = trim(utf8_encode($response->mensaje));
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($repuesta, REST_Controller::HTTP_OK);
	}

	public function moverArchivo_post()
	{
		try {
			$respuesta = 0;
			$nombrearchivo = $this->post("nombrearchivo");
			$nombrearchivo = basename($nombrearchivo);

			$rutaorigen = $this->post("rutaorigen");
			$rutaorigen = $rutaorigen . $nombrearchivo;

			$rutadestino = $this->post("rutadestino");
			$rutadestino = $rutadestino . $nombrearchivo;

			if (file_exists($rutaorigen)) {
				if (copy($rutaorigen, $rutadestino)) {
					$respuesta = 1;
					unlink($rutaorigen);
				} else {
					$respuesta = 0;
				}
			} else {
				$respuesta = "Imagen no exixte en el directorio";
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}
		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function obtenerRespuestaPrevalidador_post()
	{
		$arrDatos = array();
		try {

			$iFolio = $this->post('iFolio');
			$response = $this->MdlconstanciaAfiliacion->obtenerRespuestaPrevalidador($iFolio);


			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerdialogograbadormovil_post()
	{
		try {
			$iTipodialogo = $this->post("iTipodialogo");
			$iFolio = $this->post('iFolio');

			$response = $this->MdlconstanciaAfiliacion->obtenerdialogograbadormovil($iTipodialogo, $iFolio);

			if ($response) {
				$repuesta = trim(utf8_encode($response->dialogo));
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($repuesta, REST_Controller::HTTP_OK);
	}

	public function almacenanumerotarjetamovil_post()
	{
		try {
			$cNumeroTarjeta = $this->post("cNumeroTarjeta");
			$ipCliente = $this->post("ipCliente");
			$respuesta = 0;

			$response = $this->MdlconstanciaAfiliacion->almacenanumerotarjetamovil($cNumeroTarjeta, $ipCliente);

			if ($response) {
				$respuesta = 1;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function obtenerMensajeErrorPrevalidador_post()
	{
		$arrDatos = array();
		try {

			$cCodigo = $this->post('cCodigo');
			$response = $this->MdlconstanciaAfiliacion->obtenerMensajeErrorPrevalidador($cCodigo);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCLienteBCplMovil_post()
	{
		try {
			$cNumTarjetaBCpl = $this->post('cNumTarjetaBCpl');
			$cTemplate = $this->post('cTemplate');
			$cTemplate = str_replace('\/', '/', $cTemplate);

			$respuesta = 0;

			$response = $this->MdlconstanciaAfiliacion->ejecutaServicioBCpl($cNumTarjetaBCpl, $cTemplate);

			if ($response) {
				$respuesta = $response->EstadoProc->Estado;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function obtenerBandMensajePromotor_post()
	{
		$arrDatos = array();
		try {
			$response = $this->MdlconstanciaAfiliacion->obtenerBandMensajePromotor();

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['valor'] = (int) $response->valor;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['valor'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarBandMensajePromotor_post()
	{
		$arrDatos = array();
		try {
			$foliosolicitud = $this->post('foliosolicitud');
			$valor =  $this->post('valor');

			$response = $this->MdlconstanciaAfiliacion->actualizarBandMensajePromotor($foliosolicitud, $valor);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = (int) $response->respuesta;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultacatalogoSolicitante_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->ConsultacatalogoSolicitante();
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'cdescripcion');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerIpCluster_post()
	{
		$arrDatos = array();
		try {
			$iPmodulo = $this->post("iPmodulo");

			$response = $this->MdlconstanciaAfiliacion->obtenerIpCluster($iPmodulo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerClaveOperacion_post()
	{
		$arrDatos = array();
		try {

			$iPmodulo = $this->post("iPmodulo");

			$response = $this->MdlconstanciaAfiliacion->obtenerClaveOperacion($iPmodulo);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultarDialogoVideo_post()
	{
		$arrDatos = array();
		try {

			$TipoDialogo = $this->post("TipoDialogo");
			$response = $this->MdlconstanciaAfiliacion->ConsultarDialogoVideo($TipoDialogo);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos  = convertircaracteresespeciales($arrDatos, 'dialogo');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ObtenerUrlsVideo_post()
	{
		$arrDatos = array();
		try {

			$iPmodulo = $this->post("iPmodulo");

			$response = $this->MdlconstanciaAfiliacion->ObtenerUrlsVideo($iPmodulo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerDatosCapturaVideo_post()
	{
		$arrDatos = array();
		try {

			$iFolioCons = $this->post("iFolioCons");
			$iEmpleado = $this->post("iEmpleado");

			$response = $this->MdlconstanciaAfiliacion->fnobtenerDatosCapturaVideo($iFolioCons, $iEmpleado);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'nombrepromotor');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'apellidopaternoprom');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'apellidomaternoprom');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'nombretrabajador');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'apellidopaternotrab');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'apellidomaternotrab');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerEstatusVideo_post()
	{
		$arrDatos = array();
		try {

			$iFolioCons = $this->post("iFolioCons");
			$response = $this->MdlconstanciaAfiliacion->obtenerEstatusVideo($iFolioCons);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarenrolamientoConstancia_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");

			$response = $this->MdlconstanciaAfiliacion->validarenrolamientoConstancia($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarVideoAfiliacion_post()
	{
		$arrDatos = array();

		try {

			$iFolioCons = $this->post("iFolioCons");
			$nombreVideo = $this->post("nombreVideo");
			$iPmodulo = $this->post("iPmodulo");

			$response = $this->MdlconstanciaAfiliacion->guardarVideoAfiliacion($iFolioCons, $nombreVideo, $iPmodulo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerestatusfinado_post()
	{
		$arrDatos = array();
		try {

			$iFolioCons = $this->post("iFolioCons");

			$response = $this->MdlconstanciaAfiliacion->obtenerestatusfinado($iFolioCons);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarestatusvideo_post()
	{
		$arrDatos = array();
		try {

			$iFolioCons = $this->post("iFolioCons");
			$estatusvideo = $this->post("estatusvideo");

			$response = $this->MdlconstanciaAfiliacion->actualizarestatusvideo($iFolioCons, $estatusvideo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificarEnrolamiento_post()
	{
		$arrDatos = array();
		try {

			$iFolioSolicitud = $this->post("iFolioSolicitud");

			$response = $this->MdlconstanciaAfiliacion->verificarEnrolamiento($iFolioSolicitud);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarEstatusEnrolamiento_post()
	{

		$arrDatos = array();

		try {

			$iFolioEnrolamiento = $this->post("iFolioEnrolamiento");
			$iEstatusEnrolamiento = $this->post("iEstatusEnrolamiento");
			$sCurp = $this->post("sCurp");

			$response = $this->MdlconstanciaAfiliacion->actualizarEstatusEnrolamiento($iFolioEnrolamiento, $iEstatusEnrolamiento, $sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarcontrolenviofolio_post()
	{
		$arrDatos = array();

		try {

			$iFolioSolicitud = $this->post("iFolioSolicitud");
			$IdentificadorEnvio = $this->post("IdentificadorEnvio");

			$response = $this->MdlconstanciaAfiliacion->guardarcontrolenviofolio($iFolioSolicitud, $IdentificadorEnvio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnactualizadatossolicitanterenapo_post()
	{
		$arrDatos = array();

		try {

			$nombreSolicitante = $this->post("nombreSolicitante");
			$apellidoPatSolicitante = $this->post("apellidoPatSolicitante");
			$apellidoMatSolicitante = $this->post("apellidoMatSolicitante");
			$iFolioCons = $this->post("iFolioCons");

			$response = $this->MdlconstanciaAfiliacion->fnactualizadatossolicitanterenapo($nombreSolicitante, $apellidoPatSolicitante, $apellidoMatSolicitante, $iFolioCons);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ActualizaestatusSolconstancia_post()
	{
		$arrDatos = array();

		try {

			$iFolioCons = $this->post("iFolioCons");

			$response = $this->MdlconstanciaAfiliacion->ActualizaestatusSolconstancia($iFolioCons, 3);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarRespuestaFolio_post()
	{
		$arrDatos = array();

		try {

			$iFolioCons = $this->post("iFolioCons");

			$response = $this->MdlconstanciaAfiliacion->validarRespuestaFolio($iFolioCons);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function generarFolioEnrolamiento_post()
	{
		$arrDatos = array();

		try {

			$iFolioCons = $this->post("iFolioCons");
			$iEmpleado = $this->post("iEmpleado");
			$sCurp = $this->post("sCurp");
			$iTipoConstancia = $this->post("iTipoConstancia");

			$response = $this->MdlconstanciaAfiliacion->generarFolioEnrolamiento($iFolioCons, $iEmpleado, $sCurp, $iTipoConstancia);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarestatusprocesosolconstancia_post()
	{
		$arrDatos = array();
		try {

			$iFolioCons = $this->post("iFolioCons");

			$response = $this->MdlconstanciaAfiliacion->ActualizaestatusSolconstancia($iFolioCons, 4);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarExpediente_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");
			$sIpRemoto = $this->post("sIpRemoto");
			$response = $this->MdlconstanciaAfiliacion->validarExpediente($sCurp, $sIpRemoto);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function grabarExcepcion_post()
	{
		$arrDatos = array();
		try {

			$excepcion = $this->post("excepcion");
			$iFolioCons = $this->post("iFolioCons");
			$iEmpleado = $this->post("iEmpleado");

			$response = $this->MdlconstanciaAfiliacion->grabarExcepcion($excepcion, $iFolioCons, $iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function indexarFormatoEnrolamiento_post()
	{
		try {

			$respuesta = 0;
			$ipRemoto = 0;
			$iFolio = $this->post("iFolio");
			$documento = $this->post("documento");
			$ipOffline = $this->post("ipOffline");

			$response = $this->MdlconstanciaAfiliacion->indexarFormatoEnrolamiento($iFolio, $documento, $ipOffline);

			if ($response) {
				$respuesta = 1;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function CargarCatalogoDoctosFirmaDigital()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlconstanciaAfiliacion->CargarCatalogoDoctosFirmaDigital();

			if ($response) {
				foreach ($response as $reg) {
					$arrDatos[] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos[] = null;
		}

		return $arrDatos;
	}

	public function ConsultarFirmaElectronica($iTabla, $iFolioAfiSer)
	{

		try {

			$response = $this->MdlconstanciaAfiliacion->ConsultarFirmaElectronica($iTabla, $iFolioAfiSer);

			if ($response) {
				foreach ($response as $reg) {
					$shFirma = $reg->firma;
					$shPublico = $reg->publico;
				}

				if ($shFirma != 0) {
					if ($shPublico == 1) {
						$shRet = 1;
					} else {
						$shRet = -1;
					}
				}
			}
		} catch (Exception $mensaje) {
			$shRet = null;
		}

		return $shRet;
	}

	public function buscarimagenFTE0($iFolioAfiSer)
	{
		try {
			$shRet = 0;
			$sNombreFTE0 = "C:/TEMP/SERVICIOS/" . $iFolioAfiSer . "FTE0.tif";

			if ($file = fopen($sNombreFTE0, 'r')) {
				fclose($file);
				$shRet = 1;
			}
		} catch (Exception $mensaje) {
			$shRet = null;
		}

		return $shRet;
	}

	public function obtenerLigaIne_post()
	{
		$arrDatos = array();
		try {

			$empleado = $this->post("empleado");
			$curp = $this->post("curp");
			$pasoRenapo = $this->post("pasoRenapo");
			$ipModulo = $this->post("ipModulo");

			$response = $this->MdlconstanciaAfiliacion->obtenerLigaIne($empleado, $curp, $pasoRenapo, $ipModulo);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerLigaIne01_post()
	{
		$arrDatos = array();
		try {

			$empleado = $this->post("empleado");
			$curp = $this->post("curp");
			$pasoRenapo = $this->post("pasoRenapo");
			$ipModulo = $this->post("ipModulo");
			$iAuten = $this->post("iAutenticacion");

			$response = $this->MdlconstanciaAfiliacion->obtenerLigaIne01($empleado, $curp, $pasoRenapo, $ipModulo, $iAuten);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerEstatusIne_post()
	{
		$arrDatos = array();
		try {

			$curp = $this->post("curp");

			$response = $this->MdlconstanciaAfiliacion->obtenerEstatusIne($curp);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarFolioIne_post()
	{
		$arrDatos = array();
		try {

			$folioCons = $this->post("folioCons");
			$curp = $this->post("curp");
			$empleado = $this->post("empleado");
			$foliosoline = $this->post("foliosoline");

			$response = $this->MdlconstanciaAfiliacion->actualizarFolioIne($folioCons, $curp, $empleado, $foliosoline);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function indexarcodigopromotormovil_post()
	{
		try {

			$respuesta = 0;
			$folio = $this->post("folio");
			$sTipo = $this->post("sTipo");

			$response = $this->MdlconstanciaAfiliacion->indexarcodigopromotormovil($folio, $sTipo);

			if ($response) {
				$respuesta = 1;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function subirvideo_post()
	{
		try {
			$folio = $this->post("iFolio");
			$nombreimagen = $this->post("nombrearchivo");

			$respuesta = 0;
			$rutaArchivo = "";
			$rutafisica = "/sysx/progs/web/entrada/video/";

			$rutaArchivo = $rutafisica . $nombreimagen;

		

			if (file_exists($rutaArchivo)) {
				$im = file_get_contents($rutaArchivo);
				$imdata = base64_encode($im);
				$iLenght = strlen($imdata);
				$i = 1;

				if ($iLenght <= 490000) {

				

					$respuesta = $this->MdlconstanciaAfiliacion->subirvideo($folio, $nombreimagen, $imdata, $i);
				} else {
					while ($iLenght > 0) {
						$cut1 = substr($imdata, 0, 490000);
						$imdata = str_replace($cut1, "", $imdata);
						$iLenght = strlen($imdata);

					

						$respuesta = $this->MdlconstanciaAfiliacion->subirvideo($folio, $nombreimagen, $cut1, $i);

					

						$i++;
					}
				}
				unlink($rutaArchivo); //Borrar el video temporal.
			} else {
				$respuesta = -1;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}
		$this->response($respuesta->fnsubirvideosafiliacion, REST_Controller::HTTP_OK);
	}

	public function moverFormatoEnrol_post()
	{
		try {
			$respuesta = 0;
			$rutadestino = "";

			$folio = $this->post("folio");
			$rutaorigen = "/sysx/progs/web/salida/formatoreenroltrabajador";

			$dir = $rutaorigen . "/" . $folio . "*";
			// Abrir un directorio conocido, y proceder a leer su contenido
			foreach (glob($dir) as $file) {
				$rutaorigen = $file;
			}

			$rutadestino = str_replace('reenroltrabajador', 'senrolamiento', $rutaorigen);
			$rutadestino = str_replace('ReenrolTrab', 'EnrolTrab', $rutadestino);

			if (file_exists($rutaorigen)) {
				if (copy($rutaorigen, $rutadestino)) {
					$respuesta = 1;
					unlink($rutaorigen);
				} else {
					$respuesta = 0;
				}
			} else {
				$respuesta = "Imagen no existe en el directorio";
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}
		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function actualizaIDRE_post()
	{
		try {

			$respuesta = 0;
			$iFolio = $this->post("iFolio");

			$rutaorigen = "/sysx/smbxtempol";

			$dir = $rutaorigen . "/" . $iFolio . "*";
			// Abrir un directorio conocido, y proceder a leer su contenido
			foreach (glob($dir) as $file) {

				$rutadestinoID00 = str_replace('IDRE', 'ID00', $file);
				rename($file, $rutadestinoID00);

				$rutadestinoID0A = str_replace('IDRA', 'ID0A', $file);
				rename($file, $rutadestinoID0A);

				$rutadestinoID0R = str_replace('IDRR', 'ID0R', $file);
				rename($file, $rutadestinoID0R);
			}

			$response = $this->MdlconstanciaAfiliacion->actualizaIDRE($iFolio);

			if ($response) {
				$respuesta = 1;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function almacenaDispositivoAfiliacion_post()
	{
		try {

			$respuesta = 0;
			$folio = $this->post("folio");
			$sistemaOperativo = $this->post("sistemaOperativo");
			$tipoConexion = $this->post("tipoConexion");

			$response = $this->MdlconstanciaAfiliacion->almacenaDispositivoAfiliacion($folio, $sistemaOperativo, $tipoConexion);

			if ($response) {
				$respuesta = 1;
			}
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function subirImgServidorIntermedio_post()
	{
		try {
			$respuesta = 0;
			$cont = 0;

			$folio = $this->post("folio");

			$responseFolio = $this->MdlconstanciaAfiliacion->obtenerDatosFolio($folio);

			$curp = $responseFolio->curp;
			$tipoconstancia = $responseFolio->tipoconstancia;
			$folioenrolamiento = $responseFolio->folioenrolamiento;

			if ($tipoconstancia == '26' || $tipoconstancia == '33') {
				$sClave = '000303';
			} else {
				$sClave = '000302';
			}

			$responseFiles = $this->MdlconstanciaAfiliacion->obtenerImagenes($folio);

			foreach ($responseFiles as $reg) {
				$documento = str_replace('.pdf', '.tif', $reg->documento);
				$sNome = $reg->tipodocumento;
				$newname = $folio . "_" . $curp . $sClave . date("Ymd") . $sNome . ".tif";
				$response = $this->MdlconstanciaAfiliacion->actualizaImagenesEnBD($folio, $sNome, $newname, $folioenrolamiento);
				$cont++;
			}

			$respuesta = 1;
		
			$this->ftp->close();
		} catch (Exception $mensaje) {
			$respuesta = null;
		}

		$this->response($respuesta, REST_Controller::HTTP_OK);
	}

	public function rotarVideo_post()
	{
		$arrDatos = array();
		try {

			$info = $this->post("info");
			$info = json_decode($info, true);
			$name_archivo = $info["sNombre"];

			//Ruta donde se encuentra el archivo
			$ruta_in = "/sysx/progs/web/entrada/video/" . $name_archivo . ".mp4";
			//Ruta donde se alojara el archivo de la salida
			$ruta_out = "/sysx/progs/web/entrada/video/" . $name_archivo . "_out.mp4";
			//expresion regular que verifica si el video se encuenta rotado
			$rotate_pattern = "/rotate\\s*:\s*([0-9]+)/i";
			//se usa como bandera para indicar si el video esta girado
			$has_rotation = false;
			//comando de linux que se encarga de traer la informacion del video
			$cmd = "ffmpeg -v verbose -i '$ruta_in' 2>&1";
			//se ejecuta el comando
			$video_info = shell_exec($cmd);

			//let's check against the rotation pattern
			preg_match($rotate_pattern, $video_info, $matches);
			//se comprueba la rotacion
			if (is_array($matches)) {
				if (isset($matches[1])) {
					$has_rotation = true;
				}
			}
			//let's see if the video has rotation or not
			if ($has_rotation) {
				//rotate the video using transpose and write an output file
				$cmd = "ffmpeg -v verbose -i '$ruta_in' -vf 'transpose=2,transpose=1' -y '$ruta_out' 2>&1";

				shell_exec($cmd);

				//eliminamos archivo original
				if (file_exists($ruta_in)) {
					unlink($ruta_in);
				}
				if (file_exists($ruta_out)) {
					rename($ruta_out, $ruta_in = "/sysx/progs/web/entrada/video/" . $name_archivo . ".webm");
				}

				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			} else {
				rename($ruta_in, $ruta_in = "/sysx/progs/web/entrada/video/" . $name_archivo . ".webm");
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = '';
				$arrDatos['respuesta'] = null;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function cambiarliga($arrDatos, $scampo)
	{

		foreach ($arrDatos['registros'] as $key => $reg) {
			$svar = $reg->$scampo;

			$host = $_SERVER["HTTP_HOST"];

			$svar = str_replace('10.26.190.179', $host, $svar);

			$reg->$scampo = $svar;
		}
		return $arrDatos;
	}

	public function obtenerEstatusExpedienteEnrolamiento_post()
	{
		$arrDatos = array();
		try {
			$iFolioServicio = $this->post("iFolioServicio");

			$mensaje = "METODO -> obtenerEstatusExpedienteEnrolamiento, Variable -> [iFolioServicio] = " . $iFolioServicio;
		

			$response = $this->MdlconstanciaAfiliacion->obtenerEstatusExpedienteEnrolamiento($iFolioServicio);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
	
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarHuellasIndices_post()
	{
		$arrDatos = array();

		try {
			$sRuta = "/sysx/progs/web/entrada/huellas/enrolamiento/";

			$sNombreArchivoIzq = $this->post("sNombreArchivoIzq");
			$sNombreArchivoDer = $this->post("sNombreArchivoDer");
			$sCurpTrabajador = $this->post("sCurpTrabajador");
			$ipModulo = $this->post("ipModulo");
			$iEmpleado = $this->post("iEmpleado");

			$mensaje = "METODO -> guardarHuellasIndices, Variables -> [sNombreArchivoIzq] = " . $sNombreArchivoIzq . ", [sNombreArchivoDer] = " . $sNombreArchivoDer . ", [sCurpTrabajador] = " . $sCurpTrabajador . ", [ipModulo] = " . $ipModulo;
		

			$huellaIndiceIzquierdo = "";
			$huellaIndiceDerecho = "";
			$arrIndiceIzquierdo = array();
			$arrIndiceDerecho = array();

			$huellaIndiceIzquierdo = file_get_contents($sRuta . $sNombreArchivoIzq);
			$huellaIndiceDerecho = file_get_contents($sRuta . $sNombreArchivoDer);

			//Posicion [0] -> Huella Dedo indice Izquierdo, Posicion [1] -> Huella Dedo Indice Derecho
			$arrIndiceIzquierdo = explode("|", $huellaIndiceIzquierdo);
			$arrIndiceDerecho = explode("|", $huellaIndiceDerecho);

			//Se elimina la ultima posicion ya que no cuenta con valor
			unset($arrIndiceIzquierdo[88]);
			unset($arrIndiceDerecho[88]);

			$response = $this->MdlconstanciaAfiliacion->guardarHuellasIndices($sCurpTrabajador, $iEmpleado, $ipModulo, $arrIndiceIzquierdo, $arrIndiceDerecho);

			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
	
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function limpiarHuellasServicio_post()
	{
		$arrDatos = array();
		$arrDatosServicio = array(
			'sNombreArchivoIzq' => $this->post('sNombreArchivoIzq'),
			'sNombreArchivoDer' => $this->post('sNombreArchivoDer')
		);
		try {
			$response = $this->MdlconstanciaAfiliacion->limpiarHuellasServicio($arrDatosServicio);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['borrado'] = $response;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = "ERROR";
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificaEjecucionServicio_post()
	{
		$arrDatos = array();
		try {
			$response = $this->MdlconstanciaAfiliacion->verificaEjecucionServicio();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function elminarinformacionServicio_post()
	{
		$arrDatos = array();
		try {
			$response = $this->MdlconstanciaAfiliacion->elminarinformacionServicio();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarfoliobancoppel_post()
	{
		$arrDatos = array();

		$folio 		= $this->post("iFolio");
		$curp 		= $this->post("cCurp");
		$empleado 	= $this->post("iEmpleado");
		$folioBcpl 	= $this->post("iFolioBanco");

		try {
			$response   = $this->MdlconstanciaAfiliacion->actualizarfoliobancoppel($folio, $curp, $empleado, $folioBcpl);
			if ($response) {
				$arrDatos['estatus']        = 1;
				$arrDatos['descripcion']    = "EXITO";
				$arrDatos['registros'][]    = $response;
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion fnactualizarfoliobancoppel";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensajeExcep) {
			$arrDatos['estatus']        = -1;
			$arrDatos['descripcion']    = $mensajeExcep;
			$arrDatos['registros']      = null;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function tieneautenticacionbancoppel_post()
	{
		$arrDatos = array();
		$curp 		= $this->post("cCurp");
		$empleado = $this->post("iEmpleado");
		$ipmodulo = $this->post("cIpmodulo");
		try {
			$response   = $this->MdlconstanciaAfiliacion->tieneautenticacionbancoppel($curp, $empleado, $ipmodulo);
			if ($response) {
				$arrDatos['estatus']        = 1;
				$arrDatos['descripcion']    = "EXITO";
				$arrDatos['registros'][]    = $response;
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion fntieneautenticacionbancoppel";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensajeExcep) {
			$arrDatos['estatus']        = -1;
			$arrDatos['descripcion']    = $mensajeExcep;
			$arrDatos['registros']      = null;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}



	public function obtenerRecuperacionFolioConstancia_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");

			$response = $this->MdlconstanciaAfiliacion->obtenerRecuperacionFolioConstancia($sCurp);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'] = $reg;
				}
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion obtenerRecuperacionFolioConstancia";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerCurpPromotor_post()
	{
		$arrDatos = array();
		try {

			$iNumeroEmpleado = $this->post("iNumeroEmpleado");

			$response = $this->MdlconstanciaAfiliacion->obtenerCurpPromotor($iNumeroEmpleado);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultaSolconstancia_post()
	{
		$arrDatos = array();
		try {

			$iFolio = $this->post("iFolio");

			$response = $this->MdlconstanciaAfiliacion->ConsultaSolconstancia($iFolio);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultaRechazo_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");

			$response = $this->MdlconstanciaAfiliacion->ConsultaRechazo($sCurp);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ActualizaestatusSolconstanciaSer_post()
	{
		$arrDatos = array();
		try {

			$iFolio = $this->post("iFolio");

			$response = $this->MdlconstanciaAfiliacion->ActualizaestatusSolconstanciaSer($iFolio, '');

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerRespuestaRechazo_post()
	{
		$arrDatos = array();
		try {

			$iFolioSerAfore = $this->post("iFolioSerAfore");

			$response = $this->MdlconstanciaAfiliacion->obtenerRespuestaRechazo($iFolioSerAfore);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarHuellas_post()
	{
		$arrDatos = array();
		try {

			$sCurpTrab = $this->post("sCurpTrab");
			$iFolio = $this->post("iFolio");
			$iIDServicio = $this->post("iIDServicio");
			$cTipoOperacion = $this->post("cTipoOperacion");

			$response = $this->MdlconstanciaAfiliacion->consultarHuellas($sCurpTrab, $iFolio, $iIDServicio, $cTipoOperacion);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaCatalogoAfore_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->consultaCatalogoAfore();

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validaSolicitud_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");
			$sContrasenia = $this->post("sContrasenia");

			$response = $this->MdlconstanciaAfiliacion->validaSolicitud($sCurp, $sContrasenia);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDatosConstancia_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");

			$response = $this->MdlconstanciaAfiliacion->obtenerDatosConstancia($sCurp);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarDiagnosticoConstancia_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");

			$response = $this->MdlconstanciaAfiliacion->validarDiagnosticoConstancia($sCurp);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function grabarInformacionConstanciaImpresaESar_post()
	{
		$arrDatos = array();
		try {

			$sCurp = $this->post("sCurp");
			$sContrasenia = $this->post("sContrasenia");
			$fechaEmision = $this->post("fechaEmision");
			$aforeActual = $this->post("aforeActual");
			$fechaVigenciaConstancia = $this->post("fechaVigenciaConstancia");
			$folioConstancia = $this->post("folioConstancia");

			$response = $this->MdlconstanciaAfiliacion->grabarInformacionConstanciaImpresaESar($sCurp, $sContrasenia, $fechaEmision, $aforeActual, $fechaVigenciaConstancia, $folioConstancia);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarDigitalizacion_post()
	{
		$arrDatos = array();
		try {

			$folioSolicitud = $this->post("folioSolicitud");

			$response = $this->MdlconstanciaAfiliacion->validarDigitalizacion($folioSolicitud);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obternerDiasHbiles_post()
	{
		$arrDatos = array();
		try {

			$fechaEmision = $this->post("fechaEmision");

			$response = $this->MdlconstanciaAfiliacion->obternerDiasHbiles($fechaEmision);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}


	public function obternerFechaActual_post()
	{
		$arrDatos = array();
		try {

			$response = $this->MdlconstanciaAfiliacion->obternerFechaActual();

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function revisarAfore_post()
	{
		$arrDatos = array();
		try {

			$aforeRevisa = $this->post("aforeRevisa");

			$response = $this->MdlconstanciaAfiliacion->revisarAfore($aforeRevisa);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerFlagAudio_post()
	{
		$arrDatos = array();
		try {

			$iFolioSolicitud = $this->post("iFolioSolicitud");
			$iOpcion = $this->post("iOpcion");

			$response = $this->MdlconstanciaAfiliacion->obtenerFlagAudio($iFolioSolicitud, $iOpcion);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarExcepcionAudio_post()
	{
		$arrDatos = array();
		try {

			$iFolioEnrolamiento = $this->post("iFolioEnrolamiento");
			$iOpcion = $this->post("iOpcion");
			$iNumDedo = $this->post("iNumDedo");

			$response = $this->MdlconstanciaAfiliacion->guardarExcepcionAudio($iFolioEnrolamiento, $iOpcion, $iNumDedo);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ejecutaSentencia_post()
	{
		$arrDatos = array();
		try {

			$iOpcionFuncion = $this->post("iOpcionFuncion");
			$iParametro1 = $this->post("iParametro1");
			$iParametro2 = $this->post("iParametro2");
			$iParametro3 = $this->post("iParametro3");

			$response = $this->MdlconstanciaAfiliacion->ejecutaSentencia($iOpcionFuncion, $iParametro1, $iParametro2, $iParametro3);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarfirmapublicacion_post()
	{
		$arrDatos = array();
		try {

			$iFolio = $this->post("iFolio");
			$iOpcion = $this->post("iOpcion");
			$response = $this->MdlconstanciaAfiliacion->actualizarfirmapublicacion($iFolio, $iOpcion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerclaveoperacionconstancia_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		$iTabla = $this->post('iTabla');

		try {

			$response = $this->MdlconstanciaAfiliacion->obtenerclaveoperacionconstancia($iFolio, $iTabla);
			if ($response) {
				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarestatusfirmas_post()
	{
		$arrDatos = array();

		$iOpcion = $this->post('iOpcion');
		$iFolio = $this->post('iFolio');
		$iEstatus = $this->post('iEstatus');

		try {

			$response = $this->MdlconstanciaAfiliacion->actualizarestatusfirmas($iOpcion, $iFolio, $iEstatus);
			if ($response) {
				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validartiposolicitanteexcepcion_post()
	{
		$arrDatos = array();

		$iFolioSol = $this->post('iFolioSol');
		try {

			$response = $this->MdlconstanciaAfiliacion->validartiposolicitanteexcepcion($iFolioSol);

			if ($response) {
				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ligaMenuAfore_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlconstanciaAfiliacion->ligaMenuAfore();

			if ($response) {
				$arrDatos['respuesta'] = $response->respuesta;
			}
		} catch (Exception $mensaje) {
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}


	public function obtenerClaveOperacionImp_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlconstanciaAfiliacion->obtenerClaveOperacionImp($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerEstatusAfiliacion_post()
	{
		$arrDatos = array();
		$curp	= $this->post("sCurp");

		try {
			$response   = $this->MdlconstanciaAfiliacion->obtenerEstatusAfiliacion($curp);

			if ($response) {
				$arrDatos['estatus']        = 1;
				$arrDatos['descripcion']    = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'] = $reg;
				}
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion obtenerEstatusAfiliacion";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensajeExcep) {
			$arrDatos['estatus']        = -1;
			$arrDatos['descripcion']    = $mensajeExcep;
			$arrDatos['registros']      = null;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function verificaExpedientePermanente_post()
	{
		$arrDatos = array();

		try {

			$iFolioSerAfore = $this->post("iFolioSerAfore");

			$response = $this->MdlconstanciaAfiliacion->verificaExpedientePermanente($iFolioSerAfore);

			if ($response) {

				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'] = $reg;
				}
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion obtenerRecuperacionFolioConstancia";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensaje) {

			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function getPlatform($user_agent)
	{
		$plataformas = array(
			'Windows 10' => 'Windows NT 10.0+',
			'Windows 8.1' => 'Windows NT 6.3+',
			'Windows 8' => 'Windows NT 6.2+',
			'Windows 7' => 'Windows NT 6.1+',
			'Windows Vista' => 'Windows NT 6.0+',
			'Windows XP' => 'Windows NT 5.1+',
			'Windows 2003' => 'Windows NT 5.2+',
			'Windows' => 'Windows otros',
			'iPhone' => 'iPhone',
			'iPad' => 'iPad',
			'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
			'Mac otros' => 'Macintosh',
			'Android' => 'Android',
			'BlackBerry' => 'BlackBerry',
			'Linux' => 'Linux',
		);
		foreach ($plataformas as $plataforma => $pattern) {
			//if (array_exist($pattern, $user_agent))
			if (preg_match('/' . $pattern . '/', $user_agent))
				return $plataforma;
		}
		return 'No detectado';
	}

	public function dividirTemplate_post()
	{

		$ucImg = $this->post("ucImg");

		$template = array();

		$n = ceil(strlen($ucImg) / 88);

		for ($i = 0; $i < 88; $i++) {
			$temporal =  substr($ucImg, 0, $n);
			//$template[$i] = $this->str2bin($temporal); // CONVIERTE A BINARIO
			$template[$i] = bin2hex($temporal); // CONVIERTE A HEXADECIMAL
			$ucImg =  substr($ucImg, - (strlen($ucImg) - $n));
		}

		$this->response($template, REST_Controller::HTTP_OK);
	}

	public function str2bin($str)
	{

		$bin = "";
		$str_arr = str_split($str, 4);

		for ($i = 0; $i < count($str_arr); $i++)
			$bin = $bin . str_pad(decbin(hexdec(bin2hex($str_arr[$i]))), strlen($str_arr[$i]) * 8, "0", STR_PAD_LEFT);
		return $bin;
	}

	public function generarArchivoHuellas_post()
	{
		$arrDatos = array();

		try {
			$nombreArchivo = $this->post("nombreArchivo");
			$template = $this->post("template");
			$sRuta = "/sysx/progs/web/entrada/huellas/enrolamiento/";
			$sRuta = $sRuta . $nombreArchivo . '.txt';

			if (file_put_contents($sRuta, $template)) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			} else {
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "ERROR AL GENERAR ARCHIVO";
				$arrDatos['respuesta'] = 0;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = 0;
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificarTelefono_post()
	{
		$numero	= $this->post("sNumero");
		$numeroLada	= $this->post("sNumeroLada");
		try {
			$response = $this->MdlconstanciaAfiliacion->verificarTelefono($numero, $numeroLada);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['registros'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function obtenerErrorMensajeFct_post()
	{
		$arrDatos = [];
	
		$cFolioConoTrasp = $this->post("sFolioConoTrasp");
		try {
		
			$response = $this->MdlconstanciaAfiliacion->obtenerErrorMensajeFct($cFolioConoTrasp);
			if ($response) {
				//$resultAPI = json_encode($response,true);

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['irespuesta'] = $reg->mensaje;
					$arrDatos['icodigo'] = $reg->codigo;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}
	
	
	
	
	
		//$array = json_decode(json_encode($arrDatos), True);
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function fnGuardarFolioColsolicitud_post()
	{
		$arrDatos = [];
	
		$cfolioTrasCono = $this->post("sFolioTrasCono");
		$cCurpBucar = $this->post("sCurpBucar");
	
	
		try {
			$response = $this->MdlconstanciaAfiliacion->fnGuardarFolioColsolicitud($cfolioTrasCono, $cCurpBucar);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}
	
		//$array = json_decode(json_encode($arrDatos), True);
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function insertarTipoEntregaCorreoTelefono_post()
	{
		$arrDatos = [];
	
		$tipoEntrega = $this->post("stipoEntrega");
		$cCurptrabajado = $this->post("sCurptrabajado");
		$tipoCorreoTelefono = $this->post("stipoCorreoTelefono");
		$ipservidor = $this->post("ipservidor");
		$numPromotor =  $this->post("iEmpleado");
	
	
	
	
	
		try {
			$response = $this->MdlconstanciaAfiliacion->insertarTipoEntregaCorreoTelefono($tipoEntrega, $cCurptrabajado, $tipoCorreoTelefono, $ipservidor, $numPromotor);
		
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}
	
		//$array = json_decode(json_encode($arrDatos), True);
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function obtencionFolioSulicitud_post()
	{
		$arrDatos = [];
	
		$sCurp = $this->post("sCurpBucar");
	
		try {
			$response = $this->MdlconstanciaAfiliacion->obtencionFolioSulicitud($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}

		//$array = json_decode(json_encode($arrDatos), True);
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function obtenerIPServidor_post()
	{
		$sIpRemoto = $this->post('sIpRemoto');
		//
		try {
			$response = $this->MdlconstanciaAfiliacion->obtenerIPServidor($sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarcodigoautenticacion_post()
	{
		$iFolioCons	= $this->post("iFolioCons");
		$codigoIne	= $this->post("codigoIne");

		try {
			$response   = $this->MdlconstanciaAfiliacion->validarcodigoautenticacion($iFolioCons, $codigoIne);

			if ($response) {
				$arrDatos['estatus']        = 1;
				$arrDatos['descripcion']    = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'] = $reg;
				}
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion validarcodigoautenticacion";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensajeExcep) {
			$arrDatos['estatus']        = -1;
			$arrDatos['descripcion']    = $mensajeExcep;
			$arrDatos['registros']      = null;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function actualizarSolEstatusExpiden_post()
	{
		$folioCons = $this->post("folioCons");
		$curp = $this->post("curp");

		try {
			$response = $this->MdlconstanciaAfiliacion->actualizarSolEstatusExpiden($folioCons, $curp);
			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function insertaDatosAcuse_post()
	{
		$nombre = $this->post('nombre');
		$curp = $this->post('curp');
		$folio = $this->post('folio');
		$opcionacuse = $this->post('opcionacuse');
		//
		try {
			$response = $this->MdlconstanciaAfiliacion->insertaDatosAcuse($nombre,$curp,$folio,$opcionacuse);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function envioexp_post()
	{
		$folioafiliacion = $this->post('folioafiliacion');
		$archivo = $this->post('archivo');
		$imdata = $this->post('imdata');
		//
		try {
			$response = $this->MdlconstanciaAfiliacion->envioexp($folioafiliacion,$archivo,$imdata);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ActualizarExpedientePermante_post(){
		$folioCons = $this->post("folioConst");
		$curp = $this->post("curp");

		try {
				$response   = $this->MdlconstanciaAfiliacion->ActualizarExpedientePermante($folioCons,$curp);

				if ($response){
						$arrDatos['estatus']        = 1;
						$arrDatos['descripcion']    = "EXITO";
						
						foreach($response as $reg)
						{
							$arrDatos['actualizaestatus'] = $reg;
						}
						
				}else{
						$arrDatos['estatus']        = -2;
						$arrDatos['descripcion']    = "Error al ejecutar la funcion ActualizarExpedientePermante";
						$arrDatos['actualizaestatus']      = null;
				}
		}catch (Exception $mensajeExcep){
				$arrDatos['estatus']        = -1;
				$arrDatos['descripcion']    = $mensajeExcep;
				$arrDatos['actualizaestatus']      = null;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function ObtenerDoctoIndexado_post()
    {
		$arrDatos = array();
		try {

			$info = $this->post("info");
			$info = json_decode($info,true);
			$iOpcion = $info["iOpcion"];
			$iFolioAfiSer = $info["iFolioAfiSer"];
			$cNomenclatura = $info["cNomenclatura"];
			$iTabla = $info["iTabla"];

			$shRet = 0;
			$shFirmaElectronica = 0;
			$shTotalDoctosFirmaDigital = 0;
			$iBandera_FTE0 = 0;
			$respuesta=0;
		
			if($iTabla != 8){

				if(($iTabla == 3) || ($iTabla == 6) || ($iTabla == 1) || ($iTabla == 2)){

					$var=$cNomenclatura;
					$shRet = $this->MdlconstanciaAfiliacion->fnExisteCurpRenapo($iFolioAfiSer,$var);

					foreach($shRet as $reg){
						$respuesta=(int)$reg->count;
					}
				}
			}else{ //REENROLAMIENTO
				$shRet = $this->MdlconstanciaAfiliacion->fnconsultardocumento($iFolioAfiSer,$cNomenclatura,$iOpcion);
				foreach($shRet as $reg){
					$respuesta=(int)$reg->fnconsultardocumento;
				}
			}

			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			$arrDatos['nomenclatura'] = $cNomenclatura;
			$arrDatos['respuesta'] = $respuesta;

		}
    	catch (Exception $mensaje)
    	{
				$arrDatos['estatus'] = -1;
				$arrDatos['nomenclatura'] = $info;
				$arrDatos['descripcion'] = $mensaje;
				$arrDatos['respuesta'] =null;
		  }
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenertipocomprobante_post()
	{
		$arrDatos = array();
		try
		{
			$info = $this->post("info");
			$info = json_decode($info,true);
			$iFolio = (int)$info["iFolio"];

			$response = $this->MdlconstanciaAfiliacion->obtenertipocomprobante($iFolio);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = (int)$response->tipocomprobante;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function rotarVideo($name_archivo)
    {
    	try {

			//Ruta donde se encuentra el archivo
			$ruta_in ="/sysx/progs/web/entrada/video/".$name_archivo.".mp4";
			//Ruta donde se alojara el archivo de la salida
			$ruta_out ="/sysx/progs/web/entrada/video/".$name_archivo."_out.mp4";
			//expresion regular que verifica si el video se encuenta rotado
			$rotate_pattern = "/rotate\\s*:\s*([0-9]+)/i";
			//se usa como bandera para indicar si el video esta girado
			$has_rotation = false;
			//comando de linux que se encarga de traer la informacion del video
			$cmd = "ffmpeg -v verbose -i '$ruta_in' 2>&1";
			//se ejecuta el comando
			$video_info = shell_exec($cmd);

			//let's check against the rotation pattern
			preg_match($rotate_pattern,$video_info,$matches);
			//se comprueba la rotacion
			if(is_array($matches))
			{
				if(isset($matches[1]))
				{
					$has_rotation = true;
				}
			}
			//let's see if the video has rotation or not
			if($has_rotation)
			{
				//rotate the video using transpose and write an output file
				$cmd = "ffmpeg -v verbose -i '$ruta_in' -vf 'transpose=2,transpose=1' -y '$ruta_out' 2>&1";

				shell_exec($cmd);

				//eliminamos archivo original
				if(file_exists($ruta_in)) {
					unlink($ruta_in);
				}
				if (file_exists($ruta_out)) {
					rename ($ruta_out, $ruta_in ="/sysx/progs/web/entrada/video/".$name_archivo.".webm");
				}

			
			}
			else{
				rename ($ruta_in, $ruta_in ="/sysx/progs/web/entrada/video/".$name_archivo.".webm");
				return false;
			}
    	}
    	catch (Exception $mensaje)
    	{
			return false;
		}

		return true;
	}

	public function fntieneautenticacionine_post()
	{
		$arrDatos = array();
		$sCurp = $this->post("sCurp");
		$iEmpleado = $this->post("iEmpleado");
		
		try {
			$response = $this->MdlconstanciaAfiliacion->fntieneautenticacionine($sCurp,$iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//**********************Folio 539*********************** */

	public function obtenerLlaveDatosBancoppel_post(){
		$arrDatos = array();

		//Obtengo los parametros
		$curp = $this->post("curp");

		try
		{
			$response = $this->MdlconstanciaAfiliacion->obtenerLlaveDatosBancoppel($curp);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response;
			}else{
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error en la consulta";
				$arrDatos['respuesta'] = null;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerEntidadNacimiento_post(){
		$arrDatos = array();

		try
		{
			$response = $this->MdlconstanciaAfiliacion->obtenerEntidadNacimiento();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response;
			}else{
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error en la consulta";
				$arrDatos['respuesta'] = null;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerurlwsbancoppel_post(){
		$arrDatos = array();

		try
		{
			$response = $this->MdlconstanciaAfiliacion->obtenerurlwsbancoppel();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response;
			}else{
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error en la consulta";
				$arrDatos['respuesta'] = null;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarBitacoraRespuestaBancoppel_post(){
		$arrDatos = array();

		//Obtengo los parametros
		$folioafiliacion = $this->post("folioafiliacion");
		$fechaconsulta = $this->post("fechaconsulta");
		$horaconsulta = $this->post("horaconsulta");
		$iTienda = $this->post("iTienda");
		$empleado = $this->post("empleado");
		$idanverso = $this->post("idanverso");
		$idreverso = $this->post("idreverso");
		$curp = $this->post("curp");
		$appaterno = $this->post("appaterno");
		$apmaterno = $this->post("apmaterno");
		$nombres = $this->post("nombres");
		$rfc = $this->post("rfc");
		$fechanac = $this->post("fechanac");
		$entidad = $this->post("entidad");
		$genero = $this->post("genero");
		$nacionalidad = $this->post("nacionalidad");
		$edoCivil = $this->post("edoCivil");
		$numClienteCoppel = $this->post("numClienteCoppel");
		$numClienteBancoppel = $this->post("numClienteBancoppel");
		$nvlEstudio = $this->post("nvlEstudio");
		$profesion = $this->post("profesion");
		$giroNegocio = $this->post("giroNegocio");
		$telefono1 = $this->post("telefono1");
		$telefono2 = $this->post("telefono2");
		$correo = $this->post("correo");
		$calle = $this->post("calle");
		$numExterior = $this->post("numExterior");
		$numInterior = $this->post("numInterior");
		$codigoPostal = $this->post("codigoPostal");
		$colonia = $this->post("colonia");
		$ciudad = $this->post("ciudad");
		$delegacion = $this->post("delegacion");
		$estado = $this->post("estado");
		$pais = $this->post("pais");
		$calleCobranza = $this->post("calleCobranza");
		$numExteriorCobranza = $this->post("numExteriorCobranza");
		$numInteriorCobranza = $this->post("numInteriorCobranza");
		$codigoPostalCobranza = $this->post("codigoPostalCobranza");
		$coloniaCobranza = $this->post("coloniaCobranza");
		$ciudadCobranza = $this->post("ciudadCobranza");
		$delegacionCobranza = $this->post("delegacionCobranza");
		$estadoCobranza = $this->post("estadoCobranza");
		$paisCobranza = $this->post("paisCobranza");
		$calleLaboral = $this->post("calleLaboral");
		$numExteriorLaboral = $this->post("numExteriorLaboral");
		$numInteriorLaboral = $this->post("numInteriorLaboral");
		$codigoPostalLaboral = $this->post("codigoPostalLaboral");
		$coloniaLaboral = $this->post("coloniaLaboral");
		$ciudadLaboral = $this->post("ciudadLaboral");
		$delegacionLaboral = $this->post("delegacionLaboral");
		$estadoLaboral = $this->post("estadoLaboral");
		$paisLaboral = $this->post("paisLaboral");
		$apPaternoRef1 = $this->post("apPaternoRef1");
		$apMaternoRef1 = $this->post("apMaternoRef1");
		$nombresRef1 = $this->post("nombresRef1");
		$apPaternoRef2 = $this->post("apPaternoRef2");
		$apMaternoRef2 = $this->post("apMaternoRef2");
		$nombresRef2 = $this->post("nombresRef2");
		$ctaTradicionalVigente = $this->post("ctaTradicionalVigente");
		$ctaNomina = $this->post("ctaNomina");
		$opcionbcpl = $this->post("opcionbcpl");

		try
		{
			$response = $this->MdlconstanciaAfiliacion->guardarBitacoraRespuestaBancoppel($folioafiliacion,$fechaconsulta,$horaconsulta,$iTienda,$empleado,$idanverso,$idreverso,$curp,$appaterno,$apmaterno,$nombres,$rfc,$fechanac,$entidad,$genero,$nacionalidad,$edoCivil,$numClienteCoppel,$numClienteBancoppel,$nvlEstudio,$profesion,$giroNegocio,$telefono1,$telefono2,$correo,$calle,$numExterior,$numInterior,$codigoPostal,$colonia,$ciudad,$delegacion,$estado,$pais,$calleCobranza,$numExteriorCobranza,$numInteriorCobranza,$codigoPostalCobranza,$coloniaCobranza,$ciudadCobranza,$delegacionCobranza,$estadoCobranza,$paisCobranza,$calleLaboral,$numExteriorLaboral,$numInteriorLaboral,$codigoPostalLaboral,$coloniaLaboral,$ciudadLaboral,$delegacionLaboral,$estadoLaboral,$paisLaboral,$apPaternoRef1,$apMaternoRef1,$nombresRef1,$apPaternoRef2,$apMaternoRef2,$nombresRef2,$ctaTradicionalVigente,$ctaNomina,$opcionbcpl);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response;
			}else{
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error en la consulta";
				$arrDatos['respuesta'] = null;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function tieneDatosBancoppel_post(){
		$arrDatos = array();
		$folioafiliacion = $this->post("folioafiliacion");
		$curp = $this->post("curp");
		$opcionbcpl = $this->post("opcionbcpl");

		try
		{
			$response = $this->MdlconstanciaAfiliacion->tieneDatosBancoppel($folioafiliacion, $opcionbcpl);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response;
			}else{
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error en la consulta";
				$arrDatos['respuesta'] = null;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerParametrosWSBancoppel_post(){
		$arrDatos = array();

		try
		{
			$response = $this->MdlconstanciaAfiliacion->obtenerParametrosWSBancoppel();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response;
			}else{
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = "Error en la consulta";
				$arrDatos['respuesta'] = null;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validaEstatusVideo_post(){
		$arrDatos = array();
		$folioafiliacion = $this->post("folioafiliacion");
		
		try {
			$response = $this->MdlconstanciaAfiliacion->validaEstatusVideo($folioafiliacion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function validarFoliosPendientesRenapo_post()
    {

    	try {

    		$response = $this->MdlconstanciaAfiliacion->validarFoliosPendientesRenapo();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][] = $reg;
				}
			}
    	}
    	catch (Exception $mensaje)
    	{
    		$arrDatos['estatus'] = -1;
			  $arrDatos['descripcion'] = $mensaje;
			  $arrDatos['registros'] = null;
		  }

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function actualizaStatusProcesoSol_post()
    {

    	try {
			$iOpcion = $this->post('iOpcion');
			$iFolio = $this->post('iFolio');
			$sCurp = $this->post('sCurp');

    		$response = $this->MdlconstanciaAfiliacion->actualizaStatusProcesoSol($iOpcion, $iFolio, $sCurp);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][] = $reg;
				}
			}
    	}
    	catch (Exception $mensaje)
    	{
    		$arrDatos['estatus'] = -1;
			  $arrDatos['descripcion'] = $mensaje;
			  $arrDatos['registros'] = null;
		  }

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	public function verificarBanderaRenapo_post()
    {
		try
		{

			$response = $this->MdlconstanciaAfiliacion->verificarBanderaRenapo();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][]=$reg;
				}
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['registros'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);

	}
	public function consultarDiasSolicitudRenapo_post()
    {
		try
		{

			$response = $this->MdlconstanciaAfiliacion->consultarDiasSolicitudRenapo();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][]=$reg;
				}
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['registros'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);

	}
	
	public function consultarHorasSolicitudRenapo_post()
    {
		try
		{

			$response = $this->MdlconstanciaAfiliacion->consultarHorasSolicitudRenapo();

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][]=$reg;
				}
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['registros'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);

	}
	
	public function consultartoken_post()
	{
		$arrDatos = array();
		$token = $this->post('token');
		$direccionip = $this->post('direccionip');

		try {
			$response = $this->MdlconstanciaAfiliacion->consultartoken($token,$direccionip);
			
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	public function verificajfuse_post()
	{
		$arrDatos = array();
		$iFolioAfore = $this->post('iFolioAfore');

		try {
			$response = $this->MdlconstanciaAfiliacion->verificajfuse($iFolioAfore);
			
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	public function insertartoken_post()
	{
		$arrDatos = array();
		$token = $this->post('token');
		$direccionip = $this->post('direccionip');

		try {
			$response = $this->MdlconstanciaAfiliacion->insertartoken($token,$direccionip);
			
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	//FOLIO 1498
	public function fnGestorBandera_post()
	{
		$arrDatos = array();
		$iflag = $this->post("iflag");
		
		try {
			$response = $this->MdlconstanciaAfiliacion->fnGestorBandera($iflag);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	public function Autenticadactilar_post()
	{
		$arrDatos = array();
		$iFolioGestor = $this->post('iFolioGestor');
		$sCurp = $this->post('sCurp');
		$iopc = $this->post('iopc');

		try {
			$response = $this->MdlconstanciaAfiliacion->Autenticadactilar($iFolioGestor,$sCurp,$iopc);
			
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK); 
		
	}//FOLIO 1498
}
