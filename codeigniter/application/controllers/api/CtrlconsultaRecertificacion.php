<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlConsultaRecertificacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('MdlconsultaRecertificacion');
	}

	public function obtnerinformacionpromotor_post()
	{
		$arrDatos = array();
		$iEmpleado = $this->post('iEmpleado');
		try {
			$response = $this->MdlconsultaRecertificacion->obtnerinformacionpromotor($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerSolicitudRecertificacion_post()
	{
		$arrDatos = array();
		$iFolioservicio = $this->post('iFolioservicio');
		$cCurp = $this->post('cCurp');
		$cNss = $this->post('cNss');
		try {
			$response = $this->MdlconsultaRecertificacion->obtenerSolicitudRecertificacion($iFolioservicio, $cCurp, $cNss);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDatosTrabajadorAfop_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		try {
			$response = $this->MdlconsultaRecertificacion->obtenerDatosTrabajadorAfop($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerdiagnosticoservicio_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		try {
			$response = $this->MdlconsultaRecertificacion->fnobtenerdiagnosticoservicio($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
