<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class Ctrldoctorn extends Util
{

    // SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
    private $result = array();

    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();

        $componente = (!$this->post('componente')) ? false : true;
        $this->result = $this->verificacionPermisos($componente, "");
        //load models
        $this->load->model('Mdldoctorn');
        $this->load->model('MdlMetodosGenerales');
    }

    public function folioColSolicitudes_post()
    {
        $arrDatos = array();
        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->folioColSolicitudes($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function empNominaColPromotorTitular_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->empNominaColPromotorTitular($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function empCapturoColSolicitudes_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->empCapturoColSolicitudes($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }
    public function claveConsarNombreEmpnomina_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->claveConsarNombreEmpnomina($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function claveConsarNombreEmpNominaColPromotor_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->claveConsarNombreEmpNominaColPromotor($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function tipoSolicitudAforecedenteColsolicitudes_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->tipoSolicitudAforecedenteColsolicitudes($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function nombreAforeCataforehrnIdCodigoAfore_post()
    {
        $arrDatos = array();
        $iAforeCedente = $this->post("iAforeCedente");

        try {
            $response   = $this->Mdldoctorn->nombreAforeCataforehrnIdCodigoAfore($iAforeCedente);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function coldoctosrendimientosneto_post()
    {
        $arrDatos = array();
        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->coldoctosrendimientosneto($iFolio);
            $arrDatos = successSinforeachConObject($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function colDoctoComisiones_post()
    {
        $arrDatos = array();
        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->colDoctoComisiones($iFolio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function colinfxPromotoresClaveConsar_post()
    {
        $arrDatos = array();
        $cDatPromotorCveConsar = $this->post("cDatPromotorCveConsar");

        try {
            $response   = $this->Mdldoctorn->colinfxPromotoresClaveConsar($cDatPromotorCveConsar);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnRespuestaDoctoRendimientoNetoProcesar_post()
    {
        $arrDatos = array();
        $iSiefore = $this->post("iSiefore");

        try {
            $response   = $this->Mdldoctorn->fnRespuestaDoctoRendimientoNetoProcesar($iSiefore);
            $arrDatos = successforeach($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function datosDoctorn_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response   = $this->Mdldoctorn->datosDoctorn($iFolio);
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnobtenersieforegeneracionalag_post()
    {
        $arrDatos = array();

        $cCurpTrab = $this->post("cCurpTrab");

        try {
            $response = $this->Mdldoctorn->fnobtenersieforegeneracionalag($cCurpTrab);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnobtenertiposolicitanteexcepcion_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");

        try {
            $response = $this->Mdldoctorn->fnobtenertiposolicitanteexcepcion($iFolio);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnobtenerhuellasdoctosafiliacion_post()
    {
        $arrDatos = array();

        $iFolio = $this->post("iFolio");
        $sCurpTrab = $this->post("sCurpTrab");
        $iIDServicio = $this->post("iIDServicio");
        $cTipoOperacion = $this->post("cTipoOperacion");
        $tiposolicitante = $this->post("tiposolicitante");

        try {
            $response = $this->Mdldoctorn->fnobtenerhuellasdoctosafiliacion($iFolio,$sCurpTrab,$iIDServicio,$cTipoOperacion,$tiposolicitante);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnobtenerfondosgeneracionalesag_post()
    {
        $arrDatos = array();

        try {
            $response = $this->Mdldoctorn->fnobtenerfondosgeneracionalesag();    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnobtenernombrefondosgeneracionalesag_post()
    {
        $arrDatos = array();
        $folio = $this->post("folio");

        try {
            $response = $this->Mdldoctorn->fnobtenernombrefondosgeneracionalesag($folio);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function actualizarpublicarimagenservicios_post()
    {
        $arrDatos = array();
        $iFolio = $this->post("iFolio");
        $iOpcionFunc = $this->post("iOpcionFunc");

        try {
            $response = $this->Mdldoctorn->actualizarpublicarimagenservicios($iFolio,$iOpcionFunc);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function guardarpdf_post()
    {
        $arrDatos = array();
        $iFolioSol = $this->post("iFolioSol");
        $rutaPdf = $this->post("rutaPdf");

        try {
            $response = $this->Mdldoctorn->guardarpdf($iFolioSol,$rutaPdf);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function fnConsultarEmpleadoConstancia_post()
    {
        $arrDatos = array();
        $iEmpleado = $this->post("iEmpleado");

        try {
            $response = $this->Mdldoctorn->fnConsultarEmpleadoConstancia($iEmpleado);    
            $arrDatos = successSinforeachConObject($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    
    public function tbfondosgeneracionalesag_post()
    {
        $arrDatos = array();
        $iSiefore = $this->post("iSiefore");

        try {
            $response = $this->Mdldoctorn->tbfondosgeneracionalesag($iSiefore);    
            $arrDatos = successforeach($response);
        }catch (Exception $mensajeExcep){
            $arrDatos = error($mensajeExcep);

        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }


}
