<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class CtrlsolicitudAfiliacion extends Util
{

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->verificacionPermisos($componente, TRUE, "");

		//load models
		$this->load->model('MdlsolicitudAfiliacion');
	}

	public function consultarHuellas_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		$sCurpTrab = $this->post('sCurpTrab');
		$iIDServicio = $this->post('iIDServicio');
		$cTipoOperacion = $this->post('cTipoOperacion');

		try {
			$response = $this->MdlsolicitudAfiliacion->consultarHuellas($iFolio, $sCurpTrab, $iIDServicio, $cTipoOperacion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarHuellasRegistro_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$sCurpTrab = $this->post('sCurpTrab');
		$iIDServicio = $this->post('iIDServicio');
		$cTipoOperacion = $this->post('cTipoOperacion');
		$tiposolicitante = $this->post('tiposolicitante');

		try {
			$response = $this->MdlsolicitudAfiliacion->consultarHuellasRegistro($iFolio, $sCurpTrab, $iIDServicio, $cTipoOperacion, $tiposolicitante);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validartiposolicitanteexcepcion_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');

		try {
			$response = $this->MdlsolicitudAfiliacion->validartiposolicitanteexcepcion($iFolioSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerCurpPromotor_post()
	{
		$arrDatos = array();
		$iNumeroEmpleado = $this->post('iNumeroEmpleado');

		try {
			$response = $this->MdlsolicitudAfiliacion->obtenerCurpPromotor($iNumeroEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarFirmaPublicacion_post()
	{
		$arrDatos = array();

		try {

			$iFolio = $this->post('iFolio');
			$iOpcion = $this->post('iOpcion');
			$response = $this->MdlsolicitudAfiliacion->actualizarFirmaPublicacion($iFolio, $iOpcion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerclaveoperacion_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		$iTabla = $this->post('iTabla');

		try {
			$response = $this->MdlsolicitudAfiliacion->obtenerclaveoperacion($iFolio, $iTabla);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaColSolicitudes_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlsolicitudAfiliacion->consultaColSolicitudes($iFolio);
			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaColgenerasolicitudregtrasp_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->MdlsolicitudAfiliacion->consultaColgenerasolicitudregtrasp($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaTiposolicitud_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlsolicitudAfiliacion->consultaTiposolicitud($iFolio);
			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaMotivotraspaso_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlsolicitudAfiliacion->consultaMotivotraspaso($iFolio);
			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaTelefonos_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->MdlsolicitudAfiliacion->consultaTelefonos($iFolio);
			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function curpColgenerasolicitudregtrasp_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->MdlsolicitudAfiliacion->curpColgenerasolicitudregtrasp($iFolio);
			$arrDatos = successforeachSinObject($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ctrlImagenesPorTransmitir_post()
	{
		$arrDatos = array();

		$iOpcion = $this->post('iOpcion');
		$iFolio = $this->post('iFolio');
		$cNombreArchivo = $this->post('cNombreArchivo');

		try {
			$response = $this->MdlsolicitudAfiliacion->ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo);
			$arrDatos = successforeachSinObject($response );
		} catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}