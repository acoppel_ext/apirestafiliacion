<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlFormatoCurpRenapo extends Util
{

    // SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
    private $result = array();

    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }

        parent::__construct();

        $componente = (!$this->post('componente')) ? false : true;
        $this->result = $this->verificacionPermisos($componente, "");


        //load models
        $this->load->model('MdlformatoCurpRenapo');
    }

    public function consultarBotones_post()
    {
        $arrDatos = array();
        $folio = $this->post('folio');

        try {
            $response = $this->MdlformatoCurpRenapo->consultarBotones($folio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function estatusPDF_post()
    {
        $arrDatos = array();
        $folioAfore = $this->post('folioAfore');
        $folioSolicitud = $this->post('folioSolicitud');

        try {
            $response = $this->MdlformatoCurpRenapo->estatusPDF($folioAfore, $folioSolicitud);
            $arrDatos = successforeach($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function publicarImg_post()
    {
        $arrDatos = array();
        $folio = $this->post('folio');
        $idOpcion = $this->post('idOpcion');

        try {
            $response = $this->MdlformatoCurpRenapo->publicarImg($folio, $idOpcion);
            $arrDatos = successforeach($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function afiliacionServicio_post()
    {
        $arrDatos = array();
        $folio = $this->post('folio');

        try {
            $response = $this->MdlformatoCurpRenapo->afiliacionServicio($folio);
            $arrDatos = successforeach($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function imagenesTransmitir_post()
    {
        $arrData = array(
            'iOpcion' => $this->post('iOpcion'),
            'folio' => $this->post('folio'),
            'cIdImagen' => $this->post('cIdImagen'),
            'estatusPublica' => $this->post('estatusPublica'),
            'nomDocumento' => $this->post('nomDocumento')
        );
        $arrDatos = array();

        try {
            $response = $this->MdlformatoCurpRenapo->imagenesTransmitir($arrData);
            $arrDatos = successforeach($response);
        } catch (Exception $mensaje) {
            $arrDatos = error($mensaje);
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }
}
