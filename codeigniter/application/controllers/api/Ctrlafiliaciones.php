<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH.'/libraries/REST_Controller.php');
require(APPPATH.'/controllers/util/Util.php');

class ctrlafiliaciones extends Util {

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

    public function __construct()
    {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		parent::__construct();
	
		$componente = (!$this->post('componente') ) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");
        $this->load->model('Mdlafiliaciones');
    }

	public function Obtenerconfiguracionpantallas_post()
    {
		$arrStep = array('isstep' => false);
		$arrStep = (object)$arrStep;

		try
		{
    		$response = $this->Mdlafiliaciones->Obtenerconfiguracionpantallas();


			if($response)
			{
				//indicador que asigna estatus 1, sea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$registro = $this->Mdlafiliaciones->removerCaracteresEspeciales($reg);

					$registro->isstep = true;
					$arrDatos['registros'][] = $registro;
					$arrDatos['registros'][] = $arrStep;
				}
			}
    	}
    	catch (Exception $mensaje)
    	{
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
    }
}
