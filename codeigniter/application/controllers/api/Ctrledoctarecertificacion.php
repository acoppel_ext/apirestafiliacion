<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlEdoctarecertificacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('Mdledoctarecertificacion');
	}

	public function obtenAnioCuatrimestres_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdledoctarecertificacion->obtenAnioCuatrimestres();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function MostrarDatosCliente_post()
	{
		$arrDatos = array();
		$curp = $this->post('curp');
		$fechaini = $this->post('fechaini');
		$fechafin = $this->post('fechafin');
		$iPeriodo = $this->post('iPeriodo');
		try {
			$response = $this->Mdledoctarecertificacion->MostrarDatosCliente($curp, $fechaini, $fechafin, $iPeriodo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function MostrarInformacionEstados_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdledoctarecertificacion->MostrarInformacionEstados();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function UltimoCuatrimestre_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdledoctarecertificacion->UltimoCuatrimestre();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ObtenerReverso_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		try {
			$response = $this->Mdledoctarecertificacion->ObtenerReverso($sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function Reversocatorce_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		$fechaini = $this->post('fechaini');
		$fechafin = $this->post('fechafin');
		try {
			$response = $this->Mdledoctarecertificacion->Reversocatorce($sCurp, $fechaini, $fechafin);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
