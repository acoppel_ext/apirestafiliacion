<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlconsultaCurprenapo extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");

		//load models
		$this->load->model('MdlconsultaCurprenapo');
	}

	public function obtenerCurpPromotor_post()
	{
		$arrDatos = array();
		$iNumeroEmpleado = $this->post('iNumeroEmpleado');

		try {
			$response = $this->MdlconsultaCurprenapo->obtenerCurpPromotor($iNumeroEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function servicioEjecutarAplicacion_post()
	{
		$arrDatos = array();
		$idServicio = $this->post('idServicio');
		try {
			$response = $this->MdlconsultaCurprenapo->servicioEjecutarAplicacion($idServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function servicioObtenerRespuesta_post()
	{
		$arrDatos = array();
		$idServicio = $this->post('idServicio');

		try {
			$response = $this->MdlconsultaCurprenapo->servicioObtenerRespuesta($idServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaRespuestaRenapo_post()
	{
		$arrDatos = array();
		$iFolioAfore = $this->post('iFolioAfore');

		try {
			$response = $this->MdlconsultaCurprenapo->consultaRespuestaRenapo($iFolioAfore);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarSolicitudRenapo_post()
	{
		$arrDatos = array();

		try {
			$iFolioAfore 			= $this->post('iFolioAfore');
			$iFolioSolicitud 		= $this->post('iFolioSolicitud');
			$iProceso 				= $this->post('iProceso');
			$iEmpleado 				= $this->post('iEmpleado');
			$idsubproceso 			= $this->post('idsubproceso');
			$cCurp 					= $this->post('cCurp');
			$cApellidopaterno 		= $this->post('cApellidopaterno');
			$cApellidomaterno 		= $this->post('cApellidomaterno');
			$cNombres 				= $this->post('cNombres');
			$cSexo 					= $this->post('cSexo');
			$cFechaNacimiento 		= $this->post('cFechaNacimiento');
			$iSlcEntidadNacimiento 	= $this->post('iSlcEntidadNacimiento');
			$iPmodulo				= $this->post('iPmodulo');

			$response = $this->MdlconsultaCurprenapo->guardarSolicitudRenapo(
				$iFolioAfore,
				$iFolioSolicitud,
				$iProceso,
				$iEmpleado,
				$idsubproceso,
				$cCurp,
				$cApellidopaterno,
				$cApellidomaterno,
				$cNombres,
				$cSexo,
				$cFechaNacimiento,
				$iSlcEntidadNacimiento,
				$iPmodulo
			);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ValidarMayoriaEdad_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlconsultaCurprenapo->ValidarMayoriaEdad();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function FechaMaximaMayoriaEdad_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlconsultaCurprenapo->FechaMaximaMayoriaEdad();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoEntidadNacimiento_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlconsultaCurprenapo->consultarCatalogoEntidadNacimiento();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
