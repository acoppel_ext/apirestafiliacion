<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');
class ctrlautenticacionBancoppel extends Util
{

    // SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
    private $result = array();

    public function __construct()
    {

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();

        $componente = (!$this->post('componente')) ? false : true;
        $this->result = $this->verificacionPermisos($componente, "");

        $this->load->model('MdlautenticacionBancoppel');
        $this->load->model('MdlMetodosGenerales');
    }

    public function agregaratostbautenticacionbancoppel_post()
    {

        $arrDatos = array();

        $arrDatosBanco  = array(
            "curp"      => $this->post("cCurp"),
            "nombre"    => $this->post("cNomb"),
            "paterno"   => $this->post("cPaterno"),
            "materno"   => $this->post("cMaterno"),
            "fchnac"    => $this->post("dFchNac"),
            "genero"    => $this->post("iGenero"),
            "tizq"      => $this->post("tempIzq"),
            "tder"      => $this->post("tempDer"),
            "wder"      => $this->post("wsqDer"),
            "modulo"    => $this->post("cModulo"),
            "promotor"  => $this->post("iPromotor"),
            "folbanco"  => 0
        );

        try {
            /*********************************************************************************
             * ID de autenticacion bancoppel 9949
             * Guardar los datos en la tabla control de bancoppel "tbautenticacionbancoppel"
             * Retonar el folio bancoppel
             *********************************************************************************/
            $idServicio = 9949; //IDWS autenticacion bancoppel
            $responseDatos = $this->MdlautenticacionBancoppel->agregaratostbautenticacionbancoppel($arrDatosBanco);
            if ($responseDatos) {
                $arrDatosBanco["folbanco"] = $responseDatos[0]->respuesta;
                /*********************************************************************************
                 * Obtener la cabecera del JSON de envio de datos y conformarlo
                 * Guardar los datos en la tabla control del ws de bancoppel "tbautenticacionbancoppelws"
                 * Retonar si se guardo con exito el JSON de envio
                 *********************************************************************************/
                $responseJson = $this->MdlautenticacionBancoppel->creacionjsonbancoppel($arrDatosBanco["folbanco"]);
                if ($responseJson) {
                    if ($responseJson[0]->respuesta == 1) {
                        $arrDatosXML  = array(
                            "identificationIDControlIdentification" => "0102",
                            "folioSolicitud"                        => $arrDatosBanco["folbanco"],
                            "curpTrabajador"                        => $arrDatosBanco["curp"],
                            "numeroEmpleado"                        => $arrDatosBanco["promotor"]
                        );
                        /*********************************************************************************
                         * Se envia el ID del servicio para la detonacion del WS
                         * Se arma el XML con los datos pertinentes del WS
                         * Se obtiene la URL del WS
                         * Se detona el SOAP con el xml
                         * Retonar la respuesta del WS
                         *********************************************************************************/
                        $responseWS = $this->MdlMetodosGenerales->ejecutarWS($idServicio, $arrDatosXML);
                        if ($responseWS) {
                            $arrDatosResp       = array(
                                "foliobanco"    => $arrDatosBanco["folbanco"],
                                "WS"            => $responseWS
                            );
                            $arrDatos['estatus']        = 1;
                            $arrDatos['descripcion']    = "EXITO";
                            $arrDatos['registros'][]    = $arrDatosResp;
                        } else {
                            $arrDatos['estatus']        = -5;
                            $arrDatos['descripcion']    = "Error al detonar el WS de autenticacion bancoppel";
                            $arrDatos['registros']      = null;
                        }
                    } else {
                        $arrDatos['estatus']        = -4;
                        $arrDatos['descripcion']    = "Error al guardar en tbautenticacionbancoppel";
                        $arrDatos['registros']      = null;
                    }
                    $arrDatosBanco  = array();
                } else {
                    $arrDatos['estatus']        = -3;
                    $arrDatos['descripcion']    = "Error al crear el json de envio";
                    $arrDatos['registros']      = null;
                }
            } else {
                $arrDatos['estatus']        = -2;
                $arrDatos['descripcion']    = "Error al guardar en tbautenticacionbancoppel";
                $arrDatos['registros']      = null;
            }
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function validarautenticacionbancoppelws_post()
    {

        $arrDatos = array();

        $foliobanco = $this->post("iFolBanco");
        $porcentaje = $this->post("dPorciento");

        try {

            $response   = $this->MdlautenticacionBancoppel->validarautenticacionbancoppelws($foliobanco, $porcentaje);
            $arrDatos = successSinforeachConObject($response);
            
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function python_post()
    {
        try {
            $folioserv = $this->post('folioserv');
            $output = array();

            $resultado = exec("/bin/python /sysx/progs/afore/enviosmsine/enviosmsine.py " . $folioserv . " 2", $output);

            if ($resultado == 1) {

                $arrDatos['estatus']        = 1;
                $arrDatos['descripcion']    = "EXITO";
                $arrDatos['respuesta']    = 1;

            } else {

                $arrDatos['estatus']        = -1;
                $arrDatos['descripcion'] = "Error al ejecutar la consulta";
                $arrDatos['respuesta']    = 0;
            }

        } catch (Exception $mensaje) {
            $arrDatos['respuesta']    = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function leerIP_post()
    {
        try {
            if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
                $resultado = $_SERVER["HTTP_CLIENT_IP"];
            } else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $resultado = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } else {
                $resultado = $_SERVER["REMOTE_ADDR"];
            }
        } catch (Exception $mensaje) {
            $resultado = "";
        }

        $this->response($resultado, REST_Controller::HTTP_OK);
    }

    public function ejecucionBD_post()
    {
        define("DEFAULT__",         0);
        define("CNXEXP__",             -3);
        define("CONSOK__",             4);
        define("CONSERR__",         -5);

        $idCons = $this->post('idCons');
        $Consulta = $this->post('Consulta');
        $BaseDatos = $this->post('BaseDatos');

        $arrDatos = array("irespuesta" => DEFAULT__, $idCons => null);

        try {

            $response = $this->MdlautenticacionBancoppel->ejecucionBD($Consulta, $BaseDatos);

            if ($response) {

                $arrDatos['estatus']        = 1;
                $arrDatos['descripcion']    = "EXITO";
                
                foreach ($response as $reg) {
                    $arrDatos["irespuesta"] = CONSOK__;
                    $arrDatos[$idCons][] = $reg;
                }
            } else {
                $arrDatos['estatus']        = -1;
                $arrDatos['descripcion'] = "Error al ejecutar la consulta";
                $arrDatos["irespuesta"] = CONSERR__;
            }
        } catch (Exception $mensaje) {
            $arrDatos["irespuesta"] = CNXEXP__;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function consultarColonia_post()
    {

        $arrDatos = array();

        $cCodPostal = $this->post("cCodPostal");
        $iEstado = $this->post("iEstado");
        $iCiudad = $this->post("iCiudad");
        $iMunicipio = $this->post("iMunicipio");
        $cNomColonia = $this->post("cNomColonia");

        try {

            $response   = $this->MdlautenticacionBancoppel->consultarColonia($cCodPostal, $iEstado, $iCiudad, $iMunicipio, $cNomColonia);
            $arrDatos = successSinforeachConObject($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function consultarParteDomicilio_post()
    {

        $arrDatos = array();

        $iIdParte = $this->post("iIdParte");
        $iEstado = $this->post("iEstado");

        try {

            $response   = $this->MdlautenticacionBancoppel->consultarParteDomicilio($iIdParte, $iEstado);
            $arrDatos = successSinforeachConObject($response);
        } catch (Exception $mensajeExcep) {
            $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }
}