<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlAutenticaciondeltrabajador extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('Mdlautenticaciondeltrabajador');
	}

	public function obtnerinformacionpromotor_post()
	{
		$arrDatos = array();
		try {
			$iEmpleado = $this->post('iEmpleado');
			$response = $this->Mdlautenticaciondeltrabajador->obtnerinformacionpromotor($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerfolioafilaicion_post()
	{
		$arrDatos = array();
		try {
			$ibusqueda = $this->post('ibusqueda');
			$scurpnss = $this->post('scurpnss');
			$response = $this->Mdlautenticaciondeltrabajador->obtenerfolioafilaicion($ibusqueda, $scurpnss);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerPaginaRedireccionar_post()
	{
		$arrDatos = array();
		try {
			$opcionpag = $this->post('opcionpag');
			$foliosol = $this->post('foliosol');
			$iEmpleado = $this->post('iEmpleado');
			$sIpRemoto = $this->post('sIpRemoto');
			$response = $this->Mdlautenticaciondeltrabajador->obtenerPaginaRedireccionar($opcionpag, $foliosol, $iEmpleado, $sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarPendiente_post()
	{
		$arrDatos = array();

		try {

			$empleado = $this->post('empleado');
			$response = $this->Mdlautenticaciondeltrabajador->validarPendiente($empleado);
			$arrDatos = successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obteneMotivosIncompleta_post()
	{
		$arrDatos = array();
		try {

			$response = $this->Mdlautenticaciondeltrabajador->obteneMotivosIncompleta();
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}

		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarPendiente_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');
		$codMot = $this->post('codMot');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->actualizarPendiente($folio, $codMot);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//Clase CAutenticarDomicilio

	public function GrabarAutenticacion_post()
	{
		$arrDatos = array();
		$metodoAutificacion = $this->post('metodoAutificacion');
		$nss = $this->post('nss');
		$curp = $this->post('curp');
		$calleRegistrada = $this->post('calleRegistrada');
		$coloniaRegistrada = $this->post('coloniaRegistrada');
		$ciudadRegistrada = $this->post('ciudadRegistrada');
		$empleado = $this->post('empleado');
		$origen = $this->post('origen');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->GrabarAutenticacion($metodoAutificacion, $nss, $curp, $calleRegistrada, $coloniaRegistrada, $ciudadRegistrada, $empleado, $origen);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//Clase CGeneral

	public function EntidadNacimiento_post()
	{
		$arrDatos = array();
		$idcatalogo = $this->post('idcatalogo');
		$iEstado = $this->post('iEstado');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->EntidadNacimiento($idcatalogo, $iEstado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ejecutivoUeap_post()
	{
		$arrDatos = array();
		$nEmpleado = $this->post('nEmpleado');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->ejecutivoUeap($nEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function buscarFolioAfiliacion_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		$sNss = $this->post('sNss');
		$sCurp = $this->post('sCurp');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->buscarFolioAfiliacion($iFolio, $sNss, $sCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaClienteAutenticacion_post()
	{
		$arrDatos = array();
		$iOrigen = $this->post('iOrigen');
		$sNombre = $this->post('sNombre');
		$sApPaterno = $this->post('sApPaterno');
		$sApMaterno = $this->post('sApMaterno');
		$iEntidadNac = $this->post('iEntidadNac');
		$sFechaNac = $this->post('sFechaNac');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->consultaClienteAutenticacion($iOrigen, $sNombre, $sApPaterno, $sApMaterno, $iEntidadNac, $sFechaNac);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerpuestoempleado_post()
	{
		$arrDatos = array();
		$iEmpleado = $this->post('iEmpleado');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->obtenerpuestoempleado($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}


	public function ConsultarRequiereReEnrolamiento_post()
	{
		$arrDatos = array();
		$sNombre = $this->post('sNombre');
		$sApPaterno = $this->post('sApPaterno');
		$sApMaterno = $this->post('sApMaterno');
		$iEntidadNac = $this->post('iEntidadNac');
		$sFechaNac = $this->post('sFechaNac');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->ConsultarRequiereReEnrolamiento($sNombre, $sApPaterno, $sApMaterno, $iEntidadNac, $sFechaNac);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarAsignado_post()
	{
		$arrDatos = array();
		$sNombre = $this->post('sNombre');
		$sApPaterno = $this->post('sApPaterno');
		$sApMaterno = $this->post('sApMaterno');
		$sFechaNac = $this->post('sFechaNac');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->validarAsignado($sNombre, $sApPaterno, $sApMaterno, $sFechaNac);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function autenticarcita_post()
	{
		$arrDatos = array();
		$iOpcion = $this->post('iOpcion');
		$cCurp = $this->post('cCurp');
		$cNss = $this->post('cNss');
		$iNumEmpleado = $this->post('iNumEmpleado');

		try {
			$response = $this->Mdlautenticaciondeltrabajador->autenticarcita($iOpcion, $cCurp, $cNss, $iNumEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
