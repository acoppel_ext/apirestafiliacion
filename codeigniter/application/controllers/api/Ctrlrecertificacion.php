<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlRecertificacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");
		$this->load->model('Mdlrecertificacion');
	}

	public function obtnerinformacionpromotor_post()
	{
		$arrDatos = array();
		$iEmpleado = $this->post('iEmpleado');

		try {
			$response = $this->Mdlrecertificacion->obtnerinformacionpromotor($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtnerinformacionpromotor_ipRemoto_post()
	{
		$arrDatos = array();
		$sIpRemoto = $this->post('sIpRemoto');

		try {
			$response = $this->Mdlrecertificacion->obtnerinformacionpromotor_ipRemoto($sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function tienerecertificacionpendiente_post()
	{
		$arrDatos = array();
		$cCurpNss = $this->post('cCurpNss');

		try {
			$response = $this->Mdlrecertificacion->tienerecertificacionpendiente($cCurpNss);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function tienerecertificacionpendienteFolio_post()
	{
		$arrDatos = array();
		$iFoliosol = $this->post('iFoliosol');
		$cCurpNss = $this->post('cCurpNss');
		$iOpcion = $this->post('iOpcion');
		$cfechaFin = $this->post('cfechaFin');
		$cFolioSer = $this->post('cFolioSer');
		$cCurp1 = $this->post('cCurp1');
		$cNss1 = $this->post('cNss1');
		$cFechaAlta = $this->post('cFechaAlta');

		try {
			$response = $this->Mdlrecertificacion->tienerecertificacionpendienteFolio($iFoliosol, $cCurpNss, $iOpcion, $cfechaFin, $cFolioSer, $cCurp1, $cNss1, $cFechaAlta);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerTipoTrabajador_post()
	{
		$arrDatos = array();
		$cNss = $this->post('cNss');

		try {
			$response = $this->Mdlrecertificacion->obtenerTipoTrabajador($cNss);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarsolicitudrecertificacion_post()
	{
		$arrDatos = array();
		$iFoliosol = $this->post('iFoliosol');
		$cCurpNss = $this->post('cCurpNss');

		try {
			$response = $this->Mdlrecertificacion->validarsolicitudrecertificacion($iFoliosol, $cCurpNss);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerLigaRecert_post()
	{
		$arrDatos = array();
		$iOrigen = $this->post('iOrigen');
		$iOrigenPagina = $this->post('iOrigenPagina');
		$sIpRemoto = $this->post('sIpRemoto');
		$iImprimir = $this->post('iImprimir');
		$cRutaPDF = $this->post('cRutaPDF');
		$iFoliosol = $this->post('iFoliosol');
		$iEmpleado = $this->post('iEmpleado');
		$iFolioSer = $this->post('iFolioSer');
		$iReguiereEB = $this->post('iReguiereEB');
		$iServicio = $this->post('iServicio');
		$cCurp = $this->post('cCurp');
		$iConsultaEdoc = $this->post('iConsultaEdoc');
		$iTipoImpresion = $this->post('iTipoImpresion');
		$iTipoAcuse = $this->post('iTipoAcuse');
		$iSiefore = $this->post('iSiefore');

		try {
			$response = $this->Mdlrecertificacion->obtenerLigaRecert($iOrigen, $iOrigenPagina, $sIpRemoto, $iImprimir, $cRutaPDF, $iFoliosol, $iEmpleado, $iFolioSer, $iReguiereEB, $iServicio, $cCurp, $iConsultaEdoc, $iTipoImpresion, $iTipoAcuse, $iSiefore);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function insertarDatosRecservicios_post()
	{
		$arrDatos = array();
		$arrDatosRec = $this->post('arrDatosRec');
		$sIpRemoto = $this->post('sIpRemoto');

		try {
			$response = $this->Mdlrecertificacion->insertarDatosRecservicios($arrDatosRec, $sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function insertarDatosSolrecertificacion_post()
	{
		$arrDatos = array();
		$arrDatosSol = $this->post('arrDatosSol');

		try {
			$response = $this->Mdlrecertificacion->insertarDatosSolrecertificacion($arrDatosSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function datosrecertificacion_post()
	{
		$arrDatos = array();
		$iOrigen = $this->post('iOrigen');
		$iFolioSer = $this->post('iFolioSer');
		$iEstatusRecer = $this->post('iEstatusRecer');
		$iCorreoImpresion = $this->post('iCorreoImpresion');
		$sCorreo = $this->post('sCorreo');

		try {
			$response = $this->Mdlrecertificacion->datosrecertificacion($iOrigen, $iFolioSer, $iEstatusRecer, $iCorreoImpresion, $sCorreo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerSiefore_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdlrecertificacion->obtenerSiefore();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerSieforeCurp_post()
	{
		$arrDatos = array();
		$cCurp = $this->post('cCurp');
		$fechaini = $this->post('fechaini');
		$fechafin = $this->post('fechafin');

		try {
			$response = $this->Mdlrecertificacion->obtenerSieforeCurp($cCurp, $fechaini, $fechafin);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function marcarCuenta_post()
	{
		$arrDatos = array();
		$iTipoTrabajador = $this->post('iTipoTrabajador');
		$cNss = $this->post('cNss');

		try {
			$response = $this->Mdlrecertificacion->marcarCuenta($iTipoTrabajador, $cNss);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerConfiguracionSUV_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdlrecertificacion->obtenerConfiguracionSUV();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarrequieresellosuv_post()
	{
		$arrDatos = array();
		$iFolioServicio = $this->post('iFolioServicio');

		try {
			$response = $this->Mdlrecertificacion->validarrequieresellosuv($iFolioServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificaFechaFirma_post()
	{
		$arrDatos = array();
		$cNss = $this->post('cNss');
		$cCurp = $this->post('cCurp');

		try {
			$response = $this->Mdlrecertificacion->verificaFechaFirma($cNss, $cCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarExpedientes_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');
		$cCurp = $this->post('cCurp');

		try {
			$response = $this->Mdlrecertificacion->validarExpedientes($iFolioSer, $cCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarflujoservicio_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');

		try {
			$response = $this->Mdlrecertificacion->actualizarflujoservicio($iFolioSer);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function datosCurpEI_post()
	{
		$arrDatos = array();
		$cNss = $this->post('cNss');
		$cCurp = $this->post('cCurp');

		try {
			$response = $this->Mdlrecertificacion->datosCurpEI($cNss, $cCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function buscarImganeCurp_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');
		$itiposervicio = $this->post('itiposervicio');
		$cCurp = $this->post('cCurp');

		try {
			$response = $this->Mdlrecertificacion->buscarImganeCurp($iFolioSer, $itiposervicio, $cCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function generoFormatoRenapo_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');

		try {
			$response = $this->Mdlrecertificacion->generoFormatoRenapo($iFolioSer);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}


	public function consultarNss_post()
	{
		$arrDatos = array();
		$cCurp = $this->post('cCurp');

		try {
			$response = $this->Mdlrecertificacion->consultarNss($cCurp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificarRequiereExpediente_post()
	{
		$arrDatos = array();
		$iServicio = $this->post('iServicio');

		try {
			$response = $this->Mdlrecertificacion->verificarRequiereExpediente($iServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function huellastrabajador_post()
	{
		$arrDatos = array();
		try {
			$cCurp = $this->post('cCurp');
			$response = $this->Mdlrecertificacion->huellastrabajador($cCurp);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'dedo');
			}
			
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function huellaspromotor_post()
	{
		$arrDatos = array();
		$iEmpleado = $this->post('iEmpleado');

		try {
			$response = $this->Mdlrecertificacion->huellaspromotor($iEmpleado);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'dedo');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function excepciones_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlrecertificacion->excepciones();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ObtenerUrlsVideo_post()
	{
		$arrDatos = array();
		$iPmodulo = $this->post('iPmodulo');

		try {
			$response = $this->Mdlrecertificacion->ObtenerUrlsVideo($iPmodulo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultarDialogoVideo_post()
	{
		$arrDatos = array();
		$TipoDialogo = $this->post('TipoDialogo');
		$iFolioSer = $this->post('iFolioSer');

		try {
			$response = $this->Mdlrecertificacion->ConsultarDialogoVideo($TipoDialogo, $iFolioSer);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'dialogo');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function subirvideo_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');
		$nombreimagen = $this->post('nombreimagen');
		$base64 = $this->post('base64');
		$fraccionado = $this->post('fraccionado');

		try {
			$response = $this->Mdlrecertificacion->subirvideo($folio, $nombreimagen, $base64, $fraccionado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarMovilExpIE_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');

		try {
			$response = $this->Mdlrecertificacion->validarMovilExpIE($iFolioSer);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function catalogoCompaniaMovil_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlrecertificacion->catalogoCompaniaMovil();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificarTelefono_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');
		$movil = $this->post('movil');

		try {
			$response = $this->Mdlrecertificacion->verificarTelefono($iFolioSer, $movil);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarMovilCAT_post()
	{
		$arrDatos = array();
		$iFolioSer = $this->post('iFolioSer');
		$movil = $this->post('movil');

		try {
			$response = $this->Mdlrecertificacion->guardarMovilCAT($iFolioSer, $movil);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
