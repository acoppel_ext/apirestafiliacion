<?php
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class CtrlimpresionDocumentosafiliacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('MdlimpresionDocumentosafiliacion');
	}

	public function obtnerinformacionpromotor_post()
	{
		$arrDatos = array();

		$emplado = $this->post("iEmpleado");
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtnerinformacionpromotor($emplado);
			$arrDatos =  successforeach($response);

			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'cnombre');
			}
			$arrDatos["registros"] = $arrDatos["registros"][0];
			
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function consultarcatalogocorreoei_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->consultarcatalogocorreoei();
			if ($response) {
				$arrDatos['estatus']        = 1;
				$arrDatos['descripcion']    = "EXITO";
				$arrDatos['registros'] 		= $response;
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion fnconsultarcatalogocorreoei";
				$arrDatos['registros']      = null;
			}
		
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtenerdatossolicitudafi_post()
	{
		$arrDatos = array();

		$foliosol = $this->post("iFolioSol");
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtenerdatossolicitudafi($foliosol);
			$arrDatos =  successforeachSinObject($response);
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function validarregistroenrolhuellaservicio_post()
	{
		$arrDatos = array();

		$foliosol 	= $this->post("iFolioSol");
		$tiposol 	= $this->post("iTipoSol");
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->validarregistroenrolhuellaservicio($foliosol, $tiposol);
			$arrDatos =  successforeachSinObject($response);
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtenerligasimpafi_post()
	{
		$arrDatos = array();

		$foliosol 	= $this->post("iFolioSol");
		$folioenrol = $this->post("iFolioEnrol");
		$promotor 	= $this->post("iEmpleado");
		$tipoafi 	= $this->post("iTipoSol");
		$enrol 		= $this->post("iEnrol");
		$ipmodulo 	= $this->post("cModulo");
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtenerligasimpafi($foliosol, $folioenrol, $promotor, $tipoafi, $enrol, $ipmodulo);
			if ($response) {
				$arrDatos['estatus']        = 1;
				$arrDatos['descripcion']    = "EXITO";
				$arrDatos['registros'] 		= $response;
			} else {
				$arrDatos['estatus']        = -2;
				$arrDatos['descripcion']    = "Error al ejecutar la funcion fnobtenerligasimpafi";
				$arrDatos['registros']      = null;
			}
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function entregaexpedienteafiliacion_post()
	{
		$arrDatos = array();

		$opcion 	= $this->post("iOpcion");
		$foliosol 	= $this->post("iFolioSol");
		$correo 	= $this->post("sCorreo");
		$entrega 	= $this->post("iEntrega");
		$os 		= $this->post("iOs");
		$estado 	= $this->post("iEstado");
		$ruta 		= $this->post("sRuta");
		$nombre 	= $this->post("sNombre");

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->entregaexpedienteafiliacion($opcion, $foliosol, $correo, $entrega, $os, $estado, $ruta, $nombre);
			$arrDatos =  successforeachSinObject($response);
		} catch (Exception $mensajeExcep) {
			$arrDatos['estatus']        = -1;
			$arrDatos['descripcion']    = $mensajeExcep;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}


	public function enviarcorreo_post(){
		$foliosol 	= $this->post("iFolioSol");
		//$ipspa 		= $this->post("sIpSpa");
		$resultado = 0;
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->enviarcorreo($foliosol);
			if($response)
			{
				$resultado = 1;
			}
			$arrDatos['estatus'] 		= $resultado;
			$arrDatos['descripcion'] 	= "Se inserta en tabla control de correos.";
		}catch (Exception $mensajeExcep){
			$arrDatos['estatus']        = -1;
			$arrDatos['descripcion']    = $mensajeExcep;
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function guardarllamadacatafiliacion_post()
	{
		$arrDatos = array();

		$foliosol 	= $this->post("iFolioSol");
		$tiposol 	= $this->post("iTipoSol");
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->guardarllamadacatafiliacion($foliosol, $tiposol);
			$arrDatos =  successforeachSinObject($response);
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtenerrespuesta_post()
	{
		$arrDatos = array();
		$sIpLocal = $this->post('sIpLocal');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtenerrespuesta($sIpLocal);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarrespuestawebservices_post()
	{
		$arrDatos = array();

		$foliosol 	= $this->post("iFolioSol");
		$emplado = $this->post("iEmpleado");
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->consultarrespuestawebservices($foliosol, $emplado);
			$arrDatos =  successforeachSinObject($response);
		} catch (Exception $mensajeExcep) {
			$arrDatos = error($mensajeExcep);
		}
		$this->response($arrDatos, REST_controller::HTTP_OK);
	}

	public function obtnenersiefore_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtnenersiefore($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ExtraerTabla_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->ExtraerTabla();
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'descripcion');
			}
			
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerInformacionAfiliacion_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtenerInformacionAfiliacion($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerLigaImpresion_post()
	{
		$arrDatos = array();
		$iTipoImpresion = $this->post('iTipoImpresion');
		$iFolioSol = $this->post('iFolioSol');
		$iNumeroEmpl = $this->post('iNumeroEmpl');
		$sIpRemoto = $this->post('sIpRemoto');
		$iReeimpresion = $this->post('iReeimpresion');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtenerLigaImpresion($iTipoImpresion, $iFolioSol, $iNumeroEmpl, $sIpRemoto, $iReeimpresion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerCurp_TipoSolicitud_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');
		try {
			$response = $this->MdlimpresionDocumentosafiliacion->obtenerCurp_TipoSolicitud($iFolioSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function cuentaConManos_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->cuentaConManos($iFolioSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validartiposolicitante_post()
	{

		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->validartiposolicitante($iFolioSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function tieneexcepcionimpresion_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->tieneexcepcionimpresion($iFolioSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarCodigoAutenticacion_post()
	{

		$arrDatos = array();
		$cCurp = $this->post('cCurp');
		$cCodigo = $this->post('cCodigo');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->validarCodigoAutenticacion($cCurp, $cCodigo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function GenerafolioAutenticacion_post()
	{

		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->GenerafolioAutenticacion($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificaEnrolamientoCompleto_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('foliosol');
		$iTiposol = $this->post('curp');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->verificaEnrolamientoCompleto($iFolioSol, $iTiposol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	/////////////////////////////////////////////////////// 1498 empieza modificacion

	public function ObtenerRspuestaGestor_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolioSol');
		$iOpc = $this->post('iopc');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->ObtenerRspuestaGestor($iFolio,$iOpc);
			if($response)
				{
					foreach($response as $reg)
					{	
						$arrDatos['Bandera'] = $reg;
					}
						
				}else{
				}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	
	public function Insertarsegundosellodevoluntaddactilar_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolioSol');
		$iEmpleado = $this->post('iEmpleado');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->Insertarsegundosellodevoluntaddactilar($iFolio,$iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	
	/////////////////////////////////////////////////////////// 1498 Terina modifiacion 
	
	

	public function validarEnrolhuellasservicio_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');
		$iTiposol = $this->post('iTipoSol');

		try {
			$response = $this->MdlimpresionDocumentosafiliacion->validarEnrolhuellasservicio($iFolioSol, $iTiposol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
