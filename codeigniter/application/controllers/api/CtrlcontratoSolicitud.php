<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class CtrlcontratoSolicitud extends Util
{
	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];

		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('MdlcontratoSolicitud');
	}

	public function infoSolicitud_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');

		try {
			$response = $this->MdlcontratoSolicitud->infoSolicitud($folio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerHuellas_post()
	{
		$arrDatos = array();
		$data = array(
			'folio' => $this->post('folio'),
			'curp' => $this->post('curp'),
			'idServicio' => $this->post('idServicio'),
			'tipoOperacion' => $this->post('tipoOperacion')
		);

		try {

			$response = $this->MdlcontratoSolicitud->obtenerHuellas($data);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDatosComplementarios_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');

		try {

			$response = $this->MdlcontratoSolicitud->obtenerDatosComplementarios($folio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
