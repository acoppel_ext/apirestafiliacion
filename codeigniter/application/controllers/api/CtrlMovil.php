<?php

if (!defined("BASEPATH")) exit("No direct script access allowed");
//include Rest Controller library
require(APPPATH."/libraries/REST_Controller.php");
require(APPPATH.'/controllers/util/Util.php');

class CtrlMovil extends Util {

    public function __construct()
	{  // API Configuration 
		$arrDatos = array();
        parent::__construct();

		$this->verificacionPermisos(TRUE,TRUE,"");

        //load models
		$this->load->model('MdlcapturaAfiliacion');
        $this->load->model('MdlconstanciaAfiliacion');
        
    }

    public function ObtenerDoctoIndexado_post()
    {
		$arrDatos = array();
		try {

			$info = $this->post("info");
			$info = json_decode($info,true);
			$iOpcion = $info["iOpcion"];
			$iFolioAfiSer = $info["iFolioAfiSer"];
			$cNomenclatura = $info["cNomenclatura"];
			$iTabla = $info["iTabla"];

			$shRet = 0;
			$shFirmaElectronica = 0;
			$shTotalDoctosFirmaDigital = 0;
			$iBandera_FTE0 = 0;
			$respuesta=0;
		
			if($iTabla != 8){

				if(($iTabla == 3) || ($iTabla == 6) || ($iTabla == 1) || ($iTabla == 2)){

					$var=$cNomenclatura;
					$shRet = $this->MdlconstanciaAfiliacion->fnExisteCurpRenapo($iFolioAfiSer,$var);

					foreach($shRet as $reg){
						$respuesta=(int)$reg->count;
					}
				}
			}else{ //REENROLAMIENTO
				$shRet = $this->MdlconstanciaAfiliacion->fnconsultardocumento($iFolioAfiSer,$cNomenclatura,$iOpcion);
				foreach($shRet as $reg){
					$respuesta=(int)$reg->fnconsultardocumento;
				}
			}

			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			$arrDatos['nomenclatura'] = $cNomenclatura;
			$arrDatos['respuesta'] = $respuesta;

		}
    	catch (Exception $mensaje)
    	{
				$arrDatos['estatus'] = -1;
				$arrDatos['nomenclatura'] = $info;
				$arrDatos['descripcion'] = $mensaje;
				$arrDatos['respuesta'] =null;
		  }
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
    public function obtenertipocomprobante_post()
	{
		$arrDatos = array();
		try
		{
			$info = $this->post("info");
			$info = json_decode($info,true);
			$iFolio = (int)$info["iFolio"];

			$response = $this->MdlconstanciaAfiliacion->obtenertipocomprobante($iFolio);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = (int)$response->tipocomprobante;
			}
		}
		catch (Exception $mensaje)
		{

			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
    public function actualizarfolioenrolamiento_post()
	{
		$arrDatos = array();
		try
		{
			$info = $this->post("info");
			$info = json_decode($info,true);
			$iFolioSolicitud = (int)$info["iFolioSolicitud"];
			$iFolioEnrolamiento = (int)$info["iFolioEnrolamiento"];

			$response = $this->MdlcapturaAfiliacion->actualizarfolioenrolamiento($iFolioSolicitud, $iFolioEnrolamiento);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] =null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarTipoConexion_post()
    {
		$arrDatos = array();
    	try {

			$info = $this->post("info");
			$info = json_decode($info,true);
			$folio = (int)$info["iFolio"];
			$tipoConexion = (int)$info["iTipoConexion"];

			$response = $this->MdlcapturaAfiliacion->actualizarTipoConexion($folio,$tipoConexion);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			}
    	}
    	catch (Exception $mensaje)
    	{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] =null;
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function subirVideo_post()
    {
		$arrDatos = array();
    	try {
			$info = $this->post("info");
			$info = json_decode($info,true);
			$nombreimagen = $info["nombreimagen"];
			$base64 = $info["base64"];
			$filepath = "/sysx/progs/web/entrada/video/".$nombreimagen.".mp4";
			$data = base64_decode($base64);
			file_put_contents($filepath, $data);
			$this->rotarVideo($nombreimagen);

			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			$arrDatos['respuesta'] = 1;
	
    	}
    	catch (Exception $mensaje)
    	{
			
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] =null;
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarNumPagDigitalizador_post()
    {
		$arrDatos = array();

    	try {
			

			$info = $this->post("info");
			$info = json_decode($info,true);
			$paginas = $info["paginas"];
			$folio = $info["folio"];
			
			
			
			$respuesta = $this->MdlconstanciaAfiliacion->guardarNumPagDigitalizador($paginas, $folio);

			if($respuesta)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			}
    	}
    	catch (Exception $mensaje)
    	{
			
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] =null;
		}
		$this->response($arrDatos, 500);
	}

	public function rotarVideo($name_archivo)
    {
    	try {

			//Ruta donde se encuentra el archivo
			$ruta_in ="/sysx/progs/web/entrada/video/".$name_archivo.".mp4";
			//Ruta donde se alojara el archivo de la salida
			$ruta_out ="/sysx/progs/web/entrada/video/".$name_archivo."_out.mp4";
			//expresion regular que verifica si el video se encuenta rotado
			$rotate_pattern = "/rotate\\s*:\s*([0-9]+)/i";
			//se usa como bandera para indicar si el video esta girado
			$has_rotation = false;
			//comando de linux que se encarga de traer la informacion del video
			$cmd = "ffmpeg -v verbose -i '$ruta_in' 2>&1";
			//se ejecuta el comando
			$video_info = shell_exec($cmd);

			//let's check against the rotation pattern
			preg_match($rotate_pattern,$video_info,$matches);
			//se comprueba la rotacion
			if(is_array($matches))
			{
				if(isset($matches[1]))
				{
					$has_rotation = true;
				}
			}
			//let's see if the video has rotation or not
			if($has_rotation)
			{
				//rotate the video using transpose and write an output file
				$cmd = "ffmpeg -v verbose -i '$ruta_in' -vf 'transpose=2,transpose=1' -y '$ruta_out' 2>&1";

				shell_exec($cmd);

				//eliminamos archivo original
				if(file_exists($ruta_in)) {
					unlink($ruta_in);
				}
				if (file_exists($ruta_out)) {
					rename ($ruta_out, $ruta_in ="/sysx/progs/web/entrada/video/".$name_archivo.".webm");
				}

			
			}
			else{
				rename ($ruta_in, $ruta_in ="/sysx/progs/web/entrada/video/".$name_archivo.".webm");
				return false;
			}
    	}
    	catch (Exception $mensaje)
    	{
			return false;
		}

		return true;
	}
}
