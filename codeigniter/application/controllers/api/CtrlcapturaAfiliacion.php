<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlCapturaAfiliacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");

		$this->load->model('MdlcapturaAfiliacion');
		$this->load->model('MdlMetodosGenerales');
	}

	public function fnobtenerfolioine_post()
	{
		$arrDatos = array();

		try {

			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->fnobtenerfolioine($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDiagnosticoSolicitudSafre_af_post()
	{
		$arrDatos = array();

		try {

			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->obtenerDiagnosticoSolicitudSafre_af($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnactualizaropcioncombo_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		$opcioncombo = $this->post('opcioncombo');

		try {
			$response = $this->MdlcapturaAfiliacion->fnactualizaropcioncombo($sCurp, $opcioncombo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerLigaIVR_post()
	{
		$arrDatos = array();
		$iopcionLigaine = $this->post('iopcionLigaine');

		try {
			$response = $this->MdlcapturaAfiliacion->fnobtenerLigaIVR($iopcionLigaine);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnenvioIVR_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->fnenvioIVR();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerparaivr_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		$folioine = $this->post('folioine');

		try {
			$response = $this->MdlcapturaAfiliacion->fnobtenerparaivr($sCurp, $folioine);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtnerinformacionpromotor_post()
	{
		$arrDatos = array();
		$iEmpleado = $this->post('iEmpleado');

		try {

			$response = $this->MdlcapturaAfiliacion->obtnerinformacionpromotor($iEmpleado);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'cnombre');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenertiposolicitud_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->obtenertiposolicitud();
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'cdescripcion');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validacionCurpAfiliacion_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		$iTipoSol = $this->post('iTipoSol');
		$sIpRemoto = $this->post('sIpRemoto');

		try {
			$response = $this->MdlcapturaAfiliacion->validacionCurpAfiliacion($sCurp, $iTipoSol, $sIpRemoto);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'cmensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarVideo_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->validarVideo($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarExpediente_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		$sIpRemoto = $this->post('sIpRemoto');

		try {

			$response = $this->MdlcapturaAfiliacion->validarExpediente($sCurp, $sIpRemoto);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerFolioEnrolamiento_post()
	{
		$arrDatos = array();

		try {

			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->obtenerFolioEnrolamiento($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerNombreAdudio_post()
	{
		$arrDatos = array();

		try {
			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->obtenerNombreAdudio($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizaAudioGrabado_post()
	{
		$arrDatos = array();

		try {

			$folioenrol = $this->post('folioenrol');
			$response = $this->MdlcapturaAfiliacion->actualizaAudioGrabado($folioenrol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarFolioSol_post()
	{
		$arrDatos = array();

		try {

			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->validarFolioSol($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function cuentaConManos_post()
	{
		$arrDatos = array();

		try {

			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->cuentaConManos($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerIPServidor_post()
	{
		$arrDatos = array();

		try {
			$sIpRemoto = $this->post('sIpRemoto');
			$response = $this->MdlcapturaAfiliacion->obtenerIPServidor($sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarInfoAfiliacion_post()
	{
		$arrDatos = array();

		try {

			$iOpcGuar = $this->post('iOpcGuar');
			$foliosol = $this->post('foliosol');
			$iTipoSol = $this->post('iTipoSol');
			$esimss = $this->post('esimss');
			$tipotrasp = $this->post('tipotrasp');
			$cFolioEdo = $this->post('cFolioEdo');
			$cAforeEdo = $this->post('cAforeEdo');
			$cCuatriEdo = $this->post('cCuatriEdo');
			$cMotivo = $this->post('cMotivo');
			$cFolioImpli = $this->post('cFolioImpli');
			$iTipoAdmon = $this->post('iTipoAdmon');
			$cDepPrev = $this->post('cDepPrev');
			$aforeced = $this->post('aforeced');

			$response = $this->MdlcapturaAfiliacion->guardarInfoAfiliacion($iOpcGuar, $foliosol, $iTipoSol, $esimss, $tipotrasp, $cFolioEdo, $cAforeEdo, $cCuatriEdo, $cMotivo, $cFolioImpli, $iTipoAdmon, $cDepPrev, $aforeced);

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerPaginaRedireccionar_post()
	{
		$arrDatos = array();

		try {

			$opcionpag = $this->post('opcionpag');
			$foliosol = $this->post('foliosol');
			$iEmpleado = $this->post('iEmpleado');
			$sIpRemoto = $this->post('sIpRemoto');
			$reimpre = $this->post('reimpre');
			$response = $this->MdlcapturaAfiliacion->obtenerPaginaRedireccionar($opcionpag, $foliosol, $iEmpleado, $sIpRemoto, $reimpre);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerCurpSolcontancia_post()
	{
		$arrDatos = array();
		$nss = $this->post('nss');
		$iTipoSol = $this->post('iTipoSol');
		$sIpRemoto = $this->post('sIpRemoto');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerCurpSolcontancia($nss, $iTipoSol, $sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarNSS_post()
	{
		$arrDatos = array();
		$curp = $this->post('curp');

		try {
			$response = $this->MdlcapturaAfiliacion->consultarNSS($curp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultarSolicitudConstancia_post()
	{
		$arrDatos = array();
		$sDato = $this->post('sDato');

		try {
			$response = $this->MdlcapturaAfiliacion->ConsultarSolicitudConstancia($sDato);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarfolioSolicitud_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->validarfolioSolicitud($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llenarcbxMotivoRegistro_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->llenarcbxMotivoRegistro();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llenarcbxTipoAdmin_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->llenarcbxTipoAdmin();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarDatosMotivoAfiliacion_post()
	{
		$arrDatos = array();
		$opc = $this->post('opc');
		$foliosol = $this->post('foliosol');
		$tipoafi = $this->post('tipoafi');
		$esimss = $this->post('esimss');
		$tipotraspaso = $this->post('tipotraspaso');
		$motregi = $this->post('motregi');
		$folconstaimple = $this->post('folconstaimple');
		$iTipoAdmin = $this->post('iTipoAdmin');
		$depo = $this->post('depo');

		try {
			$response = $this->MdlcapturaAfiliacion->guardarDatosMotivoAfiliacion($opc, $foliosol, $tipoafi, $esimss, $tipotraspaso, $motregi, $folconstaimple, $iTipoAdmin, $depo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarSolicitudBD_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');
		$iEmpleado = $this->post('iEmpleado');
		$sIpRemoto = $this->post('sIpRemoto');
		$fecha = $this->post('fecha');

		try {
			$response = $this->MdlcapturaAfiliacion->guardarSolicitudBD($foliosol, $iEmpleado, $sIpRemoto, $fecha);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//FUNCIONES PARA LA CLASE CTipoTraspasoAfiliacion

	public function obtenerCURP_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerCURP($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function comboTipoTraspaso_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->comboTipoTraspaso();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function comboAforeCedente_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->comboAforeCedente();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function comboPeriodoEstado_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->comboPeriodoEstado();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ObtenerNumeroTarjeta_post()
	{
		$arrDatos = array();
		$sIpRemoto = $this->post('sIpRemoto');

		try {
			$response = $this->MdlcapturaAfiliacion->ObtenerNumeroTarjeta($sIpRemoto);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarNumeroTarjeta_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');
		$numtarjeta = $this->post('numtarjeta');

		try {
			$response = $this->MdlcapturaAfiliacion->guardarNumeroTarjeta($foliosol, $numtarjeta);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//METODOS PARA LA CLASE CRetConsultaSolicitudesAfiliacion
	public function obtenerSolicitudAfiliacion_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');
		$curp = $this->post('curp');
		$nss = $this->post('nss');
		$nocliente = $this->post('nocliente');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerSolicitudAfiliacion($foliosol, $curp, $nss, $nocliente);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDatosSolicitudAfiliacionGeneral_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerDatosSolicitudAfiliacionGeneral($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDomicilio_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');
		$tipodom = $this->post('tipodom');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerDomicilio($foliosol, $tipodom);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerComprobanteDomicilio_post()
	{
		$arrDatos = array();
		$compdom = $this->post('compdom');

		try {

			$response = $this->MdlcapturaAfiliacion->obtenerComprobanteDomicilio($compdom);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'comprobante');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDatosSolicitudAfiliacionTelefonos_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerDatosSolicitudAfiliacionTelefonos($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerOcupacionProf_post()
	{
		$arrDatos = array();
		$ocupacionprof = $this->post('ocupacionprof');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerOcupacionProf($ocupacionprof);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerActividadGiro_post()
	{
		$arrDatos = array();
		$activinegocio = $this->post('activinegocio');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerActividadGiro($activinegocio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDatosSolicitudAfiliacionBeneficiarios_post()
	{
		$arrDatos = array();

		try {
			$foliosol = $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->obtenerDatosSolicitudAfiliacionBeneficiarios($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDiagnosticoSolicitud_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerDiagnosticoSolicitud($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function usarConexionInformix_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->usarConexionInformix();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenercodigoHTML_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlcapturaAfiliacion->obtenercodigoHTML();

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}
				foreach ($arrDatos['registros'] as $key => $reg2) {
					$html = $reg2->html;
					$html = str_replace("''", "'", $html);
					$html = str_replace("\"", "'", $html);
					$html = str_replace("\n", " ", $html);
					$html = utf8_encode($html);

					$reg2->html = $html;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerligapagina_post()
	{
		$arrDatos = array();
		$iOpcionPagina = $this->post('iOpcionPagina');
		$iFolio = $this->post('iFolio');
		$iEmpleado = $this->post('iEmpleado');
		$sIp = $this->post('sIp');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerligapagina($iOpcionPagina, $iFolio, $iEmpleado, $sIp);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnexistenciafolioprocesar_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');
		$foliosolpro = $this->post('foliosolpro');

		try {
			$response = $this->MdlcapturaAfiliacion->fnexistenciafolioprocesar($foliosol, $foliosolpro);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function existenciafechaProcesar_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->existenciafechaProcesar($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function tieneexcepcion_post()
	{
		$arrDatos = array();
		$iFolioSol = $this->post('iFolioSol');

		try {
			$response = $this->MdlcapturaAfiliacion->tieneexcepcion($iFolioSol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//METODOS DEL FOLIO 654

	public function validacionine_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->validacionine($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnvalidavigenciasemilla_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');
		$foliosemilla = $this->post('foliosemilla');

		try {
			$response = $this->MdlcapturaAfiliacion->fnvalidavigenciasemilla($foliosol, $foliosemilla);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenercurpconstancia_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->fnobtenercurpconstancia($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function generarFolioIne_post()
	{
		$arrDatos = array();
		$sCurp = $this->post('sCurp');
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->generarFolioIne($sCurp, $foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnvalidastatusenvioine_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->fnvalidastatusenvioine($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnejecutapy_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->fnejecutapy();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//folio 877
	public function configuracionconsultafolioafi_post()
	{
		$arrDatos = array();

		try {
			$response = $this->MdlcapturaAfiliacion->configuracionconsultafolioafi();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarconsultaregistra_post()
	{
		$arrDatos = array();

		try {
			$iFoliosol = $this->post('foliosol');
			$cCurp = $this->post('curp');
			$iAccion = $this->post('accion');
			$cFolioprocesar = $this->post('folioprocesar');
			$response = $this->MdlcapturaAfiliacion->actualizarconsultaregistra($iFoliosol, $cCurp, $iAccion, $cFolioprocesar);
			$mensaje = ' Valor de foliosolicitud-> ' . $iFoliosol . ' Valor de curp-> ' . $cCurp . 'Valor de accion->  ' . $iAccion . ' Valor de folio procesar-> ' . $cFolioprocesar;

			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validardatosconsultafolioafiliacion_post()
	{
		$arrDatos = array();
		$iKeyx = $this->post('keyx');

		try {
			$response = $this->MdlcapturaAfiliacion->validardatosconsultafolioafiliacion($iKeyx);
			$arrDatos = successforeach($response);
			if ($arrDatos['estatus'] != -1) {
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensajeerror');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'mensaje');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function agregardatosconsultafolioafiliacion_post()
	{
		$arrDatos = array();

		try {
			$iEmpleado 				= $this->post('empleado');
			$cCurp 					= $this->post('curp');
			$cClaveaforegenerofolio = $this->post('claveaforegenerofolio');
			$iTipofolio 			= $this->post('tipofolio');
			$cFolioregtrasp 		= $this->post('folioregtrasp');
			$iFoliosolicitud 		= $this->post('foliosol');
			$response = $this->MdlcapturaAfiliacion->agregardatosconsultafolioafiliacion($cCurp, $cClaveaforegenerofolio, $iTipofolio, $cFolioregtrasp, $iFoliosolicitud);

			$mensaje = 'Valor de empleado-> ' . $iEmpleado . ' Valor de curp-> ' . $cCurp . ' Valor de clave afore-> ' . $cClaveaforegenerofolio . ' Valor de clave tipo folio-> ' . $iTipofolio . ' Valor de clave tipo folio registro traspaso-> ' . $cFolioregtrasp . ' Valor de folio solicitud-> ' . $iFoliosolicitud;

			if ($response) {
				$mensaje = "RESPUESTA->" . $response[0]->respuesta;
					$resultado = $response[0]->respuesta;
				if ($resultado > 0) {
					/*********************************************************************************
					 * Se envia el ID del servicio para la detonacion del WS
					 * Se arma el XML con los datos pertinentes del WS
					 * Se obtiene la URL del WS
					 * Se detona el SOAP con el xml
					 * Retonar la respuesta del WS
					 *********************************************************************************/
					$idServicio 	= 9948; //ID WS Autenticacion INE
					$arrDatosXML = array(
						'numeroEmpleado ' => $iEmpleado,
						'curpTrabajador ' => $cCurp,
						'keyx ' => $resultado
					);
					$responseWS = $this->MdlMetodosGenerales->ejecutarWS($idServicio, $arrDatosXML);
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';
					$arrDatos['registros'] = $responseWS;
					$arrDatos['registros']->keyx = $resultado;
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerXML_post()
	{
		$arrDatos = array();
		$array = $this->post('array');

		try {
			$response = $this->MdlcapturaAfiliacion->obtenerXML($array);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obTenercurpTitular_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->obTenercurpTitular($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llamarivr_post()
	{

		$url = $this->post('url');

		$res = file_get_contents($url);

		$this->response($res, REST_Controller::HTTP_OK);
	}

	public function getPlatform($user_agent)
	{

		$plataformas = array(
			'Windows 10' => 'Windows NT 10.0+',
			'Windows 8.1' => 'Windows NT 6.3+',
			'Windows 8' => 'Windows NT 6.2+',
			'Windows 7' => 'Windows NT 6.1+',
			'Windows Vista' => 'Windows NT 6.0+',
			'Windows XP' => 'Windows NT 5.1+',
			'Windows 2003' => 'Windows NT 5.2+',
			'Windows' => 'Windows otros',
			'iPhone' => 'iPhone',
			'iPad' => 'iPad',
			'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
			'Mac otros' => 'Macintosh',
			'Android' => 'Android',
			'BlackBerry' => 'BlackBerry',
			'Linux' => 'Linux',
		);
		foreach ($plataformas as $plataforma => $pattern) {
			//if (array_exist($pattern, $user_agent))
			if (preg_match('/' . $pattern . '/', $user_agent))
				return $plataforma;
		}
		return 'No detectado';
	}

	public function fnobtenersemillaafiliacion_post()
	{
		$arrDatos = array();
		$foliosol = $this->post('foliosol');

		try {
			$response = $this->MdlcapturaAfiliacion->fnobtenersemillaafiliacion($foliosol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarfolioenrolamiento_post()
	{
		$arrDatos = array();
		try
		{
			$info = $this->post("info");
			$info = json_decode($info,true);
			$iFolioSolicitud = (int)$info["iFolioSolicitud"];
			$iFolioEnrolamiento = (int)$info["iFolioEnrolamiento"];

			$response = $this->MdlcapturaAfiliacion->actualizarfolioenrolamiento($iFolioSolicitud, $iFolioEnrolamiento);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] =null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarTipoConexion_post()
    {
		$arrDatos = array();
    	try {

			$info = $this->post("info");
			$info = json_decode($info,true);
			$folio = (int)$info["iFolio"];
			$tipoConexion = (int)$info["iTipoConexion"];

			$response = $this->MdlcapturaAfiliacion->actualizarTipoConexion($folio,$tipoConexion);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = 1;
			}
    	}
    	catch (Exception $mensaje)
    	{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['respuesta'] =null;
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function ValidaConsultaRenapoPrevia_post()
	{
		try 
		{
			$sCurp = $this->post('sCurp');
			$iEsTitular = $this->post('iEsTitular');

			$response = $this->MdlcapturaAfiliacion->ValidaConsultaRenapoPrevia($sCurp, $iEsTitular);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][] = $reg;
				}
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['registros'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	public function obtenerCurpBeneficiario_post()
	{
		try 
		{
			$sCurp = $this->post('sCurp');

			$response = $this->MdlcapturaAfiliacion->obtenerCurpBeneficiario($sCurp);

			if($response)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][] = $reg;
				}
			}
		}
		catch (Exception $mensaje)
		{
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['registros'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}