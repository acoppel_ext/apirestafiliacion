<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class Ctrlexpedienteidentificacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('Mdlexpedienteidentificacion');
	}

	public function verificarPromotor_post()
	{
		$arrDatos = array();
		$iNumeroEmpleado = $this->post('iNumeroEmpleado');

		try {
			$response = $this->Mdlexpedienteidentificacion->verificarPromotor($iNumeroEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerInformacionTrabajadorAfiliacion_post()
	{
		$arrDatos = array();

		$cFolio = $this->post("cFolio");
		$iOpcion = $this->post("i");

		try {

			$response = $this->Mdlexpedienteidentificacion->obtenerInformacionTrabajadorAfiliacion($cFolio, $iOpcion);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}
				$arrDatos = convertircaracteresespeciales($arrDatos, 'appaterno');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'apmaterno');
				$arrDatos = convertircaracteresespeciales($arrDatos, 'nombres');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ConsultacatalogoComprobante_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->ConsultacatalogoComprobante();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//Integracion Quetzaly
	public function guardarExpedienteIdentificacion_post()
	{
		$arrData = array();
		$arrDatos = array();
		$arrData['cSql'] = $this->post("cSql");
		$arrData['cFolio'] = $this->post("cFolio");

		try {

			$response = $this->Mdlexpedienteidentificacion->guardarExpedienteIdentificacion($arrData);

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['codigoRespuesta'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $response[0]->guardo;
			} else {
				$arrDatos['codigoRespuesta'] = 7;
				$arrDatos['descripcion'] = " ERROR guardarExpedienteIdentificacion ";
				$arrDatos['estatus'] = -1;
				$arrDatos['respuesta'] = 7;
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarExpedientePublicado_post()
	{
		$arrDatos = array();
		$cfolio = $this->post('cfolio');

		try {
			$response = $this->Mdlexpedienteidentificacion->validarExpedientePublicado($cfolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarEIDatosTrabajador_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');
		try {
			$response = $this->Mdlexpedienteidentificacion->consultarEIDatosTrabajador($folio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarEIBeneficiarios_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');
		try {
			$response = $this->Mdlexpedienteidentificacion->consultarEIBeneficiarios($folio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarEIDomicilios_post()
	{
		$arrDatos = array();
		$folio = $this->post('folio');
		try {
			$response = $this->Mdlexpedienteidentificacion->consultarEIDomicilios($folio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerExcepcionEnrolamiento_post()
	{
		$arrDatos = array();
		try {
			$iFolio = $this->post('folio');

			$response = $this->Mdlexpedienteidentificacion->obtenerExcepcionEnrolamiento($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos['respuesta'] = -1;
			$arrDatos['mensaje'] = $mensaje;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerDescripcionServicio_post()
	{
		$arrDatos = array();

		try {
			$iTipoServicio = $this->post('iTipoServicio');
			$response = $this->Mdlexpedienteidentificacion->obtenerDescripcionServicio($iTipoServicio);

			if ($response) {
				if (count($response) > 0) {
					$arrDatos['servicio'] = $response;
					$arrDatos['codigoRespuesta'] = 1;
					$arrDatos['descripcion'] = "";
				} else {
					$arrDatos['servicio'] = '';
					$arrDatos['codigoRespuesta'] = -1;
					$arrDatos['descripcion'] = "No se encontr&oacute; informaci&oacute;n del trabajador, para el folio proporcionado.";
				}
			} else {
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al consultar la informaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
			}
		} catch (Exception $mensaje) {
			$arrDatos['codigoRespuesta'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarEstatusExpediente_post()
	{

		$arrDatos = array();

		try {
			$cFolio = $this->post('cFolio');
			$response = $this->Mdlexpedienteidentificacion->actualizarEstatusExpediente($cFolio);

			if ($response) {

				$arrDatos['codigoRespuesta'] = 1;
				$arrDatos['descripcion'] = "";
			} else {
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al actualizar el estado del Expediente de Identificaci&oacute;n, por favor, reporte a Mesa de Ayuda.";
			}
		} catch (Exception $mensaje) {
			$arrDatos['codigoRespuesta'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerVigenciaFotografia_post()
	{
		$arrDatos = array();
		try {
			$arrDatos['cCurp'] = $this->post("cCurp");
			$arrDatos['cNss'] = $this->post("cNss");

			$response = $this->Mdlexpedienteidentificacion->obtenerVigenciaFotografia($arrDatos);

			if ($response) {

				$arrDatos['vigenciaexpirada'] = $response[0]->vigenciaexpirada;
				$arrDatos['codigoRespuesta'] = 1;
				$arrDatos['descripcion'] = "";
			} else {
				$arrDatos['vigenciaexpirada'] = '';
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al consultar la fecha m&aacute;xima para el comprobante del domicilio, por favor, reporte a Mesa de Ayuda.";
			}
		} catch (Exception $mensaje) {
			$arrDatos['vigenciaexpirada'] = '';
			$arrDatos['codigoRespuesta'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guadarLlamadaCat_post()
	{
		$arrDatos = array();
		try {
			$arrDatos['cFolio'] = $this->post("cFolio");
			$arrDatos['cDescServicio'] = $this->post("cDescServicio");

			$response = $this->Mdlexpedienteidentificacion->guadarLlamadaCat($arrDatos);

			if ($response) {
				$arrDatos['guardoLlamadaCat'] = $response[0]->llamada;
				$arrDatos['codigoRespuesta'] = 1;
				$arrDatos['descripcion'] = "";
			} else {
				$arrDatos['guardoLlamadaCat'] = '';
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al guardar la informaci&oacute;n para el CAT, por favor, reporte a Mesa de Ayuda.";
			}
		} catch (Exception $mensaje) {
			$arrDatos['guardoLlamadaCat'] = '';
			$arrDatos['codigoRespuesta'] = -1;
			$arrDatos['descripcion'] = $mensaje;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerFechaMaximaComprobanteDomicilio_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdlexpedienteidentificacion->obtenerFechaMaximaComprobanteDomicilio();

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "";
				$arrDatos['fechamaxima'] = $response[0]->fechamaxima;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['fechamaxima'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerFechaMaximaMayoriaEdad_post()
	{
		$arrDatos = array();
		try {

			$response = $this->Mdlexpedienteidentificacion->obtenerFechaMaximaMayoriaEdad();

			if ($response) {
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['fechamaxima'] = $response[0]->fechamaxima;
				$arrDatos['codigoRespuesta'] = 1;
				$arrDatos['descripcion'] = "";
			} else {
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
				$arrDatos['fechamaxima'] = null;
			}
		} catch (Exception $mensaje) {
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$arrDatos['fechamaxima'] = null;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerVigenciaIdentificacionOficial_post()
	{
		$arrDatos = array();
		try {
			$arrDatos['cNss'] = $this->post("cNss");
			$arrDatos['cCurp'] = $this->post("cCurp");

			$response = $this->Mdlexpedienteidentificacion->obtenerVigenciaIdentificacionOficial($arrDatos);

			if ($response) {

				$arrDatos['vigenciaexpirada'] = $response[0]->vigenciaexpirada;
				$arrDatos['codigoRespuesta'] = 1;
				$arrDatos['descripcion'] = "";
			} else {
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al guardar la informaci&oacute;n para el CAT, por favor, reporte a Mesa de Ayuda.";
			}
		} catch (Exception $mensaje) {

			$arrDatos['codigoRespuesta'] = -1;
			$arrDatos['descripcion'] = "Ocurri&oacute; un error al guardar la informaci&oacute;n para el CAT, por favor, reporte a Mesa de Ayuda.";
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerURLFormatoCurp_post()
	{
		$arrDatos = array();
		try {
			$arrDatos['iOpcionFormatoRenapo'] = $this->post("iOpcionFormatoRenapo");
			$arrDatos['iFolioAfore'] = $this->post("iFolioAfore");
			$arrDatos['sIpRemoto'] = $this->post("sIpRemoto");

			$response = $this->Mdlexpedienteidentificacion->obtenerURLFormatoCurp($arrDatos);

			if ($response) {

				$arrDatos['URL'] = $response[0]->url;
				if ($arrDatos['URL'] == null) {
					$arrDatos['URL'] = '';
				}
			} else {
				$arrDatos['codigoRespuesta'] = -1;
				$arrDatos['descripcion'] = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
				$arrDatos['mensaje'] = 'No se logro ejectuar la consulta obtenerURLFormatoCurp en Base de Datos';
			}
		} catch (Exception $mensaje) {
			$arrDatos['codigoRespuesta'] = ERR__;
			$arrDatos['descripcion'] = "Ocurri&oacute; un error al guardar la informaci&oacute;n para el CAT, por favor, reporte a Mesa de Ayuda.";
			$arrDatos['mensaje'] = $mensaje;
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerExpedienteDeIdentificacion_post()
	{
		$arrDatos = array();
		$sNss = $this->post('sNss');
		$sCurp = $this->post('sCurp');
		$cFolio = $this->post('cFolio');
		$opcion = $this->post('opcion');

		try {
			$response = $this->Mdlexpedienteidentificacion->obtenerExpedienteDeIdentificacion($sNss, $sCurp, $cFolio, $opcion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function extraerDatosBancoppel_post()
	{
		$arrDatos = array();
		try {
			$cFolio = $this->post('cFolio');
			$response = $this->Mdlexpedienteidentificacion->extraerDatosBancoppel($cFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function extraerDatosRenapo_post()
	{
		$arrDatos = array();
		$cFolio = $this->post('cFolio');
		try {
			$response = $this->Mdlexpedienteidentificacion->extraerDatosRenapo($cFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarSolicitudRenapo_post()
	{
		$arrDatos = array();
		$iFolioAfore = $this->post('iFolioAfore');
		$iFolioSolicitud = $this->post('iFolioSolicitud');
		$iProceso = $this->post('iProceso');
		$iEmpleado = $this->post('iEmpleado');
		$idsubproceso = $this->post('idsubproceso');
		$cCurp = $this->post('cCurp');
		$cApellidopaterno = $this->post('cApellidopaterno');
		$cApellidomaterno = $this->post('cApellidomaterno');
		$cNombres = $this->post('cNombres');
		$cSexo = $this->post('cSexo');
		$cFechaNacimiento = $this->post('cFechaNacimiento');
		$iSlcEntidadNacimiento = $this->post('iSlcEntidadNacimiento');
		$cIpModulo = $this->post('cIpModulo');
		try {
			$response = $this->Mdlexpedienteidentificacion->guardarSolicitudRenapo($iFolioAfore, $iFolioSolicitud, $iProceso, $iEmpleado, $idsubproceso, $cCurp, $cApellidopaterno, $cApellidomaterno, $cNombres, $cSexo, $cFechaNacimiento, $iSlcEntidadNacimiento, $cIpModulo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultaRespuestaRenapo_post()
	{
		$arrDatos = array();
		$iFolioAfore = $this->post('iFolioAfore');
		try {
			$response = $this->Mdlexpedienteidentificacion->consultaRespuestaRenapo($iFolioAfore);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarTelefonoListaNegra_post()
	{
		$arrDatos = array();
		$cTelefono1 = $this->post('cTelefono1');
		$cTelefono2 = $this->post('cTelefono2');
		$cFolio = $this->post('cFolio');
		$iEmpleado = $this->post('iEmpleado');
		try {
			$response = $this->Mdlexpedienteidentificacion->validarTelefonoListaNegra($cTelefono1, $cTelefono2, $cFolio, $iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	//Metodos de Archivo CCatalogo.php

	public function consultarCatalogoTelefono_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoTelefono();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoCorreoElectronico_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoCorreoElectronico();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoOcupacion_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoOcupacion();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoActividadGiroNegocio_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoActividadGiroNegocio();

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = $reg;
				}
				$arrDatos = convertircaracteresespeciales($arrDatos, 'descripcion');
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoEntidadNacimiento_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoEntidadNacimiento();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoIdentificacionesOficiales_post()
	{
		$arrDatos = array();
		$sFecha = $this->post('sFecha');

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoIdentificacionesOficiales($sFecha);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoGenero_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoGenero();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoParentesco_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoParentesco();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoNivelEstudio_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoNivelEstudio();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoNacionalidad_post()
	{
		$arrDatos = array();
		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoNacionalidad();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarCatalogoComprobanteDomicilio_post()
	{

		$arrDatos = array();
		try {
			$response = $this->Mdlexpedienteidentificacion->consultarCatalogoComprobanteDomicilio();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerExcepcionSolicitante_post()
	{
		$arrDatos = array();
		$cFolio = $this->post('cFolio');

		try {
			$response = $this->Mdlexpedienteidentificacion->obtenerExcepcionSolicitante($cFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerconstanciatrabajadorfallecido_post()
	{
		$arrDatos = array();
		$cFolio = $this->post('cFolio');

		try {
			$response = $this->Mdlexpedienteidentificacion->fnobtenerconstanciatrabajadorfallecido($cFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenertiposolicitante_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');

		try {
			$response = $this->Mdlexpedienteidentificacion->obtenertiposolicitante($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenercatalogoparentesco_post()
	{
		$arrDatos = array();

		try {
			$iFolio = $this->post('iFolio');
			$response = $this->Mdlexpedienteidentificacion->obtenerCatalogoParentesco($iFolio);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = array_map('trim', (array) $reg);
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerdatossolitante_post()
	{
		$arrDatos = array();

		try {
			$iFolio = $this->post('iFolio');
			$response = $this->Mdlexpedienteidentificacion->obtenerdatossolitante($iFolio);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = array_map('utf8_encode', (array) $reg);
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardardatossolicitante_post()
	{
		$arrDatos = array();

		try {
			$iFolio = $this->post('iFolio');
			$sTipoSolicitante = $this->post('sTipoSolicitante');
			$sApellidoPatSol = $this->post('sApellidoPatSol');
			$sApellidoMatSol = $this->post('sApellidoMatSol');
			$sNombresSol = $this->post('sNombresSol');
			$sCurpSol = $this->post('sCurpSol');
			$sFechaNacSol = $this->post('sFechaNacSol');
			$iEntidadSol = $this->post('iEntidadSol');
			$iNacionalidadSol = $this->post('iNacionalidadSol');
			$iGeneroSol = $this->post('iGeneroSol');
			$sRFCSol = $this->post('sRFCSol');
			$iNumeroHoja = $this->post('iNumeroHoja');
			$iNumeroNotario = $this->post('iNumeroNotario');
			$sNombreNotario = $this->post('sNombreNotario');
			$iNumeroTestimonio = $this->post('iNumeroTestimonio');
			$sFechaTestimonio = $this->post('sFechaTestimonio');
			$sNombreJuzgado = $this->post('sNombreJuzgado');
			$iNumeroJuicio = $this->post('iNumeroJuicio');
			$iPromotorActivo = $this->post('iPromotorActivo');
			$iPromotorInterno = $this->post('iPromotorInterno');
			$iClaveConsar = $this->post('iClaveConsar');
			$iParentesco = $this->post('iParentesco');

			$response = $this->Mdlexpedienteidentificacion->guardardatossolicitante(
				$iFolio,
				$sTipoSolicitante,
				$sApellidoPatSol,
				$sApellidoMatSol,
				$sNombresSol,
				$sCurpSol,
				$sFechaNacSol,
				$iEntidadSol,
				$iNacionalidadSol,
				$iGeneroSol,
				$sRFCSol,
				$iNumeroHoja,
				$iNumeroNotario,
				$sNombreNotario,
				$iNumeroTestimonio,
				$sFechaTestimonio,
				$sNombreJuzgado,
				$iNumeroJuicio,
				$iPromotorActivo,
				$iPromotorInterno,
				$iClaveConsar,
				$iParentesco
			);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = array_map('trim', (array) $reg);
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function permisosCurpsReferecias_post()
	{
		$arrDatos = array();
		$iTipoServicio = $this->post('iTipoServicio');

		try {
			$response = $this->Mdlexpedienteidentificacion->permisosCurpsReferecias($iTipoServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function curpsGenericasReferecias_post()
	{
		$arrDatos = array();
		$iTipoReferencia = $this->post('iTipoReferencia');

		try {
			$response = $this->Mdlexpedienteidentificacion->curpsGenericasReferecias($iTipoReferencia);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function vaidarMayorDeEdad_post()
	{
		$arrDatos = array();

		try {
			$response = $this->Mdlexpedienteidentificacion->vaidarMayorDeEdad();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarpublicarimagenservicioei_post()
	{
		$arrDatos = array();
		$iFolio = $this->post('iFolio');
		$iOpcion = $this->post('iOpcion');

		try {
			$response = $this->Mdlexpedienteidentificacion->actualizarpublicarimagenservicioei($iFolio, $iOpcion);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function servicioEjecutarAplicacion_post()
	{
		$arrDatos = array();
		$idservidor = $this->post('idservidor');
		try {
			$response = $this->Mdlexpedienteidentificacion->servicioEjecutarAplicacion($idservidor);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerCurpPromotor_post()
	{
		$arrDatos = array();
		$iNumeroEmpleado = $this->post('iNumeroEmpleado');

		try {
			$response = $this->Mdlexpedienteidentificacion->obtenerCurpPromotor($iNumeroEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function servicioEjecutarAplicacionBus_post()
	{
		$arrDatos = array();
		$idservidor = $this->post('idservidor');

		try {
			$response = $this->Mdlexpedienteidentificacion->servicioEjecutarAplicacionBus($idservidor);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function servicioObtenerRespuesta_post()
	{
		$arrDatos =  array();
		$idservidor = $this->post('idservidor');

		try {
			$response = $this->Mdlexpedienteidentificacion->servicioObtenerRespuesta($idservidor);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerVerificacionei_post()
	{
		$arrDatos = array();

		try {
			$cCurpTrabajador = $this->post('cCurpTrabajador');
			$iNumeroEmpleado = $this->post('iNumpEmpleado');
			$cTipoServicio	 = $this->post('cTipoServicio');

			$mensaje = "METODO -> obtenerVerificacionei, Variable -> [cCurpTrabajador] = " . $cCurpTrabajador . " [iNumeroEmpleado] = " . $iNumeroEmpleado . "[cTipoServicio] = " . $cTipoServicio;
			

			$response = $this->Mdlexpedienteidentificacion->obtenerVerificacionei($cCurpTrabajador, $iNumeroEmpleado, $cTipoServicio);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = array_map('utf8_encode', (array) $reg);
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}
		
		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerInformacionServicioEi_post()
	{
		$arrDatos = array();

		try {
			$curpTrabajador = $this->post('curpTrabajador');

			$mensaje = "METODO -> obtenerInformacionServicioEi, Variable -> [curpTrabajador] = " . $curpTrabajador;
			

			$response = $this->Mdlexpedienteidentificacion->obtenerInformacionServicioEi($curpTrabajador);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = array_map('utf8_encode', (array) $reg);
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	public function obtenerbanderabancoppel_post()
	{
		$arrDatos = array();

		try {
			$Foliosolicitud = $this->post('Foliosolicitud');

			$mensaje = "METODO -> obtenerInformacionServicioEi, Variable -> [curpTrabajador] = " . $curpTrabajador;
			

			$response = $this->Mdlexpedienteidentificacion->obtenerbanderabancoppel($Foliosolicitud);

			if ($response) {
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($response as $reg) {
					$arrDatos['registros'][] = array_map('utf8_encode', (array) $reg);
				}
			}
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
	//Folio 1514
	public function guardarDatosSwitch_post()
	{
		$arrDatos = array();
		$cCurp = $this->post('cCurp');
		$iNss = $this->post('iNss');
		$iTiposolicitud = $this->post('iTiposolicitud');
		$cCorreo = $this->post('cCorreo');
		$iBandnotificaciones = $this->post('iBandnotificaciones');
		$iBandedocta = $this->post('iBandedocta');
		try {
			$response = $this->Mdlexpedienteidentificacion->guardarDatosSwitch($cCurp, $iNss, $iTiposolicitud, $cCorreo, $iBandnotificaciones, $iBandedocta);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
	
}
