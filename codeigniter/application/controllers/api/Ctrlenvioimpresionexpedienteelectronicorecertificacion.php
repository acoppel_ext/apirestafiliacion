<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class ctrlEnvioimpresionexpedienteelectronicorecertificacion extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");


		//load models
		$this->load->model('Mdlenvioimpresionexpedienteelectronicorecertificacion');
	}

	public function fnconsultarempleadoconstancia_post()
	{
		$arrDatos = array();
		$iempleado = $this->post('iempleado');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fnconsultarempleadoconstancia($iempleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerlistadodominioscorreos_post()
	{
		$arrDatos = array();
		$iopciondominio = $this->post('iopciondominio');
		$iidcorreo = $this->post('iidcorreo');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fnobtenerlistadodominioscorreos($iopciondominio, $iidcorreo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fndatosrecertificacion_post()
	{
		$arrDatos = array();
		$iorigen = $this->post('iorigen');
		$ifolioservicio = $this->post('ifolioservicio');
		$iestatusrecer = $this->post('iestatusrecer');
		$icorreoimpresion = $this->post('icorreoimpresion');
		$scorreo = $this->post('scorreo');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fndatosrecertificacion($iorigen, $ifolioservicio, $iestatusrecer, $icorreoimpresion, $scorreo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnvalidarnumeromaximocorreocapturado_post()
	{
		$arrDatos = array();
		$ccorreoelectronico = $this->post('ccorreoelectronico');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fnvalidarnumeromaximocorreocapturado($ccorreoelectronico);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerligasrecertificacion_post()
	{
		$arrDatos = array();
		$iorigen = $this->post('iorigen');
		$iorigenpagina = $this->post('iorigenpagina');
		$sipremoto = $this->post('sipremoto');
		$iimprimir = $this->post('iimprimir');
		$crutapdf = $this->post('crutapdf');
		$ifolioafiliacion = $this->post('ifolioafiliacion');
		$iempleado = $this->post('iempleado');
		$ifolioservicio = $this->post('ifolioservicio');
		$ireguiereeb = $this->post('ireguiereeb');
		$iservicio = $this->post('iservicio');
		$ccurp = $this->post('ccurp');
		$iconsultaedoc = $this->post('iconsultaedoc');
		$itipoimpresion = $this->post('itipoimpresion');
		$itipoacuse = $this->post('itipoacuse');
		$isiefore = $this->post('isiefore');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fnobtenerligasrecertificacion($iorigen, $iorigenpagina, $sipremoto, $iimprimir, $crutapdf, $ifolioafiliacion, $iempleado, $ifolioservicio, $ireguiereeb, $iservicio, $ccurp, $iconsultaedoc, $itipoimpresion, $itipoacuse, $isiefore);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnguardarenviocorreoexpedienteelectronicorecertificacion_post()
	{
		$arrDatos = array();
		$ifolioservicio = $this->post('ifolioservicio');
		$crutapdf3x1 = $this->post('crutapdf3x1');
		$cipsvrlinux = $this->post('cipsvrlinux');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fnguardarenviocorreoexpedienteelectronicorecertificacion($ifolioservicio, $crutapdf3x1, $cipsvrlinux);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function fnobtenerpdfsrecertificacion_post()
	{
		$arrDatos = array();
		$ifolioservicio = $this->post('ifolioservicio');
		try {
			$response = $this->Mdlenvioimpresionexpedienteelectronicorecertificacion->fnobtenerpdfsrecertificacion($ifolioservicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
