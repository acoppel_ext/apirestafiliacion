<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH.'/libraries/REST_Controller.php');

class ctrlDocumentosMenuafore extends REST_Controller {

    public function __construct()
    {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

        parent::__construct();
        //load models
        $this->load->model('Mdldocumentosmenuafore');
    }

    public function obtenerdocumentos_post()
    {
        $tipodoc = $this->post('tipodoc');
        $ejecuta = $this->post('ejecuta');
        $texto = $this->post('texto');

        try 
        {
            $response = $this->Mdldocumentosmenuafore->obtenerdocumentos($tipodoc,$ejecuta,$texto);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'][] = $reg;
                }
            }
			else
			{
				$arrDatos['estatus'] = 0;
				$arrDatos['descripcion'] = 'ERROR';
				$arrDatos['registros'] = null;
			}
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }
}

?>