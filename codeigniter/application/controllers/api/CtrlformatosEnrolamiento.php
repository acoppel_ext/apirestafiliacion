<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH.'/libraries/REST_Controller.php');
require(APPPATH.'/controllers/util/Util.php');

class ctrlFormatosEnrolamiento extends Util
{
	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
    private $result = array();

    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];

		if($method == "OPTIONS") {
			die();
		}

        parent::__construct();

		$componente = (!$this->post('componente') )? false : true;
        $this->result = $this->verificacionPermisos($componente, "");

        //load models
        $this->load->model('MdlformatosEnrolamiento');
    }

    public function consultarTrabajadorCount_post()
    {
    	$arrDatos = array();
        $folioEnrolamiento = $this->post('folioEnrolamiento');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->consultarTrabajadorCount($folioEnrolamiento);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function consultarTrabajador_post()
    {
    	$arrDatos = array();
        $folioEnrolamiento = $this->post('folioEnrolamiento');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->consultarTrabajador($folioEnrolamiento);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function obtenerExcepcion_post()
    {
    	$arrDatos = array();
        $folioEnrolamiento = $this->post('folioEnrolamiento');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->obtenerExcepcion($folioEnrolamiento);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function consultarEnrolamientoMovil_post()
    {
        $arrDatos = array();
        $folioEnrolamiento = $this->post('folioEnrolamiento');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->consultarTrabajador($folioEnrolamiento);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function consultarEmpleadoEnroladoCount_post()
    {
        $arrDatos = array();
        $enrolado = $this->post('enrolado');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->consultarEmpleadoEnroladoCount($enrolado);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function consultarEmpleadoEnrolado_post()
    {
        $arrDatos = array();
        $enrolado = $this->post('enrolado');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->consultarEmpleadoEnrolado($enrolado);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function fnconsultaempleadoenrolamientotesCount_post()
    {
        $arrDatos = array();
        $itestigo = $this->post('itestigo');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->fnconsultaempleadoenrolamientotesCount($itestigo);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function fnconsultaempleadoenrolamientotes_post()
    {
        $arrDatos = array();
        $itestigo = $this->post('itestigo');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->fnconsultaempleadoenrolamientotes($itestigo);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function actualizarImagenIndexada_post()
    {
        $arrDatos = array();
        $arrData = array(
            'cFolio' => $this->post('cFolio'),
            'folio' => $this->post('folio'),
            'obtIpTienda' => $this->post('obtIpTienda')
        );

        try 
        {
            $response = $this->MdlformatosEnrolamiento->actualizarImagenIndexada($arrData);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function actualizarpublicarimagenservicios_post()
    {
        $arrDatos = array();
        $iFolio = $this->post('iFolio');
        $iOpcion = $this->post('iOpcion');

        try 
        {
            $response = $this->MdlformatosEnrolamiento->actualizarpublicarimagenservicios($iFolio,$iOpcion);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function actualizarpublicarimagenserviciosMovil_post()
    {
        $arrDatos = array();
        $iFolio = $this->post('iFolio');
        
        try 
        {
            $response = $this->MdlformatosEnrolamiento->actualizarpublicarimagenserviciosMovil($iFolio);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function actualizarfirmapublicacion_post()
    {
        $arrDatos = array();
        $cFolio = $this->post('cFolio');
        $iOpcion = $this->post('iOpcion');
        $iTes = $this->post('iTes');
        
        try 
        {
            $response = $this->MdlformatosEnrolamiento->actualizarfirmapublicacion($cFolio,$iOpcion,$iTes);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function actualizarpublicarimagen_post()
    {
        $arrDatos = array();
        $iFolio = $this->post('iFolio');
        $iOpcion = $this->post('iOpcion');
        
        try 
        {
            $response = $this->MdlformatosEnrolamiento->actualizarpublicarimagen($iFolio, $iOpcion);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function actualizarpublicarimagenMovil_post()
    {
        $arrDatos = array();
        $iFolio = $this->post('iFolio');
        
        try 
        {
            $response = $this->MdlformatosEnrolamiento->actualizarpublicarimagenMovil($iFolio);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }

    public function ctrlImagenesPorTransmitir_post()
    {
        $arrDatos = array();
        $iFolio = $this->post('iFolio');
        $iOpcion = $this->post('iOpcion');
        $cNombreArchivo = $this->post('cNombreArchivo');
        
        try 
        {
            $response = $this->MdlformatosEnrolamiento->ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo);

            if($response)
            {
                //indicador que asigna estatus 1, osea correctamente y su descripcion
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "EXITO";

                foreach($response as $reg)
                {
                    $arrDatos['registros'] = $reg;
                }
            }
        }
        catch (Exception $mensaje)
        {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = $mensaje;
            $arrDatos['registros'] = null;
        }

        $this->response($arrDatos, REST_Controller::HTTP_OK);
    }
}