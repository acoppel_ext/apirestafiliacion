<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH.'/libraries/REST_Controller.php');
require(APPPATH.'/controllers/util/Util.php');

class CtrlimpAfiliacionDocs extends Util{
    
    // SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
    private $result = array();

    public function __construct() {

		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {die();}
        parent::__construct();

        $componente = (!$this->post('componente') )? false : true;
        $this->result = $this->verificacionPermisos($componente, "");

        $this->load->model('MdlimpAfiliacionDocs');
    }
    
    public function consultarfoliocredencial_post()
    {
        
        $arrDatos = array();
        $empleado = $this->post("empleado");

        try {
            $response = $this->MdlimpAfiliacionDocs->consultarfoliocredencial($empleado);
            $arrDatos =  successforeach($response);
        }catch (Exception $mensajeExcep){
           $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function descargarimagen_post()
    {
        $arrDatos = array();

        $opcion = $this->post("opcion");
        $folioImagen = $this->post("folioImagen");

        try {
            $response = $this->MdlimpAfiliacionDocs->descargarimagen($opcion, $folioImagen);
            $arrDatos =  successforeach($response);
        }catch (Exception $mensajeExcep){
           $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function obtenerBase64_post()
    {
        $arrDatos = array();

        $id = $this->post("id");

        try {
            $response = $this->MdlimpAfiliacionDocs->obtenerBase64($id);
            $arrDatos =  successforeach($response);
        }catch (Exception $mensajeExcep){
           $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function obtenerCurp_post()
    {
        $arrDatos = array();

        $folioCurp = $this->post("folioCurp");

        try {
            $response = $this->MdlimpAfiliacionDocs->obtenerCurp($folioCurp);
            $arrDatos =  successforeach($response);
        }catch (Exception $mensajeExcep){
           $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function obtenerSiefore_post()
    {
        $arrDatos = array();

        $sCurp = $this->post("sCurp");

        try {
            $response = $this->MdlimpAfiliacionDocs->obtenerSiefore($sCurp);
            $arrDatos =  successforeach($response);
        }catch (Exception $mensajeExcep){
           $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }

    public function envioexp_post()
    {
        $arrDatos = array();
        $folioafiliacion = $this->post("folioafiliacion");
        $archivo = $this->post("archivo");
        $imdata = $this->post("imdata");

        try {
            $response = $this->MdlimpAfiliacionDocs->envioexp($folioafiliacion,$archivo,$imdata);
            $arrDatos =  successforeach($response);
        }catch (Exception $mensajeExcep){
           $arrDatos = error($mensajeExcep);
        }
        $this->response($arrDatos, REST_controller::HTTP_OK);
    }
}
?>