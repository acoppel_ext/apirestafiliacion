<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/controllers/util/Util.php');

class CtrlenrolamientoTrabajador extends Util
{

	// SE USA PARA EL KEYX QUE SE COLOCA EN EL LOG 
	private $result = array();

	public function __construct()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}

		parent::__construct();

		$componente = (!$this->post('componente')) ? false : true;
		$this->result = $this->verificacionPermisos($componente, "");

		//load models
		$this->load->model('MdlenrolamientoTrabajador');
	}

	public function validarEstadoPromotor_post()
	{
		$arrDatos = array();

		$iEmpleado = $this->post('iEmpleado');

		try {

			$response = $this->MdlenrolamientoTrabajador->validarEstadoPromotor($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function borrarExcepciones_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlenrolamientoTrabajador->borrarExcepciones($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarEstatusEnrol_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$iEstatusCancelar = $this->post('iEstatusCancelar');

		try {

			$response = $this->MdlenrolamientoTrabajador->actualizarEstatusEnrol($iFolio, $iEstatusCancelar);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarDatos_post()
	{
		$arrDatos = array();

		$iEmpleado = $this->post('iEmpleado');
		$solicitud = $this->post('solicitud');
		$curp = $this->post('curp');
		$iFolioSolicitud = $this->post('iFolioSolicitud');

		try {

			$response = $this->MdlenrolamientoTrabajador->guardarDatos($iEmpleado, $solicitud, $curp, $iFolioSolicitud);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function cancelarEnrolamiento_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$estatus = $this->post('estatus');

		try {

			$response = $this->MdlenrolamientoTrabajador->cancelarEnrolamiento($iFolio, $estatus);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validaNIST_post()
	{
		$arrDatos = array();

		$iFolioEnrol = $this->post('iFolioEnrol');

		try {

			$response = $this->MdlenrolamientoTrabajador->validaNIST($iFolioEnrol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarDedosEnManos_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$iSelecciono = $this->post('iSelecciono');

		try {

			$response = $this->MdlenrolamientoTrabajador->consultarDedosEnManos($iFolio, $iSelecciono);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardaExcepciones_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$chExcepcion = $this->post('chExcepcion');
		$idedos = $this->post('idedos');

		try {

			$response = $this->MdlenrolamientoTrabajador->guardaExcepciones($iFolio, $chExcepcion, $idedos);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerLigaFormatoEnrolamiento_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$ipTienda = $this->post('ipTienda');
		$iOpcion = $this->post('iOpcion');
		$iTestigo = $this->post('iTestigo');
		$iNumTestigo = $this->post('iNumTestigo');

		try {

			$response = $this->MdlenrolamientoTrabajador->obtenerLigaFormatoEnrolamiento($iFolio, $ipTienda, $iOpcion, $iTestigo, $iNumTestigo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function verificacionEnrolAutoriza_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlenrolamientoTrabajador->verificacionEnrolAutoriza($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function limpiarHuellasEnrol_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlenrolamientoTrabajador->limpiarHuellasEnrol($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function actualizarEnrolamiento_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');

		try {

			$response = $this->MdlenrolamientoTrabajador->actualizarEnrolamiento($iFolio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function excepcionarDedosEnRolTrabajador_post()
	{
		$arrDatos = array();

		$iFolioEnrol = $this->post('iFolioEnrol');

		try {

			$response = $this->MdlenrolamientoTrabajador->excepcionarDedosEnRolTrabajador($iFolioEnrol);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function ObtenerDatosConfiguracion_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlenrolamientoTrabajador->ObtenerDatosConfiguracion();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llamarMarcaEI_post()
	{
		$arrDatos = array();

		$cFolioServicio = $this->post('cFolioServicio');

		try {

			$response = $this->MdlenrolamientoTrabajador->llamarMarcaEI($cFolioServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llamarMarcaEIConsecutivo_post()
	{
		$arrDatos = array();

		try {

			$response = $this->MdlenrolamientoTrabajador->llamarMarcaEIConsecutivo();
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llamarMarcaEIMarcarCuenta_post()
	{
		$arrDatos = array();

		$iCodigoMotivo = $this->post('iCodigoMotivo');
		$cNss = $this->post('cNss');
		$iConsecutivo = $this->post('iConsecutivo');
		$iEmpleado = $this->post('iEmpleado');

		try {

			$response = $this->MdlenrolamientoTrabajador->llamarMarcaEIMarcarCuenta($iCodigoMotivo, $cNss, $iConsecutivo, $iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function llamarMarcaEIActualizarServicioAfore_post()
	{
		$arrDatos = array();

		$iConsecutivo = $this->post('iConsecutivo');
		$cFolioServicio = $this->post('cFolioServicio');

		try {

			$response = $this->MdlenrolamientoTrabajador->llamarMarcaEIActualizarServicioAfore($iConsecutivo, $cFolioServicio);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function validarLevantamientoEI_post()
	{
		$arrDatos = array();

		$iFolio = $this->post('iFolio');
		$sCurpTrabajador = $this->post('sCurpTrabajador');

		try {

			$response = $this->MdlenrolamientoTrabajador->validarLevantamientoEI($iFolio, $sCurpTrabajador);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function consultarnumempleado_post()
	{
		$arrDatos = array();

		$iEmpleado = $this->post('iEmpleado');

		try {

			$response = $this->MdlenrolamientoTrabajador->consultarnumempleado($iEmpleado);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenermensajegerenteenrol_post()
	{
		$arrDatos = array();

		$opcionmensaje = $this->post('opcionmensaje');

		try {

			$response = $this->MdlenrolamientoTrabajador->obtenermensajegerenteenrol($opcionmensaje);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function guardarbitacoraenrolamiento_post()
	{
		$arrDatos = array();

		$iFolioSolicitud = $this->post('iFolioSolicitud');
		$iFolioEnrol = $this->post('iFolioEnrol');
		$iEmpleado = $this->post('iEmpleado');
		$iNumEmpGerente = $this->post('iNumEmpGerente');
		$curp = $this->post('curp');
		$iautorizaciongerencial = $this->post('iautorizaciongerencial');
		$itipoenrolamiento = $this->post('itipoenrolamiento');
		$iExcepcionesHuellas = $this->post('iExcepcionesHuellas');
		$tienda = $this->post('tienda');
		try {

			$response = $this->MdlenrolamientoTrabajador->guardarbitacoraenrolamiento($iFolioSolicitud, $iFolioEnrol, $tienda, $iEmpleado, $iNumEmpGerente, $curp, $iautorizaciongerencial, $itipoenrolamiento, $iExcepcionesHuellas);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerclaveoperacion_post()
	{
		$arrDatos = array();

		$iFolioSolicitud = $this->post('iFolioSolicitud');

		try {

			$response = $this->MdlenrolamientoTrabajador->obtenerclaveoperacion($iFolioSolicitud);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenerclaveoperacionservicios_post()
	{
		$arrDatos = array();

		$iFolioSolicitud = $this->post('iFolioSolicitud');

		try {

			$response = $this->MdlenrolamientoTrabajador->obtenerclaveoperacionservicios($iFolioSolicitud);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}

	public function obtenertienda_post()
	{
		$arrDatos = array();

		try {
			$ipModulo = $this->post('ipModulo');
			$response = $this->MdlenrolamientoTrabajador->obtenertienda($ipModulo);
			$arrDatos = successforeach($response);
		} catch (Exception $mensaje) {
			$arrDatos = error($mensaje);
		}

		$this->response($arrDatos, REST_Controller::HTTP_OK);
	}
}
