<?php

define('RUTA_LOGX',					'/sysx/progs/afore/log/apirestafiliacion');
if (!defined("BASEPATH")) exit("No direct script access allowed");

class Util extends REST_Controller
{
 
	public function verificacionPermisos($requireAuthorization, $metodo)
	{  
		  
		$cadena = "\r\n-Controlador: " .  $this->uri->segment(2) .
        "\r\n-Metodo: " .  $this->uri->segment(3) . 
        "\r\n-Datos: " .json_encode($this->post(), JSON_UNESCAPED_SLASHES);

		
		// Se utiliza para obtener el usuario logeado
		$result =  array();
		// Se carga el modele de autorizacion
		$this->load->model("MdlAuth");
		/**
		 * SE USA PARA: 
		 * - Metodos 
		 * - Componentes
		 * las cuales no se necesitan autenticacion
		 */
		if($metodo == "ligaMenuAfore") {
			$requireAuthorization = false;
		}
		
		/** 
		* 	Configuracion que indica
		* - Peticiones Permitidas [POST,GET,PUT,DELETE].
		* - Si el Controlador requiere Autorizacion TRUE ó FALSE. 
		*/
		$this->_APIConfig([
			'methods' => ['POST'],
			'requireAuthorization' => $requireAuthorization,
		]);
		/**
		 * Se verifica: 
		 * - Qee se envie la autrorizacion por el headers
		 * - Que exista en base de datos 
		 * - Y que la sesion que se genera en la api coincida con lo que se reciba del headers 
		 */ 
		if($requireAuthorization  && getallheaders()["Authorization"]) {
			$result =  $this->MdlAuth->verificarSesionToken(getallheaders()["Authorization"]);
			// Si no cumple con las condiciones antes nombrada, no se le permitira hacer la peticion 
			if (!$result) {
				
				$this->grabarLogX($cadena, $result ? $result[0]->keyx : 0);

				$this->response( [
					"status" => 0,
					"result" => $this->session->userdata('authorizationAfore'),
					"estatus" => 0,
					"tipo" => "token",
					"descripcion" => "El promotor no ha iniciado sesión",
				],
				200);
			}
		}
		

		$this->grabarLogX($cadena, $result ? $result[0]->keyx : 0);
		return $result ? $result[0]->keyx : 0;
	}

	public function grabarLogX($cadLogx, $keyx = 0)
	{

		$rutaLog =  RUTA_LOGX .  '-' . date("Y-m-d") . ".log";
		$cad = date("Y-m|H:i:s|") . getmypid() . "| [" .  $keyx  . "] | " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

	public function getRealIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

		return $_SERVER['REMOTE_ADDR'];
	}

	public function obtenerTienda($iEmpleado)
    {
		$tienda = 0;
		$this->load->model("MdlconstanciaAfiliacion");
		try
		{
			$response = $this->MdlconstanciaAfiliacion->obtenerTiendaEmpleado($iEmpleado);
			
			if($response){
				$tienda=(int)$response->tienda;
			}

		}
		catch (Exception $mensaje)
		{
				$tienda = 0;
		}

		return $tienda;
	}
}
