<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	function successforeach($response)
	{
        $arrDatos = array();

        if ($response) {
            //indicador que asigna estatus 1, osea correctamente y su descripcion
            $arrDatos['estatus'] = 1;
            $arrDatos['descripcion'] = "EXITO";

            foreach ($response as $reg) {
                $arrDatos['registros'][] = $reg;
            }
            
        } else {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = "Sin Resultados";
            $arrDatos['registros'] = null;
        }
        
        return $arrDatos;
    }
    function successforeachSinObject($response)
	{
        $arrDatos = array();

        if ($response) {
            //indicador que asigna estatus 1, osea correctamente y su descripcion
            $arrDatos['estatus'] = 1;
            $arrDatos['descripcion'] = "EXITO";

            foreach ($response as $reg) {
                $arrDatos['registros'] = $reg;
            }
            
        } else {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = "Sin Resultados";
            $arrDatos['registros'] = null;
        }
        
        return $arrDatos;
    }

    function successSinforeachSinObject($response)
	{
        $arrDatos = array();

        if ($response) {
            $arrDatos['estatus'] = 1;
            $arrDatos['descripcion'] = "EXITO";
            $arrDatos['registros'] = $response;
        } else {
            $arrDatos['estatus'] = -1;
            $arrDatos['descripcion'] = "Error al ejecutar la consulta";
            $arrDatos['registros']      = null;
        }
        
        return $arrDatos;
    }

    function successSinforeachConObject($response)
	{
        $arrDatos = array();

        if ($response){
            $arrDatos['estatus']        = 1;
            $arrDatos['descripcion']    = "EXITO";
            $arrDatos['registros'][]    = $response;
        }else{
            $arrDatos['estatus']        = -2;
            $arrDatos['descripcion']    = "Error al ejecutar la consulta";
            $arrDatos['registros']      = null;
        }
        
        return $arrDatos;
    }
    
    function error($mensaje)
    {
        $arrDatos = array();

        $arrDatos['estatus'] = -1;
        $arrDatos['descripcion'] = $mensaje;
        $arrDatos['registros'] = null;

        return $arrDatos;
    }