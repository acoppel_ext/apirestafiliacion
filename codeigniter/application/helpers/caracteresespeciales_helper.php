<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	function convertircaracteresespeciales($arrDatos, $scampo)
	{

		foreach ($arrDatos['registros'] as $key => $reg) {
			$svar = $reg->$scampo;

			$svar = trim($svar);
			$svar = utf8_encode($svar);

			$reg->$scampo = $svar;
		}
		return $arrDatos;
	}

	function convertircaracteresespecialenie($arrDatos, $scampo)
	{

		foreach ($arrDatos['registros'] as $key => $reg) {
			$svar = $reg->$scampo;

			$svar = htmlspecialchars($svar, ENT_QUOTES);

			$reg->$scampo = $svar;
		}
		return $arrDatos;
	}

