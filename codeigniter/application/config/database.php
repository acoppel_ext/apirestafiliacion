<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificates in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (default),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
*/
$active_group = 'default';
$query_builder = TRUE;

$configaraciones = simplexml_load_file("../../conf/webconfig.xml");

//Conexion posgresql
$ipServidor = $configaraciones->Spa;
$sUsuario = $configaraciones->Usuario;
$sBasedeDatos = $configaraciones->Basededatos;
$sPassword = $configaraciones->Password;

//Conexion Inofrmix
$ipServidorInfx = $configaraciones->Ipinformix;
$sUsuarioInfx = $configaraciones->UsuarioInf;
$sBasedeDatosInfx = $configaraciones->BasededatosInf;
$sPasswordInfx = $configaraciones->PasswordInf;

//Conexion Bustramites
$ipServidorBusTramites = $configaraciones->IpBusTramites;
$sUsuarioBusTramites = $configaraciones->UsuarioBusTramites;
$sBasedeDatosBusTramites = $configaraciones->BasededatosBusTramites;
$sPasswordBusTramites = $configaraciones->PasswordBusTramites;
$sUsuarioPublicaFtp = $configaraciones->UsuarioPublicacionFtp;
$PswdPublicaFtp = $configaraciones->PasswordPublicacionFtp;
$ipPublicacionImagenes = $configaraciones->IpPublicacionImagenes;

//Conexion SERVICIOS AFORE
$ipServidorServiciosAfore = $configaraciones->IpServiciosAfore;
$sUsuarioServiciosAfore = $configaraciones->UsuarioServiciosAforeBD;
$sBasedeDatosServiciosAfore = $configaraciones->BasedeDatosServiciosAfore;
$sPassworServiciosAfore = $configaraciones->PasswordServiciosAfore;

//Conexion IMAGENES
$ipServidorImagenes = $configaraciones->IpImagenes;
$sUsuarioImagenes = $configaraciones->UsuarioImg;
$sBasedeDatosImagenes= $configaraciones->BasededatosImg;
$sPassworImagenes = $configaraciones->PasswordImg;

//Conexion VIDEOS
$ipServidorVideos = $configaraciones->IpVideosMovil;
$sUsuarioVideos = $configaraciones->UsuarioVideosBDMovil;
$sBasedeDatosVideos= $configaraciones->BasedeDatosVideosMovil;
$sPassworVideos = $configaraciones->PasswordVideosMovil;
$iPuertoVideos = $configaraciones->PuertoVideosMovil;

//Conexion AdmonAfore
$ipAdmonafore = $configaraciones->IpAdmonafore;
$sUsuarioAdmonafore = $configaraciones->UsuarioAdmonafore;
$sBasededatosAdmonafore= $configaraciones->BasededatosAdmonafore;
$sPasswordAdmonafore = $configaraciones->PasswordAdmonafore;

//Conexion AFORECOPPEL.COM
$IpAforeCoppelCom = $configaraciones->IpAforeCoppelCom;
$UsuarioAforeCoppelCom = $configaraciones->UsuarioAforeCoppelCom;
$BasedeDatosAforeCoppelCom = $configaraciones->BasedeDatosAforeCoppelCom;
$PasswordAforeCoppelCom = $configaraciones->PasswordAforeCoppelCom;

//Conexion CAPACITACION
$IpCapacitacionAfore = $configaraciones->IpCapacitacionAfore;
$UsuarioCapacitacionAforeBD = $configaraciones->UsuarioCapacitacionAforeBD;
$BasedeDatosCapacitacionAfore = $configaraciones->BasedeDatosCapacitacionAfore;
$PasswordCapacitacionAfore = $configaraciones->PasswordCapacitacionAfore; 
$PuertoCapacitacionAfore = $configaraciones->PuertoCapacitacionAfore;

//Conexion Edocta
$IpEdocta = $configaraciones->IpEdocta;
$UsuarioEdocta = $configaraciones->UsuarioEdocta;
$BasededatosEdocta = $configaraciones->BasededatosEdocta;
$PasswordEdocta = $configaraciones->PasswordEdocta; 

//Conexion Examenes y Examenes innternos
$IpExamenes = $configaraciones->IpExamenes;
$UsuarioExamenes = $configaraciones->UsuarioExamenes;
$BasededatosExamenes = $configaraciones->BasededatosExamenes; 
$BasededatosExamenesInternos = $configaraciones->BasededatosExamenesInternos;
$PasswordExamenes = $configaraciones->PasswordExamenes; 

//Conexion Huellas
$IpHuellas = $configaraciones->IpHuellas;
$UsuarioHuellas = $configaraciones->UsuarioHuellas;
$BasededatosHuellas = $configaraciones->BasededatosHuellas;
$PasswordHuellas = $configaraciones->PasswordHuellas; 

//Conexion IMAGENES_HIST
$IpServidorHistorico = $configaraciones->IpServidorHistorico;
$UsuarioServidorHistorico = $configaraciones->UsuarioServidorHistorico;
$BasededatosServidorHistorico = $configaraciones->BasededatosServidorHistorico;
$PasswordServidorHistorico = $configaraciones->PasswordServidorHistorico; 

//Conexion IMAGENES
$IpServidorHistorico = $configaraciones->IpServidorHistorico;
$UsuarioServidorHistorico = $configaraciones->UsuarioServidorHistorico;
$BasededatosServidorHistorico = $configaraciones->BasededatosServidorHistorico;
$PasswordServidorHistorico = $configaraciones->PasswordServidorHistorico; 

$db['default'] = array(
    'dsn'   => '',
    'hostname' => $ipServidor,
    'username' => $sUsuario,
    'password' => $sPassword,
    'database' => $sBasedeDatos,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['Pgaforeglobal'] = array(
    'dsn'   => '',
    'hostname' => $ipServidor,
    'username' => $sUsuario,
    'password' => $sPassword,
    'database' => $sBasedeDatos,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['bustramites'] = array(
    'dsn'   => '',
    'hostname' => $ipServidorBusTramites,
    'username' => $sUsuarioBusTramites,
    'password' => $sPasswordBusTramites,
    'database' => $sBasedeDatosBusTramites,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['Pgsafre'] = array(
        'dsn'        => '',
        'subdriver' => 'informix',
        'hostname' => $ipServidorInfx,
        'port' => '1521',
        'username' => $sUsuarioInfx,
        'password' => $sPasswordInfx,
        'database' => $sBasedeDatosInfx,
        'server' => 'safre_tcp',
        'dbdriver' => 'pdo',
        'db_debug' => TRUE,
        'pconnect' => FALSE,
        'cache_on' => FALSE
);

$db['PgServiciosAfore'] = array(
    'dsn'   => '',
    'hostname' => $ipServidorServiciosAfore,
    'username' => $sUsuarioServiciosAfore,
    'password' => $sPassworServiciosAfore,
    'database' => $sBasedeDatosServiciosAfore,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgImagenes'] = array(
    'dsn'   => '',
    'hostname' => $ipServidorImagenes,
    'username' => $sUsuarioImagenes,
    'password' => $sPassworImagenes,
    'database' => $sBasedeDatosImagenes,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgVideos'] = array(
    'dsn'   => '',
    'hostname' => $ipServidorVideos,
    'port' => $iPuertoVideos,
    'username' => $sUsuarioVideos,
    'password' => $sPassworVideos,
    'database' => $sBasedeDatosVideos,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgAdmonAfore'] = array(
    'dsn'   => '',
    'hostname' => $ipAdmonafore,
    'username' => $sUsuarioAdmonafore,
    'password' => $sPasswordAdmonafore,
    'database' => $sBasededatosAdmonafore,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['Pgaforecoppel'] = array(
    'dsn'   => '',
    'hostname' => $IpAforeCoppelCom,
    'username' => $UsuarioAforeCoppelCom,
    'password' => $PasswordAforeCoppelCom,
    'database' => $BasedeDatosAforeCoppelCom,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgCapacitacionAfore'] = array(
    'dsn'      => '',
    'hostname' => $IpCapacitacionAfore,
    'port'     => $PuertoCapacitacionAfore,
    'username' => $UsuarioCapacitacionAforeBD,
    'password' => $PasswordCapacitacionAfore,
    'database' => $BasedeDatosCapacitacionAfore,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgEdocta'] = array(
    'dsn'      => '',
    'hostname' => $IpEdocta,
    'port'     => '5432',
    'username' => $UsuarioEdocta,
    'password' => $PasswordEdocta,
    'database' => $BasededatosEdocta,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgExamenes'] = array(
    'dsn'      => '',
    'hostname' => $IpExamenes,
    'port'     => '5432',
    'username' => $UsuarioExamenes,
    'password' => $PasswordExamenes,
    'database' => $BasededatosExamenes,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgExamenes_Internos'] = array(
    'dsn'      => '',
    'hostname' => $IpExamenes,
    'port'     => '5432',
    'username' => $UsuarioExamenes,
    'password' => $PasswordExamenes,
    'database' => $BasededatosExamenesInternos,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgHuellas'] = array(
    'dsn'      => '',
    'hostname' => $IpHuellas,
    'port'     => '5432',
    'username' => $UsuarioHuellas,
    'password' => $PasswordHuellas,
    'database' => $BasededatosHuellas,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['PgIMAGENES_HIST'] = array(
    'dsn'      => '',
    'hostname' => $IpServidorHistorico,
    'port'     => '5432',
    'username' => $UsuarioServidorHistorico,
    'password' => $PasswordServidorHistorico,
    'database' => $BasededatosServidorHistorico,
    'dbdriver' => 'postgre',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'development'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);